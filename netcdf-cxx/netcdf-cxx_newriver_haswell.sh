#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load hdf5/1.8.16
module load netcdf/4.3.3.1
module load netcdf-cxx/4.2.1
#
echo "NETCDF-CXX_NEWRIVER_HASWELL: Normal beginning of execution."
#
g++ -c -I$NETCDF_CXX_INC -I$NETCDF_DIR/include writer.cpp
if [ $? -ne 0 ]; then
  echo "NETCDF-CXX_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
g++ -o writer writer.o -L$NETCDF_LIB -L$NETCDF_CXX_LIB -lnetcdf_c++ -lnetcdf
if [ $? -ne 0 ]; then
  echo "NETCDF-CXX_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm writer.o
#
./writer > netcdf-cxx_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "NETCDF-CXX_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm writer
#
echo "NETCDF-CXX_NEWRIVER_HASWELL: Normal end of execution."
exit 0
