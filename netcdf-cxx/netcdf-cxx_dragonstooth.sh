#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load hdf5/1.8.16
module load netcdf/4.3.3.1
module load netcdf-c/4.4.0
module load netcdf-cxx/4.2.1
#
echo "NETCDF-CXX_DRAGONSTOOTH: Normal beginning of execution."
#
g++ -c -I$NETCDF_CXX_INC -I$NETCDF_DIR/include writer.cpp
if [ $? -ne 0 ]; then
  echo "NETCDF-CXX_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
g++ -o writer writer.o -L$NETCDF_LIB -L$NETCDF_CXX_LIB -lnetcdf_c++ -lnetcdf
if [ $? -ne 0 ]; then
  echo "NETCDF-CXX_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm writer.o
#
./writer > netcdf-cxx_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "NETCDF-CXX_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm writer
#
echo "NETCDF-CXX_DRAGONSTOOTH: Normal end of execution."
exit 0
