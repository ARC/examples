# include <cstdlib>
# include <cstring>
# include <ctime>
# include <iomanip>
# include <iostream>

# include "netcdf.h"
# include "netcdfcpp.h"

using namespace std;

int main ( );
int *create_data ( int nc, int nr );
int write_data ( int nc, int nr, int *array, char *filename );
void timestamp ( );

//****************************************************************************80

int main ( )

//****************************************************************************80
//
//  Purpose:
//
//    MAIN is the main program for WRITER.
//
//  Modified:
//
//    04 July 2017
//
{
  int *array;
  char filename[] = "writer.nc";
  int nc = 12;
  int nr = 6;
  int status;

  timestamp ( );
  cout << "\n";
  cout << "WRITER:\n";
  cout << "  C++ version\n";
  cout << "  Program to write NETCDF files.\n";

  cout << "\n";
  cout << "  Create data array.\n";
  array = create_data ( nc, nr );

  status = write_data ( nc, nr, array, filename );
  cout << "  Data stored in file '" << filename << "'\n";
//
//  Free memory.
//
  delete [] array;
//
//  Terminate.
//
  cout << "\n";
  cout << "WRITER:\n";
  cout << "  Normal end of execution.\n";
  cout << "\n";
  timestamp ( );

  return 0;
}
//****************************************************************************80

int *create_data ( int nc, int nr )

//****************************************************************************80
//
//  Purpose:
//
//    CREATE_DATA creates the data array.
//
//  Discussion:
//
//    This data is probably:
//
//      0  1  2  3  4  5  6  7  8  9 10 11
//     12 13 14 15 16 17 18 19 20 21 22 23
//     24 25 26 27 28 29 30 31 32 33 34 35 
//     36 37 38 39 40 41 42 43 44 45 46 47
//     48 49 50 51 52 53 54 55 56 57 58 59
//     60 61 62 63 64 65 66 67 68 69 70 71 
//
//  Modified:
//
//    29 March 2017
//
//  Parameters:
//
//    Input, int NC, NR, the number of rows and columns.
//
//    Output, int CREATE_DATA[NC*NR], the data array.
//
{
  int *array;
  int i;
  int j;
  int k;

  array = new int[nc*nr];

  k = 0;
  for ( i = 0; i < nr; i++ )
  {
    for ( j = 0; j < nc; j++ )
    {
      array[k] = k;
    }
  }

  return array;
}
//****************************************************************************80

int write_data ( int nc, int nr, int *array, char *filename )

//****************************************************************************80
//
//  Purpose:
//
//    WRITE_DATA writes the data array to a NETCDF file.
//
//  Modified:
//
//    29 March 2017
//
//  Parameters:
//
//    Input, int NC, NR, the number of rows and columns.
//
//    Input, int ARRAY[NC*NR], the data array.
//
//    Input, string FILENAME, the name of the file to create.
//
{
//
//  Create the file. 
//
  NcFile dataFile ( filename, NcFile::Replace );
// 
//  Define the dimensions. 
//
  NcDim *ncDim = dataFile.add_dim ( "nc", nc );

  NcDim *nrDim = dataFile.add_dim ( "nr", nr );
//
//  Define the ARRAY.
//
  NcVar *arrayVar = dataFile.add_var ( "array", ncInt, ncDim, nrDim);
//
//  Assign values.
//
  arrayVar->put ( array, nc, nr );

  return 0;
}
//****************************************************************************80

void timestamp ( )

//****************************************************************************80
//
//  Purpose:
//
//    TIMESTAMP prints the current YMDHMS date as a time stamp.
//
//  Example:
//
//    31 May 2001 09:45:54 AM
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license. 
//
//  Modified:
//
//    08 July 2009
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    None
//
{
# define TIME_SIZE 40

  static char time_buffer[TIME_SIZE];
  const struct std::tm *tm_ptr;
  size_t len;
  std::time_t now;

  now = std::time ( NULL );
  tm_ptr = std::localtime ( &now );

  len = std::strftime ( time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm_ptr );

  std::cout << time_buffer << "\n";

  return;
# undef TIME_SIZE
}
