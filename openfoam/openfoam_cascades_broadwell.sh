#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load openmpi/2.0.0
module load python/2.7.10
module load OpenFOAM/5.0
#
echo "OPENFOAM_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Define OpenFOAM environment.
#
. $OPENFOAM_DIR/OpenFOAM-5.0/etc/bashrc
#
blockMesh > cavity_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "OPENFOAM_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
icoFoam >> cavity_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "OPENFOAM_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "OPENFOAM_CASCADES_BROADWELL: Normal end of execution."
exit 0
