#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.1.0
module load openmpi/1.8.4
module load OpenFOAM/3.0.1
#
echo "OPENFOAM_BLUERIDGE: Normal beginning of execution."
#
#  Define OpenFOAM environment.
#
. $OPENFOAM_DIR/OpenFOAM-3.0.1/etc/bashrc
#
blockMesh > cavity_blueridge.txt
if [ $? -ne 0 ]; then
  echo "OPENFOAM_BLUERIDGE: Run error!"
  exit 1
fi
#
icoFoam >> cavity_blueridge.txt
if [ $? -ne 0 ]; then
  echo "OPENFOAM_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "OPENFOAM_BLUERIDGE: Normal end of execution."
exit 0
