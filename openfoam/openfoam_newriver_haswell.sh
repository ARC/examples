#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.6.4
module load OpenFOAM/3.0.1
#
echo "OPENFOAM_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Define OpenFOAM environment.
#
. $OPENFOAM_DIR/OpenFOAM-3.0.1/etc/bashrc
#
blockMesh > cavity_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "OPENFOAM_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
icoFoam >> cavity_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "OPENFOAM_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "OPENFOAM_NEWRIVER_HASWELL: Normal end of execution."
exit 0
