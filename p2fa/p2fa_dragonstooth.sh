#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load python/2.7.10
module load sox/14.4.2
module load htk/3.4.0
module load p2fa/1.003
#
echo "P2FA_DRAGONSTOOTH: Normal beginning of execution."
#
#  With no arguments, the program will print usage instructions:
#
python $P2FA_DIR/align.py
if [ $? -ne 0 ]; then
  echo "P2FA_DRAGONSTOOTH: Run error."
  exit 1
fi
#
python $P2FA_DIR/align.py BREY00538.wav BREY00538.txt BREY00538.TextGrid
if [ $? -ne 0 ]; then
  echo "P2FA_DRAGONSTOOTH: Run error."
  exit 1
fi
#
rm -r tmp
#
echo "P2FA_DRAGONSTOOTH: Normal end of execution."
exit 0
