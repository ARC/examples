#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load yasm/1.3
module load x264/1.0
module load fdk-aac/1.0
module load lame/3.99.5
module load ffmpeg/2.5.4
#
echo "FFMPEG_BLUERIDGE: Normal beginning of execution."
#
#  Remove any previous copy of bee.mp4, or program will fail.
#
rm -f bee.mp4
#
tar xvf foo.tar
#
#  Concatenate all files of the form "bahnnnnnn.png" into a movie.
#
#  -r 24              <-- movie should use 24 frames per second;
#  -i bah%06d.png     <-- input file names have form "bahNNNNNN.png";
#  -b:v 16384k        <-- set video bit rate to 16,384 kilobytes/second;
#  -vf scale=1014:-1  <-- resize the output to width 1014 pixels, 
#                         and set height to preserve aspect ratio;
#  bee.mp4            <-- name of output movie.
#
ffmpeg -r 24 -i bah%06d.png -b:v 16384k -vf scale=1024:-1 bee.mp4
if [ $? -ne 0 ]; then
  echo "FFMPEG_BLUERIDGE: Run error."
  exit 1
fi
#
rm bah000*.png
#
echo "FFMPEG_BLUERIDGE: Normal end of execution."
exit 0
