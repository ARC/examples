#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.8.5
module load mpiP/3.4.1
#
echo "MPIP_NEWRIVER_HASWELL: Normal beginning of execution."
#
mpicc -c wave_mpi.c
if [ $? -ne 0 ]; then
  echo "MPIP_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
mpicc -o wave_mpi wave_mpi.o -L$MPIP_LIB -lmpiP -lm -lbfd -liberty -lunwind
if [ $? -ne 0 ]; then
  echo "MPIP_NEWRIVER_HASWELL: Load error."
  exit 1
fi
rm wave_mpi.o
#
mpirun -np 4 ./wave_mpi > mpip_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "MPIP_NEWRIVER_HASWELL: Run error."
  exit 1
fi
rm wave_mpi
#
echo "MPIP_NEWRIVER_HASWELL: Normal end of execution."
exit 0
