#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/7.3.0
module load openmpi/3.0.0
module load mpiP/3.4.1
#
echo "MPIP_CASCADES_SKYLAKE: Normal beginning of execution."
#
mpicc -c wave_mpi.c
if [ $? -ne 0 ]; then
  echo "MPIP_CASCADES_SKYLAKE: Compile error."
  exit 1
fi
#
mpicc -o wave_mpi wave_mpi.o -L$MPIP_LIB -lmpiP -lm -lbfd -liberty -lunwind
if [ $? -ne 0 ]; then
  echo "MPIP_CASCADES_SKYLAKE: Load error."
  exit 1
fi
rm wave_mpi.o
#
mpirun -np 4 ./wave_mpi > mpip_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "MPIP_CASCADES_SKYLAKE: Run error."
  exit 1
fi
rm wave_mpi
#
echo "MPIP_CASCADES_SKYLAKE: Normal end of execution."
exit 0
