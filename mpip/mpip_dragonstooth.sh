#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load impi/5.0
module load mpiP/3.4.1
#
echo "MPIP_DRAGONSTOOTH: Normal beginning of execution."
#
mpiicc -c wave_mpi.c
if [ $? -ne 0 ]; then
  echo "MPIP_DRAGONSTOOTH: Compile error."
  exit 1
fi
#
mpiicc -o wave_mpi wave_mpi.o -L$MPIP_LIB -lmpiP -lm -lbfd -liberty -lunwind
if [ $? -ne 0 ]; then
  echo "MPIP_DRAGONSTOOTH: Load error."
  exit 1
fi
rm wave_mpi.o
#
mpirun -np 4 ./wave_mpi > mpip_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "MPIP_DRAGONSTOOTH: Run error."
  exit 1
fi
rm wave_mpi
#
echo "MPIP_DRAGONSTOOTH: Normal end of execution."
exit 0
