#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load gsl/1.9
#
echo "GSL_NEWRIVER_HASWELL: Normal beginning of execution."
#
g++ -c -I$GSL_INC gsl_test.cpp
if [ $? -ne 0 ]; then
  echo "GSL_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
g++ -o gsl_test gsl_test.o -L$GSL_LIB -lgsl -lgslcblas -lm
if [ $? -ne 0 ]; then
  echo "GSL_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm gsl_test.o
#
./gsl_test > gsl_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "GSL_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm gsl_test
#
echo "GSL_NEWRIVER_HASWELL: Normal end of execution."
exit 0
