#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load gsl/1.15
#
echo "GSL_BLUERIDGE: Normal beginning of execution."
#
g++ -c -I$GSL_INC gsl_test.cpp
if [ $? -ne 0 ]; then
  echo "GSL_BLUERIDGE: Compile error!"
  exit 1
fi
#
g++ -o gsl_test gsl_test.o -L$GSL_LIB -lgsl -lgslcblas -lm
if [ $? -ne 0 ]; then
  echo "GSL_BLUERIDGE: Load error!"
  exit 1
fi
rm gsl_test.o
#
./gsl_test > gsl_blueridge.txt
if [ $? -ne 0 ]; then
  echo "GSL_BLUERIDGE: Run error!"
  exit 1
fi
rm gsl_test
#
echo "GSL_BLUERIDGE: Normal end of execution."
exit 0
