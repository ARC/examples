#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/7.3.0
module load gsl/2.4
#
echo "GSL_CASCADES_SKYLAKE: Normal beginning of execution."
#
g++ -c -I$GSL_INC gsl_test.cpp
if [ $? -ne 0 ]; then
  echo "GSL_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
g++ -o gsl_test gsl_test.o -L$GSL_LIB -lgsl -lgslcblas -lm
if [ $? -ne 0 ]; then
  echo "GSL_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm gsl_test.o
#
./gsl_test > gsl_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "GSL_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm gsl_test
#
echo "GSL_CASCADES_SKYLAKE: Normal end of execution."
exit 0
