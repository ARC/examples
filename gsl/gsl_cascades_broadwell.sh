#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load gsl/2.4
#
echo "GSL_CASCADES_BROADWELL: Normal beginning of execution."
#
g++ -c -I$GSL_INC gsl_test.cpp
if [ $? -ne 0 ]; then
  echo "GSL_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
g++ -o gsl_test gsl_test.o -L$GSL_LIB -lgsl -lgslcblas -lm
if [ $? -ne 0 ]; then
  echo "GSL_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm gsl_test.o
#
./gsl_test > gsl_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "GSL_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm gsl_test
#
echo "GSL_CASCADES_BROADWELL: Normal end of execution."
exit 0
