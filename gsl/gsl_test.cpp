# include <cmath>
# include <cstdlib>
# include <ctime>
# include <iostream>
# include <iomanip>

using namespace std;

# include <gsl/gsl_eigen.h>
# include <gsl/gsl_fft_complex.h>
# include <gsl/gsl_integration.h>
# include <gsl/gsl_linalg.h>
# include <gsl/gsl_math.h>
# include <gsl/gsl_poly.h>
# include <gsl/gsl_qrng.h>
# include <gsl/gsl_multiroots.h>
# include <gsl/gsl_vector.h>

int main ( );
void gsl_eigen_nonsymm_test ( );
void gsl_fft_complex_test ( );
double gsl_integration_f ( double x, void *params );
void gsl_integration_qng_test ( );
void gsl_linalg_LU_test ( );
double gsl_monte_f ( double *x, size_t dim, void *params );
void gsl_multiroot_fsolver_test ( );
void gsl_multiroot_print_state ( size_t iter, gsl_multiroot_fsolver *s );
void gsl_poly_eval_test ( );
void gsl_qrng_niederreiter_2_test ( );
void gsl_qrng_sobol_test ( );
int rosenbrock_f ( const gsl_vector *x, void *params, gsl_vector *f );;
void timestamp ( );
//
//  Rosenbrock parameter structure.
//
struct rparams
{
  double a;
  double b;
};

//****************************************************************************80

int main ( )

//****************************************************************************80
//
//  Purpose:
//
//    MAIN is the main program for the GSL tests.
//
//  Modified:
//
//    09 March 2017
//
//  Author:
//
//    John Burkardt
//
{
  timestamp ( );

  cout << "\n";
  cout << "GSL_TEST:\n";
  cout << "  C++ version\n";
  cout << "  Test GSL, the GNU Scientific Library.\n";

  gsl_eigen_nonsymm_test ( );

  gsl_fft_complex_test ( );

  gsl_integration_qng_test ( );

  gsl_linalg_LU_test ( );

  gsl_multiroot_fsolver_test ( );

  gsl_poly_eval_test ( );

  gsl_qrng_niederreiter_2_test ( );

  gsl_qrng_sobol_test ( );
//
//  Terminate.
//
  cout << "\n";
  cout << "GSL_TEST:\n";
  cout << "  Normal end of execution.\n";
  cout << "\n";
  timestamp ( );

  return 0;
}
//****************************************************************************80

void gsl_eigen_nonsymm_test ( )

//****************************************************************************80
//
//  Purpose:
//
//    GSL_EIGEN_NONSYMM_TEST tests GSL_EIGEN_NONSYMM.
//
//  Modified:
//
//    09 March 2017
//
//  Reference:
//
//    Mark Gelassi, Jim Davies, James Tyler, Bryan Gough,
//    Reid Priedhorsky, Gerard Jungman, Michael Booth, Fabrice Rossi,
//    GNU Scientific Library Reference Manual.
//
{
  double data[] = { -1.0, 1.0, -1.0, 1.0,
                    -8.0, 4.0, -2.0, 1.0,
                    27.0, 9.0, 3.0, 1.0,
                    64.0, 16.0, 4.0, 1.0 };
//
//  Convert C array to GSL matrix.
//
  gsl_matrix_view m = gsl_matrix_view_array ( data, 4, 4 );
//
//  Allocate GSL matrix and vector space for eigenvectors and eigenvalues.
//
  gsl_matrix_complex *evec = gsl_matrix_complex_alloc ( 4, 4 );
  gsl_vector_complex *eval = gsl_vector_complex_alloc ( 4 );
//
// Set up workspace W.
//
  gsl_eigen_nonsymmv_workspace *w = gsl_eigen_nonsymmv_alloc ( 4 );

  cout << "\n";
  cout << "GSL_EIGEN_NONSYMM_TEST:\n";
  cout << "  GSL_EIGEN_NONYSMM computes the eigenvalues and eigenvectors\n";
  cout << "  of a nonsymmetric matrix.\n";
//
//  Compute the eigenvalues and eigenvectors.
//
  gsl_eigen_nonsymmv ( &m.matrix, eval, evec, w );
//
//  Free the workspace memory.
//
  gsl_eigen_nonsymmv_free ( w );
//
//  Sort the results.
//
  gsl_eigen_nonsymmv_sort ( eval, evec, GSL_EIGEN_SORT_ABS_DESC );
//
//  Print the results.
//  
  for ( int i = 0; i < 4; i++ )
  {
    gsl_complex eval_i = gsl_vector_complex_get ( eval, i );
    gsl_vector_complex_view evec_i = gsl_matrix_complex_column ( evec, i );

    printf ( "\n" );
    printf ( "  Eigenvalue(%d) = %g + %gi\n", 
      i, GSL_REAL ( eval_i ), GSL_IMAG ( eval_i ) );

    printf ( "\n" );
    printf ( "  Eigenvector(%d) = \n", i );
    for ( int j = 0; j < 4; j++ )
    {
      gsl_complex z = gsl_vector_complex_get ( &evec_i.vector, j );
      printf ( "  %g + %gi\n", GSL_REAL ( z ), GSL_IMAG ( z ) );
    }
  }
//
//  Free memory.
//
  gsl_vector_complex_free ( eval );
  gsl_matrix_complex_free ( evec );

  return;
}
//****************************************************************************80

void gsl_fft_complex_test ( )

//****************************************************************************80
//
//  Purpose:
//
//    GSL_FFT_COMPLEX_TEST tests GSL_FFT_COMPLEX.
//
//  Modified:
//
//    09 March 2017
//
//  Reference:
//
//    Mark Gelassi, Jim Davies, James Tyler, Bryan Gough,
//    Reid Priedhorsky, Gerard Jungman, Michael Booth, Fabrice Rossi,
//    GNU Scientific Library Reference Manual.
//
{ 
  double *data;
  int i;

  data = new double[2*128];

  cout << "\n";
  cout << "GSL_FFT_COMPLEX_TEST:\n";
  cout << "  GSL_FFT_COMPLEX computes the fast Fourier transform\n";
  cout << "  of a complex vector of data.\n";

  for ( i = 0; i < 128; i++ )
  {
    data[0+i*2] = 0.0;
    data[1+i*2] = 0.0;
  }
  data[0+0*2] = 1.0;
//
//  Insert a pulse.
//  Make it symmetric so the transform will actually be real.
//
  for ( i = 1; i <= 10; i++ )
  {
    data[0+i*2] = 1.0;
    data[0+(128-i)*2] = 1.0;
  }
//
//  Print the input.
//
  printf ( "\n" );
  printf ( "  Input data (a symmetric pulse):\n" );
  printf ( "\n" );
  for ( i = 0; i < 128; i++ )
  {
    printf ( "%3d %e %e\n", i, data[0+i*2], data[1+i*2] );
  }
//
//  Compute the FFT.
//
  gsl_fft_complex_radix2_forward ( data, 1, 128 );
//
//  Print the scaled output.
//
  printf ( "\n" );
  printf ( "  Output data:\n" );
  printf ( "\n" );
  for ( i = 0; i < 128; i++ )
  {
    printf ( "%3d %e %e\n", 
      i, data[0+i*2] / sqrt ( 128.0 ), data[1+i*2] / sqrt ( 128.0 ) );
  }
//
//  Free memory.
//
  delete [] data;

  return;
}
//****************************************************************************80

double gsl_integration_f ( double x, void *params )

//****************************************************************************80
//
//  Purpose:
//
//    GSL_INTEGRATION_F evaluates the integrand for quadrature test.
//
//  Discussion:
//
//    Integral ( 0 <= x <= 1 ) 2/(2+sin(10*pi*x)) = 2/sqrt(3).
//
//  Modified:
//
//    09 March 2017
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Mark Gelassi, Jim Davies, James Tyler, Bryan Gough,
//    Reid Priedhorsky, Gerard Jungman, Michael Booth, Fabrice Rossi,
//    GNU Scientific Library Reference Manual.
//
//  Parameters:
//
//    Input, double *X, the evaluation point.
//
//    Input, void *PARAMS, optional parameters.
//
//    Output, double GSL_INTEGRATION_F, the value of the integrand at X.
//
{
  double value;

  value = 2.0 / ( 2.0 + sin ( 10.0 * M_PI * x ) );
  
  return value;
}
//****************************************************************************80

void gsl_integration_qng_test ( )

//****************************************************************************80
//
//  Purpose:
//
//    GSL_INTEGRATION_QNG_TEST tests GSL_INTEGRATION_QNG.
//
//  Modified:
//
//    09 March 2017
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Mark Gelassi, Jim Davies, James Tyler, Bryan Gough,
//    Reid Priedhorsky, Gerard Jungman, Michael Booth, Fabrice Rossi,
//    GNU Scientific Library Reference Manual.
//
{
  double a;
  double abserr;
  double abserr_exact;
  double b;
  double epsabs;
  double epsrel;
  int error_code;
  gsl_function F;
  size_t neval;
  double result;
  double result_exact;

  cout << "\n";
  cout << "GSL_INTEGRATION_QNG_TEST:\n";
  cout << "  GSL_INTEGRATION_QNG uses the Gauss-Kronrod rule to estimate\n";
  cout << "  the integral of f(x) over [a,b].\n";

  F.function = &gsl_integration_f;
  F.params = NULL;
  a = 0.0;
  b = 1.0;
//
//  If we reduce EPSABS much more, then the code returns an error.
//
  epsabs = 1.0E-01;
  epsrel = 0.0E+00;

  error_code = gsl_integration_qng ( &F, a, b, epsabs, epsrel, &result, 
    &abserr, &neval );

  if ( error_code != 0 )
  {
    cout << "\n";
    cout << "  GSL_INTEGRATION_QNG returned error code = " << error_code << "\n";
  }
  else
  {
    cout << "\n";
    cout << "  Integral estimate = " << result 
         << ", error estimate = " << abserr << "\n";
    cout << "  Number of function evaluations was " << neval << "\n";
    result_exact = 2.0 / sqrt ( 3.0 );
    abserr_exact = fabs ( result - result_exact );
    cout << "  Exact integral = " << result_exact 
         << ", Exact error = " << abserr_exact << "\n";
  }

  return;
}
//****************************************************************************80

void gsl_linalg_LU_test ( )

//****************************************************************************80
//
//  Purpose:
//
//    GSL_LINAL_LU_TEST tests GSL_LINALG_LU.
//
//  Modified:
//
//    09 March 2017
//
//  Reference:
//
//    Mark Gelassi, Jim Davies, James Tyler, Bryan Gough,
//    Reid Priedhorsky, Gerard Jungman, Michael Booth, Fabrice Rossi,
//    GNU Scientific Library Reference Manual.
//
{
  double a_data[] = { 0.18, 0.60, 0.57, 0.96,
                      0.41, 0.24, 0.99, 0.58,
                      0.14, 0.30, 0.97, 0.66,
                      0.51, 0.13, 0.19, 0.85 };

  double b_data[] = { 1.0, 2.0, 3.0, 4.0 };
//
//  Create GSL_MATRIX M.
//
  gsl_matrix_view m = gsl_matrix_view_array ( a_data, 4, 4 );
//
//  Create GSL_VECtOR B.
//
  gsl_vector_view b = gsl_vector_view_array ( b_data, 4 );
//
//  Allocate GSL_VECTOR X (solution).
//
  gsl_vector *x = gsl_vector_alloc ( 4 );
  
  int s;
//
//  Allocate GSL_PERMUTATION P (pivot vector).
//
  gsl_permutation *p = gsl_permutation_alloc ( 4 );

  cout << "\n";
  cout << "GSL_LINALG_LU_TEST\n";
  cout << "  GSL_LINALG_LU_DECOMP computes LU factors of a matrix A.\n";
  cout << "  GSL_LINALG_LU_SOLVE solves linear systems A*x=b.\n";
//
//  LU factor the matrix M.
//
  gsl_linalg_LU_decomp ( &m.matrix, p, &s );
//
//  Solve M*x=b.
//
  gsl_linalg_LU_solve ( &m.matrix, p, &b.vector, x );
//
//  Display the solution.
//
  printf ( "\n" );
  printf ( "  Computed solution vector X: \n" );
  printf ( "\n" );

  gsl_vector_fprintf ( stdout, x, "%g" );
//
//  Free memory.
//
  gsl_permutation_free ( p );
  gsl_vector_free ( x );

  return;
}
//****************************************************************************80

double gsl_monte_f ( double *x, size_t dim_num, void *params )

//****************************************************************************80
//
//  Purpose:
//
//    GSL_MONTE_F evaluates the integrand for a Monte Carlo test.
//
//  Modified:
//
//    26 February 2007
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Mark Gelassi, Jim Davies, James Tyler, Bryan Gough,
//    Reid Priedhorsky, Gerard Jungman, Michael Booth, Fabrice Rossi,
//    GNU Scientific Library Reference Manual.
//
//  Parameters:
//
//    Input, double *X, the evaluation point.
//
//    Input, size_t DIM, the spatial dimension.
//
//    Input, void *PARAMS, optional parameters.
//
//    Output, double GSL_MONTE_F, the value of the integrand at X.
//
{
  double c;
  double den;
  int dim;
  double value;

  c = 1.0 / pow ( 2.0 * M_PI, dim_num );

  den = 1.0;
  for ( dim = 0; dim < dim_num; dim++ )
  {
    den = den * cos ( x[dim] );
  }

  den = 1.0 - den;

  if ( den == 0.0 )
  {
    value = 100000.0;
  }
  else
  {
    value = c / den;
  }
  
  return value;
}
//****************************************************************************80

void gsl_multiroot_fsolver_test ( )

//****************************************************************************80
//
//  Purpose:
//
//    GSL_MULTIROOT_FSOLVER_TEST tests the N-dimensional root finder.
//
//  Modified:
//
//    05 August 2005.
//
//  Author:
//
//    John Burkardt
//
{
  size_t i;
  size_t iter = 0;
  const size_t n = 2;
  struct rparams p = { 1.0, 10.0 };
  gsl_multiroot_fsolver *s;
  int status;
  const gsl_multiroot_fsolver_type *T;
  gsl_vector *x;
  double x_init[2] = { -10.0, -5.0 };

  cout << "\n";
  cout << "GSL_MULTIROOT_FSOLVER_TEST:\n";
  cout << "  Demonstrate the ability to find a root of a\n";
  cout << "  set of nonlinear equations.\n";
  cout << "\n";
  cout << "  In this case, we have two functions in two unknowns,\n";
  cout << "  and the only root is X = (1,1).\n";
  cout << "\n";

  gsl_multiroot_function f = { &rosenbrock_f, n, &p };
  x = gsl_vector_alloc ( n );

  gsl_vector_set ( x, 0, x_init[0] );
  gsl_vector_set ( x, 1, x_init[1] );

  T = gsl_multiroot_fsolver_hybrids;

  s = gsl_multiroot_fsolver_alloc ( T, 2 );

  gsl_multiroot_fsolver_set ( s, &f, x );

  gsl_multiroot_print_state ( iter, s );

  do 
  {
    iter++;

    status = gsl_multiroot_fsolver_iterate ( s );

    gsl_multiroot_print_state ( iter, s );

    if ( status )
    {
      break;
    }
    status = gsl_multiroot_test_residual ( s->f, 1.0E-07 );

  } while ( status == GSL_CONTINUE && iter < 1000 );

  printf ( "  status = %s\n", gsl_strerror ( status ) );
//
//  Free memory.
//
  gsl_multiroot_fsolver_free ( s );
  gsl_vector_free ( x );

  return;
}
//****************************************************************************80

void gsl_multiroot_print_state ( size_t iter, gsl_multiroot_fsolver *s )

//****************************************************************************80
//
//  Purpose:
//
//    GSL_MULTIROOT_PRINT_STATE prints the state of the root finding iteration.
//
//  Modified:
//
//    05 August 2005
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, SIZE_T ITER, the iteration counter.
//
//    Input, GSL_MULTIROOT_FSOLVER *S, the solver state.
//
{
  printf ( "  iter = %3lu x = %.3f %.3f f(x) = %.3e  %.3e\n",
    iter,
    gsl_vector_get ( s->x, 0 ),
    gsl_vector_get ( s->x, 1 ),
    gsl_vector_get ( s->f, 0 ),
    gsl_vector_get ( s->f, 1 ) );

  return;
}
//****************************************************************************80

void gsl_poly_eval_test ( )

//****************************************************************************80
//
//  Purpose:
//
//    GSL_POLY_EVAL_TEST tests the GSL real polynomial evaluator.
//
//  Modified:
//
//    08 March 2017
//
//  Author:
//
//    John Burkardt
//
{
  double c[4] = {-18.0, 27.0, -10.0, 1.0 };
  int i;
  int len = 4;
  double p;
  double x;

  cout << "\n";
  cout << "GSL_POLY_EVAL_TEST:\n";
  cout << "  GSL_POLY_EVAL evaluates a real polynomial.\n";
  cout << "  p(x) = (x-1)*(x-3)*(x-6)\n";
  cout << "       = x^3 - 10x^2 + 27x - 18.\n";
  cout << "\n";
  for ( i = 0; i <= 20; i++ )
  {
    x = ( double ) i / 2.0;
    p = gsl_poly_eval ( c, len, x );
    cout << "  " << setw(8) << x
         << "  " << setw(8) << p << "\n";
  }

  return;
}
//****************************************************************************80

void gsl_qrng_niederreiter_2_test ( )

//****************************************************************************80
//
//  Purpose:
//
//    GSL_QRNG_NIEDERREITER_2_TEST tests the GSL Niederreiter sequence routine.
//
//  Modified:
//
//    09 February 2007
//
//  Author:
//
//    John Burkardt
//
{
# define M 2
# define N 25

  int i;
  int j;
  double v[M];

  gsl_qrng * q = gsl_qrng_alloc ( gsl_qrng_niederreiter_2, M );

  cout << "\n";
  cout << "GSL_QRNG_NIEDERREITER_2_TEST:\n";
  cout << "  GSL_QRNG_ALLOC sets aside space for a sequence;\n";
  cout << "  GSL_QRNG_SOBOL requests the Niederreiter_2 sequence;\n";
  cout << "  GSL_QRNG_GET gets the next entry of the requested sequence;\n";
  cout << "\n";
  cout << "  Determine the first " << N << " points of the Niederreiter2\n";
  cout << "  quasi-random sequence in " << M << " dimensions.\n";
  cout << "\n";
  cout << "     I       X(I)\n";
  cout << "\n";

  for ( i = 0; i < N; i++ )
  {
    gsl_qrng_get ( q, v );

    cout << setw(6)  << i << "  ";
    for ( j = 0; j < M; j++ )
    {
      cout << setw(12) << v[j] << "  ";
    }
    cout << "\n";
  }

  return;
# undef M
# undef N
}
//****************************************************************************80

void gsl_qrng_sobol_test ( )

//****************************************************************************80
//
//  Purpose:
//
//    GSL_QRNG_SOBOL_TEST tests the GSL Sobol sequence routine.
//
//  Modified:
//
//    09 February 2007
//
//  Author:
//
//    John Burkardt
//
{
# define M 2
# define N 25

  int i;
  int j;
  double v[M];

  gsl_qrng * q = gsl_qrng_alloc ( gsl_qrng_sobol, M );

  cout << "\n";
  cout << "GSL_QRNG_SOBOL_TEST:\n";
  cout << "  GSL_QRNG_ALLOC sets aside space for a sequence;\n";
  cout << "  GSL_QRNG_SOBOL requests the Sobol sequence;\n";
  cout << "  GSL_QRNG_GET gets the next entry of the requested sequence;\n";
  cout << "\n";
  cout << "  Determine the first " << N << " points of the Sobol\n";
  cout << "  quasi-random sequence in " << M << " dimensions.\n";
  cout << "\n";
  cout << "     I       X(I)\n";
  cout << "\n";

  for ( i = 0; i < N; i++ )
  {
    gsl_qrng_get ( q, v );

    cout << setw(6)  << i << "  ";
    for ( j = 0; j < M; j++ )
    {
      cout << setw(12) << v[j] << "  ";
    }
    cout << "\n";
  }

  return;
# undef M
# undef N
}
//****************************************************************************80

int rosenbrock_f ( const gsl_vector *x, void *params, gsl_vector *f )

//****************************************************************************80
//
//  Purpose:
//
//    ROSENBROCK_F evaluates the Rosenbrock function.
//
//  Modified:
//
//    05 August 2005.
//
//  Author:
//
//    John Burkardt
//
{
  double a = ( ( struct rparams *) params )-> a;
  double b = ( ( struct rparams *) params )-> b;

  const double x0 = gsl_vector_get ( x, 0 );
  const double x1 = gsl_vector_get ( x, 1 );
  const double y0 = a * ( 1.0 - x0 );
  const double y1 = b * ( x1 - x0 * x0 );
  gsl_vector_set ( f, 0, y0 );
  gsl_vector_set ( f, 1, y1 );

  return GSL_SUCCESS;
}
//****************************************************************************80

void timestamp ( )

//****************************************************************************80
//
//  Purpose:
//
//    TIMESTAMP prints the current YMDHMS date as a time stamp.
//
//  Example:
//
//    May 31 2001 09:45:54 AM
//
//  Modified:
//
//    24 September 2003
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    None
//
{
# define TIME_SIZE 40

  static char time_buffer[TIME_SIZE];
  const struct tm *tm;
  size_t len;
  time_t now;

  now = time ( NULL );
  tm = localtime ( &now );

  len = strftime ( time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm );

  cout << time_buffer << "\n";

  return;
# undef TIME_SIZE
}
