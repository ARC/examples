#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load matlab/R2016b
#
echo "MATLAB_CASCADES_SKYLAKE: Normal beginning of execution."
#
matlab -nosplash -nodisplay -nojvm -nodesktop < power_table.m > matlab_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "MATLAB_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "MATLAB_CASCADES_SKYLAKE: Normal end of execution."
exit 0
