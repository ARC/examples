#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load matlab/R2016b
#
echo "MATLAB_NEWRIVER_HASWELL Normal beginning of execution."
#
matlab -nosplash -nodisplay -nojvm -nodesktop < power_table.m > matlab_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "MATLAB_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "MATLAB_NEWRIVER_HASWELL: Normal end of execution."
exit 0
