#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load matlab/R2016b
#
echo "MATLAB_NEWRIVER_BROADWELL: Normal beginning of execution."
#
matlab -nosplash -nodisplay -nojvm -nodesktop < power_table.m > matlab_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "MATLAB_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
#
echo "MATLAB_NEWRIVER_BROADWELL: Normal end of execution."
exit 0
