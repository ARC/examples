#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load cmake/3.6.2
#
echo "CMAKE_CASCADES_BROADWELL: Normal beginning of execution."
#
ls
#
cmake .
if [ $? -ne 0 ]; then
  echo "CMAKE_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
ls
#
make
if [ $? -ne 0 ]; then
  echo "CMAKE_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
ls
#
echo "CMAKE_CASCADES_BROADWELL: Normal end of execution."
exit 0

