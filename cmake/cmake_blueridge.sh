#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe

cd $PBS_O_WORKDIR

module purge
module load gcc/4.7.2
module load cmake/3.1.3
#
echo "CMAKE_BLUERIDGE: Normal beginning of execution."
#
#  Remove old files
#
rm -f CMakeCache.txt
rm -rf CMakeFiles
rm -f cmake_install.cmake
rm -f Tutorial
ls
#
cmake .
if [ $? -ne 0 ]; then
  echo "CMAKE_BLUERIDGE: Run error!"
  exit 1
fi
#
ls
#
make
if [ $? -ne 0 ]; then
  echo "CMAKE_BLUERIDGE: Run error!"
  exit 1
fi
#
ls
#
echo "CMAKE_BLUERIDGE: Normal end of execution."
exit 0

