#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load cmake/3.7.2
#
echo "CMAKE_NEWRIVER_HASWELL: Normal beginning of execution."
#
ls
#
cmake .
if [ $? -ne 0 ]; then
  echo "CMAKE_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
ls
#
make
if [ $? -ne 0 ]; then
  echo "CMAKE_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
ls
#
echo "CMAKE_NEWRIVER_HASWELL: Normal end of execution."
exit 0

