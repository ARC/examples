#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load cmake/3.8.2
#
echo "CMAKE_CASCADES_SKYLAKE: Normal beginning of execution."
#
ls
#
cmake .
if [ $? -ne 0 ]; then
  echo "CMAKE_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
ls
#
make
if [ $? -ne 0 ]; then
  echo "CMAKE_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
ls
#
echo "CMAKE_CASCADES_SKYLAKE: Normal end of execution."
exit 0

