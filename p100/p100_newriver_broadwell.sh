#! /bin/bash
#
#PBS -l nodes=1:ppn=28:gpus=2
#PBS -l walltime=00:05:00
#PBS -q p100_dev_q
#PBS -W group_list=newriver
#PBS -A arctest
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load mvapich2/2.2
module load cuda/8.0.61
module load occa/2017Jun12
#
echo "P100_NEWRIVER_BROADWELL: Normal beginning of execution."
#
#  Print occa devices.
#
echo "Print occa devices:"
occa info
#
#  Copy the occa addVectors C++ example directory.
#
echo "Copy occa addVectors C++ example directory."
cp -r $OCCA_DIR/examples/addVectors/cpp occaAddVectors
cd occaAddVectors
#
#  Make the example.
#
echo "Make the example."
make
if [ $? -ne 0 ]; then
  echo "P100_NEWRIVER_BROADWELL: Make error."
  exit 1
fi
#
#  Run the example.
#
echo "Run the example."
./main
if [ $? -ne 0 ]; then
  echo "P100_NEWRIVER_BROADWELL: Run error."
  exit 1
fi
#
#  Remove the copy of the occa addVectors C++ example directory.
#
echo "Clean up."
cd ..
rm -r occaAddVectors
#
echo "P100_NEWRIVER_BROADWELL: Normal end of execution."
exit 0
