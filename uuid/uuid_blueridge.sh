#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load uuid/1.6.2
#
echo "UUID_BLUERIDGE: Normal beginning of execution."
#
#  Once UUID is set up (but not before!), 
#  you can type "man uuid" for more information.
#
man uuid
#
#  Version 1 is "time based".
#  It can be used to assign a UUID to a given time,
#  or to recover the time from a given UUID.
#
#  Calling it twice in a row gives different values because of different times.
#
echo ""
echo "Run    uuid -v 1 (twice in a row):"
echo ""
uuid -v 1
if [ $? -ne 0 ]; then
  echo "UUID_BLUERIDGE: Run error!"
  exit 1
fi
uuid -v 1
if [ $? -ne 0 ]; then
  echo "UUID_BLUERIDGE: Run error!"
  exit 1
fi
#
#  Capture the uuid in a file.
#
echo ""
echo "Run    uuid -v1 -o file:  (no output to screen!)"
echo ""
uuid -v 1 -o file
if [ $? -ne 0 ]; then
  echo "UUID_BLUERIDGE: Run error!"
  exit 1
fi
#
#  Version 1 UUID's can be decoded, to recover the time.
#  The following command looks strange, but we're really
#  just trying to issue the command
#    uuid -d XXXXX
#  where XXXXX is the value stored in "file".
#
echo ""
echo "Run    cat file | uuid -d -"
echo ""
cat file | uuid -d -
if [ $? -ne 0 ]; then
  echo "UUID_BLUERIDGE: Run error!"
  exit 1
fi
#
#  Version 3 UUID's expect a "namespace" argument that describes
#  a particular string for which a UUID is desired.  A common
#  namespace is "URL", which indicates that the string is a URL.
#
echo ""
echo "Run    uuid -v 3 ns:URL https://en.wikipedia.org/wiki/Universally_unique_identifier"
echo ""
uuid -v 3 ns:URL https://en.wikipedia.org/wiki/Universally_unique_identifier
if [ $? -ne 0 ]; then
  echo "UUID_BLUERIDGE: Run error!"
  exit 1
fi
#
#  Version 5 UUID's are similar to version 3, but using a different
#  hashing scheme.
#
echo ""
echo "Run    uuid -v 5 ns:URL https://en.wikipedia.org/wiki/Universally_unique_identifier"
echo ""
uuid -v 5 ns:URL https://en.wikipedia.org/wiki/Universally_unique_identifier
if [ $? -ne 0 ]; then
  echo "UUID_BLUERIDGE: Run error!"
  exit 1
fi
#
#  For version 3, a given pair of namespace and string will produce the 
#  same UUID output upon repeated calls.
#
echo ""
echo "Run    uuid -v 3 ns:URL DonaldDuck (twice)"
echo ""
uuid -v 3 ns:URL DonaldDuck
if [ $? -ne 0 ]; then
  echo "UUID_BLUERIDGE: Run error!"
  exit 1
fi
uuid -v 3 ns:URL DonaldDuck
if [ $? -ne 0 ]; then
  echo "UUID_BLUERIDGE: Run error!"
  exit 1
fi
#
#  Also true for version 5, although the output will not be the same
#  as for version 3.
#
echo ""
echo "Run    uuid -v 5 ns:URL DonaldDuck (twice)"
echo ""
uuid -v 5 ns:URL DonaldDuck
if [ $? -ne 0 ]; then
  echo "UUID_BLUERIDGE: Run error!"
  exit 1
fi
uuid -v 5 ns:URL DonaldDuck 
if [ $? -ne 0 ]; then
  echo "UUID_BLUERIDGE: Run error!"
  exit 1
fi
#
echo ""
echo "UUID_BLUERIDGE: Normal end of execution."
exit 0
