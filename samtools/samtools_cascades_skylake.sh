#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load gsl/2.4
module load samtools/1.3.1
#
echo "SAMTOOLS_CASCADES_SKYLAKE: Normal beginning of execution."
#
#  Convert a SAM file to BAM format.
#  * -b indicates the output should be in BAM format;
#  * -S indicates the input is in SAM format;
#  * -o names the output file.
#
samtools view -b -S -o sim_reads_aligned.bam sim_reads_aligned.sam
if [ $? -ne 0 ]; then
  echo "SAMTOOLS_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "SAMTOOLS_CASCADES_SKYLAKE: Normal end of execution."
exit 0
