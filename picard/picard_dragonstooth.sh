#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load jdk/1.6.0
module load picard/1.138
#
echo "PICARD_DRAGONSTOOTH: Normal beginning of execution."
#
#  Compare two SAM files.
#
java -jar $PICARD_DIR/picard.jar CompareSAMs unmapped_first.sam unmapped_second.sam
if [ $? -ne 0 ]; then
  echo "PICARD_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "PICARD_DRAGONSTOOTH: Normal end of execution."
exit 0
