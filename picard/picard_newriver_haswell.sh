#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load jdk/1.6.0
module load picard/1.138
#
echo "PICARD_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Compare two SAM files.
#
java -jar $PICARD_DIR/picard.jar CompareSAMs unmapped_first.sam unmapped_second.sam
if [ $? -ne 0 ]; then
  echo "PICARD_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "PICARD_NEWRIVER_HASWELL: Normal end of execution."
exit 0
