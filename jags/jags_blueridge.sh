#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.6.5
module load jags/3.4.0
#
echo "JAGS_BLUERIDGE: Normal beginning of execution."
#
jags < bones.inp > jags_blueridge.txt
if [ $? -ne 0 ]; then
  echo "JAGS_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "JAGS_BLUERIDGE: Normal end of execution."
exit 0
