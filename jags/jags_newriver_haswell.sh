#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/2.0.0
module load JAGS/4.2.0
#
echo "JAGS_NEWRIVER_HASWELL: Normal beginning of execution."
#
jags < bones.inp > jags_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "JAGS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "JAGS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
