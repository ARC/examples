###############################################################################
#
#   CORA v3.6
#   Template - Biofidelity Rating According To ISO/TR9790
#
###############################################################################
#
###############################################################################
#
#   Global Parameters
#
###############################################################################
BEGIN GLOBAL_PARAMETERS
   DES_MOD             Biofidelity rating - Example ; Header of the evaluation
   DES_GLO             ISO/TR9790                   ; Sub-header of the evaluation
#
# Global settings to define the interval of evaluation
  A_THRES             0.030                 ; Threshold to set the start of the interval of evaluation [0,...,1]
  B_THRES             0.075                 ; Threshold to set the end of the interval of evaluation [0,...,1]
  A_EVAL              0.010                 ; Extension of the interval of evaluation [0,...,1]
  B_DELTA_END         0.200                 ; Additional parameter to shorten the interval of evaluation (width of the corridor: A_DELTA_END*Y_NORM) 0 = disabled
  T_MIN/T_MAX         automatic automatic   ; Manually defined start (time) and end (time) of the interval of evaluation (automatic = calculated for each channel)
  T_UNIT              s                     ; Unit of T_MIN, T_MAX, t_min and t_max
#
# Global settings of the corridor method
  K                   2                     ; Transition between ratings of 1 and 0 of the corridor method [-] (1 = linear, 2 = quadratic ...)
  G_1                 0.50                  ; Weighting factor of the corridor method [-]
  a_0/b_0             0.05     0.50         ; Width of the inner and outer corridor [-]
  a_sigma/b_sigma     0        0            ; Multiples of the standard deviation to widen the inner and outer corridor [-]
# Global settings of the cross correlation method
  D_MIN               0.01                  ; delta_min as share of the interval of evaluation [0,...,1]
  D_MAX               0.12                  ; delta_max as share of the interval of evaluation [0,...,1]
  INT_MIN             0.80                  ; Minimum overlap of the interval [0,...,1]
  K_V                 10                    ; Transition between ratings of 1 and 0 of the progression rating [-] (1 = linear, 2 = quadratic ...)
  K_G                 1                     ; Transition between ratings of 1 and 0 of the size rating [-] (1 = linear, 2 = quadratic ...)
  K_P                 1                     ; Transition between ratings of 1 and 0 of the phase shift rating [-] (1 = linear, 2 = quadratic ...)
  G_V                 0.50                  ; Weighting factors of the progression rating [-]
  G_G                 0.25                  ; Weighting factors of the size rating [-]
  G_P                 0.25                  ; Weighting factors of the phase shift rating [-]
  G_2                 0.50                  ; Weighting factors of the cross correlation method [-]
# Normalisation of the the weighting factors
  WF_NORM             YES                   ; Normalisation of the weighting factors [YES/NO]?
# Signal settings
  ISONAME_1-2/11-12   NO   NO               ; Consideration of the position 1/2 (test object, seating position) and 11/12 (fine location 3 - dummy) of the ISO code [YES/NO]
  MIN_NORM            0.00                  ; Threshold (as fraction of the global absolute maximum amplitude) to start special treatment of secondary axis [0,...,1]
  Y_NORM              extremum              ; Type of calculation of Y_NORM (extremum or value)
#
# Format settings of the html report
  OUTPUT_FORMAT       Hypergraph            ; Export format (LSPOST, PAMVIEW or Hypergraph)
# Layout of the html report
  FONT_SMALL          12                    ; Size of the small font
  FONT_LARGE          14                    ; Size of the large font
  PreT_LC/PostT_LC    0.1  0.1              ; Expansion of the plotted interval of the curves (-1: complete curve)
END GLOBAL_PARAMETERS
#
#
###############################################################################
#
#  Loadcase
#  x = use global settings
#
###############################################################################
BEGIN LOADCASE
  NAM_LC              Evaluation of the head               ; Header of the loadcase
  DES_LC              ISO/TR9790                           ; Sub-header of the loadcase
  WF_LC               7                                    ; Weighting factor of the loadcase
#
# Layout of the html report
  PreT_LC             x                                    ; Expansion of the plotted interval of the curves (pre)
  PostT_LC            x                                    ; Expansion of the plotted interval of the curves (post)
  MinOrd_LC           1.0                                  ; Scale factor of the plotted ymin
  MaxOrd_LC           1.0                                  ; Scale factor of the plotted ymax
#
  BEGIN SUBLOADCASE
    NAM_SLC          Head test 1                           ; Header of the loadcase
    DES_SLC          200 mm rigid drop (section 5.1)       ; Sub-header of the loadcase
    WF_SLC           8                                     ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                   unit    g    timeshift
      data/dummy/head/01/head_01_generic.dat              m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm   t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                              a_sigma  b_sigma  D_min  D_max  Filter
      00HEAD000000ACR0  9   peak     x      x      x    x    x    x   x   x    x     data/corridors/head/iso9790_head_01_00HEAD000000ACR0.dat      x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Head test 2                          ; Header of the loadcase
    DES_SLC          1000 mm padded drop (section 5.2)    ; Sub-header of the loadcase
    WF_SLC           4                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/head/02/head_02_generic.dat             m-kg-s  NO  0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm   t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                              a_sigma  b_sigma  D_min  D_max  Filter
      00HEAD000000ACR0  9   peak     x      x      x    x    x    x   x   x    x     data/corridors/head/iso9790_head_02_00HEAD000000ACR0.dat      x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
END LOADCASE
#
#
###############################################################################
BEGIN LOADCASE
  NAM_LC              Evaluation of the neck               ; Header of the loadcase
  DES_LC              ISO/TR9790                           ; Sub-header of the loadcase
  WF_LC               6                                    ; Weighting factor of the loadcase
#
# Layout of the html report
  PreT_LC             x                                    ; Expansion of the plotted interval of the curves (pre)
  PostT_LC            x                                    ; Expansion of the plotted interval of the curves (post)
  MinOrd_LC           1.0                                  ; Scale factor of the plotted ymin
  MaxOrd_LC           1.0                                  ; Scale factor of the plotted ymax
#
  BEGIN SUBLOADCASE
    NAM_SLC          Neck test 1                          ; Header of the loadcase
    DES_SLC          7.2 G sled test (section 6.1)        ; Sub-header of the loadcase
    WF_SLC           7                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/neck/01/neck_01_generic.dat             m-kg-s  YES  0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm   t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                              a_sigma  b_sigma  D_min  D_max  Filter
      00THSP010000ACY0  5   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_01_00THSP010000ACY0.dat      x        x        x      x      0
      00THSP010000DCY0  5   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_01_00THSP010000DCY0.dat      x        x        x      x      0
      00HEAD000000DCY0  8   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_01_00HEAD000000DCY0.dat      x        x        x      x      0
      00HEAD000000DCZ0  6   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_01_00HEAD000000DCZ0.dat      x        x        x      x      0
      00HEAD000000EV00  5   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_01_00HEAD000000EV00.dat      x        x        x      x      0
      00HEAD000000ACY0  5   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_01_00HEAD000000ACY0.dat      x        x        x      x      0
      00HEAD000000ACZ0  5   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_01_00HEAD000000ACZ0.dat      x        x        x      x      0
      00NECK000000ANY0  7   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_01_00NECK000000ANY0.dat      x        x        x      x      0
      00NECK000000ANZ0  4   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_01_00NECK000000ANZ0.dat      x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Neck test 2                          ; Header of the loadcase
    DES_SLC          6.7 G sled test (section 6.2)        ; Sub-header of the loadcase
    WF_SLC           6                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/neck/02/neck_02_generic.dat             m-kg-s  YES  0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm   t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                              a_sigma  b_sigma  D_min  D_max  Filter
      00NECK000000ANY0  7   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_02_00NECK000000ANY0.dat      x        x        x      x      0
      00NECK000000MOY0  7   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_02_00NECK000000MOY0.dat      x        x        x      x      0
      00NECK000000MOX0  3   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_02_00NECK000000MOX0.dat      x        x        x      x      0
      00NECK000000MOZ0  4   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_02_00NECK000000MOZ0.dat      x        x        x      x      0
      00NECK000000FOY0  7   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_02_00NECK000000FOY0.dat      x        x        x      x      0
      00NECK000000FOZ0  6   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_02_00NECK000000FOZ0.dat      x        x        x      x      0
      00NECK000000FOX0  3   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_02_00NECK000000FOX0.dat      x        x        x      x      0
      00HEAD000000ACR0  4   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_02_00HEAD000000ACR0.dat      x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC             Neck test 3                       ; Header of the loadcase
    DES_SLC             12.2 G sled test (section 6.3)    ; Sub-header of the loadcase
    WF_SLC              3                                 ; Weighting factor of the loadcase
    METHOD              ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/neck/03/neck_03_generic.dat             m-kg-s  YES  0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm   t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                              a_sigma  b_sigma  D_min  D_max  Filter
      00THSP010000ACY0  5   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_03_00THSP010000ACY0.dat      x        x        x      x      0
      00HEAD000000ACY0  5   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_03_00HEAD000000ACY0.dat      x        x        x      x      0
      00HEAD000000DCY0  5   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_03_00HEAD000000DCY0.dat      x        x        x      x      0
      00NECK000000ANY0  7   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_03_00NECK000000ANY0.dat      x        x        x      x      0
      00NECK000000ANZ0  4   peak     x      x      x    x    x    x   x   x    x     data/corridors/neck/iso9790_neck_03_00NECK000000ANZ0.dat      x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
END LOADCASE
#
#
###############################################################################
BEGIN LOADCASE
  NAM_LC              Evaluation of the shoulder           ; Header of the loadcase
  DES_LC              ISO/TR9790                           ; Sub-header of the loadcase
  WF_LC               5                                    ; Weighting factor of the loadcase
# Layout of the html report
  PreT_LC             x                                    ; Expansion of the plotted interval of the curves (pre)
  PostT_LC            x                                    ; Expansion of the plotted interval of the curves (post)
  MinOrd_LC           1.0                                  ; Scale factor of the plotted ymin
  MaxOrd_LC           1.0                                  ; Scale factor of the plotted ymax
#
  BEGIN SUBLOADCASE
    NAM_SLC          Shoulder test 1                      ; Header of the loadcase
    DES_SLC          4.5 m/s pendulum test (section 4.1)  ; Sub-header of the loadcase
    WF_SLC           6                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/shoulder/01/shoulder_01_generic.dat     m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                   a_sigma  b_sigma  D_min  D_max  Filter
      00SENS000000FOX0  8   corridor  x      x      x    x    x    x   x   x    x     data/corridors/shoulder/iso9790_shoulder_01_00SENS000000FOX0.dat   x        x        x      x      0
      00SHLD000000DSY0  6   peak      x      x      x    x    x    x   x   x    x     data/corridors/shoulder/iso9790_shoulder_01_00SHLD000000DSY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Shoulder test 2                      ; Header of the loadcase
    DES_SLC          7.2 m/s sled test (section 6.1)      ; Sub-header of the loadcase
    WF_SLC           5                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/shoulder/02/shoulder_02_generic.dat     m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                   a_sigma  b_sigma  D_min  D_max  Filter
      00THSP010000ACY0  6   peak      x      x      x    x    x    x   x   x    x     data/corridors/shoulder/iso9790_shoulder_02_00THSP010000ACY0.dat   x        x        x      x      0
      00THSP010000DCY0  6   peak      x      x      x    x    x    x   x   x    x     data/corridors/shoulder/iso9790_shoulder_02_00THSP010000DCY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Shoulder test 3                      ; Header of the loadcase
    DES_SLC          12.2 m/s sled test (section 6.3)     ; Sub-header of the loadcase
    WF_SLC           3                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/shoulder/03/shoulder_03_generic.dat     m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                   a_sigma  b_sigma  D_min  D_max  Filter
      00THSP010000ACY0  6   peak      x      x      x    x    x    x   x   x    x     data/corridors/shoulder/iso9790_shoulder_03_00THSP010000ACY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Shoulder test 4                      ; Header of the loadcase
    DES_SLC          WSU type sled test (section 6.5)     ; Sub-header of the loadcase
    WF_SLC           7                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/shoulder/04/shoulder_04_generic.dat     m-kg-s  NO  0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                   a_sigma  b_sigma  D_min  D_max  Filter
      00SHLD000000FOY0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/shoulder/iso9790_shoulder_04_00SHLD000000FOY0.dat   x        x        x      x      0
####  no Korridor in ISO given
###   00THSP010000DSY0  5   corridor  x      x      x    x    x    x   x   x    x     data/corridors/shoulder/iso9790_shoulder_04_00THSP010000DSY0.dat   x        x        x      x      0
    END SIGNALS
   END SUBLOADCASE
END LOADCASE
#
#
###############################################################################
BEGIN LOADCASE
  NAM_LC              Evaluation of the thorax             ; Header of the loadcase
  DES_LC              ISO/TR9790                           ; Sub-header of the loadcase
  WF_LC               10                                   ; Weighting factor of the loadcase
# Layout of the html report
  PreT_LC             x                                    ; Expansion of the plotted interval of the curves (pre)
  PostT_LC            x                                    ; Expansion of the plotted interval of the curves (post)
  MinOrd_LC           1.0                                  ; Scale factor of the plotted ymin
  MaxOrd_LC           1.0                                  ; Scale factor of the plotted ymax
#
  BEGIN SUBLOADCASE
    NAM_SLC          Thorax test 1                        ; Header of the loadcase
    DES_SLC          4.3 m/s pendulum test (section 4.2)  ; Sub-header of the loadcase
    WF_SLC           9                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/thorax/01/thorax_01_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                               a_sigma  b_sigma  D_min  D_max  Filter
      00SENS000000FOX0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_01_00SENS000000FOX0.dat   x        x        x      x      0
      00SPINUP0000ACY0  7   corridor  x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_01_00SPINUP0000ACY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Thorax test 2                        ; Header of the loadcase
    DES_SLC          6.7 m/s pendulum test (section 4.2)  ; Sub-header of the loadcase
    WF_SLC           9                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                    unit    g    timeshift
      data/dummy/thorax/02/thorax_02_generic.dat   m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                               a_sigma  b_sigma  D_min  D_max  Filter
      00SENS000000FOX0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_02_00SENS000000FOX0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Thorax test 3                        ; Header of the loadcase
    DES_SLC          1000 mm rigid drop (section 5.3)     ; Sub-header of the loadcase
    WF_SLC           6                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/thorax/03/thorax_03_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                               a_sigma  b_sigma  D_min  D_max  Filter
      00TRRI000000FOY0  8   corridor  x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_03_00TRRI000000FOY0.dat   x        x        x      x      0
      00TRRI000000DSY0  8   peak      x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_03_00TRRI000000DSY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Thorax test 4                        ; Header of the loadcase
    DES_SLC          2000 mm padded drop (section 5.3)    ; Sub-header of the loadcase
    WF_SLC           5                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/thorax/04/thorax_04_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                               a_sigma  b_sigma  D_min  D_max  Filter
      00TRRI000000FOY0  8   corridor  x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_04_00TRRI000000FOY0.dat   x        x        x      x      0
      00TRRI000000DSY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_04_00TRRI000000DSY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Thorax test 5                           ; Header of the loadcase
    DES_SLC          Heidelberg sled test type (section 6.4) ; Sub-header of the loadcase
    WF_SLC           7                                       ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                     unit    g    timeshift
      data/dummy/thorax/05/thorax_05_generic.dat            m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                               a_sigma  b_sigma  D_min  D_max  Filter
      00TRRI000000FOY0  8   corridor  x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_05_00TRRI000000FOY0.dat   x        x        x      x      0
      00SPINUP0000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_05_00SPINUP0000ACY0.dat   x        x        x      x      0
      00SPINLO0000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_05_00SPINLO0000ACY0.dat   x        x        x      x      0
      00TRRI000000ACY0  6   peak      x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_05_00TRRI000000ACY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Thorax test 6                          ; Header of the loadcase
    DES_SLC          WSU type sled test (section 6.5)       ; Sub-header of the loadcase
    WF_SLC           7                                      ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                    unit    g    timeshift
      data/dummy/thorax/06/thorax_06_generic.dat           m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                               a_sigma  b_sigma  D_min  D_max  Filter
      00SHLD000000FOY0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_06_00SHLD000000FOY0.dat   x        x        x      x      0
      00THSP120000DSY0  5   peak      x      x      x    x    x    x   x   x    x     data/corridors/thorax/iso9790_thorax_06_00THSP120000DSY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
END LOADCASE
#
#
###############################################################################
BEGIN LOADCASE
  NAM_LC              Evaluation of the abdomen            ; Header of the loadcase
  DES_LC              ISO/TR9790                           ; Sub-header of the loadcase
  WF_LC               8                                    ; Weighting factor of the loadcase
#
# Layout of the html report
  PreT_LC             x                                    ; Expansion of the plotted interval of the curves (pre)
  PostT_LC            x                                    ; Expansion of the plotted interval of the curves (post)
  MinOrd_LC           1.0                                  ; Scale factor of the plotted ymin
  MaxOrd_LC           1.0                                  ; Scale factor of the plotted ymax
#
  BEGIN SUBLOADCASE
    NAM_SLC          Abdomen test 1                                ; Header of the loadcase
    DES_SLC          1000 mm drop into rigid armrest (section 5.4) ; Sub-header of the loadcase
    WF_SLC           7                                             ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/abdomen/01/abdomen_01_generic.dat       m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00DOOR000000FOY0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/abdomen/iso9790_abdomen_01_00DOOR000000FOY0.dat   x        x        x      x      0
      00SPINLO0000ACY0  6   peak      x      x      x    x    x    x   x   x    x     data/corridors/abdomen/iso9790_abdomen_01_00SPINLO0000ACY0.dat   x        x        x      x      0
      00TRRI000000ACY0  4   peak      x      x      x    x    x    x   x   x    x     data/corridors/abdomen/iso9790_abdomen_01_00TRRI000000ACY0.dat   x        x        x      x      0
      00ABDO000000DSY0  9   peak_low  x      x      x    x    x    x   x   x    x     data/corridors/abdomen/iso9790_abdomen_01_00ABDO000000DSY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Abdomen test 2                                ; Header of the loadcase
    DES_SLC          2000 mm drop into rigid armrest (section 5.4) ; Sub-header of the loadcase
    WF_SLC           6                                             ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/abdomen/02/abdomen_02_generic.dat       m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00DOOR000000FOY0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/abdomen/iso9790_abdomen_02_00DOOR000000FOY0.dat   x        x        x      x      0
      00SPINLO0000ACY0  6   peak      x      x      x    x    x    x   x   x    x     data/corridors/abdomen/iso9790_abdomen_02_00SPINLO0000ACY0.dat   x        x        x      x      0
      00TRRI000000ACY0  4   peak      x      x      x    x    x    x   x   x    x     data/corridors/abdomen/iso9790_abdomen_02_00TRRI000000ACY0.dat   x        x        x      x      0
      00ABDO000000DSY0  9   peak_low  x      x      x    x    x    x   x   x    x     data/corridors/abdomen/iso9790_abdomen_02_00ABDO000000DSY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Abdomen test 3                       ; Header of the loadcase
    DES_SLC          WSU type sled test (section 6.5)     ; Sub-header of the loadcase
    WF_SLC           3                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/abdomen/03/abdomen_03_generic.dat       m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00ABDO000000FOY0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/abdomen/iso9790_abdomen_03_00ABDO000000FOY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Abdomen test 4                       ; Header of the loadcase
    DES_SLC          WSU type sled test (section 6.5)     ; Sub-header of the loadcase
    WF_SLC           3                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/abdomen/04/abdomen_04_generic.dat       m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00ABDO000000FOY0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/abdomen/iso9790_abdomen_04_00ABDO000000FOY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Abdomen test 5                       ; Header of the loadcase
    DES_SLC          WSU type sled test (section 6.5)     ; Sub-header of the loadcase
    WF_SLC           7                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/abdomen/05/abdomen_05_generic.dat       m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00ABDO000000FOY0  9   corridor  x       x     x    x    x    x   x   x    x     data/corridors/abdomen/iso9790_abdomen_05_00ABDO000000FOY0.dat   x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
END LOADCASE
#
#
###############################################################################
BEGIN LOADCASE
  NAM_LC              Evaluation of the pelvis             ; Header of the loadcase
  DES_LC              ISO/TR9790                           ; Sub-header of the loadcase
  WF_LC               8                                    ; Weighting factor of the loadcase
#
# Layout of the html report
  PreT_LC             x                                    ; Expansion of the plotted interval of the curves (pre)
  PostT_LC            x                                    ; Expansion of the plotted interval of the curves (post)
  MinOrd_LC           1.0                                  ; Scale factor of the plotted ymin
  MaxOrd_LC           1.0                                  ; Scale factor of the plotted ymax
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 1                        ; Header of the loadcase
    DES_SLC          6 m/s pendulum test (section 4.3)    ; Sub-header of the loadcase
    WF_SLC           8                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/01/pelvis_01_generic.dat         m-kg-s  NO  0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00SENS000000FOX0  9   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_01_00SENS000000FOX0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 2                        ; Header of the loadcase
    DES_SLC          10 m/s pendulum test (section 4.3)   ; Sub-header of the loadcase
    WF_SLC           9                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                   unit    g    timeshift
      data/dummy/pelvis/02/pelvis_02_generic.dat          m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00SENS000000FOX0  9   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_02_00SENS000000FOX0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 3                        ; Header of the loadcase
    DES_SLC          500 mm rigid drop (section 5.3)      ; Sub-header of the loadcase
    WF_SLC           4                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/03/pelvis_03_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00PELV000000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_03_00PELV000000ACY0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 4                        ; Header of the loadcase
    DES_SLC          1000 mm rigid drop (section 5.3)     ; Sub-header of the loadcase
    WF_SLC           4                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/04/pelvis_04_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00PELV000000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_04_00PELV000000ACY0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 5                        ; Header of the loadcase
    DES_SLC          2000 mm padded drop (section 5.3)    ; Sub-header of the loadcase
    WF_SLC           3                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/05/pelvis_05_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00PELV000000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_05_00PELV000000ACY0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 6                        ; Header of the loadcase
    DES_SLC          3000 mm padded drop (section 5.3)    ; Sub-header of the loadcase
    WF_SLC           5                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/06/pelvis_06_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00PELV000000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_06_00PELV000000ACY0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 7                           ; Header of the loadcase
    DES_SLC          Heidelberg sled test type (section 6.4) ; Sub-header of the loadcase
    WF_SLC           8                                       ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/07/pelvis_07_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00PELV000000FOY0  9   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_07_00PELV000000FOY0.dat     x        x        x      x      0
      00PELV000000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_07_00PELV000000ACY0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 8                           ; Header of the loadcase
    DES_SLC          Heidelberg sled test type (section 6.4) ; Sub-header of the loadcase
    WF_SLC           7                                       ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/08/pelvis_08_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00PELV000000FOY0  8   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_08_00PELV000000FOY0.dat     x        x        x      x      0
      00PELV000000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_08_00PELV000000ACY0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 9                           ; Header of the loadcase
    DES_SLC          Heidelberg sled test type (section 6.4) ; Sub-header of the loadcase
    WF_SLC           8                                       ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/09/pelvis_09_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00PELV000000FOY0  9   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_09_00PELV000000FOY0.dat     x        x        x      x      0
      00PELV000000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_09_00PELV000000ACY0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 10                       ; Header of the loadcase
    DES_SLC          WSU type sled test (section 6.5)     ; Sub-header of the loadcase
    WF_SLC           3                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/10/pelvis_10_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00PELV000000FOY0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_10_00PELV000000FOY0.dat     x        x        x      x      0
      00PELV000000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_10_00PELV000000ACY0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 11                       ; Header of the loadcase
    DES_SLC          WSU type sled test (section 6.5)     ; Sub-header of the loadcase
    WF_SLC           3                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/11/pelvis_11_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00PELV000000FOY0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_11_00PELV000000FOY0.dat     x        x        x      x      0
      00PELV000000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_11_00PELV000000ACY0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 12                       ; Header of the loadcase
    DES_SLC          WSU type sled test (section 6.5)     ; Sub-header of the loadcase
    WF_SLC           3                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/12/pelvis_12_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00PELV000000FOY0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_12_00PELV000000FOY0.dat     x        x        x      x      0
      00PELV000000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_12_00PELV000000ACY0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
#
  BEGIN SUBLOADCASE
    NAM_SLC          Pelvis test 13                       ; Header of the loadcase
    DES_SLC          WSU type sled test (section 6.5)     ; Sub-header of the loadcase
    WF_SLC           7                                    ; Weighting factor of the loadcase
    METHOD           ISO9790
#
    BEGIN DATAFILES
#     Name                                                  unit    g    timeshift
      data/dummy/pelvis/13/pelvis_13_generic.dat         m-kg-s  NO   0.0
    END DATAFILES
    BEGIN SIGNALS
#     Channels
#     Name              WF  Y_norm    t_min  t_max  g_V  g_G  g_P  g1  g2  a_0  b_0   a_t                                                                 a_sigma  b_sigma  D_min  D_max  Filter
      00PELV000000FOY0  9   corridor  x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_13_00PELV000000FOY0.dat     x        x        x      x      0
      00PELV000000ACY0  7   peak      x      x      x    x    x    x   x   x    x     data/corridors/pelvis/iso9790_pelvis_13_00PELV000000ACY0.dat     x        x        x      x      0
    END SIGNALS
  END SUBLOADCASE
END LOADCASE
