#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load cora/3.6.1
echo ""
echo "CORA_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Define the location of the CORA installation.
#
export INST_PATH=$CORA_DIR
#
cora_361 iso9790_example.cps
if [ $? -ne 0 ]; then
  echo "CORA_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo ""
echo "CORA_CASCADES_BROADWELL: Normal end of execution."
exit 0
