#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/7.3.0
#
echo "GCC_CASCADES_SKYLAKE: Normal beginning of execution."
#
gcc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "GCC_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
gcc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "GCC_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > gcc_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "GCC_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm heated_plate
#
echo "GCC_CASCADES_SKYLAKE: Normal end of execution."
exit 0

