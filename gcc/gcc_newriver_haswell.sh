#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
#
echo "GCC_NEWRIVER_HASWELL: Normal beginning of execution."
#
gcc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "GCC_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
gcc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "GCC_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > gcc_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "GCC_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm heated_plate
#
echo "GCC_NEWRIVER_HASWELL: Normal end of execution."
exit 0

