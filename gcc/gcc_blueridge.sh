#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.3.0
#
echo "GCC_BLUERIDGE: Normal beginning of execution."
#
gcc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "GCC_BLUERIDGE: Compile error!"
  exit 1
fi
#
gcc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "GCC_BLUERIDGE: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > gcc_blueridge.txt
if [ $? -ne 0 ]; then
  echo "GCC_BLUERIDGE: Run error!"
  exit 1
fi
rm heated_plate
#
echo "GCC_BLUERIDGE: Normal end of execution."
exit 0

