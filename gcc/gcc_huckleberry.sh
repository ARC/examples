#! /bin/bash
#
#SBATCH -J gcc_huckleberry
#SBATCH -p normal_q
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 00:05:00
#SBATCH --mem=100M
#
cd $SLURM_SUBMIT_DIR
#
module purge
module load gcc
#
echo "GCC_HUCKLEBERY: Normal beginning of execution."
#
gcc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "GCC_HUCKLEBERRY: Compile error!"
  exit 1
fi
#
gcc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "GCC_HUCKLEBERRY: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > gcc_huckleberry.txt
if [ $? -ne 0 ]; then
  echo "GCC_HUCKLEBERRY: Run error!"
  exit 1
fi
rm heated_plate
#
echo "GCC_HUCKLEBERRY: Normal end of execution."
exit 0

