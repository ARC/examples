#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load pango/1.40.5
#
echo "PANGO_NEWRIVER_HASWELL: Normal beginning of execution."
#
gcc -I$PANGO_INC -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/include/cairo -c pango_test.c
if [ $? -ne 0 ]; then
  echo "PANGO_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
gcc -o pango_test pango_test.o -lglib-2.0 -lgobject-2.0 -lpango-1.0 -lm -lpangocairo-1.0 -lcairo
if [ $? -ne 0 ]; then
  echo "PANGO_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
rm pango_test.o
#
./pango_test text.png
if [ $? -ne 0 ]; then
  echo "PANGO_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
rm pango_test
#
echo "PANGO_NEWRIVER_HASWELL: Normal end of execution."
exit 0
