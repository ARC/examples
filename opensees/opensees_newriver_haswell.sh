#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.10.2
module load opensees/2.5.0
#
echo "OPENSEES_NEWRIVER_HASWELL: Normal beginning of execution."
#
OpenSeesSP truss.tcl > opensees_newriver_opensees.txt
if [ $? -ne 0 ]; then
  echo "OPENSEES_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "OPENSEES_NEWRIVER_HASWELL: Normal end of execution."
exit 0
