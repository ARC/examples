#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.10.2
module load opensees/2.4.5
#
echo "OPENSEES_DRAGONSTOOTH: Normal beginning of execution."
#
OpenSeesSP truss.tcl > opensees_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "OPENSEES_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "OPENSEES_DRAGONSTOOTH: Normal end of execution."
exit 0
