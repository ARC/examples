#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.8.4
module load opensees/2.4.5
#
echo "OPENSEES_BLUERIDGE: Normal beginning of execution."
#
OpenSeesSP truss.tcl > opensees_blueridge.txt
if [ $? -ne 0 ]; then
  echo "OPENSEES_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "OPENSEES_BLUERIDGE: Normal end of execution."
exit 0
