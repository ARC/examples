#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.4.0
module load openmpi/3.0.0
module load opensees/2.5.0
#
echo "OPENSEES_CASCADES_SKYLAKE: Normal beginning of execution."
#
OpenSeesSP truss.tcl > opensees_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "OPENSEES_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "OPENSEES_CASCADES_SKYLAKE: Normal end of execution."
exit 0
