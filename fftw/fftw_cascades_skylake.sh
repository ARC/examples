#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/2.1.3
module load fftw/3.3.6
#
echo "FFTW_CASCADES_SKYLAKE: Normal beginning of execution."
#
gcc -c -I$FFTW3_INC fftw_test.c
if [ $? -ne 0 ]; then
  echo "FFTW_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
gcc -o fftw_test fftw_test.o -L$FFTW3_LIB -lfftw3
if [ $? -ne 0 ]; then
  echo "FFTW_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm fftw_test.o
#
./fftw_test > fftw_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "FFTW_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm fftw_test
#
echo "FFTW_CASCADES_SKYLAKE: Normal end of execution."
exit 0
