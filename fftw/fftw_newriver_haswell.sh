#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.8.5
module load fftw/3.3.4
#
echo "FFTW_NEWRIVER_HASWELL: Normal beginning of execution."
#
gcc -c -I$FFTW3_INC fftw_test.c
if [ $? -ne 0 ]; then
  echo "FFTW_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
gcc -o fftw_test fftw_test.o -L$FFTW3_LIB -lfftw3
if [ $? -ne 0 ]; then
  echo "FFTW_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm fftw_test.o
#
./fftw_test > fftw_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "FFTW_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm fftw_test
#
echo "FFTW_NEWRIVER_HASWELL: Normal end of execution."
exit 0
