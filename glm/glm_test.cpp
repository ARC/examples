# include <iostream>
# include <glm/glm.hpp>

glm::vec3 computeNormal ( glm::vec3 const & a, glm::vec3 const & b,
  glm::vec3 const & c );

//****************************************************************************80

int main ( )

//****************************************************************************80
//
//  Purpose:
//
//    MAIN is the main program for GLM_TEST.
//
//  Modified:
//
//    09 May 2017
//
//  Author:
//
//    John Burkardt
//
{
  std::cout << "\n";
  std::cout << "GLM_TEST\n";
  std::cout << "  GLM is the OpenGL Mathematics Library.\n";
  std::cout << "\n";
  std::cout << "  This simple test demonstrates the computation of\n";
  std::cout << "  the unit normal vector the plane defined by\n";
  std::cout << "  three points A, B and C.\n";

  glm::vec3 a = glm::vec3 (  0.0,  0.0,  0.0 );
  glm::vec3 b = glm::vec3 (  4.0,  9.0,  2.0 );
  glm::vec3 c = glm::vec3 (  3.0, -3.0,  1.0 );
  glm::vec3 d;

  std::cout << "\n";
  std::cout << "  Point A: (" << a[0] << ","  << a[1] << "," << a[2] << ")\n";
  std::cout << "  Point B: (" << b[0] << ","  << b[1] << "," << b[2] << ")\n";
  std::cout << "  Point C: (" << c[0] << ","  << c[1] << "," << c[2] << ")\n";

  d = computeNormal ( a, b, c );

  std::cout << "\n";
  std::cout << "  Unit normal to plane ABC: (" 
    << d[0] << ","  << d[1] << "," << d[2] << ")\n";
//
//  Terminate.
//
  std::cout << "\n";
  std::cout << "GLM_TEST:\n";
  std::cout << "  Normal end of execution.\n";

  return 0;
}
//****************************************************************************80

glm::vec3 computeNormal ( glm::vec3 const & a, glm::vec3 const & b,
  glm::vec3 const & c )

//****************************************************************************80
//
//  Purpose:
//
//    COMPUTENORMAL computes the unit normal to the plane of A, B, C.
//
//  Modified:
//
//    09 May 2017
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, GLM::VEC3 A, B, C, three points.
//
//    Output, GLM::VEC3 COMPUTE_NORMAL, the unit normal vector to the plane.
//
{
  return glm::normalize ( glm::cross ( c - a, b - a ) );
}
