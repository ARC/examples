#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load glm/0.9.4.3
#
echo "GLM_NEWRIVER_HASWELL: Normal beginning of execution."
#
g++ -c -I$GLM_DIR glm_test.cpp
if [ $? -ne 0 ]; then
  echo "GLM_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
g++ -o glm_test glm_test.o
if [ $? -ne 0 ]; then
  echo "GLM_NEWRIVER_HASWELL: Load error."
  exit 1
fi
rm glm_test.o
#
./glm_test > glm_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "GLM_NEWRIVER_HASWELL: Run error."
  exit 1
fi
rm glm_test
#
echo "GLM_NEWRIVER_HASWELL: Normal end of execution."
exit 0
