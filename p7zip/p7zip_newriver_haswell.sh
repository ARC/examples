#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load p7zip/15.09
#
echo "P7ZIP_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Make a copy of a file.
#
cp robinson_crusoe.txt robinson_crusoe2.txt
#
#  Compress the copy.
#
7za a robinson_crusoe.7z robinson_crusoe2.txt
if [ $? -ne 0 ]; then
  echo "P7ZIP_NEWRIVER_HASWELL: Run error."
  exit 1
fi
#
#  Discard the copy.
#
rm robinson_crusoe2.txt
#
#  Decompress the file.
#
7za x robinson_crusoe.7z robinson_crusoe2.txt
if [ $? -ne 0 ]; then
  echo "P7ZIP_NEWRIVER_HASWELL: Run error."
  exit 1
fi
#
#  Do a file listing to compare sizes.
#
ls -l rob* > p7zip_newriver.txt
#
#  Clean up
#
rm robinson_crusoe2.txt
rm robinson_crusoe.7z
#
echo "P7ZIP_NEWRIVER_HASWELL: Normal end of execution."
exit 0
