#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load scons/2.3.4
#
echo "SCONS_NEWRIVER_HASWELL: Normal beginning of execution."
#
echo "Initial directory listing:"
ls -l
#
scons
if [ $? -ne 0 ]; then
  echo "SCONS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "Directory listing after scons command:"
ls -l
#
#  Clean up.
#
scons -c
if [ $? -ne 0 ]; then
  echo "SCONS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "Directory listing after scons -clean command:"
ls -l
#
echo "SCONS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
