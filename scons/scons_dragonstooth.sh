#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load scons/2.3.4
#
echo "SCONS_DRAGONSTOOTH: Normal beginning of execution."
#
echo "Initial directory listing:"
ls -l
#
scons
if [ $? -ne 0 ]; then
  echo "SCONS_DRAGONSTOOTH: Run error! scons"
  exit 1
fi
#
echo "Directory listing after scons command:"
ls -l
#
#  Clean up.
#
scons -c
if [ $? -ne 0 ]; then
  echo "SCONS_DRAGONSTOOTH: Run error!  scons -c"
  exit 1
fi
#
echo "Directory listing after scons -clean command:"
ls -l
#
echo "SCONS_DRAGONSTOOTH: Normal end of execution."
exit 0
