# include <stdlib.h>
# include <stdio.h>

# include "asa005.h"

int main ( );
void asa005_test01 ( );

/******************************************************************************/

int main ( )

/******************************************************************************/
/*
  Purpose:

    MAIN is the main program for ASA005_PRB.

  Discussion:

    ASA005_PRB tests the ASA005 library.

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    23 October 2010

  Author:

    John Burkardt
*/
{
  timestamp ( );
  printf ( "\n" );
  printf ( "ASA005_TEST:\n" );
  printf ( "  C version\n" );
  printf ( "  Test the ASA005 library.\n" );

  asa005_test01 ( );
/*
  Terminate.
*/
  printf ( "\n" );
  printf ( "ASA005_TEST:\n" );
  printf ( "  Normal end of execution.\n" );
  printf ( "\n" );
  timestamp ( );

  return 0;
}
