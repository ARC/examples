#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load python/2.7.10
module load boost/1.58.0
module load bowtie2/2.2.5
module load tophat/2.1
#
echo "TOPHAT_NEWRIVER_HASWELL: Normal beginning of execution."
#
tophat -r 20 test_ref reads_1.fq reads_2.fq > tophat_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "TOPHAT_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "TOPHAT_NEWRIVER_HASWELL: Normal end of execution."
exit 0
