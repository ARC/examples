#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load mvapich2/1.9a2
module load python/2.7.10
module load boost/1_49_0
module load bowtie2/2.1.0
module load tophat/2.1
#
echo "TOPHAT_BLUERIDGE: Normal beginning of execution."
#
tophat -r 20 test_ref reads_1.fq reads_2.fq > tophat_blueridge.txt
if [ $? -ne 0 ]; then
  echo "TOPHAT_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "TOPHAT_BLUERIDGE: Normal end of execution."
exit 0
