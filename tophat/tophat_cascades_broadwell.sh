#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openblas/0.2.20
module load python/2.7.10
module load boost/1.58.0
module load bowtie2/2.2.9
module load tophat/2.1
#
echo "TOPHAT_CASCADES_BROADWELL: Normal beginning of execution."
#
tophat -r 20 test_ref reads_1.fq reads_2.fq > tophat_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "TOPHAT_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "TOPHAT_CASCADES_BROADWELL: Normal end of execution."
exit 0
