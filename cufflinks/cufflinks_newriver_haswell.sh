#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load python/2.7.10
module load boost/1.58.0
module load samtools/1.2
module load cufflinks/2.2.1
#
echo "CUFFLINKS_NEWRIVER_HASWELL: Normal beginning of execution."
#
cufflinks sim_reads_aligned_sorted.sam &> cufflinks_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "CUFFLINKS_NEWRIVERHASWELL: Run error!"
  exit 1
fi
#
echo "CUFFLINKS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
