#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load python/2.7.10
module load openmpi/1.6.5
module load boost/1_49_0
module load samtools/1.2
module load cufflinks/2.2.1
#
echo "CUFFLINKS_BLUERIDGE: Normal beginning of execution."
#
cufflinks sim_reads_aligned_sorted.sam &> cufflinks_blueridge.txt
if [ $? -ne 0 ]; then
  echo "CUFFLINKS_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "CUFFLINKS_BLUERIDGE: Normal end of execution."
exit 0
