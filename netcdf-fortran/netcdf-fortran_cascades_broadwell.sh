#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load hdf5/1.8.15
module load netcdf-c/4.4.0
module load netcdf-fortran/4.4.3
module load szip/2.1
#
echo "NETCDF-FORTRAN_CASCADES_BROADWELL: Normal beginning of execution."
#
gfortran -c -I$NETCDF_F_INC netcdf-fortran_test.f90
if [ $? -ne 0 ]; then
  echo "NETCDF-FORTRAN_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
gfortran -o netcdf-fortran_test netcdf-fortran_test.o -L$NETCDF_F_LIB -L$NETCDF_C_LIB -lnetcdff -lnetcdf
if [ $? -ne 0 ]; then
  echo "NETCDF-FORTRAN_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm netcdf-fortran_test.o
#
./netcdf-fortran_test > netcdf-fortran_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "NETCDF-FORTRAN_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm netcdf-fortran_test
#
echo "NETCDF-FORTRAN_CASCADES_BROADWELL: Normal end of execution."
exit 0
