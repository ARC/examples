program main

!*****************************************************************************80
!
!! MAIN is the main program for NETCDF-FORTRAN_TEST.
!
  use netcdf
  implicit none

  integer ( kind = 4 ), allocatable :: array(:,:)
  integer ( kind = 4 ), allocatable :: array2(:,:)
  character ( len = 80 ) filename
  integer ( kind = 4 ) nx
  integer ( kind = 4 ) ny

  filename = 'netcdf-fortran_test.nc'

  write ( *, '(a)' ) ''
  write ( *, '(a)' ) 'NETCDF-FORTRAN_TEST:'
  write ( *, '(a)' ) '  Test FORTRAN90 interface to NETCDF.'
!
!  Create the data.
!
  nx = 6
  ny = 12
  allocate ( array(1:nx,1:ny) )
  call create_data ( nx, ny, array )
!
!  Write the data to a file.
!
  call write_data ( filename, nx, ny, array )
!
!  Read the data from the file.
!
  allocate ( array2(1:nx,1:ny) )
  call read_data ( filename, nx, ny, array2 )
!
!  Compare the two versions of the data.
!
  call compare_data ( nx, ny, array, array2 )
!
!  Free memory.
!
  deallocate ( array )
  deallocate ( array2 )
!
!  Terminate.
!
  write ( *, '(a)' ) ''
  write ( *, '(a)' ) 'NETCDF-FORTRAN_TEST:'
  write ( *, '(a)' ) '  Normal end of execution.'

  stop
end
subroutine create_data ( nx, ny, array )

!*****************************************************************************80
!
!! CREATE_DATA sets up the data array.
!
  implicit none

  integer ( kind = 4 ) nx
  integer ( kind = 4 ) ny

  integer ( kind = 4 ) array(nx,ny)
  integer ( kind = 4 ) i
  integer ( kind = 4 ) j
  integer ( kind = 4 ) k

  k = 0
  do j = 1, ny
    do i = 1, nx
      k = k + 1
      array(i,j) = k
    end do
  end do

  return
end
subroutine write_data ( filename, nx, ny, array )

!*****************************************************************************80
!
!! WRITE_DATA writes the array values to a NETCDF file.
!
  use netcdf
  implicit none

  integer ( kind = 4 ) nx
  integer ( kind = 4 ) ny

  integer ( kind = 4 ) array(nx,ny)
  integer ( kind = 4 ) dimids(2)
  character ( len = * ) filename
  integer ( kind = 4 ) ncid
  integer ( kind = 4 ) status
  integer ( kind = 4 ) varid
  integer ( kind = 4 ) x_dimid
  integer ( kind = 4 ) y_dimid
!
!  Create the netCDF file. 
!
  status = nf90_create ( filename, NF90_CLOBBER, ncid )
!
!  Define the name and value of each dimensions, and receive an ID for each.
!
  status = nf90_def_dim ( ncid, "x", NX, x_dimid ) 
  status = nf90_def_dim ( ncid, "y", NY, y_dimid ) 
!
!  Define the name, type and dimension of ARRAY, and get an ID.
!
  dimids = (/ x_dimid, y_dimid /)

  status = nf90_def_var ( ncid, "array", NF90_INT, dimids, varid )
!
!  Terminate the "define" mode.
!
  status = nf90_enddef ( ncid )
!
!  Write the data for ARRAY.
!
  status = nf90_put_var ( ncid, varid, array )
!
!  Close the file.
!
  status = nf90_close ( ncid )

  return
end
subroutine read_data ( filename, nx, ny, array2 )

!*****************************************************************************80
!
!! READ_DATA reads the array values from the NETCDF file.
!
  use netcdf
  implicit none

  integer ( kind = 4 ) nx
  integer ( kind = 4 ) ny

  integer ( kind = 4 ) array2(nx,ny)
  character ( len = * ) filename
  integer ( kind = 4 ) ncid
  integer ( kind = 4 ) status
  integer ( kind = 4 ) varid
!
!  Open the file and get an ID for it.
!
  status = nf90_open ( filename, NF90_NOWRITE, ncid )
!
!  Use the name of ARRAY to get an ID for it.
!
  status = nf90_inq_varid ( ncid, "array", varid )
!
!  Read the data.
!
  status = nf90_get_var ( ncid, varid, array2 )
!
!  Close the file.
!
  status = nf90_close ( ncid )

  return
end
subroutine compare_data ( nx, ny, array, array2 )

!*****************************************************************************80
!
!! COMPARE_DATA compares the two versions of the data array.
!
  implicit none

  integer ( kind = 4 ) nx
  integer ( kind = 4 ) ny

  integer ( kind = 4 ) array(nx,ny)
  integer ( kind = 4 ) array2(nx,ny)
  integer ( kind = 4 ) i
  integer ( kind = 4 ) j
  integer ( kind = 4 ) k

  k = 0
  do j = 1, ny
    do i = 1, nx
      if ( array(i,j) /= array2(i,j) ) then
        k = k + 1
      end if
    end do
  end do

  write ( *, '(a)' ) ''
  write ( *, '(a,i6)' ) '  COMPARE_DATA discrepancies = ', k

  return
end
