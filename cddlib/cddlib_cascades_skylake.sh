#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load cddlib/0.94h
#
echo "CDDLIB_CASCADES_SKYLAKE: Normal beginning of execution."
#
gcc -c -I$CDD_INC cddlib_test.c
if [ $? -ne 0 ]; then
  echo "CDDLIB_CASCADES_SKYLAKE: Compile error!"
  exit
fi
#
gcc -o cddlib_test cddlib_test.o -L$CDD_LIB -lcdd
if [ $? -ne 0 ]; then
  echo "CDDLIB_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm cddlib_test.o
#
./cddlib_test < cddlib_input.txt > cddlib_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "CDDLIB_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm cddlib_test
#
echo "CDDLIB_CASCADES_SKYLAKE: Normal end of execution."
exit 0
