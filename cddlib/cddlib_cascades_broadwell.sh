#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load cddlib/0.94h
#
echo "CDDLIB_CASCADES_BROADWELL: Normal beginning of execution."
#
gcc -c -I$CDD_INC cddlib_test.c
if [ $? -ne 0 ]; then
  echo "CDDLIB_CASCADES_BROADWELL: Compile error!"
  exit
fi
#
gcc -o cddlib_test cddlib_test.o -L$CDD_LIB -lcdd
if [ $? -ne 0 ]; then
  echo "CDDLIB_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm cddlib_test.o
#
./cddlib_test < cddlib_input.txt > cddlib_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "CDDLIB_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm cddlib_test
#
echo "CDDLIB_CASCADES_BROADWELL: Normal end of execution."
exit 0
