#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load metis/5.1.0
#
echo "METIS_DRAGONSTOOTH: Normal beginning of execution."
#
gpmetis 4elt.graph 4 > gpmetis_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "METIS_DRAGONSTOOTH: Run error."
  exit 1
fi
#
echo "METIS_DRAGONSTOOTH: Normal end of execution."
exit 0
