#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load metis/5.0.2
#
echo "METIS_BLUERIDGE: Normal beginning of execution."
#
$METIS_BIN/gpmetis 4elt.graph 4 > gpmetis_blueridge.txt
if [ $? -ne 0 ]; then
  echo "METIS_BLUERIDGE: Run error."
  exit 1
fi
#
echo "METIS_BLUERIDGE: Normal end of execution."
exit 0
