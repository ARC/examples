#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load metis/5.1.0
#
echo "METIS_CASCADES_SKYLAKE: Normal beginning of execution."
#
gpmetis 4elt.graph 4 > metis_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "METIS_CASCADES_SKYLAKE: Run error."
  exit 1
fi
#
echo "METIS_CASCADES_SKYLAKE: Normal end of execution."
exit 0
