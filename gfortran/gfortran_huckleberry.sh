#! /bin/bash
#
#SBATCH -J gcc_huckleberry
#SBATCH -p normal_q
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 00:05:00
#SBATCH --mem=100M
#
cd $SLURM_SUBMIT_DIR
#
module purge
module load gcc/6.1.0
#
echo "GFORTRAN_HUCKLEBERY: Normal beginning of execution."
#
gfortran -c heated_plate.f90
if [ $? -ne 0 ]; then
  echo "GFORTRAN_HUCKLEBERRY: Compile error!"
  exit 1
fi
#
gfortran -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "GFORTRAN_HUCKLEBERRY: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > gfortran_huckleberry.txt
if [ $? -ne 0 ]; then
  echo "GFORTRAN_HUCKLEBERRY: Run error!"
  exit 1
fi
rm heated_plate
#
echo "GFORTRAN_HUCKLEBERRY: Normal end of execution."
exit 0

