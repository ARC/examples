#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
#
echo "GFORTRAN_DRAGONSTOOTH: Normal beginning of execution."
#
gfortran -c heated_plate.f90
if [ $? -ne 0 ]; then
  echo "GFORTRAN_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
gfortran -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "GFORTRAN_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > gfortran_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "GFORTRAN_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm heated_plate
#
echo "GFORTRAN_DRAGONSTOOTH: Normal end of execution."
exit 0

