#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load flint/2.5.2
#
echo "FLINT_CASCADES_BROADWELL: Normal beginning of execution."
#
gcc -c -I$FLINT_INC primegen.c
if [ $? -ne 0 ]; then
  echo "FLINT_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
gcc -o primegen primegen.o -L$FLINT_LIB -lflint
if [ $? -ne 0 ]; then
  echo "FLINT_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm primegen.o
#
./primegen -c 1000000 > flint_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "FLINT_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm primegen
#
echo "FLINT_CASCADES_BROADWELL: Normal end of execution."
exit 0
