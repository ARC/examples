#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load flint/2.5.2
#
echo "FLINT_NEWRIVER_HASWELL: Normal beginning of execution."
#
gcc -c -I$FLINT_INC primegen.c
if [ $? -ne 0 ]; then
  echo "FLINT_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
gcc -o primegen primegen.o -L$FLINT_LIB -lflint
if [ $? -ne 0 ]; then
  echo "FLINT_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm primegen.o
#
./primegen -c 1000000 > flint_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "FLINT_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm primegen
#
echo "FLINT_NEWRIVER_HASWELL: Normal end of execution."
exit 0
