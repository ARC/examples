#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q largemem_q
#
cd $PBS_O_WORKDIR
#
module purge
module load jdk/1.7.0
module load trimmomatic/0.33
#
echo "TRIMMOMATIC_NEWRIVER_IVYBRIDGE: Normal beginning of execution."
#
java -jar $TRIMMOMATIC_BIN/trimmomatic-0.33.jar SE test.fastq trimmed.fastq LEADING:3 TRAILING:3 MINLEN:36 &> trimmomatic_newriver_ivybridge.txt
if [ $? -ne 0 ]; then
  echo "TRIMMOMATIC_NEWRIVER_IVYBRIDGE: Run error!"
  exit 1
fi
#
echo "TRIMMOMATIC_NEWRIVER_IVYBRIDGE: Normal end of execution."
exit 0
