#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load jdk/1.8.0
module load trimmomatic/0.33
#
echo "TRIMMOMATIC_CASCADES_BROADWELL: Normal beginning of execution."
#
java -jar $TRIMMOMATIC_BIN/trimmomatic-0.33.jar SE test.fastq trimmed.fastq LEADING:3 TRAILING:3 MINLEN:36 &> trimmomatic_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "TRIMMOMATIC_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "TRIMMOMATIC_CASCADES_BROADWELL: Normal end of execution."
exit 0
