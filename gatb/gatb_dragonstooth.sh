#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load gatb/1.0.6
#
echo "GATB_DRAGONSTOOTH: Normal beginning of execution."
#
g++ -c -I$GATB_INC debruijn5.cpp
if [ $? -ne 0 ]; then
  echo "GATB_DRAGONSTOOTH: Compile error."
  exit 1
fi
#
g++ -o debruijn5 debruijn5.o -L$GATB_LIB -lgatbcore -lhdf5 -lhdf5_tools -lz -ldl -lpthread
if [ $? -ne 0 ]; then
  echo "GATB_DRAGONSTOOTH: Load error."
  exit 1
fi
rm debruijn5.o
#
./debruijn5 debruijn5.h5
if [ $? -ne 0 ]; then
  echo "GATB_DRAGONSTOOTH: Run error."
  exit 1
fi
rm debruijn5
#
echo "GATB_DRAGONSTOOTH: Normal end of execution."
exit 0
