#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load minia/2.0.7
#
echo "MINIA_CASCADES_SKYLAKE: Normal beginning of execution."
#
minia -in input.fa -kmer-size 31 -abundance-min 3 -out results &> minia_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "MINIA_CASCADES_SKYLAKE: Compile error."
  exit 1
fi
#
echo "MINIA_CASCADES_SKYLAKE: Normal end of execution."
exit 0
