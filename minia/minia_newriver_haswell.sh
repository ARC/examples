#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load minia/2.0.7
#
echo "MINIA_NEWRIVER_HASWELL: Normal beginning of execution."
#
minia -in input.fa -kmer-size 31 -abundance-min 3 -out results &> minia_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "MINIA_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
echo "MINIA_NEWRIVER_HASWELL: Normal end of execution."
exit 0
