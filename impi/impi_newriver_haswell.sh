#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load impi/5.0
#
echo "IMPI_NEWRIVER_HASWELL: Normal beginning of execution."
#
mpiicc -c prime.c
if [ $? -ne 0 ]; then
  echo "IMPI_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
mpiicc -o prime prime.o
if [ $? -ne 0 ]; then
  echo "IMPI_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm prime.o
#
mpirun -np 8s ./prime > impi_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "IMPI_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm prime
#
echo "IMPI_NEWRIVER_HASWELL: Normal end of execution."
exit 0
