#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load openmpi/2.1.3
module load parallel-netcdf/1.8.1
#
echo "PARALLEL-NETCDF_CASCADES_SKYLAKE: Normal beginning of execution."
#
mpicc -c -I$PNETCDF_INC writer.c
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_SKYLAKE: Compile error."
  exit 1
fi
#
mpicc -o writer  writer.o -L$PNETCDF_LIB -lpnetcdf
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_SKYLAKE: Load error."
  exit 1
fi
rm writer.o
#
mpirun -np 4 writer testfile.nc
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_SKYLAKE: Run error."
  exit 1
fi
rm writer
#
mpicc -c -I$PNETCDF_INC reader.c
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_SKYLAKE: Compile error."
  exit 1
fi
#
mpicc -o reader reader.o -L$PNETCDF_LIB -lpnetcdf
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_SKYLAKE: Load error."
  exit 1
fi
rm reader.o
#
mpirun -np 4 reader testfile.nc
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_SKYLAKE: Run error."
  exit 1
fi
rm reader
#
echo "PARALLEL-NETCDF_CASCADES_SKYLAKE: Normal end of execution."
exit 0
