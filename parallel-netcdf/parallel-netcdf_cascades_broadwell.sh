#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/3.0.0
module load parallel-netcdf/1.8.1
#
echo "PARALLEL-NETCDF_CASCADES_BROADWELL: Normal beginning of execution."
#
mpicc -c -I$PNETCDF_INC writer.c
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_BROADWELL: Compile error."
  exit 1
fi
#
mpicc -o writer  writer.o -L$PNETCDF_LIB -lpnetcdf
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_BROADWELL: Load error."
  exit 1
fi
rm writer.o
#
mpirun -np 4 writer testfile.nc
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_BROADWELL: Run error."
  exit 1
fi
rm writer
#
mpicc -c -I$PNETCDF_INC reader.c
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_BROADWELL: Compile error."
  exit 1
fi
#
mpicc -o reader reader.o -L$PNETCDF_LIB -lpnetcdf
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_BROADWELL: Load error."
  exit 1
fi
rm reader.o
#
mpirun -np 4 reader testfile.nc
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_CASCADES_BROADWELL: Run error."
  exit 1
fi
rm reader
#
echo "PARALLEL-NETCDF_CASCADES_BROADWELL: Normal end of execution."
exit 0
