#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.6.5
module load parallel-netcdf/1.3.1
#
echo "PARALLEL-NETCDF_BLUERIDGE: Normal beginning of execution."
#
mpicc -c -I$PNETCDF_INC writer.c
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_BLUERIDGE: Compile error."
  exit 1
fi
#
mpicc -o writer  writer.o -L$PNETCDF_LIB -lpnetcdf
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_BLUERIDGE: Load error."
  exit 1
fi
rm writer.o
#
mpirun -np 4 writer testfile.nc
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_BLUERIDGE: Run error."
  exit 1
fi
rm writer
#
mpicc -c -I$PNETCDF_INC reader.c
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_BLUERIDGE: Compile error."
  exit 1
fi
#
mpicc -o reader reader.o -L$PNETCDF_LIB -lpnetcdf
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_BLUERIDGE: Load error."
  exit 1
fi
rm reader.o
#
mpirun -np 4 reader testfile.nc
if [ $? -ne 0 ]; then
  echo "PARALLEL-NETCDF_BLUERIDGE: Run error."
  exit 1
fi
rm reader
#
echo "PARALLEL-NETCDF_BLUERIDGE: Normal end of execution."
exit 0
