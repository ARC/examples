#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load guile/2.0.11
#
echo "GUILE_BLUERIDGE: Normal beginning of execution."
#
export GUILE_AUTO_COMPILE=0
#
chmod u+x hello
./hello
if [ $? -ne 0 ]; then
  echo "GUILE_BLUERIDGE: Run error!"
  exit 1
fi
#
chmod u+x fact
./fact 50
if [ $? -ne 0 ]; then
  echo "GUILE_BLUERIDGE: Run error!"
  exit 1
fi
#
chmod u+x choose
./choose 50 100
if [ $? -ne 0 ]; then
  echo "GUILE_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "GUILE_BLUERIDGE: Normal end of execution."
exit 0
