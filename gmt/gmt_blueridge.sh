#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load hdf5/1.8.8
module load netcdf/4.2
module load gmt/5.0.0
#
echo "GMT_BLUERIDGE: Normal beginning of execution."
#
gmt pscoast -R-130/-70/24/52 -JB-100/35/33/45/6i -Ba -B+t"Conic Projection" -N1/thickest -N2/thinnest -A500 -Ggray -Wthinnest -P > GMT_tut_4.ps
if [ $? -ne 0 ]; then
  echo "GMT_BLUERIDGE: Run error!"
  exit 1
fi
#
gmt pscoast -Rg -JG280/30/6i -Bag -Dc -A5000 -Gwhite -SDarkTurquoise -P > GMT_tut_5.ps
if [ $? -ne 0 ]; then
  echo "GMT_BLUERIDGE: Run error!"
  exit 1
fi
#
ls *.ps
#
echo "GMT_BLUERIDGE: Normal end of execution."
exit 0
