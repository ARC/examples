#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load hdf5/1.8.15
module load netcdf-c/4.4.0
module load szip/2.1
#
echo "NETCDF-C_CASCADES_BROADWELL: Normal beginning of execution."
#
gcc -c -I$NETCDF_C_INC netcdf-c_test.c
if [ $? -ne 0 ]; then
  echo "NETCDF-C_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
gcc -o netcdf-c_test netcdf-c_test.o -L$NETCDF_C_LIB -lnetcdf
if [ $? -ne 0 ]; then
  echo "NETCDF-C_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm netcdf-c_test.o
#
./netcdf-c_test > netcdf-c_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "NETCDF-C_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm netcdf-c_test
#
echo "NETCDF-C_CASCADES_BROADWELL: Normal end of execution."
exit 0
