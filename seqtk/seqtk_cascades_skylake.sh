#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load seqtk/1.2
#
echo "SEQTK_CASCADES_SKYLAKE: Normal beginning of execution."
#
#  Convert FASTQ to FASTA:
#
seqtk seq -a e_coli_1000.fq > e_coli_1000.fa
if [ $? -ne 0 ]; then
  echo "SEQTK_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "SEQTK_CASCADES_SKYLAKE: Normal end of execution."
exit 0
