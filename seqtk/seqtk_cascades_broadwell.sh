#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load seqtk/1.2
#
echo "SEQTK_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Convert FASTQ to FASTA:
#
seqtk seq -a e_coli_1000.fq > e_coli_1000.fa
if [ $? -ne 0 ]; then
  echo "SEQTK_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "SEQTK_CASCADES_BROADWELL: Normal end of execution."
exit 0
