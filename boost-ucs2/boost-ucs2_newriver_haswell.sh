#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load python-ucs2/2.7.9
module load boost-ucs2/1.58.0
#
echo "BOOST-UCS2_NEWRIVER_HASWELL: Normal beginning of execution."
#
g++ -c -I $BOOST_INC histogram.cpp
if [ $? -ne 0 ]; then
  echo "BOOST-UCS2_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
g++ -o histogram histogram.o -L$BOOST_LIB
if [ $? -ne 0 ]; then
  echo "BOOST-UCS2_NEWRIVER_HASWELL: Load error."
  exit 1
fi
rm histogram.o
#
./histogram > boost-ucs2_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "BOOST-UCS2_NEWRIVER_HASWELL: Run error."
  exit 1
fi
rm histogram
#
echo "BOOST-UCS2_NEWRIVER_HASWELL: Normal end of execution."
exit 0
