#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load parallel/20150922
#
echo "PARALLEL_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Display the list of files.
#
echo ""
echo "Initial list of files:"
echo ""
ls
#
#  Find, in the current directory, by name, all the .EPS files.
#
#  In parallel, convert each file (full name {}} to a file
#  with the same prefix, but with the ".png" suffix.
#
find . -name "*.eps" | parallel convert {} {.}.png
if [ $? -ne 0 ]; then
  echo "PARALLEL_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
#  Redisplay the list of files.
#
echo ""
echo "Final list of files:"
echo ""
ls
#
echo "PARALLEL_NEWRIVER_HASWELL: Normal end of execution."
exit 0
