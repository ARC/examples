#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load parallel/20141122
#
echo "PARALLEL_BLUERIDGE: Normal beginning of execution."
#
#  For the "convert" command, we need to load ImageMagick.
#
module load ImageMagick/6.8.9
#
#  Display the list of files.
#
echo ""
echo "Initial list of files:"
echo ""
ls
#
#  Find, in the current directory, by name, all the .EPS files.
#
#  In parallel, convert each file (full name {}} to a file
#  with the same prefix, but with the ".png" suffix.
#
find . -name "*.eps" | parallel convert {} {.}.png
if [ $? -ne 0 ]; then
  echo "PARALLEL_BLUERIDGE: Run error!"
  exit 1
fi
#
#  Redisplay the list of files.
#
echo ""
echo "Final list of files:"
echo ""
ls
#
echo "PARALLEL_BLUERIDGE: Normal end of execution."
exit 0
