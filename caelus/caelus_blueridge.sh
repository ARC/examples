#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.6.5
module load Caelus/4.10
#
echo "CAELUS_BLUERIDGE: Normal beginning of execution."
#
. $CAELUS_DIR/Caelus-4.10/etc/caelus-bashrc
#
renumberMesh -overwrite
if [ $? -ne 0 ]; then
  echo "CAELUS_BLUERIDGE: Run error!"
  exit 1
fi
#
checkMesh
if [ $? -ne 0 ]; then
  echo "CAELUS_BLUERIDGE: Run error!"
  exit 1
fi
#
simpleSolver
if [ $? -ne 0 ]; then
  echo "CAELUS_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "CAELUS_BLUERIDGE: Normal end of execution."
exit 0
