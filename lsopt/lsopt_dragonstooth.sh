#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load ls-dyna/8.0.0
module load lsopt/5.1.1
#
echo "LSOPT_DRAGONSTOOTH: Normal beginning of execution."
#
lsopt metal_MC.lsopt &> lsopt_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "LSOPT_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "LSOPT_DRAGONSTOOTH: Normal end of execution."
exit 0
