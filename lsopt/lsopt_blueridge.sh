m#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load ls-dyna/7.0.0
module load lsopt/5.1
#
echo "LSOPT_BLUERIDGE: Normal beginning of execution."
#
lsopt metal_MC.lsopt &> lsopt_blueridge.txt
if [ $? -ne 0 ]; then
  echo "LSOPT_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "LSOPT_BLUERIDGE: Normal end of execution."
exit 0
