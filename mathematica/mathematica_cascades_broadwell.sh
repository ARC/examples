#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load mathematica/11.0.0
#
echo "MATHEMATICA_CASCADES_BROADWELL: Normal beginning of execution."
#
wolfram -script awl.wl > mathematica_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "MATHEMATICA_BROADWELL_CASCADES: Run error!"
  exit 1
fi
#
echo "MATHEMATICA_CASCADES_BROADWELL: Normal end of execution."
exit 0
