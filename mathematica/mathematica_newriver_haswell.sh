#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load mathematica/11.0.0
#
echo "MATHEMATICA_NEWRIVER_HASWELL: Normal beginning of execution."
#
wolfram -script awl.wl > mathematica_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "MATHEMATICA_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "MATHEMATICA_NEWRIVER_HASWELL: Normal end of execution."
exit 0
