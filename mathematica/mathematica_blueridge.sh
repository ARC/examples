#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load mathematica/11.3.0
#
echo "MATHEMATICA_BLUERIDGE: Normal beginning of execution."
#
wolfram -script awl_blueridge.wl > mathematica_blueridge.txt
if [ $? -ne 0 ]; then
  echo "MATHEMATICA_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "MATHEMATICA_BLUERIDGE: Normal end of execution."
exit 0
