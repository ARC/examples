#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q largemem_q
#PBS -A arctest
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load mathematica/11.1.1
#
echo "MATHEMATICA_NEWRIVER_IVYBRIDGE: Normal beginning of execution."
#
wolfram -script awl.wl > mathematica_ivybridge.txt
if [ $? -ne 0 ]; then
  echo "MATHEMATICA_NEWRIVER_IVYBRIDGE: Run error!"
  exit 1
fi
#
echo "MATHEMATICA_NEWRIVER_IVYBRIDGE: Normal end of execution."
exit 0

