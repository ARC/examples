#! /usr/bin/env WolframScript
#
(* Generate high-precision samples of a mixed distribution. *)

Print [""]
Print ["AWL:"]
Print ["  Demonstrate the use of wolframscript"]
Print ["  to execute Mathematica commands."]

Print [""]
Print /@ RandomVariate[MixtureDistribution
  [
    { 1, 2 },
    {
      NormalDistribution[ 1, 2/10 ],
      NormalDistribution[ 3, 1/10 ]
    }
  ],
  10, 
  WorkingPrecision -> 50
]

Print [""]
Print ["AWL:"]
Print ["  Normal end of execution."]

