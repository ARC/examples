# include <stdlib.h>
# include <stdio.h>
# include <math.h>

# include "cv.h"
# include "highgui.h"

int main ( int argc, char *argv[] )
{
  int channels;
  uchar *data;
  int height;
  int i;
  IplImage* img = 0; 
  int iscolor;
  int j;
  int k;
  int step;
  int width;

  printf ( "\n" );
  printf ( "IMAGE_INVERT:\n" );
  printf ( "  Use OpenCV to invert the colors in an image.\n" );

  if ( argc < 2 )
  {
    printf("Usage: main <image-file-name>\n\7");
    exit(0);
  }
//
//  Load the image.
//
  iscolor = 1;
  img = cvLoadImage ( argv[1], iscolor );

  if ( ! img )
  {
    printf ( "Could not load image file: %s\n", argv[1] );
    exit ( 0 );
  }
//
//  Get the image data.
//
  height    = img->height;
  width     = img->width;
  step      = img->widthStep;
  channels  = img->nChannels;
  data      = ( uchar * ) img->imageData;

  printf ( "Processing a %dx%d image with %d channels\n", 
    height, width, channels ); 
//
//  Invert the color values in the image.
//
  for ( i = 0; i < height; i++ ) 
  {
    for ( j = 0; j < width; j++ ) 
    {
      for ( k = 0; k < channels; k++ )
      {
        data[i*step+j*channels+k] = 255 - data[i*step+j*channels+k];
      }
    }
  }
//
//  Write the image to a PNG file.
//
  cvSaveImage ( "inverted.png", img, NULL );
//
//  Release the image.
//
  cvReleaseImage ( &img );
//
//  Terminate.
//
  printf ( "\n" );
  printf ( "IMAGE_INVERT:\n" );
  printf ( "  Normal end of execution.\n" );

  return 0;
}
