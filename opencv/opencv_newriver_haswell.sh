#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load yasm/1.3
module load x264/1.0
module load fdk-aac/1.0
module load lame/3.99.5
module load ffmpeg/2.5.4
module load opencv/2.4.11
#
echo "OPENCV_NEWRIVER_HASWELL: Normal beginning of execution."
#
gcc -c -I$OPENCV_INC/opencv image_invert.c
if [ $? -ne 0 ]; then
  echo "OPENCV_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
gcc image_invert.o -o image_invert -L$OPENCV_LIB -lopencv_photo -lopencv_imgproc -lopencv_core -lopencv_highgui
if [ $? -ne 0 ]; then
  echo "OPENCV_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm image_invert.o
#
./image_invert barcelona.jpg
if [ $? -ne 0 ]; then
  echo "OPENCV_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
rm image_invert
#
echo "OPENCV_NEWRIVER_HASWELL: Normal end of execution."
exit 0
