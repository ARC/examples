#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load nastran/2014.1
#
echo "NASTRAN_CASCADES_SKYLAKE: Normal beginning of execution."
#
nast20141 tower.bdf scr=yes
if [ $? -ne 0 ]; then
  echo "NASTRAN_CASCADES_SKYLAKE: Run error."
  exit 1
fi
#
echo "NASTRAN_CASCADES_SKYLAKE: Normal end of execution."
exit 0
