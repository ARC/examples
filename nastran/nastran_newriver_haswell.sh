#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load nastran/2014.1
#
echo "NASTRAN_NEWRIVER_HASWELL: Normal beginning of execution."
#
nast20141 tower.bdf scr=yes
if [ $? -ne 0 ]; then
  echo "NASTRAN_NEWRIVER_HASWELL: Run error."
  exit 1
fi
#
echo "NASTRAN_NEWRIVER_HASWELL: Normal end of execution."
exit 0
