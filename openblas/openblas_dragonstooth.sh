#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openblas/0.2.14
#
echo "OPENBLAS_DRAGONSTOOTH: Normal beginning of execution."
#
gcc -c -I$OPENBLAS_DIR/include openblas_test_c.c
if [ $? -ne 0 ]; then
  echo "OPENBLAS_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
gcc -o openblas_test_c openblas_test_c.o -L$OPENBLAS_LIB -lopenblas
if [ $? -ne 0 ]; then
  echo "OPENBLAS_DRAGONSTOOTH: Load error!"
  exit 1
fi
#
./openblas_test_c > openblas_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "OPENBLAS_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
rm openblas_test_c.o
rm openblas_test_c
#
echo "OPENBLAS_DRAGONSTOOTH: Normal end of execution."
exit 0
