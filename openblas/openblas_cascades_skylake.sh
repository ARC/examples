#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load openblas/0.2.20
#
echo "OPENBLAS_CASCADES_SKYLAKE: Normal beginning of execution."
#
gcc -c -I$OPENBLAS_DIR/include openblas_test_c.c
if [ $? -ne 0 ]; then
  echo "OPENBLAS_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
gcc -o openblas_test_c openblas_test_c.o -L$OPENBLAS_LIB -lopenblas
if [ $? -ne 0 ]; then
  echo "OPENBLAS_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
#
./openblas_test_c > openblas_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "OPENBLAS_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
rm openblas_test_c.o
rm openblas_test_c
#
echo "OPENBLAS_CASCADES_SKYLAKE: Normal end of execution."
exit 0
