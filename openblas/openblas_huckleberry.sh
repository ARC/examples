#! /bin/bash
#
#SBATCH -J openblas_huckleberry
#SBATCH -p normal_q
#SBATCH -N 1
#SBATCH -t 00:05:00
#SBATCH --mem=100M
#
cd $SLURM_SUBMIT_DIR
#
module purge
module load gcc
module load openblas
#
echo "OPENBLAS_HUCKLEBERRY: Normal beginning of execution."
#
gcc -c -I$OPENBLAS_DIR/include openblas_test_c.c
if [ $? -ne 0 ]; then
  echo "OPENBLAS_HUCKLEBERRY: Compile error!"
  exit 1
fi
#
gcc -o openblas_test_c openblas_test_c.o -L$OPENBLAS_LIB -lopenblas
if [ $? -ne 0 ]; then
  echo "OPENBLAS_HUCKLEBERRY: Load error!"
  exit 1
fi
#
./openblas_test_c > openblas_huckleberry.txt
if [ $? -ne 0 ]; then
  echo "OPENBLAS_HUCKLEBERRY: Run error!"
  exit 1
fi
#
rm openblas_test_c.o
rm openblas_test_c
#
echo "OPENBLAS_HUCKLEBERRY: Normal end of execution."
exit 0
