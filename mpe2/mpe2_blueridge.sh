#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load mvapich2/1.9a2
module load jdk/1.7.0
module load mpe2/2.4.9b
#
echo "MPE2_BLUERIDGE: Normal beginning of execution."
#
mpecc -c example.c -mpilog
if [ $? -ne 0 ]; then
  echo "MPE2_BLUERIDGE: Compile error."
  exit 1
fi
#
mpecc -o example example.o -lm -mpilog -lpthread
if [ $? -ne 0 ]; then
  echo "MPE2_BLUERIDGE: Load error."
  exit 1
fi
#
mpirun -np 4 ./example > mpe2_blueridge.txt
if [ $? -ne 0 ]; then
  echo "MPE2_BLUERIDGE: Run error."
  exit 1
fi
rm example
#
echo "MPE2_BLUERIDGE: Normal end of execution."
exit 0
