#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load openmpi/2.1.3
module load jdk/1.8.0
module load mpe2/2.4.9b
#
echo "MPE2_CASCADES_SKYLAKE: Normal beginning of execution."
#
mpecc -c example.c -mpilog
if [ $? -ne 0 ]; then
  echo "MPE2_CASCADES_SKYLAKE: Compile error."
  exit 1
fi
#
mpecc -o example example.o -lm -mpilog -lpthread
if [ $? -ne 0 ]; then
  echo "MPE2_CASCADES_SKYLAKE: Load error."
  exit 1
fi
#
mpirun -np 4 ./example > mpe2_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "MPE2_CASCADES_SKYLAKE: Run error."
  exit 1
fi
rm example
#
echo "MPE2_CASCADES_SKYLAKE: Normal end of execution."
exit 0
