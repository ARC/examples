#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load mvapich2/2.1
module load jdk/1.8.0
module load mpe2/2.4.9b
#
echo "MPE2_NEWRIVER_HASWELL: Normal beginning of execution."
#
mpecc -c example.c -mpilog
if [ $? -ne 0 ]; then
  echo "MPE2_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
mpecc -o example example.o -lm -mpilog -lpthread
if [ $? -ne 0 ]; then
  echo "MPE2_NEWRIVER_HASWELL: Load error."
  exit 1
fi
#
mpirun -np 4 ./example > mpe2_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "MPE2_NEWRIVER_HASWELL: Run error."
  exit 1
fi
rm example
#
echo "MPE2_NEWRIVER_HASWELL: Normal end of execution."
exit 0
