#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/2.0.0
module load jdk/1.8.0
module load mpe2/2.4.9b
#
echo "MPE2_CASCADES_BROADWELL: Normal beginning of execution."
#
mpecc -c example.c -mpilog
if [ $? -ne 0 ]; then
  echo "MPE2_CASCADES_BROADWELL: Compile error."
  exit 1
fi
#
mpecc -o example example.o -lm -mpilog -lpthread
if [ $? -ne 0 ]; then
  echo "MPE2_CASCADES_BROADWELL: Load error."
  exit 1
fi
#
mpirun -np 4 ./example > mpe2_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "MPE2_CASCADES_BROADWELL: Run error."
  exit 1
fi
rm example
#
echo "MPE2_CASCADES_BROADWELL: Normal end of execution."
exit 0
