#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.6.5
module load phdf5/1.8.8
module load parallel-netcdf/1.3.1
module load netcdf-par/4.2
#
echo "NETCDF-PAR_BLUERIDGE: Normal beginning of execution."
#
mpicc -c writer.c -I$PNETCDF_INC -I$NETCDF_INC
if [ $? -ne 0 ]; then
  echo "NETCDF-PAR_BLUERIDGE: Compile error."
  exit 1
fi
#
mpicc -o writer writer.o -L$NETCDF_LIB -L$PNETCDF_LIB -L$PHDF5_LIB -lnetcdf -lpnetcdf -lhdf5_hl -lhdf5 -lz
if [ $? -ne 0 ]; then
  echo "NETCDF-PAR_BLUERIDGE: Load error."
  exit 1
fi
rm writer.o
#
mpirun -np 4 ./writer writer.nc
if [ $? -ne 0 ]; then
  echo "NETCDF-PAR_BLUERIDGE: Run error."
  exit 1
fi
rm writer
#
echo "NETCDF-PAR_BLUERIDGE: Normal end of execution."
exit 0
