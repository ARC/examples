#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load bwa/0.7.12
#
echo "BWA_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Index the reference genome.
#
bwa index dmel-all-chromosome-r5.37.fasta
if [ $? -ne 0 ]; then
  echo "BWA_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
#  Align the reads against the reference.
#
bwa aln -I -t 8 dmel-all-chromosome-r5.37.fasta read.fasta > out.sai
if [ $? -ne 0 ]; then
  echo "BWA_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
#  Convert the alignment into a SAM file.
#
bwa samse dmel-all-chromosome-r5.37.fasta out.sai read.fasta > out.sam
if [ $? -ne 0 ]; then
  echo "BWA_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "BWA_CASCADES_BROADWELL: Normal end of execution."
exit 0
