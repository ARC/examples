#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load libgtextutils/0.7
#
echo "LIBGTEXTUTILS_DRAGONSTOOTH: Normal beginning of execution."
#
g++ -c test_container_join.cpp -I $LIBGTEXTUTILS_DIR/include/gtextutils
if [ $? -ne 0 ]; then
  echo "LIBGTEXTUTILS_DRAGONSTOOTH: Compile error."
  exit 1
fi
#
g++ -o test_container_join test_container_join.o -L$LIBGTEXTUTILS_DIR/lib -lgtextutils -lglib-2.0 -lstdc++
if [ $? -ne 0 ]; then
  echo "LIBGTEXTUTILS_DRAGONSTOOTH: Load error."
  exit 1
fi
rm test_container_join.o
#
echo $LD_LIBRARY_PATH
LD_LIBRARY_PATH=$LIBGTEXTUTILS_DIR/lib:$LD_LIBRARY_PATH
./test_container_join > libgtextutils_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "LIBGTEXTUTILS_DRAGONSTOOTH: Run error."
  exit 1
fi
rm test_container_join
#
echo "LIBGTEXTUTILS_DRAGONSTOOTH: Normal end of execution."
exit 0
