#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q largemem_q
#
cd $PBS_O_WORKDIR

module purge
module load automake/1.15
#
echo "AUTOMAKE_NEWRIVER_IVYBRIDGE: Normal beginning of execution."
#
ls
ls src
#
autoreconf --install
if [ $? -ne 0 ]; then
  echo "AUTOMAKE_NEWRIVER_IVYBRIDGE: AUTORECONF error!"
  exit 1
fi
#
ls
ls src
#
./configure
if [ $? -ne 0 ]; then
  echo "AUTOMAKE_NEWRIVER_IVYBRIDGE: CONFIGURE error!"
  exit 1
fi
#
ls
ls src
#
make
if [ $? -ne 0 ]; then
  echo "AUTOMAKE_NEWRIVER_IVYBRIDGE: MAKE error!"
  exit 1
fi
#
ls
ls src
#
#  Clean up.
#
rm Makefile
rm Makefile.in
rm aclocal.m4
rm -r autom4te.cache
rm compile
rm config.h
rm config.h.in
rm config.log
rm config.status
rm configure
rm depcomp
rm install-sh
rm missing
rm stamp-h1
rm src/Makefile
rm src/Makefile.in
rm src/hello
rm src/main.o
#
echo "AUTOMAKE_NEWRIVER_IVYBRIDGE: Normal end of execution."
exit 0
