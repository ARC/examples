#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/3.0.0
module load mkl/11.2.3
module load abinit/8.0.8
#
echo "ABINIT_CASCADES_SKYLAKE: Normal beginning of execution."
#
abinit < tbase1_x.files > abinit_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "ABINIT_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "ABINIT_CASCADES_SKYLAKE: Normal end of execution."
exit 0
