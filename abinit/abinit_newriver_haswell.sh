#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load mvapich2/2.1
module load mkl/11.2.3
module load abinit/8.0.8
#
echo "ABINIT_NEWRIVER_HASWELL: Normal beginning of execution."
#
abinit < tbase1_x.files > abinit_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "ABINIT_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "ABINIT_NEWRIVER_HASWELL: Normal end of execution."
exit 0
