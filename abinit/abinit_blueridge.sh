#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load impi/5.0
module load mkl/11.2.3
module load abinit/8.0.8
#
echo "ABINIT_BLUERIDGE: Normal beginning of execution."
#
abinit < tbase1_x.files > abinit_blueridge.txt
if [ $? -ne 0 ]; then
  echo "ABINIT_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "ABINIT_BLUERIDGE: Normal end of execution."
exit 0
