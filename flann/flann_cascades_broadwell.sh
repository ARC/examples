#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/3.0.0
module load flann/1.8.4
module load hdf5/1.8.15
#
echo "FLANN_CASCADES_BROADWELL: Normal beginning of execution."
#
g++ -c -I$FLANN_INC flann_test.cpp
if [ $? -ne 0 ]; then
  echo "FLANN_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
g++ -o flann_test flann_test.o -L$FLANN_LIB -L$HDF5 -lflann -lhdf5
if [ $? -ne 0 ]; then
  echo "FLANN_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm flann_test.o
#
./flann_test > flann_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "FLANN_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm flann_test
#
echo "FLANN_CASCADES_BROADWELL: Normal end of execution."
exit 0
