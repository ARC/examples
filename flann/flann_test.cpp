# include <iostream>

# include <flann/flann.hpp> 
# include <flann/io/hdf5.h>

using namespace std;

int main ( int argc, char** argv ) 
{
  int nn = 3;
  flann::Matrix<float> dataset;
  flann::Matrix<float> query; 

  cout << "\n";
  cout << "FLANN_TEST:\n";
  cout << "  C++ version\n";
  cout << "  Test the Fast Library for Approximate Nearest Neighbors.\n";

  flann::load_from_file ( dataset, "dataset.hdf5", "dataset" ); 
  flann::load_from_file ( query, "dataset.hdf5", "query" );

  flann::Matrix<int> indices ( new int[query.rows*nn], query.rows, nn ); 
  flann::Matrix<float> dists ( new float[query.rows*nn], query.rows, nn );
//
// Construct a randomized KD-tree index using 4 KD-trees.
//
  flann::Index<flann::L2<float> > index ( dataset, flann::KDTreeIndexParams(4) ); 

  index.buildIndex();
//
//  Do a knn search, using 128 checks
//
  index.knnSearch ( query, indices, dists, nn, flann::SearchParams(128) );

  flann::save_to_file ( indices, "result.hdf5", "result" );
//
//  Free memory.
//
  delete [] dataset.ptr(); 
  delete [] dists.ptr();
  delete [] indices.ptr(); 
  delete [] query.ptr();
//
//  Terminate.
//
  cout << "\n";
  cout << "FLANN_TEST:\n";
  cout << "  Normal end of execution.\n";

  return 0; 
}
