#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/7.3.0
module load openmpi/3.0.0
module load flann/1.8.4
module load hdf5/1.8.17
#
echo "FLANN_CASCADES_SKYLAKE: Normal beginning of execution."
#
g++ -c -I$FLANN_INC flann_test.cpp
if [ $? -ne 0 ]; then
  echo "FLANN_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
g++ -o flann_test flann_test.o -L$FLANN_LIB -L$HDF5 -lflann -lhdf5
if [ $? -ne 0 ]; then
  echo "FLANN_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm flann_test.o
#
./flann_test > flann_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "FLANN_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm flann_test
#
echo "FLANN_CASCADES_SKYLAKE: Normal end of execution."
exit 0
