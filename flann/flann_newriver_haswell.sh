#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.8.5
module load flann/1.8.4
module load hdf5/1.8.15
#
echo "FLANN_NEWRIVER_HASWELL: Normal beginning of execution."
#
g++ -c -I$FLANN_INC flann_test.cpp
if [ $? -ne 0 ]; then
  echo "FLANN_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
g++ -o flann_test flann_test.o -L$FLANN_LIB -L$HDF5 -lflann -lhdf5
if [ $? -ne 0 ]; then
  echo "FLANN_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm flann_test.o
#
./flann_test > flann_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "FLANN_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm flann_test
#
echo "FLANN_NEWRIVER_HASWELL: Normal end of execution."
exit 0
