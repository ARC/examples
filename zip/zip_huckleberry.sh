#! /bin/bash
#
##SBATCH -J zip_huckleberry
#SBATCH -p normal_q
#SBATCH -N 1
#SBATCH -t 00:05:00
#SBATCH --mem=100M
#
cd $SLURM_SUBMIT_DIR
#
module purge
#
echo "ZIP_HUCKLEBERRY: Normal beginning of execution."
#
#  Make a copy.
#
cp robinson_crusoe.txt robinson_crusoe2.txt
#
#  Make a compressed copy.
#
zip robinson_crusoe.zip robinson_crusoe2.txt
if [ $? -ne 0 ]; then
  echo "ZIP_HUCKLEBERRY: Run error."
  exit 1
fi
#
#  Delete the copy.
#
rm robinson_crusoe2.txt
#
#  Decompress the ZIP file.
#
unzip robinson_crusoe.zip
if [ $? -ne 0 ]; then
  echo "ZIP_HUCKLEBERRY: Run error."
  exit 1
fi
#
#  Do a file listing to compare sizes.
#
ls -l rob* > zip_huckleberry.txt
#
#  Clean up
#
rm robinson_crusoe2.txt
rm robinson_crusoe.zip
#
echo "ZIP_HUCKLEBERRY: Normal end of execution."
exit 0
