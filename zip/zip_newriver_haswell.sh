#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
#
echo "ZIP_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Make a copy.
#
cp robinson_crusoe.txt robinson_crusoe2.txt
#
#  Make a compressed copy.
#
zip robinson_crusoe.zip robinson_crusoe2.txt
if [ $? -ne 0 ]; then
  echo "ZIP_NEWRIVER_HASWELL: Run error."
  exit 1
fi
#
#  Delete the copy.
#
rm robinson_crusoe2.txt
#
#  Decompress the ZIP file.
#
unzip robinson_crusoe.zip
if [ $? -ne 0 ]; then
  echo "ZIP_NEWRIVER_HASWELL: Run error."
  exit 1
fi
#
#  Do a file listing to compare sizes.
#
ls -l rob* > zip_newriver_haswell.txt
#
#  Clean up
#
rm robinson_crusoe2.txt
rm robinson_crusoe.zip
#
echo "ZIP_NEWRIVER_HASWELL: Normal end of execution."
exit 0
