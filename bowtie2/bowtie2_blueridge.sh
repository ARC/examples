#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load bowtie2/2.1.0
#
echo "BOWTIE2_BLUERIDGE: Normal beginning of execution."
#
bowtie2 -x e_coli -U sim_reads.fq -S sim_reads_aligned.sam > bowtie2_blueridge.txt
if [ $? -ne 0 ]; then
  echo "BOWTIE2_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "BOWTIE2_BLUERIDGE: Normal end of execution."
exit 0
