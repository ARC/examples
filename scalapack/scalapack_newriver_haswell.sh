#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load impi/5.0
module load mkl/11.2.3
module load scalapack/2.0.2
#
echo "SCALAPACK_NEWRIVER_HASWELL: Normal beginning of execution."
#
mpiifort -c scalapack_test.f90
if [ $? -ne 0 ]; then
  echo "SCALAPACK_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
mpiifort -o scalapack_test -mkl scalapack_test.o -L$MKL_LIB -lmkl_blacs_intelmpi_lp64 -lmkl_scalapack_lp64
if [ $? -ne 0 ]; then
  echo "SCALAPACK_NEWRIVER_HASWELL: Load error."
  exit 1
fi
rm scalapack_test.o
#
mpirun -np 4 ./scalapack_test > scalapack_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "SCALAPACK_NEWRIVER_HASWELL: Run error."
  exit 1
fi
rm scalapack_test
#
echo "SCALAPACK_NEWRIVER_HASWELL: Normal end of execution."
exit 0
