program main

!*****************************************************************************80
!
!! MAIN is the main program for SCALAPACK_TEST.
!
!  Discussion:
!
!    SCALAPACK_TEST tests the SCALAPACK library.
!
!    The 10x10 linear system is
!
!      1  1  1  1  1 |  1  1  1  1  1   x   1
!      1  2  2  2  2 |  2  2  2  2  2   x   0
!      1  2  3  3  3 |  3  3  3  3  3   x   0
!      1  2  3  4  4 |  4  4  4  4  4   x   0
!      1  2  3  4  5 |  5  5  5  5  5   x   0
!      --------------+--------------- * - = -
!      1  2  3  4  5 |  6  6  6  6  6   x   0
!      1  2  3  4  5 |  6  7  7  7  7   x   0
!      1  2  3  4  5 |  6  7  8  8  8   x   0
!      1  2  3  4  5 |  6  7  8  9  9   x   0
!      1  2  3  4  5 |  6  7  8  9 10   x   0
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license. 
!
!  Modified:
!
!    10 June 2017
!
!  Author:
!
!    John Burkardt
!
  implicit none

  integer ( kind = 4 ), parameter :: dlen = 9

  real ( kind = 8 ) a(5,5)
  real ( kind = 8 ) a_norm
  real ( kind = 8 ) a_save(5,5)
  real ( kind = 8 ) b(5)
  real ( kind = 8 ) b_norm
  real ( kind = 8 ) b_save(5)
  integer ( kind = 4 ) context
  integer ( kind = 4 ) desca(dlen)
  integer ( kind = 4 ) descb(dlen)
  real ( kind = 8 ) eps
  integer ( kind = 4 ) i
  integer ( kind = 4 ) i_global
  integer ( kind = 4 ) i_local
  integer ( kind = 4 ) ihi
  integer ( kind = 4 ) ilo
  integer ( kind = 4 ) info
  integer ( kind = 4 ) ipiv(5)
  integer ( kind = 4 ) j
  integer ( kind = 4 ) j_global
  integer ( kind = 4 ) j_local
  integer ( kind = 4 ) jhi
  integer ( kind = 4 ) jlo
  integer ( kind = 4 ) lld_a
  integer ( kind = 4 ) lld_b
  integer ( kind = 4 ) m
  integer ( kind = 4 ) m_a
  integer ( kind = 4 ) m_b
  integer ( kind = 4 ) mycol
  integer ( kind = 4 ) myproc
  integer ( kind = 4 ) myrow
  integer ( kind = 4 ) n
  integer ( kind = 4 ) n_a
  integer ( kind = 4 ) n_b
  integer ( kind = 4 ) npcol
  integer ( kind = 4 ) nprocs
  integer ( kind = 4 ) nprow
  integer ( kind = 4 ) nrhs
  real ( kind = 8 ) pdlamch
  real ( kind = 8 ) pdlange
  real ( kind = 8 ) resid
  real ( kind = 8 ) work(5)
  real ( kind = 8 ) x_norm

  m = 10
  n = 10
  nrhs = 1
!
!  Get a pointer CONTEXT, to the (NPROW,NPCOL) process grid.
!
  nprow = 2
  npcol = 2
  call sl_init ( context, nprow, npcol )
!
!  Assign each process in CONTEXT's process grid a place: (MYROW,MYCOL). 
!  Any unneeded process will get coordinates of -1.
!
  call blacs_gridinfo ( context, nprow, npcol, myrow, mycol )

  if ( myrow == -1 ) then
    write ( *, '(a,i4,a)' ) '  Process ', myproc, '  is not needed, so shutting down.'
    call blacs_exit ( 0 )
    stop
  end if

  if ( myrow == 0 .and. mycol == 0 ) then
    write ( *, '(a)' ) ''
    write ( *, '(a)' ) 'SCALAPACK_TEST:'
    write ( *, '(a)' ) '  FORTRAN90 version.'
    write ( *, '(a)' ) '  SCALAPACK sets up and solves a distributed linear'
    write ( *, '(a)' ) '  algebra problem in a parallel computing environment.'
  end if
!
!  Set the array descriptor for matrix A.
!
  m_a = m
  n_a = n
  lld_a = 5

  call descinit ( desca, m_a, n_a, 5, 5, 0, 0, context, lld_a, info )
!
!  Set the array descriptor for vector B.
!
  m_b = m
  n_b = nrhs
  lld_b = 5

  call descinit ( descb, m_b, n_b, 5, 1, 0, 0, context, lld_b, info )
!
!  Define the 5x5 chunk of A stored on process (MYROW,MYCOL).
!
  do i_local = 1, 5
    i_global = i_local + myrow * 5
    do j_local = 1, 5
      j_global = j_local + mycol * 5
      a(i_local,j_local) = real ( min ( i_global, j_global ), kind = 8 )
    end do
  end do
!
!  Define the 5x1 chunk of B stored on process (MYROW,0).
!
  if ( mycol == 0 ) then
    do i_local = 1, 5
      i_global = i_local + myrow * 5
      if ( i_global == 1 ) then
        b(i_local) = 1.0D+00
      else
        b(i_local) = 0.0D+00
      end if
    end do
  end if
!
!  Keep copies of A and B so we can check the result.
!  The original A and B get mangled in the computation.
!
  call pdlacpy ( 'all', n, n, a, 1, 1, desca, a_save, 1, 1, desca )

  call pdlacpy ( 'all', n, nrhs, b, 1, 1, descb, b_save, 1, 1, descb )
!
!  Call PDGESV to solve the linear system A*X=B.
!
  call pdgesv ( n, nrhs, a, 1, 1, desca, ipiv, b, 1, 1, descb, info )

  if ( myrow == 0 .and. mycol == 0 ) then
    if ( info == 0 ) then
      write ( *, '(a)' ) '  PDGESV completed the computation successfully.'
    else
      write ( *, '(a,i6)' ) '  PDGESV failed, returning INFO = ', info
    end if
  end if
!
!  Compute normalized residual ||A * X  - B|| / ( ||X|| * ||A|| * eps * N )
!
  eps = pdlamch ( context, 'epsilon' )
  a_norm = pdlange ( 'i', n, n, a, 1, 1, desca, work )
  b_norm = pdlange ( 'i', n, nrhs, b, 1, 1, descb, work )
  call pdgemm ( 'n', 'n', n, nrhs, n, 1.0D+00, a_save, 1, 1, desca, b, 1, 1, &
    descb, - 1.0D+00, b_save, 1, 1, descb )
  x_norm = pdlange ( 'i', n, nrhs, b_save, 1, 1, descb, work )
  resid = x_norm / ( a_norm * b_norm * eps * real ( n, kind = 8 ) )

  if ( myrow == 0 .and. mycol == 0 ) then
    write ( *, '(a,g14.6)' ) '  ||A*x - b|| / ( ||x||*||A||*eps*N ) = ', resid
  end if
! 
!  Free the BLACS context.
!
  call blacs_gridexit ( context )
!
!  Break the connection of this process to the BLACS.
!
  call blacs_exit ( 0 )
!
!  Terminate.
!
  if ( myrow == 0 .and. mycol == 0 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'SCALAPACK_TEST:'
    write ( *, '(a)' ) '  Normal end of execution.'
  end if

  stop
end
