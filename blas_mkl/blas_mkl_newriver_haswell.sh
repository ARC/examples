#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load mkl/11.2.3
#
echo "BLAS_MKL_NEWRIVER_HASWELL: Normal beginning of execution."
#
ifort -c -mkl blas_test.f90
if [ $? -ne 0 ]; then
  echo "BLAS_MKL_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
ifort -mkl -o blas_mkl_test blas_test.o
if [ $? -ne 0 ]; then
  echo "BLAS_MKL_NEWRIVER_HASWELL: Load error."
  exit 1
fi
rm blas_test.o
#
./blas_mkl_test > blas_mkl_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "BLAS_MKL_NEWRIVER_HASWELL: Run error."
  exit 1
fi
rm blas_mkl_test
#
echo "BLAS_MKL_NEWRIVER_HASWELL: Normal end of execution."
exit 0
