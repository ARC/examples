#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load mkl/11.2.3
#
echo "BLAS_MKL_CASCADES_BROADWELL: Normal beginning of execution."
#
ifort -c -mkl blas_test.f90
if [ $? -ne 0 ]; then
  echo "BLAS_MKL_CASCADES_BROADWELL: Compile error."
  exit 1
fi
#
ifort -mkl -o blas_mkl_test blas_test.o
if [ $? -ne 0 ]; then
  echo "BLAS_MKL_CASCADES_BROADWELL: Load error."
  exit 1
fi
rm blas_test.o
#
./blas_mkl_test > blas_mkl_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "BLAS_MKL_CASCADES_BROADWELL: Run error."
  exit 1
fi
rm blas_mkl_test
#
echo "BLAS_MKL_CASCADES_BROADWELL: Normal end of execution."
exit 0
