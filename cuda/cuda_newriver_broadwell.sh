#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1:gpus=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load cuda/8.0.61
#
echo "CUDA_NEWRIVER_BROADWELL: Normal beginning of execution."
#
nvcc -o laplace laplace.cu
if [ $? -ne 0 ]; then
  echo "CUDA_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
#
./laplace > cuda_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "CUDA_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
rm laplace
#
echo "CUDA_NEWRIVER_BROADWELL: Normal end of execution."
exit 0

