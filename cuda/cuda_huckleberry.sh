#! /bin/bash
#
#SBATCH -J openblas_huckleberry
#SBATCH -p normal_q
#SBATCH -N 1
#SBATCH -t 00:05:00
#SBATCH --mem=100M
#SBATCH --gres=gpu:pascal:1
#
cd $SLURM_SUBMIT_DIR
#
#
module purge
module load cuda/8.0.54
#
echo "CUDA_HUCKLEBERRY: Normal beginning of execution."
#
nvcc -o laplace laplace.cu
if [ $? -ne 0 ]; then
  echo "CUDA_HUCKLEBERRY: Run error!"
  exit 1
fi
#
./laplace > cuda_huckleberry.txt
if [ $? -ne 0 ]; then
  echo "CUDA_HUCKLEBERRY: Run error!"
  exit 1
fi
rm laplace
#
echo "CUDA_HUCKLEBERRY: Normal end of execution."
exit 0

