#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1:gpus=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load cuda/9.1.85
#
echo "CUDA_CASCADES_SKYLAKE: Normal beginning of execution."
#
nvcc -o laplace laplace.cu
if [ $? -ne 0 ]; then
  echo "CUDA_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
./laplace > cuda_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "CUDA_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm laplace
#
echo "CUDA_CASCADES_SKYLAKE: Normal end of execution."
exit 0

