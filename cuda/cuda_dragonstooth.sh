#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load cuda/8.0.44
#
echo "CUDA_DRAGONSTOOTH: Normal beginning of execution."
#
#  Note that you can compile and run CUDA code on DragonsTooth,
#  but since there are no GPUs installed, you will not get the results you want!
#
nvcc -o laplace laplace.cu
if [ $? -ne 0 ]; then
  echo "CUDA_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
./laplace > cuda_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "CUDA_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm laplace
#
echo "CUDA_DRAGONSTOOTH: Normal end of execution."
exit 0

