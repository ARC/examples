#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.10.2
module load atlas/3.11.36
module load glm/0.9.4.3
module load lua/5.1.4.8
module load python/2.7.10
module load boost/1.58.0
module load phdf5/1.8.16
module load p4est/1.1
module load trilinos/11.12.1
module load dealii/8.4.1
module load cmake/3.7.2
#
echo "DEALII_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Clean up files from possible previous run.
#
rm -f CMakeCache.txt
rm -rf CMakeFiles
rm -f cmake_install.cmake
rm -f Makefile
rm -f step-6
rm -f *.eps
#
cmake -DDEAL_II_DIR=$DEAL_II_DIR .
if [ $? -ne 0 ]; then
  echo "DEALII_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
make
if [ $? -ne 0 ]; then
  echo "DEALII_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
./step-6 > dealii_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "DEALII_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "DEALII_NEWRIVER_HASWELL: Normal end of execution."
exit 0
