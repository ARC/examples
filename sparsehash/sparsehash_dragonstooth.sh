#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load sparsehash/2.0.2
#
echo "SPARSEHASH_DRAGONSTOOTH: Normal beginning of execution."
#
g++ -c -I. -I$SPARSEHASH_INC simple_test.cpp
if [ $? -ne 0 ]; then
  echo "SPARSEHASH_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
g++ -o simple_test simple_test.o
if [ $? -ne 0 ]; then
  echo "SPARSEHASH_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm simple_test.o
#
./simple_test > simple_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "SPARSEHASH_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm simple_test
#
echo "SPARSEHASH_DRAGONSTOOTH: Normal end of execution."
exit 0
