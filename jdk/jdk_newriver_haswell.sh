#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load jdk/1.8.0
#
echo "JDK_NEWRIVER_HASWELL: Normal beginning of execution."
#
javac Inverse.java
if [ $? -ne 0 ]; then
  echo "JDK_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
java Inverse < input.txt > jdk_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "JDK_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm Inverse.class
#
echo "JDK_NEWRIVER_HASWELL: Normal end of execution."
exit 0
