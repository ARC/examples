#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load jdk/1.8.0
#
echo "JDK_BLUERIDGE: Normal beginning of execution."
#
javac Inverse.java
if [ $? -ne 0 ]; then
  echo "JDK_BLUERIDGE: Run error!"
  exit 1
fi
#
java Inverse < input.txt > jdk_blueridge.txt
if [ $? -ne 0 ]; then
  echo "JDK_BLUERIDGE: Run error!"
  exit 1
fi
rm Inverse.class
#
echo "JDK_BLUERIDGE: Normal end of execution."
exit 0
