#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load jdk/1.8.0
#
echo "JDK_CASCADES_SKYLAKE: Normal beginning of execution."
#
javac Inverse.java
if [ $? -ne 0 ]; then
  echo "JDK_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
java Inverse < input.txt > jdk_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "JDK_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm Inverse.class
#
echo "JDK_CASCADES_SKYLAKE: Normal end of execution."
exit 0
