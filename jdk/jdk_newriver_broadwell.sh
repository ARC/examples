#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
#
cd $PBS_O_WORKDIR
#
module purge
module load jdk/1.8.0
#
echo "JDK_NEWRIVER_BROADWELL: Normal beginning of execution."
#
javac Inverse.java
if [ $? -ne 0 ]; then
  echo "JDK_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
#
java Inverse < input.txt > jdk_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "JDK_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
rm Inverse.class
#
echo "JDK_NEWRIVER_BROADWELLL: Normal end of execution."
exit 0
