#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load dcw/1.1.1
#
echo "DCW_CASCADES_BROADWELL - Normal beginning of execution."
#
echo ""
echo "List DCW directory:"
echo ""
ls $DCW_DIR/dcw-gmt-1.1.1 
#
echo ""
echo "List size of dcw-gmt.nc file:"
echo ""
ls -l $DCW_DIR/dcw-gmt-1.1.1/dcw-gmt.nc
#
#  Load NETCDF
#
module load gcc/5.2.0
module load hdf5/1.8.16
module load netcdf/4.3.3.1
#
#  Use the NETCDF "ncdump -h" command to get a list of file contents.
#
echo ""
echo "Use ncdump -h to list contents of file."
echo ""
ncdump -h $DCW_DIR/dcw-gmt-1.1.1/dcw-gmt.nc
#
echo ""
echo "DCW_CASCADES_BROADWELL - Normal end of execution."
