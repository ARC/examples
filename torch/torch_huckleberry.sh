#! /bin/bash
#
#SBATCH -J gcc_huckleberry
#SBATCH -p normal_q
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 00:05:00
#SBATCH --mem=100M
#
cd $SLURM_SUBMIT_DIR
#
module purge
module load torch/7-3ibm2
#
echo "TORCH_HUCKLEBERRY: Normal beginning of execution."
#
th input.lua > torch_huckleberry.txt
if [ $? -ne 0 ]; then
  echo "TORCH_HUCKLEBERY: Run error!"
  exit 1
fi
#
echo "TORCH_HUCKLEBERRY: Normal end of execution."
exit 0
