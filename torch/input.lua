--
--  Set the random number seed.
--
torch.manualSeed ( 123456789 )
--
-- Choose a dimension.
--
N = 5
--
--  Fill the NxN matrix A with random values.
--
A = torch.rand ( N, N )
--
--  Now make A symmetric positive semidefinite.
--
A = A*A:t()
--
--  Add a multiple of the identity to make it symmetric positive definite.
--
A:add ( 0.05, torch.eye ( N ) )
--
--  Define a linear term b:
--
b = torch.rand ( N )
--
--  Now define a quadratic form J = 1/2 x'*A*x - b'x:
--
function J ( x )
  return 0.5 * x:dot ( A * x ) - b:dot ( x )
end
--
--  Evaluate J at a random x:
--
x = torch.rand ( N )
print ( J ( x ) )
--
--  Since A is invertible, x=inverse(A)*b will minimize J(x)
--
xs = torch.inverse ( A ) * b
print ( string.format ( '  Minimum J(x) = %g', J(xs) ) )
print ( 'Minimizing x:' )
print ( xs )
--
--  Define the gradient of J.
--
function dJ ( x )
  return A*x-b
end
--
--  Set a random initial guess for the minimizer.
--
x = torch.rand ( N )
--
--  Apply gradient descent with a learning rate LR.
--
lr = 0.02
for i = 1, 10000 do
  x = x - dJ ( x ) * lr
  if ( i % 1000 ) == 0 then
    print ( string.format ( '  J(x[%d]) = %f', i, J ( x ) ) )
    print ( x )
  end
end
print ( '' )
print ( 'Estimate for minimizing x:' )
print ( x )
