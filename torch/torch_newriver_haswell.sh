#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load cuda/8.0.44
module load torch/7_2016_12_04
#
echo "TORCH_NEWRIVER_HASWELL: Normal beginning of execution."
#
th input.lua > torch_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "TORCH_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "TORCH_NEWRIVER_HASWELL: Normal end of execution."
exit 0
