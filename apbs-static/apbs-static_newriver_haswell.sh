#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load apbs-static/1.4
#
echo "APBS-STATIC_NEWRIVER_HASWELL: Normal beginning of execution."
#
apbs input.txt > apbs-static_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "APBS-STATIC_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "APBS-STATIC_NEWRIVER_HASWELL: Normal end of execution."
exit 0
