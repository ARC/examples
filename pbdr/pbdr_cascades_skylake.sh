#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=16
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.6.5
module load R/3.0.3
module load hdf5/1.8.8
module load netcdf/4.2
module load pbdr/0.2.2
#
echo "PBDR_CASCADES_SKYLAKE: Normal beginning of execution."
#
mpirun -np $PBS_NP Rscript --vanilla mcpi.r > pbdr_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "PBDR_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "PBDR_CASCADES_SKYLAKE: Normal end of execution."
exit 0
