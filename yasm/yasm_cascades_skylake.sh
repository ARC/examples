#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load yasm/1.3
#
echo "YASM_CASCADES_SKYLAKE: Normal beginning of execution."
#
#  Assemble, load, and run HELLO.
#
yasm -f elf64 -l hello.lst hello.asm
if [ $? -ne 0 ]; then
  echo "YASM_CASCADES_SKYLAKE:: Compile error."
  exit 1
fi
#
gcc -o hello hello.o
if [ $? -ne 0 ]; then
  echo "YASM_CASCADES_SKYLAKE:: Load error."
  exit 1
fi
rm hello.o
#
./hello > yasm_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "YASM_CASCADES_SKYLAKE:: Run error."
  exit 1
fi
#
rm hello.lst
rm hello
#
#  Assemble, load, and run ARITHMETIC.
#
yasm -f elf64 arithmetic.asm
if [ $? -ne 0 ]; then
  echo "YASM_CASCADES_SKYLAKE:: Compile error."
  exit 1
fi
#
gcc -o arithmetic arithmetic.o
if [ $? -ne 0 ]; then
  echo "YASM_CASCADES_SKYLAKE:: Load error."
  exit 1
fi
rm arithmetic.o
#
./arithmetic >> yasm_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "YASM_CASCADES_SKYLAKE:: Run error."
  exit 1
fi
#
rm arithmetic
#
echo "YASM_CASCADES_SKYLAKE:: Normal end of execution."
exit 0
