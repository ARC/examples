#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load yasm/1.3
#
echo "YASM_NEWRIVER_BROADWELL: Normal beginning of execution."
#
#  Assemble, load, and run HELLO.
#
yasm -f elf64 -l hello.lst hello.asm
if [ $? -ne 0 ]; then
  echo "YASM_NEWRIVER_BROADWELL: Compile error."
  exit 1
fi
#
gcc -o hello hello.o
if [ $? -ne 0 ]; then
  echo "YASM_NEWRIVER_BROADWELL: Load error."
  exit 1
fi
rm hello.o
#
./hello > yasm_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "YASM_NEWRIVER_BROADWELL: Run error."
  exit 1
fi
#
rm hello.lst
rm hello
#
#  Assemble, load, and run ARITHMETIC.
#
yasm -f elf64 arithmetic.asm
if [ $? -ne 0 ]; then
  echo "YASM_NEWRIVER_BROADWELL: Compile error."
  exit 1
fi
#
gcc -o arithmetic arithmetic.o
if [ $? -ne 0 ]; then
  echo "YASM_NEWRIVER_BROADWELL: Load error."
  exit 1
fi
rm arithmetic.o
#
./arithmetic >> yasm_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "YASM_NEWRIVER_BROADWELL: Run error."
  exit 1
fi
#
rm arithmetic
#
echo "YASM_NEWRIVER_BROADWELL: Normal end of execution."
exit 0
