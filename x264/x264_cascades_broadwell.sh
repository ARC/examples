#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load yasm/1.3
module load x264/1.0
#
echo "X264_CASCADES_BROADWELL: Normal beginning of execution."
#
gcc -c -I$X264_INC x264_test.c
if [ $? -ne 0 ]; then
  echo "X264_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
gcc -o x264_test x264_test.o -L$X264_LIB -lx264 -lm -ldl -lpthread
if [ $? -ne 0 ]; then
  echo "X264_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm x264_test.o
#
./x264_test 176x144 input.yuv output.h264 > x264_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "X264_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm x264_test
#
echo "X264_CASCADES_BROADWELL: Normal end of execution."
exit 0
