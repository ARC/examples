#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load harfbuzz/1.4.6
#
echo "HARFBUZZ_NEWRIVER_HASWELL: Normal beginning of execution."
#
gcc -c -I. -I$HARFBUZZ_INC -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include test-font.c
if [ $? -ne 0 ]; then
  echo "HARFBUZZ_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
gcc -o test-font test-font.o -lglib-2.0 -L$HARFBUZZ_LIB -lharfbuzz
if [ $? -ne 0 ]; then
  echo "HARFBUZZ_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm test-font.o
#
./test-font > harfbuzz_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "HARFBUZZ_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm test-font
#
echo "HARFBUZZ_NEWRIVER_HASWELL: Normal end of execution."
exit 0
