%% LLOYD_RUN calls LLOYD with a specific set of parameters.
%
%  Modified:
%
%    03 April 2017
%
%  Local parameters:
%
%    Local, integer N, the number of generator points to compute.
%
%    Local, integer IT_NUM, the number of iterations to take.
%
%    Local, integer INIT, the initialization to use:
%    1, initial generator points are random in [0,1];
%    2, initial generator points are "squashed" into a subinterval.
%
n = 100;
it_num = 2000;
init = 2;

fprintf ( 1, '\n' );
fprintf ( 1, 'LLOYD_RUN\n' );
fprintf ( 1, '  OCTAVE version\n' );
fprintf ( 1, '  Apply Lloyd''s algorithm to N points in a 1D interval.\n' );
fprintf ( 1, '  Iterate, seeking Centroidal Voronoi Tessellation (CVT).\n' );
fprintf ( 1, '\n' );
fprintf ( 1, '  Number of generators is %d\n', n );
fprintf ( 1, '  Number of iterations is %d\n', it_num );
fprintf ( 1, '  Initialization option is %d\n', init );

lloyd ( n, it_num, init );
%
%  Terminate.
%
fprintf ( 1, '\n' );
fprintf ( 1, 'LLOYD_RUN\n' );
fprintf ( 1, '  Normal end of execution.\n' );

