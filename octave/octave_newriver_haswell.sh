#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openblas/0.2.14
module load hdf5/1.8.16
module load gnuplot/5.0.0
module load octave/4.0.1
#
echo "OCTAVE_NEWRIVER_HASWELL: Normal beginning of execution."
#
octave --no-gui < lloyd_run.m > octave_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "OCTAVE_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
#  Notice that three PNG files were created.
#
ls *.png
#
echo "OCTAVE_NEWRIVER_HASWELL: Normal end of execution."
exit 0
