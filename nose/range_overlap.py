#! /usr/bin/env python
#
def range_overlap_bad ( ranges ):
#
## RANGE_OVERLAP_BAD returns the intersection of a list of intervals.
#
#  Discussion:
#
#    This function is incorrect, and should fail some tests.
#
  lowest = ranges[0][0]
  highest = ranges[0][1]

  for ( low, high ) in ranges:

    lowest = max ( lowest, low )
    highest = min ( highest, high )
 
    if ( highest <= lowest ):
      return None
    else:
      return ( lowest, highest )

def range_overlap_good ( ranges ):
#
## RANGE_OVERLAP_GOOD returns the intersection of a list of intervals.
#
#  Discussion:
#
#    This is the corrected function, which should pass the tests.
#
  lowest = ranges[0][0]
  highest = ranges[0][1]

  for ( low, high ) in ranges:

    lowest = max ( lowest, low )
    highest = min ( highest, high )
 
    if ( highest <= lowest ):
      return None

    return ( lowest, highest )
