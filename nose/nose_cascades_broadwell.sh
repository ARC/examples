#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load nose/1.3.7
module load python/2.7.10
#
echo "NOSE_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Note that NOSE_TEST will encounter an error, so we WANT
#  it to return with an error flag, hence returning with
#  error flag 0 ... is an error!
#
python nose_test.py &> nose_cascades_broadwell.txt
if [ $? -eq 0 ]; then
  echo "NOSE_CASCADES_BROADWELL: NOSE failed to detect errors!"
  exit 1
fi
#
echo "NOSE_CASCADES_BROADWELL: Normal end of execution."
exit 0
