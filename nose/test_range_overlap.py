#! /usr/bin/env python
#
from range_overlap import range_overlap_bad
from range_overlap import range_overlap_good

def test_no_overlap ( ):
  assert range_overlap_bad ( [ (0.0, 1.0), (5.0, 6.0) ] ) == None
  assert range_overlap_good ( [ (0.0, 1.0), (5.0, 6.0) ] ) == None

def test_length_zero_overlap ( ):
  assert range_overlap_bad ( [ (0.0, 1.0), (1.0, 2.0) ] ) == None
  assert range_overlap_good ( [ (0.0, 1.0), (1.0, 2.0) ] ) == None

def test_single_range ( ):
  assert range_overlap_bad ( [ (0.0, 1.0) ] ) == (0.0, 1.0)
  assert range_overlap_good ( [ (0.0, 1.0) ] ) == (0.0, 1.0)

def test_negative_range ( ):
  assert range_overlap_bad ( [ (0.0, 1.0), (0.0, 2.0), (-1.0, 1.0) ] ) == (0.0, 1.0)
  assert range_overlap_good ( [ (0.0, 1.0), (0.0, 2.0), (-1.0, 1.0) ] ) == (0.0, 1.0)
