def percent_test ( ):

#*****************************************************************************80
#
## PERCENT_TEST tests the Python "percent" operator for remainders.
#
#  Discussion:
#
#    We should expect this test to fail about half the time, because
#    % does NOT always return a positive remainder.
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license.
#
#  Modified:
#
#    07 April 2017
#
#  Author:
#
#    John Burkardt
#
  import numpy as np
  import platform
  from i4_modp import i4_modp
  from random import randint
  
  test_num = 10

  n_lo = -100000
  n_hi = +100000

  d_lo = -10000
  d_hi = +10000

  print ( '' )
  print ( 'PERCENT_TEST' )
  print ( '  Python version: %s' % ( platform.python_version ( ) ) )
  print ( '  Python % is given:')
  print ( '    N, a number,' )
  print ( '    D, a divisor, D != 0,')
  print ( '  returning:' )
  print ( '    M, a multiple, and' )
  print ( '    R, a remainder,' )
  print ( '  and we would LIKE it to be true that:' )
  print ( '    1: N = D * M + R' )
  print ( '    2: 0 <= R < |D|' )
  print ( '' )
  print ( '    Number   Divisor  Multiple Remainder     D*M+R' )
  print ( '' )

  for test in range ( 0, test_num ):
    n = randint ( n_lo, n_hi )
    d = randint ( d_lo, d_hi )
    while ( d == 0 ):
      d = randint ( d_lo, d_hi )
    r = ( n % d )
    m = ( n - r ) // d
    n2 = d * m + r
    print ( '  %8d  %8d  %8d  %8d  %8d' % ( n, d, m, r, n2 ) )
    assert ( n == n2 and 0 <= r and r < abs ( d ) )
#
#  Terminate.
#
  print ( '' )
  print ( 'PERCENT_TEST' )
  print ( '  Normal end of execution.' )
  return
