#! /usr/bin/env python
#
def i4_modp_test ( ):

#*****************************************************************************80
#
## I4_MODP_TEST tests I4_MODP.
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license.
#
#  Modified:
#
#    07 April 2017
#
#  Author:
#
#    John Burkardt
#
  import numpy as np
  import platform
  from i4_modp import i4_modp
  from random import randint
  
  test_num = 10

  n_lo = -100000
  n_hi = +100000

  d_lo = -10000
  d_hi = +10000

  print ( '' )
  print ( 'I4_MODP_TEST' )
  print ( '  Python version: %s' % ( platform.python_version ( ) ) )
  print ( '  I4_MODP is given:')
  print ( '    N, a number,' )
  print ( '    D, a divisor, D != 0,')
  print ( '  returning:' )
  print ( '    M, a multiple, and' )
  print ( '    R, a remainder,' )
  print ( '  such that:' )
  print ( '    1: N = D * M + R' )
  print ( '    2: 0 <= R < |D|' )
  print ( '' )
  print ( '    Number   Divisor  Multiple Remainder     D*M+R' )
  print ( '' )

  for test in range ( 0, test_num ):
    n = randint ( n_lo, n_hi )
    d = randint ( d_lo, d_hi )
    while ( d == 0 ):
      d = randint ( d_lo, d_hi )
    r = i4_modp ( n, d )
    m = ( n - r ) // d
    n2 = d * m + r
    print ( '  %8d  %8d  %8d  %8d  %8d' % ( n, d, m, r, n2 ) )
    assert ( n == n2 and 0 <= r and r < abs ( d ) )
#
#  Terminate.
#
  print ( '' )
  print ( 'I4_MODP_TEST' )
  print ( '  Normal end of execution.' )
  return
