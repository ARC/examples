#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load pgi/15.7
module load zlib/1.2.8
#
echo "ZLIB_DRAGONSTOOTH: Normal beginning of execution."
#
gcc -c -I$ZLIB_INC zpipe.c
if [ $? -ne 0 ]; then
  echo "ZLIB_DRAGONSTOOTH: Compile error."
  exit 1
fi
#
gcc -o zpipe zpipe.o -L$ZLIB_LIB -lz
if [ $? -ne 0 ]; then
  echo "ZLIB_DRAGONSTOOTH: Load error."
  exit 1
fi
rm zpipe.o
#
#  Compress a file.
#
./zpipe    < robinson_crusoe.txt   > robinson_crusoe.txt.z
if [ $? -ne 0 ]; then
  echo "ZLIB_DRAGONSTOOTH: Run error."
  exit 1
fi
#
#  Decompress the file.
#
./zpipe -d < robinson_crusoe.txt.z > robinson_crusoe2.txt
if [ $? -ne 0 ]; then
  echo "ZLIB_DRAGONSTOOTH: Run error."
  exit 1
fi
#
#  Do a file listing.
#
ls -l rob* > zpipe_dragonstooth.txt
#
#  Clean up
#
rm robinson_crusoe.txt.z
rm robinson_crusoe2.txt
rm zpipe
#
echo "ZLIB_DRAGONSTOOTH: Normal end of execution."
exit 0

