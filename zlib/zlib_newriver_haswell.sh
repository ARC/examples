#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load pgi/15.7
module load zlib/1.2.8
#
echo "ZLIB_NEWRIVER_HASWELL: Normal beginning of execution."
#
gcc -c -I$ZLIB_INC zpipe.c
if [ $? -ne 0 ]; then
  echo "ZLIB_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
gcc -o zpipe zpipe.o -L$ZLIB_LIB -lz
if [ $? -ne 0 ]; then
  echo "ZLIB_NEWRIVER_HASWELL: Load error."
  exit 1
fi
rm zpipe.o
#
#  Compress a file.
#
./zpipe    < robinson_crusoe.txt   > robinson_crusoe.txt.z
if [ $? -ne 0 ]; then
  echo "ZLIB_NEWRIVER_HASWELL: Run error."
  exit 1
fi
#
#  Decompress the file.
#
./zpipe -d < robinson_crusoe.txt.z > robinson_crusoe2.txt
if [ $? -ne 0 ]; then
  echo "ZLIB_NEWRIVER_HASWELL: Run error."
  exit 1
fi
#
#  Do a file listing.
#
ls -l rob* > zpipe_newriver_haswell.txt
#
#  Clean up
#
rm robinson_crusoe.txt.z
rm robinson_crusoe2.txt
rm zpipe
#
echo "ZLIB_NEWRIVER_HASWELL: Normal end of execution."
exit 0

