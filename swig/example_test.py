#! /usr/bin/env python
#
def example_test ( ):

  import example

  print ( '' )
  print ( 'EXAMPLE_TEST:' )
  print ( '  A python program calling C functions through a SWIG interface.')

  value = example.fact ( 5 )
  print ( '  fact ( 5 ) = %d' % ( value ) )

  value = example.my_mod ( 7, 3 )
  print ( '  mod ( 7, 3 ) = %d' % ( value ) )

  value = example.get_time ( )
  print ( '  time: %s' % ( value ) )

  print ( '' )
  print ( 'EXAMPLE_TEST:' )
  print ( '  Normal end of execution.' )
  return

if ( __name__ == '__main__' ):
  example_test ( )

