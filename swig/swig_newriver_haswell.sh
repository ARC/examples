#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load swig/3.0.11
#
echo "SWIG_NEWRIVER_HASWELL: Normal beginning of execution."
#
swig -python -I$CGAL_DIR/share/swig/3.0.11 -I$CGAL_DIR/share/swig/3.0.11/python example.i
if [ $? -ne 0 ]; then
  echo "SWIG_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
gcc -c -fPIC -I /usr/include/python2.7 example.c example_wrap.c
if [ $? -ne 0 ]; then
  echo "SWIG_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
gcc -shared example.o example_wrap.o -o _example.so
if [ $? -ne 0 ]; then
  echo "SWIG_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
#
python example_test.py
if [ $? -ne 0 ]; then
  echo "SWIG_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
#  Clean up.
#
rm example.o
rm example.py
rm example.pyc
rm example_wrap.c
rm example_wrap.o
rm _example.so
#
echo "SWIG_NEWRIVER_HASWELL: Normal end of execution."
exit 0
