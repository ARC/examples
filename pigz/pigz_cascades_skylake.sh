#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load pigz
#
echo "PIGZ_CASCADES_SKYLAKE: Normal beginning of execution."
#
ls -l robinson_crusoe.txt

cp robinson_crusoe.txt robinson_crusoe2.txt
time pigz -p 1 robinson_crusoe2.txt
if [ $? -ne 0 ]; then
  echo "PIGZ_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm robinson_crusoe2.txt.gz
#
cp robinson_crusoe.txt robinson_crusoe2.txt
time pigz -p 2 robinson_crusoe2.txt
if [ $? -ne 0 ]; then
  echo "PIGZ_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm robinson_crusoe2.txt.gz
#
cp robinson_crusoe.txt robinson_crusoe2.txt
time pigz -p 4 robinson_crusoe2.txt
if [ $? -ne 0 ]; then
  echo "PIGZ_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
ls -l robinson_crusoe2.txt.gz
unpigz robinson_crusoe2.txt.gz
if [ $? -ne 0 ]; then
  echo "PIGZ_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
ls -l robinson_crusoe2.txt

rm robinson_crusoe2.txt
#
echo "PIGZ_CASCADES_SKYLAKE: Normal end of execution."
exit 0
