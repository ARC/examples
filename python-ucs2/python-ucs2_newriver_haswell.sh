#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load python-ucs2/2.7.9
#
echo "PYTHON-UCS2_NEWRIVER_HASWELL: Normal beginning of execution."
#
python four_fifths.py > python-ucs2_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "PYTHON-UCS2_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "PYTHON-UCS2_NEWRIVER_HASWELL: Normal end of execution."
exit 0
