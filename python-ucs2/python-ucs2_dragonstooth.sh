#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load python-ucs2/2.7.9\
#
echo "PYTHON-UCS2_DRAGONSTOOTH: Normal beginning of execution."
#
python four_fifths.py > python-ucs2_newriver.txt
if [ $? -ne 0 ]; then
  echo "PYTHON-UCS2_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "PYTHON-UCS2_DRAGONSTOOTH: Normal end of execution."
exit 0
