#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/2.0.0
module load mkl/2017.0
module load fftw/3.3.5
module load scalapack/2.0.2
module load espresso/5.1.2
#
echo "ESPRESSO_CASCADES_BROADWELL: Normal beginning of execution."
#
mkdir -p tempdir
#
./run_example
if [ $? -ne 0 ]; then
  echo "ESPRESSO_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "ESPRESSO_CASCADES_BROADWELL: Normal end of execution."
exit 0

