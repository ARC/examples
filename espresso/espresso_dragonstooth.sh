#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/1.10.2
module load mkl/11.2.3
module load fftw/3.3.4
module load scalapack/2.0.2
module load espresso/5.1.2
#
echo "ESPRESSO_DRAGONSTOOTH: Normal beginning of execution."
#
mkdir -p tempdir
#
./run_example
if [ $? -ne 0 ]; then
  echo "ESPRESSO_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "ESPRESSO_DRAGONSTOOTH: Normal end of execution."
exit 0

