#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/13.1
module load openmpi/1.6.5
module load mkl/11
module load fftw/3.3
module load scalapack/2.0.1
module load espresso/5.1.2
#
echo "ESPRESSO_BLUERIDGE: Normal beginning of execution."
#
mkdir -p tempdir
#
./run_example
if [ $? -ne 0 ]; then
  echo "ESPRESSO_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "ESPRESSO_BLUERIDGE: Normal end of execution."
exit 0

