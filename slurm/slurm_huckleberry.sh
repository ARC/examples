#! /bin/bash
#
#SBATCH -J slurm_huckleberry
#SBATCH -p normal_q
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 00:05:00
#SBATCH --mem=100M
#
cd $SLURM_SUBMIT_DIR
#
echo "SLURM_HUCKLEBERRY: Normal beginning of execution."
#
#  module load commands go here.
#
echo "Commands to be executed by your SLURM job go here."
#
echo ""
echo "SLURM_HUCKLEBERRY: Normal end of execution."
exit 0

