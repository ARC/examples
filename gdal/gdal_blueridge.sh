#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.1.0
module load gdal/2.0.1
#
echo "GCC_BLUERIDGE: Normal beginning of execution."
#
gcc -c -I$GDAL_INC gdal_test.c
if [ $? -ne 0 ]; then
  echo "GDAL_BLUERIDGE: Compile error."
  exit 1
fi
#
gcc -o gdal_test gdal_test.o -L$GDAL_LIB -lgdal
if [ $? -ne 0 ]; then
  echo "GDAL_BLUERIDGE: Load error."
  exit 1
fi
rm gdal_test.o
#
./gdal_test > gdal_newriver.txt
if [ $? -ne 0 ]; then
  echo "GDAL_BLUERIDGE: Run error."
  exit 1
fi
rm gdal_test
#
echo "GDAL_BLUERIDGE: Normal end of execution."
exit 0
