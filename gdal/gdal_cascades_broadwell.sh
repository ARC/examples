#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load gdal/2.0.1
#
echo "GDAL_CASCADES_BROADWELL: Normal beginning of execution."
#
gcc -c -I$GDAL_INC gdal_test.c
if [ $? -ne 0 ]; then
  echo "GDAL_CASCADES_BROADWELL: Compile error."
  exit 1
fi
#
gcc -o gdal_test gdal_test.o -L$GDAL_LIB -lgdal
if [ $? -ne 0 ]; then
  echo "GDAL_CASCADES_BROADWELL: Load error."
  exit 1
fi
rm gdal_test.o
#
./gdal_test > gdal_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "GDAL_CASCADES_BROADWELL: Run error."
  exit 1
fi
rm gdal_test
#
echo "GDAL_CASCADES_BROADWELL: Normal end of execution."
exit 0
