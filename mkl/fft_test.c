# include <math.h>
# include <stdio.h>
# include <stdlib.h>

# include "mkl_dfti.h"

/******************************************************************************/

int main ( int argc, char *argv[] )

/******************************************************************************/
{
  float cp;
  int i;
  int ic;
  float rp;
  MKL_LONG status;
  float _Complex x[32];
  DFTI_DESCRIPTOR_HANDLE x_handle;
  int x_size = 32;
  float y[32+2];
  DFTI_DESCRIPTOR_HANDLE y_handle;
  int y_size = 32;

  printf ( "\n" );
  printf ( "FFT_TEST:\n" );
  printf ( "  C version\n" );
  printf ( "  Demonstrate MKL 1D FFT functions.\n" );
/*
  Complex to complex transform.
*/
  for ( i = 0; i < x_size; i++ )
  {
    x[i] = ( float _Complex ) ( ( i + 1 ) * ( i + 1 ) );
  }

  status = DftiCreateDescriptor ( &x_handle, DFTI_SINGLE, DFTI_COMPLEX, 1, x_size );
  status = DftiCommitDescriptor ( x_handle );
  status = DftiComputeForward ( x_handle, x );
  status = DftiFreeDescriptor ( &x_handle );

  printf ( "\n" );
  printf ( "  Result of complex-to-complex FFT:\n" );
  printf ( "\n" );

  for ( i = 0; i < x_size; i++ )
  {
    printf ( "  %2d  %14.6g  %14.6g\n", i, creal ( x[i] ), cimag ( x[i] ) );
  }
/*
  Real to complex.
*/
  for ( i = 0; i < y_size; i++ )
  {
    y[i] = ( float ) ( ( i + 1 ) * ( i + 1 ) );
  }
  status = DftiCreateDescriptor ( &y_handle, DFTI_SINGLE, DFTI_REAL, 1, y_size );
  status = DftiCommitDescriptor ( y_handle );
  status = DftiComputeForward ( y_handle, y );
  status = DftiFreeDescriptor ( &y_handle );

  printf ( "\n" );
  printf ( "  Result of real-to-complex FFT:\n" );
  printf ( "\n" );
  printf ( "                Real       Imaginary            Norm\n" );
  printf ( "\n" );

  ic = 0;
  for ( i = 0; i < y_size + 2; i++ )
  {
    if ( ic == 0 )
    {
      rp = y[i];
      ic = 1;
    }
    else
    {
      cp = y[i];
      printf ( "  %2d  %14.6g  %14.6g  %14.6g\n", 
        i - 1, rp, cp, sqrt ( rp * rp + cp * cp ) );
      ic = 0;
    }
  }
/*
  Terminate.
*/
  printf ( "\n" );
  printf ( "FFT_TEST:\n" );
  printf ( "  Normal end of execution.\n" );

  return 0;
}
