merSize=14

# grid info
useGrid = 1
scriptOnGrid = 1
frgCorrOnGrid = 1
ovlCorrOnGrid = 1

sge = -A assembly
sgeScript = -pe threads 16
sgeConsensus = -pe threads 1
sgeOverlap = -pe threads 16
sgeFragmentCorrection = -pe threads 2
sgeOverlapCorrection = -pe threads 1

ovlThreads = 16
