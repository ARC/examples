ec.spec
#### common ####
# input_for_database: filtered subreads in fasta or fastq format
#
input_for_database pacbio.filtered_subreads.fq
#
# min_len_for_query: the subreads longer than or equal to this value will be corrected 
#
min_len_for_query 500
#
#  if you don't know the estimated genome size, give a large number 
#
estimated_genome_size 50000
#
#  if you don't know the estimated depth of coverage, give 0 
#
estimated_depth 100
#
#  Path to wgs-assembler (Celera Assembler), the runCA command.
#
ca_path /opt/apps/gcc5_2/openmpi1_10/wgs-assembler/8.3/bin
#
#  the number of processes used by all vs. all alignment 
#  = 'partition' (in single node mode)
#  = 'pre_partition' * 'partition' (in many node mode) 
#
pre_partition 1
partition 4
#
#  if you use ezez4qsub*.pl. you MUST specify blast_path
#  blast_path: where blastn and makeblastdb exist in
#
blast_path /opt/apps/gcc5_2/ncbi-blast+/2.4.0/bin
#
#  if you use ezez4qsub*.pl. you MUST specify sprai_path
#  sprai_path: where binaries of sprai (bfmt72s, nss2v_v3 and so on) exist in 
#
sprai_path /opt/apps/gcc5_2/openmpi1_10/sprai/0.9.9.10/bin
#
#### many node mode (advanced) ####
#
#sge: options for all the SGE jobs
#sge -soft -l ljob,lmem,sjob
#
#queue_req: additional options for all the SGE jobs 
#queue_req -l s_vmem=4G -l mem_req=4 
#
#longestXx_queue_req: if valid, displaces queue_req 
#longestXx_queue_req -l s_vmem=64G -l mem_req=64
#
#BLAST_RREQ: additional options for SGE jobs of all vs. all alignment 
#BLAST_RREQ -pe def_slot 4
#
#### common (advanced) ####
#
#  used by blastn
# 
word_size 18 
evalue 1e-50 
num_threads 1
#
#valid_voters 11
#
#  trim: both ends of each alignment by blastn are trimmed 'trim' bases
#  to detect chimeric reads
#
trim 42
