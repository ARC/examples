#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.10.2
module load python/2.7.10
module load boost/1.58.0
module load hdf5/1.8.16
module load ncbi-blast+/2.4.0
module load perl/5.20.2
module load sprai/0.9.9.10
module load wgs-assembler/8.3
#
echo "SPRAI_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Convert from FASTA to FASTQ format.
#
fa2fq.pl pacbio.filtered_subreads.fasta > pacbio.filtered_subreads.fq
if [ $? -ne 0 ]; then
  echo "SPRAI_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
ezez_vx1.pl ec.spec pbasm.spec > log.txt 2>&1
if [ $? -ne 0 ]; then
  echo "SPRAI_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "SPRAI_NEWRIVER_HASWELL: Normal end of execution."
exit 0
