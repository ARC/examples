#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load bcftools/1.3.1
#
echo "BCFTOOLS_CASCADES_SKYLAKE: Normal beginning of execution."
#
bcftools call -c -v sim_variants.bcf > sim_variants.vcf
if [ $? -ne 0 ]; then
  echo "BCFTOOLS_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "BCFTOOLS_CASCADES_SKYLAKE: Normal end of execution."
exit 0
