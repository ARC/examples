#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load jdk/1.7.0
module load trinityrnaseq/2.0.6
module load samtools/1.2
#
echo "TRINITYRNASEQ_BLUERIDGE: Normal beginning of execution."
#
gunzip top100k.genome.gz
gunzip top100k.Left.fq.gz
gunzip top100k.Right.fq.gz
#
Trinity --genome_guided_max_intron 1000 --genome_guided_bam SP2.chr.bam --max_memory 2G  --output test_GG_use_bam_trinity_outdir
if [ $? -ne 0 ]; then
  echo "TRINITYRNASEQ_BLUERIDGE: Run error!"
  exit 1
fi
#
gzip top100k.genome
gzip top100k.Left.fq
gzip top100k.Right.fq
#
echo "TRINITYRNASEQ_BLUERIDGE: Normal end of execution."
exit 0
