#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load proj/4.9.2
#
echo "PROJ_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  The following command will perform UTM forward projection with a 
#  standard UTM central meridian nearest longitude 112W.  The geographic 
#  values of this example are equivalent and meant as examples of various 
#  forms of DMS input.  The x-y output data will appear as three lines of:
#
#  460769.27     5011648.45
#
proj +proj=utm  +lon_0=112w  +ellps=clrk66 -r input.txt > proj_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "PROJ_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "PROJ_NEWRIVER_HASWELL: Normal end of execution."
exit 0
