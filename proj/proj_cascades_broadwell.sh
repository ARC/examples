#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load proj/4.9.2
#
echo "PROJ_CASCADES_BROADWELL: Normal beginning of execution."
#
#  The following command will perform UTM forward projection with a 
#  standard UTM central meridian nearest longitude 112W.  The geographic 
#  values of this example are equivalent and meant as examples of various 
#  forms of DMS input.  The x-y output data will appear as three lines of:
#
#  460769.27     5011648.45
#
proj +proj=utm  +lon_0=112w  +ellps=clrk66 -r input.txt > proj_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "PROJ_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "PROJ_CASCADES_BROADWELL: Normal end of execution."
exit 0
