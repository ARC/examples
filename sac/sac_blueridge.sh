#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load sac/101.5c
#
echo "SAC_BLUERIDGE: Normal beginning of execution."
#
#  Set SAC environment.
#
source $SAC_BIN/sacinit.sh
#
sac < sac_input.txt
if [ $? -ne 0 ]; then
  echo "SAC_BLUERIDGE: Run error!"
  exit 1
fi
#
#  List SGF and PostScript files created by SAC.
#
ls *.sgf
ls *.ps
#
echo "SAC_BLUERIDGE: Normal end of execution."
exit 0
