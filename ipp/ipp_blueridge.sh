#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/13.1
module load ipp/7.1
#
echo "IPP_BLUERIDGE: Normal beginning of execution."
#
icc -c -I$IPP_INC ipp_test.c
if [ $? -ne 0 ]; then
  echo "IPP_BLUERIDGE: Compile error!"
  exit 1
fi
#
icc -o ipp_test ipp_test.o -ipp
if [ $? -ne 0 ]; then
  echo "IPP_BLUERIDGE: Load error!"
  exit 1
fi
rm ipp_test.o
#
./ipp_test > ipp_blueridge.txt
if [ $? -ne 0 ]; then
  echo "IPP_BLUERIDGE: Run error!"
  exit 1
fi
rm ipp_test
#
echo "IPP_BLUERIDGE: Normal end of execution."
exit 0
