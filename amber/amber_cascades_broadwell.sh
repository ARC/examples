#! /bin/bash
#
#PBS -l walltime=00:5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.10.0
module load cuda/7.5.18
module load python/2.7.10
module load amber/2016
#
echo "AMBER_CASCADES_BROADWELL: Normal beginning of execution."
#
echo ""
echo "AMBPDB generates the protein PDB file directly:"
echo ""
ambpdb -p 0.15_80_10_pH6.5_1ODX.top -c 0.15_80_10_pH6.5_1ODX.crd > 0.15_80_10_pH6.5_1ODX.pdb
if [ $? -ne 0 ]; then
  echo "AMBER_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo ""
echo "AWK derives the ligand structure from the PDB file:"
echo ""
awk '$4=="0E8"' 1ODX.pdb > 0E8.pdb
if [ $? -ne 0 ]; then
  echo "AMBER_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo ""
echo "REDUCE adds hydrogen atoms to the ligand:"
echo ""
export REDUCE_HET_DICT=/opt/apps/gcc4_7/openmpi1_10/amber/2016/dat/reduce_wwPDB_het_dict.txt
reduce 0E8.pdb > 0E8_H.pdb
if [ $? -ne 0 ]; then
  echo "AMBER_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo ""
echo "PDB4AMBER cleans the PDB file:"
echo ""
pdb4amber -i 0E8_H.pdb -o 0E8_clean_H.pdb
if [ $? -ne 0 ]; then
  echo "AMBER_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo ""
echo "A full analysis would go further."
echo "We stop here to keep this demonstration short."
#
echo "AMBER_CASCADES_BROADWELL: Normal end of execution."
exit 0
