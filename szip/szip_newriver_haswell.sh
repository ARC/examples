#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load hdf5/1.8.16
module load szip/2.1
#
echo "SZIP_NEWRIVER_HASWELL: Normal beginning of execution."
#
h5cc -c szip_test.c 
if [ $? -ne 0 ]; then
  echo "SZIP_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
h5cc -o szip_test szip_test.o
if [ $? -ne 0 ]; then
  echo "SZIP_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm szip_test.o
#
./szip_test
if [ $? -ne 0 ]; then
  echo "SZIP_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm szip_test
#
echo "SZIP_NEWRIVER_HASWELL: Normal end of execution."
exit 0

