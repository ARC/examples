#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load hdf5/1.8.8
module load szip/2.1
#
echo "SZIP_BLUERIDGE: Normal beginning of execution."
#
h5cc -c szip_test.c 
if [ $? -ne 0 ]; then
  echo "SZIP_BLUERIDGE: Compile error!"
  exit 1
fi
#
h5cc -o szip_test szip_test.o
if [ $? -ne 0 ]; then
  echo "SZIP_BLUERIDGE: Load error!"
  exit 1
fi
rm szip_test.o
#
./szip_test
if [ $? -ne 0 ]; then
  echo "SZIP_BLUERIDGE: Run error!"
  exit 1
fi
rm szip_test
#
echo "SZIP_BLUERIDGE: Normal end of execution."
exit 0

