#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load hdf5/1.8.16
module load szip/2.1
#
echo "SZIP_DRAGONSTOOTH: Normal beginning of execution."
#
h5cc -c szip_test.c 
if [ $? -ne 0 ]; then
  echo "SZIP_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
h5cc -o szip_test szip_test.o -L$SZIP_LIB -lsz
if [ $? -ne 0 ]; then
  echo "SZIP_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm szip_test.o
#
./szip_test
if [ $? -ne 0 ]; then
  echo "SZIP_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm szip_test
#
echo "SZIP_DRAGONSTOOTH: Normal end of execution."
exit 0

