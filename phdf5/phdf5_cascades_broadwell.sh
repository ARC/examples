#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/3.0.0
module load phdf5/1.8.16
#
echo "PHDF5_CASCADES_BROADWELL: Normal beginning of execution."
#
mpicc -c phdf5_test.c
if [ $? -ne 0 ]; then
  echo "PHDF5_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
mpicc -o phdf5_test phdf5_test.o -lhdf5
if [ $? -ne 0 ]; then
  echo "PHDF5_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm phdf5_test.o
#
mpirun -np 4 phdf5_test > phdf5_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "PHDF5_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm phdf5_test
#
echo "PHDF5_CASCADES_BROADWELL: Normal end of execution."
exit 0
