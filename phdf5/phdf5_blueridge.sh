#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.6.5
module load phdf5/1.8.8
#
mpicc -c phdf5_test.c
if [ $? -ne 0 ]; then
  echo "PHDF5_BLUERIDGE: Compile error!"
  exit 1
fi
#
mpicc -o phdf5_test phdf5_test.o -lhdf5
if [ $? -ne 0 ]; then
  echo "PHDF5_BLUERIDGE: Load error!"
  exit 1
fi
rm phdf5_test.o
#
mpirun -np 4 phdf5_test > phdf5_blueridge.txt
if [ $? -ne 0 ]; then
  echo "PHDF5_BLUERIDGE: Run error!"
  exit 1
fi
rm phdf5_test
#
echo "PHDF5_BLUERIDGE: Normal end of execution."
exit 0
