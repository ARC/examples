#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.10.2
module load phdf5/1.8.16
#
echo "PHDF5_NEWRIVER_HASWELL: Normal beginning of execution."
#
mpicc -c phdf5_test.c
if [ $? -ne 0 ]; then
  echo "PHDF5_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
mpicc -o phdf5_test phdf5_test.o -lhdf5
if [ $? -ne 0 ]; then
  echo "PHDF5_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm phdf5_test.o
#
mpirun -np 4 phdf5_test > phdf5_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "PHDF5_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm phdf5_test
#
echo "PHDF5_NEWRIVER_HASWELL: Normal end of execution."
exit 0
