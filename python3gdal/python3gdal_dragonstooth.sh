#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load python3gdal/2.0.1
#
echo "PYTHON3DGAL_DRAGONSTOOTH: Normal beginning of execution."
#
gcc -c -I$GDAL_INC gdal_test.c
if [ $? -ne 0 ]; then
  echo "PYTHON3DGDAL_DRAGONSTOOTH: Compile error."
  exit 1
fi
#
gcc -o gdal_test gdal_test.o -L$GDAL_LIB -lgdal
if [ $? -ne 0 ]; then
  echo "PYTHON3DGDAL_DRAGONSTOOTH: Load error."
  exit 1
fi
rm gdal_test.o
#
./gdal_test > python3gdal_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "PYTHON3DGDAL_DRAGONSTOOTH: Run error."
  exit 1
fi
rm gdal_test
#
echo "PYTHON3DGDAL_DRAGONSTOOTH: Normal end of execution."
exit 0
