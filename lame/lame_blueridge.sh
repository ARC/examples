#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load yasm/1.3
module load lame/3.99.5
#
echo "LAME_BLUERIDGE: Normal beginning of execution."
#
lame -b128 bach.wav bach.mp3
if [ $? -ne 0 ]; then
  echo "LAME_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "LAME_BLUERIDGE: Normal end of execution."
exit 0
