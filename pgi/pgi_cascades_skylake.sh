#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load pgi/18.1
#
echo "PGI_CASCADES_SKYLAKE: Normal beginning of execution."
#
pgcc -c hello_openmp.c -mp
if [ $? -ne 0 ]; then
  echo "PGI_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
pgcc -o hello_openmp hello_openmp.o
if [ $? -ne 0 ]; then
  echo "PGI_CASCADES_SKYLAKE: Load errors!"
  exit 1
fi
rm hello_openmp.o
#
export OMP_NUM_THREADS=4
./hello_openmp > pgi_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "PGI_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm hello_openmp
#
echo "PGI_CASCADES_SKYLAKE: Normal end of execution."
exit 0
