#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load ea-utils/1.04.807
#
echo "EA-UTILS_CASCADES_BROADWELL: Normal beginning of execution."
#
sam-stats unmapped_first.sam > ea-utils_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "EA-UTILS_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "EA-UTILS_CASCADES_BROADWELL: Normal end of execution."
exit 0
