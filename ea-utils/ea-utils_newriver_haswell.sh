#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load ea-utils/1.04.807
#
echo "EA-UTILS_NEWRIVER_HASWELL: Normal beginning of execution."
#
sam-stats unmapped_first.sam > ea-utils_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "EA-UTILS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "EA-UTILS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
