#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load mvapich2/1.9a2
module load python/2.6.8
module load citcoms/3.2.0
#
echo "CITCOMS_BLUERIDGE: Normal beginning of execution."
#
CitcomSRegional example0 2> citcoms_blueridge.txt
if [ $? -ne 0 ]; then
  echo "CITCOMS_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "CITCOMS_BLUERIDGE: Normal end of execution."
exit 0
