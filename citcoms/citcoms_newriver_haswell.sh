#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load mvapich2/2.2
module load python/2.6.8
module load citcoms/3.2.0
#
echo "CITCOMS_NEWRIVER_HASWELL: Normal beginning of execution."
#
CitcomSRegional example0 2> citcoms_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "CITCOMS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "CITCOMS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
