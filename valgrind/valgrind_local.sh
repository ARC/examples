#! /bin/bash
#
if [ -f valgrind_local.txt ]; then
  rm valgrind_local.txt
fi
#
for test in test01 test02 test03
do
  gcc -c -g $test.c
  if [ $? -ne 0 ]; then
    echo "Compilation errors."
    exit
  fi
#
  gcc $test.o
  if [ $? -ne 0 ]; then
    echo "Load errors."
    exit
  fi
#
  mv a.out $test
#
#  Run program with valgrind
#
  valgrind ./$test &>> valgrind_local.txt
#
#  Clean up.
#
  rm $test.o
  rm $test
done
#
echo "Normal end of execution."
