#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load valgrind/3.12.0
#
echo "VALGRIND_CASCADES_BROADWELL: Normal beginning of execution."
#
if [ -f valgrind_newriver.txt ]; then
  rm valgrind_newriver.txt
fi
#
for test in test01 test02 test03
do
  gcc -c -g $test.c
  if [ $? -ne 0 ]; then
    echo "Compilation errors."
    exit
  fi
#
  gcc $test.o
  if [ $? -ne 0 ]; then
    echo "Load errors."
    exit
  fi
#
  mv a.out $test
#
#  Run program with valgrind
#
  valgrind ./$test &>> valgrind_cascades_broadwell.txt
#
#  Clean up.
#
  rm $test.o
  rm $test
done
#
echo "VALGRIND_CASCADES_BROADWELL: Normal end of execution."

