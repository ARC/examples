#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=2:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
echo "PBS_NODEFILE_21: Normal beginning of execution."
#
#  Display the contents of PBS_NODEFILE .
#
echo ""
echo "Contents of PBS_NODEFILE:"
echo ""
cat $PBS_NODEFILE
#
#  Total number of processors = number of lines in $PBS_NODEFILE.
#
PROC_NUM=`wc -l < $PBS_NODEFILE`
#
#  Display the unique elements in PBS_NODEFILE.
#
echo ""
echo "Unique node names:"
echo ""
uniq $PBS_NODEFILE
#
#  Number of nodes = unique elements in PBS_NODEFILE.
#
NODE_NUM=`uniq $PBS_NODEFILE | wc -l`
#
#  Number of processors per node is a PBS environment variable.
#
echo ""
echo "Number of nodes = "$NODE_NUM
echo "Number of processors per node = "$PBS_NUM_PPN
echo "Total number of processors = "$PROC_NUM
#
echo ""
echo "PBS_NODEFILE_21: Normal end of execution."
exit 0
