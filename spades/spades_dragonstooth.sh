#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load spades/3.6.0
#
echo "SPADES_DRAGONSTOOTH: Normal beginning of execution."
#
python $SPADES_BIN/spades.py --test > spades_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "SPADES_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "SPADES_DRAGONSTOOTH: Normal end of execution."
exit 0
