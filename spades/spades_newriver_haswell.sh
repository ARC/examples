#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load spades/3.6.0
#
echo "SPADES_NEWRIVER_HASWELL: Normal beginning of execution."
#
python $SPADES_BIN/spades.py --test > spades_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "SPADES_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "SPADES_NEWRIVER_HASWELL: Normal end of execution."
exit 0
