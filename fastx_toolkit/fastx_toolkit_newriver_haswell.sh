#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load libgtextutils/0.7
module load fastx_toolkit/0.0.14
#
echo "FASTX_TOOLKIT_NEWRIVER_HASWELL: Normal beginning of execution."
#
$FASTX_BIN/bin/fastq_to_fasta -v -i e_coli_1000.fq -o e_coli_1000.fa > fastx_toolkit_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "FASTX_TOOLKIT_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "FASTX_TOOLKIT_NEWRIVER_HASWELL: Normal end of execution."
exit 0
