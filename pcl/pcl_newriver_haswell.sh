#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.8.5
module load qt/4.8.4
module load vtk/6.3.0
module load flann/1.8.4
module load Anaconda/4.2.0
module load boost/1.58.0
module load eigen/3.2.5
module load pcl/1.7.2
#
echo "PCL_NEWRIVER_HASWELL: Normal beginning of execution."
#
g++ -c -I$PCL_INC/pcl-1.7 -I$EIGEN_INC read_pcd.cpp
if [ $? -ne 0 ]; then
  echo "PCL_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
g++ -o read_pcd read_pcd.o -L$PCL_LIB -L$BOOST_LIB -lpcl_io -lpcl_common -lboost_system
if [ $? -ne 0 ]; then
  echo "PCL_NEWRIVER_HASWELL: Load error."
  exit 1
fi
rm read_pcd.o
#
./read_pcd > pcl_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "PCL_NEWRIVER_HASWELL: Run error."
  exit 1
fi
rm read_pcd
#
echo "PCL_NEWRIVER_HASWELL: Normal end of execution."
exit 0
