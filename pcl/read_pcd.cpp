# include <cstdlib>
# include <cstring>
# include <iostream>

using namespace std;

# include <pcl/io/pcd_io.h>
# include <pcl/point_types.h>

int main ( int argc, char* argv[] )
{
  string filename = "p213.pcd";
  int flag;
//
//  Declare the array.
//
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud ( new pcl::PointCloud<pcl::PointXYZ> );

  std::cout << "\n";
  std::cout << "READ_PCD:\n";
  std::cout << "  C++ version\n";
  std::cout << "  Read a Point Cloud Data (PCD) file.\n";
//
//  Read the array from the file.
//
  flag = pcl::io::loadPCDFile<pcl::PointXYZ> ( filename, *cloud );

  if ( flag == -1 )
  {
    cerr << "\n";
    cerr << "READ_PCD:\n";
    cerr << "  Couldn't read the input file '" << filename << "'\n";
    exit ( 1 );
  }
//
//  Do some processing to the data.
//
  std::cout << "Loaded "
            << cloud->width * cloud->height
            << " data points from '" 
            << filename
            << "', with the following fields:\n";

  for ( size_t i = 0; i < cloud->points.size (); i++ )
  {
    std::cout << "    " << cloud->points[i].x
              << " "    << cloud->points[i].y
              << " "    << cloud->points[i].z << "\n";
  }
//
//  Terminate.
//
  std::cout << "\n";
  std::cout << "READ_PCD:\n";
  std::cout << "  Normal end of execution.\n";

  return 0;
}
