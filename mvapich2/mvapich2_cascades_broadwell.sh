#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load mvapich2/2.2
#
echo "MVAPICH2_CASCADES_BROADWELL: Normal beginning of execution."
#
mpicc -c prime.c
if [ $? -ne 0 ]; then
  echo "MVAPICH2_CASCADES_BROADWELL: Compile error."
  exit 1
fi
#
mpicc -o prime prime.o
if [ $? -ne 0 ]; then
  echo "MVAPICH2_CASCADES_BROADWELL: Load error."
  exit 1
fi
rm prime.o
#
mpirun -np 8 ./prime > mvapich2_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "MVAPICH2_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm prime
#
echo "MVAPICH2_CASCADES_BROADWELL: Normal end of execution."
exit 0
