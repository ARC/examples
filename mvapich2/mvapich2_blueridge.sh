#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load mvapich2/1.9a2
#
echo "MVAPICH2_BLUERIDGE: Normal beginning of execution."
#
mpicc -c prime.c
if [ $? -ne 0 ]; then
  echo "MVAPICH2_BLUERIDGE: Compile error."
  exit 1
fi
#
mpicc -o prime prime.o
if [ $? -ne 0 ]; then
  echo "MVAPICH2_BLUERIDGE: Load error."
  exit 1
fi
rm prime.o
#
mpirun -np 8 ./prime > mvapich2_blueridge.txt
if [ $? -ne 0 ]; then
  echo "MVAPICH2_BLUERIDGE: Run error!"
  exit 1
fi
rm prime
#
echo "MVAPICH2_BLUERIDGE: Normal end of execution."
exit 0
