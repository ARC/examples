#! /bin/bash
#
#PBS -l nodes=1:ppn=24:gpus=2:default
#PBS -l walltime=00:05:00
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module load gcc/6.4.0
module load openmpi/3.0.0
module load cuda/9.1.85
module load namd-gpu/2.10
#
echo "NAMD-GPU_CASCADES_SKYLAKE: Normal beginning of execution."
#
export CUDA_VISIBLE_DEVICES=1,0
#
#  We need the following files in this directory:
#
#    par_all27_prot_lipid.inp
#    ubq_wb.pdb
#    ubq_wb.psf
#    ubq_wb_eq.conf
#
ls -la
#
#  Run the program with $PBS_NP MPI processes.
#
charmrun namd2 +p$PBS_NP +idlepoll +devices 0,1 ubq_wb_eq.conf > namd-gpu_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "NAMD-GPU_CASCADES_SKYLAKE: Run error."
  exit 1
fi
#
echo "NAMD-GPU_CASCADES_SKYLAKE: Normal end of execution."
exit 0
