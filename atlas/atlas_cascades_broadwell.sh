#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load atlas/3.11.34
#
echo "ATLAS_CASCADES_BROADWELL: Normal beginning of execution."
#
gcc -c -I$ATLAS_INC atlas_test.c
if [ $? -ne 0 ]; then
  echo "ATLAS_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
gcc atlas_test.o -L$ATLAS_LIB -llapack -lptf77blas -ltatlas -lgfortran -lm
if [ $? -ne 0 ]; then
  echo "ATLAS_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm atlas_test.o
mv a.out atlas_test
#
./atlas_test > atlas_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "ATLAS_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm atlas_test
#
echo "ATLAS_CASCADES_BROADWELL: Normal end of execution."
exit 0
