# include <math.h>
# include <stdio.h>
# include <stdlib.h>
# include <time.h>

# include "cblas.h"

void cblas_dasum_test ( );
void cblas_dgemv_test ( );
void cblas_dgemm_test ( );

void dasum_test ( );
void dgemv_test ( );
void dgemm_test ( );

double *dv_random ( int n );
double *dge_random ( char trans, int lda, int m, int n );

extern double dasum_ ( int *, double *x, int * );
extern void dgemv_ ( char *, int *, int *, double *, double *, int *, 
  double *, int *, double *, double *, int * );
extern void dgemm_ ( char*, char*, int*, int*, int*, double*, double*, int*, 
  double*, int*, double*, double*, int* );

/******************************************************************************/

int main ( )

/******************************************************************************/
/*
  Purpose:

    MAIN is the main program for ATLAS_TEST.

  Discussion:

    ATLAS_TEST demonstrates how to call some BLAS routines
    in the ATLAS library.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    19 March 2017

  Author:

    John Burkardt
*/
{
  printf ( "\n" );
  printf ( "ATLAS_TEST:\n" );
  printf ( "  C version\n" );
  printf ( "  Demonstrate calls to BLAS functions in ATLAS.\n" );
/*
  Using the C interface.
*/
  srand ( 123456789 );
  cblas_dasum_test ( );
  cblas_dgemv_test ( );
  cblas_dgemm_test ( );
/*
  Using the FORTRAN interface.
*/
  srand ( 123456789 );
  dasum_test ( );
  dgemv_test ( );
  dgemm_test ( );
/*
  Terminate.
*/
  printf ( "\n" );
  printf ( "ATLAS_TEST:\n" );
  printf ( "  Normal end of execution.\n" );
  printf ( "\n" );

  return 0;
}
/******************************************************************************/

void cblas_dasum_test ( )

/******************************************************************************/
/*
  Purpose:

    CBLAS_DASUM_TEST tests CBLAS_DASUM which sums the absolute values of a vector.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    19 March 2017

  Author:

    John Burkardt
*/
{
  int i;
  int n = 10;
  double t;
  double *x;

  printf ( "\n" );
  printf ( "CBLAS_DASUM_TEST\n" );
  printf ( "  CBLAS_DASUM sums the absolute values of a vector.\n" );

  x = dv_random ( n );

  for ( i = 0; i < n; i++ )
  {
    printf ( "  %6d  %14f\n", i, x[i] );
  }

  t = cblas_dasum ( n, x, 1 );
  printf ( "\n" );
  printf ( "  result = %14f\n", t );
/*
  Free memory.
*/
  free ( x );

  return;
}
/******************************************************************************/

void cblas_dgemv_test ( )

/******************************************************************************/
/*
  Purpose:

    CBLAS_DGEMV_TEST tests CBLAS_DGEMV, matrix vector product.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    19 March 2017

  Author:

    John Burkardt
*/
{
  double *a;
  double alpha;
  double beta;
  enum CBLAS_TRANSPOSE cblas_trans;
  int i;
  int incx;
  int incy;
  int j;
  int lda;
  int m;
  int n;
  char trans;
  double *x;
  double *y;

  printf ( "\n" );
  printf ( "CBLAS_DGEMV_TEST\n" );
  printf ( "  CBLAS_DGEMV computes y := alpha * A * x + beta * y\n" );
  printf ( "  or                   y := alpha * A'' * x + beta * y.\n" );
/*
  y = alpha * A * x + beta * y
*/
  cblas_trans = CblasNoTrans;
  trans = 'N';
  m = 5;
  n = 4;
  alpha = 2.0;
  lda = m;
  a = dge_random ( trans, lda, m, n );
  x = dv_random ( n );
  incx = 1;
  beta = 3.0;
  y = dv_random ( m );
  incy = 1;

  cblas_dgemv ( CblasColMajor, cblas_trans, m, n, alpha, a, lda, x, incx, beta, 
    y, incy );

  printf ( "\n" );
  printf ( "  Matrix-vector product has been computed.\n" );
/*
  Free memory.
*/
  free ( a );
  free ( x );
  free ( y );

  return;
}
/******************************************************************************/

void cblas_dgemm_test ( )

/******************************************************************************/
/*
  Purpose:

    CBLAS_DGEMM_TIME tests CBLAS_DGEMM, which multiplies two dense matrices.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    19 March 2017

  Author:

    John Burkardt
*/
{
  double *a;
  double alpha;
  double *b;
  double beta;
  double *c;
  enum CBLAS_TRANSPOSE cblas_transa;
  enum CBLAS_TRANSPOSE cblas_transb;
  int k;
  int lda;
  int ldb;
  int ldc;
  int m;
  int n;
  int order = 10;
  char transa;
  char transb;

  printf ( "\n" );
  printf ( "CBLAS_DGEMM_TEST\n" );
  printf ( "  CBLAS_DGEMM computes C=A*B for general matrices A, B, C.\n" );

  transa = 'N';
  cblas_transa = CblasNoTrans;
  transb = 'N';
  cblas_transb = CblasNoTrans;
  m = order;
  n = order;
  k = order;
  alpha = 1.0;
  lda = order;
  a = dge_random ( transa, lda, m, k );
  ldb = order;
  b = dge_random ( transb, ldb, k, n );
  beta = 1.0;
  ldc = order;
  c = ( double * ) malloc ( ldc * n * sizeof ( double ) );
/*
  Compute the product C = A * B.
*/
  cblas_dgemm ( CblasColMajor, cblas_transa, cblas_transb, m, n, k, alpha, 
    a, lda, b, ldb, beta, c, ldc );

  printf ( "\n" );
  printf ( "  Matrix product C=A*B has been computed.\n" );
/*
  Free memory.
*/
  free ( a );
  free ( b );
  free ( c );

  return;
}
/******************************************************************************/

void dasum_test ( )

/******************************************************************************/
/*
  Purpose:

    DASUM_TEST tests DASUM which sums the absolute values of a vector.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    19 March 2017

  Author:

    John Burkardt
*/
{
  int i;
  int inc;
  int n = 10;
  double t;
  double *x;

  printf ( "\n" );
  printf ( "DASUM_TEST\n" );
  printf ( "  DASUM sums the absolute values of a vector.\n" );

  x = dv_random ( n );

  for ( i = 0; i < n; i++ )
  {
    printf ( "  %6d  %14f\n", i, x[i] );
  }

  inc = 1;
  t = dasum_ ( &n, x, &inc );
  printf ( "\n" );
  printf ( "  result = %14f\n", t );
/*
  Free memory.
*/
  free ( x );

  return;
}
/******************************************************************************/

void dgemv_test ( )

/******************************************************************************/
/*
  Purpose:

    DGEMV_TEST tests DGEMV, matrix vector product.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    19 March 2017

  Author:

    John Burkardt
*/
{
  double *a;
  double alpha;
  double beta;
  int i;
  int incx;
  int incy;
  int j;
  int lda;
  int m;
  int n;
  char trans;
  double *x;
  double *y;

  printf ( "\n" );
  printf ( "DGEMV_TEST\n" );
  printf ( "  DGEMV computes y := alpha * A * x + beta * y\n" );
  printf ( "  or             y := alpha * A'' * x + beta * y.\n" );
/*
  y = alpha * A * x + beta * y
*/
  trans = 'N';
  m = 5;
  n = 4;
  alpha = 2.0;
  lda = m;
  a = dge_random ( trans, lda, m, n );
  x = dv_random ( n );
  incx = 1;
  beta = 3.0;
  y = dv_random ( m );
  incy = 1;

  dgemv_ ( &trans, &m, &n, &alpha, a, &lda, x, &incx, &beta, y, &incy );

  printf ( "\n" );
  printf ( "  Matrix-vector product has been computed.\n" );
/*
  Free memory.
*/
  free ( a );
  free ( x );
  free ( y );

  return;
}
/******************************************************************************/

void dgemm_test ( )

/******************************************************************************/
/*
  Purpose:

    DGEMM_TIME tests DGEMM, which multiplies two dense matrices.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    19 March 2017

  Author:

    John Burkardt
*/
{
  double *a;
  double alpha;
  double *b;
  double beta;
  double *c;
  int k;
  int lda;
  int ldb;
  int ldc;
  int m;
  int n;
  int order = 10;
  char transa;
  char transb;

  printf ( "\n" );
  printf ( "DGEMM_TEST\n" );
  printf ( "  DGEMM computes C=A*B for general matrices A, B, C.\n" );

  transa = 'N';
  transb = 'N';
  m = order;
  n = order;
  k = order;
  alpha = 1.0;
  lda = order;
  a = dge_random ( transa, lda, m, k );
  ldb = order;
  b = dge_random ( transb, ldb, k, n );
  beta = 1.0;
  ldc = order;
  c = ( double * ) malloc ( ldc * n * sizeof ( double ) );
/*
  Compute the product C = A * B.
*/
  dgemm_ ( &transa, &transb, &m, &n, &k, &alpha, a, &lda, b, &ldb, &beta, 
    c, &ldc );

  printf ( "\n" );
  printf ( "  Matrix product C=A*B has been computed.\n" );
/*
  Free memory.
*/
  free ( a );
  free ( b );
  free ( c );

  return;
}
/******************************************************************************/

double *dv_random ( int n )

/******************************************************************************/
/*
  Purpose:

    DV_RANDOM returns a random double precision real vector.

  Licensing:

    This code is distributed under the GNU LGPL license.

  Modified:

    19 March 2017

  Author:

    John Burkardt

  Parameters:

    Input, int N, the size of the vector.

    Output, double DV_RANDOM[N], the vector.
*/
{
  int i;
  double t;
  double *x;

  x = ( double * ) malloc ( n * sizeof ( double ) );

  for ( i = 0; i < n; i++ )
  {
    t = ( double ) rand ( ) / ( double ) RAND_MAX; 
    x[i] = 2.0 * t - 1.0;
  }

  return x;
}
/******************************************************************************/

double *dge_random ( char trans, int lda, int m, int n )

/******************************************************************************/
/*
  Purpose:

    DGE_RANDOM sets up a double precision real matrix of random values.

  Licensing:

    This code is distributed under the GNU LGPL license.
  
  Modified:

    19 March 2017

  Author:

    John Burkardt.

  Parameters:

    Input, char TRANS, indicates whether matrix is to be 
    transposed.
    'N', no transpose.
    'T', transpose the matrix.

    Input, int LDA, the leading dimension of the matrix.

    Input, int M, N, the number of rows and columns of 
    the matrix.

    Output, double DGE_RANDOM[LDA*N], the matrix.
    if TRANS is 'N', then the matrix is stored in LDA*N entries,
    as an M x N matrix;
    if TRANS is 'T', then the matrix is stored in LDA*M entries,
    as an N x M matrix.
*/
{
  double *a;
  int i;
  int j;
  double t;

  a = ( double * ) malloc ( lda * n * sizeof ( double ) );

  for ( j = 0; j < n; j++ )
  {
    for ( i = 0; i < m; i++ )
    {
      t = ( double ) rand ( ) / ( double ) RAND_MAX; 
      a[i+j*lda] = 2.0 * t - 1.0;
    }
  }

  return a;
}
