#! /usr/bin/env python
#
def lynx ( ):

#*****************************************************************************80
#
## LYNX uses the MATPLOTLIB library to draw a curve with datapoints marked.
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license.
#
#  Modified:
#
#    05 April 2017
#
#  Author:
#
#    John Burkardt
#
  import matplotlib
#
#  Force MATPLOTLIB NOT to use X-windows!
#
  matplotlib.use ( 'Agg' )
  import matplotlib.pyplot as plt
  import platform

  print ( '' )
  print ( 'LYNX:' )
  print ( '  Python version: %s' % ( platform.python_version ( ) ) )
  print ( '  Make a curve by connecting a series of data points.' )
  print ( '  Mark the data points.' )
#
#  Read the pairs "Year, Lynx harvest" from the file.
#
  x = []
  y = []

  file = open ( 'lynx.txt', 'r' )

  for line in file:
    line = line.strip ( )
    columns = line.split ( )
    x.append ( float ( columns[0] ) )
    y.append ( float ( columns[1] ) )

  file.close ( )
#
#  Plot the data.
#
  plt.plot ( x, y, linewidth = 2, color = 'g' )
  plt.plot ( x, y, 'ko' )
  plt.grid ( True )
  plt.xlabel ( '<--- Year --->' )
  plt.ylabel ( '<--- Lynx Harvest --->' )
  plt.title ( 'Lynx Population Records' )

  plt.savefig ( 'lynx.png' )
  print ( '' )
  print ( '  Graphics data saved as "lynx.png"' )
#
#  Call SHOW if interactive use.
#
# plt.show ( )

  plt.clf ( )
#
#  Terminate.
#
  print ( '' )
  print ( 'LYNX' )
  print ( '  Normal end of execution.' )
  return

if ( __name__ == '__main__' ):
  lynx ( )
