#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load lordec/0.6
#
echo "LORDEC_DRAGONSTOOTH: Normal beginning of execution."
#
lordec-correct -2 ill-test-5K-1.fa -k 19 -s 3 \
  -i pacbio-test.fa.gz \
  -o pacbio-corrected-single.fa \
  &> pacbio-corrected-single.log
if [ $? -ne 0 ]; then
  echo "LORDEC_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "LORDEC_DRAGONSTOOTH: Normal end of execution."
exit 0
