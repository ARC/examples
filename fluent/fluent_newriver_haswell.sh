#! /bin/bash
#
#PBS -l walltime=0:20:00
#PBS -l nodes=1:ppn=24
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load ansys/17.0
#
echo "FLUENT_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Run a 3D problem, using double precision, no graphical interface,
#  and 4 processors.
#
fluent 3ddp -g -t4 < cylinder.txt > fluent_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "FLUENT_NEWRIVER_HASWELL: Run error."
  exit 1
fi
#
echo "FLUENT_NEWRIVER_HASWELL: Normal end of execution."
exit 0
