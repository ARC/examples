#! /bin/bash
#
#PBS -l walltime=0:20:00
#PBS -l nodes=1:ppn=12
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load ansys/17.0
#
echo "FLUENT_BLUERIDGE: Normal beginning of execution."
#
#  Run a 3D problem, using double precision, no graphical interface,
#  and 4 processors.
#
fluent 3ddp -g -t4 < cylinder.txt > fluent_blueridge.txt
if [ $? -ne 0 ]; then
  echo "FLUENT_BLUERIDGE: Run error."
  exit 1
fi
#
echo "FLUENT_BLUERIDGE: Normal end of execution."
exit 0
