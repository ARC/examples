#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load impi/5.0
module load parmetis/4.0.3
#
echo "PARMETIS_DRAGONSTOOTH: Normal beginning of execution."
#
mpirun -np 4 $PARMETIS_BIN/mtest metis.mesh 2
if [ $? -ne 0 ]; then
  echo "PARMETIS_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
mpirun -np 4 $PARMETIS_BIN/parmetis rotor.graph 1 6 1 1 6 1
if [ $? -ne 0 ]; then
  echo "PARMETIS_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
mpirun -np 4 $PARMETIS_BIN/ptest rotor.graph rotor.graph.xyz
if [ $? -ne 0 ]; then
  echo "PARMETIS_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "PARMETIS_DRAGONSTOOTH: Normal end of execution."
exit 0
