#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.8.5
module load parmetis/4.0.3
#
echo "PARMETIS_NEWRIVER_HASWELL: Normal beginning of execution."
#
mpirun -np 4 $PARMETIS_BIN/mtest metis.mesh 2
if [ $? -ne 0 ]; then
  echo "PARMETIS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
mpirun -np 4 $PARMETIS_BIN/parmetis rotor.graph 1 6 1 1 6 1
if [ $? -ne 0 ]; then
  echo "PARMETIS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
mpirun -np 4 $PARMETIS_BIN/ptest rotor.graph rotor.graph.xyz
if [ $? -ne 0 ]; then
  echo "PARMETIS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "PARMETIS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
