#! /bin/bash
#
#SBATCH -J mpich_huckleberry
#SBATCH -p normal_q
#SBATCH -N 1
#SBATCH -n 8
#SBATCH -t 00:05:00
#SBATCH --mem=100M
#
cd $SLURM_SUBMIT_DIR
#
module purge
module load gcc/6.1.0
module load mpich/3.2
#
echo "MPICH_HUCKLEBERRY: Normal beginning of execution."
#
mpicc -c prime.c
if [ $? -ne 0 ]; then
  echo "MPICH_HUCKLEBERRY: Compile error."
  exit 1
fi
#
mpicc -o prime prime.o
if [ $? -ne 0 ]; then
  echo "MPICH_HUCKLEBERRY: Load error."
  exit 1
fi
rm prime.o
#
mpirun -np 8 ./prime > mpich_huckleberry.txt
if [ $? -ne 0 ]; then
  echo "MPICH_HUCKLEBERRY: Run error!"
  exit 1
fi
rm prime
#
echo "MPICH_HUCKLEBERRY: Normal end of execution."
exit 0
