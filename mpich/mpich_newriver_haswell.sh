#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load mpich/3.1.4
#
echo "MPICH_NEWRIVER_HASWELL: Normal beginning of execution."
#
mpicc -c prime.c
if [ $? -ne 0 ]; then
  echo "MPICH_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
mpicc -o prime prime.o
if [ $? -ne 0 ]; then
  echo "MPICH_NEWRIVER_HASWELL: Load error."
  exit 1
fi
rm prime.o
#
mpirun -np 8 ./prime > mpich_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "MPICH_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm prime
#
echo "MPICH_NEWRIVER_HASWELL: Normal end of execution."
exit 0
