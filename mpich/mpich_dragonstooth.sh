#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load mpich/3.1.4
#
echo "MPICH_DRAGONSTOOTH: Normal beginning of execution."
#
mpicc -c prime.c
if [ $? -ne 0 ]; then
  echo "MPICH_DRAGONSTOOTH: Compile error."
  exit 1
fi
#
mpicc -o prime prime.o
if [ $? -ne 0 ]; then
  echo "MPICH_DRAGONSTOOTH: Load error."
  exit 1
fi
rm prime.o
#
mpirun -np 8 ./prime > mpich_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "MPICH_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm prime
#
echo "MPICH_DRAGONSTOOTH: Normal end of execution."
exit 0
