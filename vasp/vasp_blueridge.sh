#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/13.1
module load openmpi/1.6.5
module load mkl/11
module load vasp/5.3.3
#
echo "VASP_BLUERIDGE: Normal beginning of execution."
#
#  Instead of the command
#
#  mpirun -np 4 vasp
#
#  we use the following command, which sets the stacksize to "unlimited":
#
mpirun -np 4 /bin/bash -c "ulimit -s unlimited; vasp"
if [ $? -ne 0 ]; then
  echo "VASP_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "VASP_BLUERIDGE: Normal end of execution."
exit 0

