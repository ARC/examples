#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/3.0.0
module load mkl/2017.0
module load vasp/5.4.4
#
echo "VASP_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Instead of the command
#
#  mpirun -np 4 vasp
#
#  we use the following command, which sets the stacksize to "unlimited":
#
mpirun -np 4 /bin/bash -c "ulimit -s unlimited; vasp_std"
if [ $? -ne 0 ]; then
  echo "VASP_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "VASP_CASCADES_BROADWELL: Normal end of execution."
exit 0

