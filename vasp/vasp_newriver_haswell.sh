#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/1.8.5
module load mkl/11.2.3
module load vasp/5.3.3
#
echo "VASP_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Instead of the command
#
#  mpirun -np 4 vasp
#
#  we use the following command, which sets the stacksize to "unlimited":
#
mpirun -np 4 /bin/bash -c "ulimit -s unlimited; vasp"
if [ $? -ne 0 ]; then
  echo "VASP_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "VASP_NEWRIVER_HASWELL: Normal end of execution."
exit 0

