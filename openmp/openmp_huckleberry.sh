#! /bin/bash
#
#SBATCH -J openmp_huckleberry
#SBATCH -p normal_q
#SBATCH -N 1
#SBATCH -n 8
#SBATCH -t 00:05:00
#SBATCH --mem=100M
#
cd $SLURM_SUBMIT_DIR
#
module purge
module load gcc
#
echo "OPENMP_HUCKLEBERRY: Normal beginning of execution."
#
gcc -fopenmp -o jacobi jacobi.c -lm
if [ $? -ne 0 ]; then
  echo "OPENMP_HUCKLEBERRY: Compile error!"
  exit 1
fi
#
export OMP_NUM_THREADS=1
./jacobi > openmp_huckleberry.txt
export OMP_NUM_THREADS=2
./jacobi >> openmp_huckleberry.txt
export OMP_NUM_THREADS=4
./jacobi >> openmp_huckleberry.txt
export OMP_NUM_THREADS=8
./jacobi >> openmp_huckleberry.txt
#
rm jacobi
#
echo "OPENMP_HUCKLEBERRY: Normal end of execution."
exit 0

