#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc
#
echo "OPENMP_BLUERIDGE: Normal beginning of execution."
#
gcc -fopenmp -o jacobi jacobi.c -lm
if [ $? -ne 0 ]; then
  echo "OPENMP_BLUERIDGE: Compile error!"
  exit 1
fi
#
export OMP_NUM_THREADS=1
./jacobi > openmp_blueridge.txt
export OMP_NUM_THREADS=2
./jacobi >> openmp_blueridge.txt
export OMP_NUM_THREADS=4
./jacobi >> openmp_blueridge.txt
export OMP_NUM_THREADS=8
./jacobi >> openmp_blueridge.txt
#
rm jacobi
#
echo "OPENMP_BLUERIDGE: Normal end of execution."
exit 0

