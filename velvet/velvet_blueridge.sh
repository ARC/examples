#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load velvet/1.2.08
#
echo "VELVET_BLUERIDGE: Normal beginning of execution."
#
#  Check data in FASTA format.
#
velveth fmtAuto_fai.bin 31 -create_binary -shortPaired -fmtAuto reads.fa.gz
if [ $? -ne 0 ]; then
  echo "VELVET_BLUERIDGE: Run error!"
  exit 1
fi
#
cmp -s Roadmaps.31 fmtAuto_fai.bin/Roadmaps

if [ $? -ne 0 ]; then
  echo "reads.fa.gz binary mode Roadmap differs from Roadmaps.31"
else
  echo "reads.fa.gz binary mode Roadmap is ok."
fi
rm -r fmtAuto_fai.bin
#
#  Check data in FASTQ format.
#
velveth fmtAuto_fqi.bin 31 -create_binary -shortPaired -fmtAuto reads.fq.gz
if [ $? -ne 0 ]; then
  echo "VELVET_BLUERIDGE: Run error!"
  exit 1
fi
#
cmp -s Roadmaps.31 fmtAuto_fqi.bin/Roadmaps

if [ $? -ne 0 ]; then
  echo "reads.fq.gz binary mode Roadmap differs from Roadmaps.31"
else
  echo "reads.fq.gz binary mode Roadmap is ok."
fi
rm -r fmtAuto.fqi.bin
#
echo "VELVET_BLUERIDGE: Normal end of execution."
exit 0
