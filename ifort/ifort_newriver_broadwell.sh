#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/18.2
#
echo "IFORT_NEWRIVER_BROADWELL: Normal beginning of execution."
#
ifort -c heated_plate.f90
if [ $? -ne 0 ]; then
  echo "IFORT_NEWRIVER_BROADWELL: Compile error!"
  exit 1
fi
#
ifort -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "IFORT_NEWRIVER_BROADWELL: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > ifort_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "IFORT_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
rm heated_plate
#
echo "IFORT_NEWRIVER_BROADWELL: Normal end of execution."
exit 0

