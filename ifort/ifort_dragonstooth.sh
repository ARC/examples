#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/16.1
#
echo "IFORT_DRAGONSTOOTH: Normal beginning of execution."
#
ifort -c heated_plate.f90
if [ $? -ne 0 ]; then
  echo "IFORT_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
ifort -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "IFORT_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > intel_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "IFORT_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm heated_plate
#
echo "IFORT_DRAGONSTOOTH: Normal end of execution."
exit 0

