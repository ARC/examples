#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load jdk/1.8.0
module load apache-ant/1.9.2
#
echo "APACHE-ANT_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Move the source code file to src/oata, to simulate how
#  a large software package might be set up.
#
mkdir src
mkdir src/oata
mv HelloWorld.java src/oata/HelloWorld.java
#
#  Compile the files in the src/oata directory.
#
ant compile
if [ $? -ne 0 ]; then
  echo "APACHE-ANT_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
#  Package the project into the build/jar directory.
#
ant jar
if [ $? -ne 0 ]; then
  echo "APACHE-ANT_NEWRIVER_HASWELL: JAR error!"
  exit 1
fi
#
#  Run the program.
#
ant run
if [ $? -ne 0 ]; then
  echo "APACHE-ANT_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
#  Clean up.
#
mv src/oata/HelloWorld.java HelloWorld.java
rmdir src/oata
rmdir src
rm -r build
#
echo "APACHE-ANT_NEWRIVER_HASWELL: Normal end of execution."
exit 0

