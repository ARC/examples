#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
#
cd $PBS_O_WORKDIR
#
module purge
module load lua/5.1.4.8
#
echo "LUA_NEWRIVER_BROADWELL: Normal beginning of execution."
#
lua markov_chain.lua < robinson_crusoe.txt > lua_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "LUA_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
#
echo "LUA_NEWRIVER_BROADWELL: Normal end of execution."
exit 0

