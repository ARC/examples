#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load lua/5.1.4.8
#
echo "LUA_DRAGONSTOOTH: Normal beginning of execution."
#
lua markov_chain.lua < robinson_crusoe.txt > lua_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "LUA_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "LUA_DRAGONSTOOTH: Normal end of execution."
exit 0

