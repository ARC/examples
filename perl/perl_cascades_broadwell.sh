#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load perl/5.20.2
#
echo "PERL_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Get rid of old results file if it exists.
#
if [ -f perl_cascades_broadwell.txt ]; then
  rm perl_cascades_broadwell.txt
fi
#
#  Run several test values of N.
#
for n in 1 2 3 6 7 9 18 25 27 54 73 97 129
do
  perl collatz_count.pl $n >> perl_cascades_broadwell.txt
  if [ $? -ne 0 ]; then
    echo "PERL_CASCADES_BROADWELL: Run error!"
    exit 1
  fi
done
#
echo "PERL_CASCADES_BROADWELL: Normal end of execution."
exit 0

