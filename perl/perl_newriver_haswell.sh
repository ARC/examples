#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load perl/5.20.2
#
echo "PERL_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Get rid of old results file if it exists.
#
if [ -f perl_newriver_haswell.txt ]; then
  rm perl_newriver_haswell.txt
fi
#
#  Run several test values of N.
#
for n in 1 2 3 6 7 9 18 25 27 54 73 97 129
do
  perl collatz_count.pl $n >> perl_newriver_haswell.txt
  if [ $? -ne 0 ]; then
    echo "PERL_NEWRIVER_HASWELL: Run error!"
    exit 1
  fi
done
#
echo "PERL_NEWRIVER_HASWELL: Normal end of execution."
exit 0

