#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.4.0
module load openmpi/2.1.3
module load openblas/0.2.20
module load python/2.7.13
module load boost/1.58.0
module load sparsehash/2.0.2
module load abyss/2.0.2
#
echo "ABYSS_CASCADES_SKYLAKE: Normal beginning of execution."
#
#  Remove old output file if it exists.
#
rm -f abyss_cascades_skylake.txt
#
#  Assemble a small synthetic dataset
#
abyss-pe k=25 name=test in='reads1.fastq reads2.fastq' &>> abyss_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "ABYSS_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
#  Calculate assembly contiquity statistics.
#
abyss-fac test-unitigs.fa &>> abyss_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "ABYSS_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
#  Say goodbye.
#
echo "" &>> abyss_cascades_skylake.txt
echo "Normal end of execution." &>> abyss_cascades_skylake.txt
#
echo "ABYSS_CASCADES_SKYLAKE: Normal end of execution."
exit 0
