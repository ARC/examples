#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc
module load hdf5
module load szip/2.1
#
echo "HDF5_NEWRIVER_HASWELL: Normal beginning of execution."
#
h5cc -c hdf5_test.c
if [ $? -ne 0 ]; then
  echo "HDF5_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
h5cc -o hdf5_test hdf5_test.o
if [ $? -ne 0 ]; then
  echo "HDF5_NEWRIVER_HASWELL: Load error."
  exit 1
fi
rm hdf5_test.o
#
./hdf5_test > hdf5_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "HDF5_NEWRIVER_HASWELL: Run error."
  exit 1
fi
rm hdf5_test
#
echo "HDF5_NEWRIVER_HASWELL: Normal end of execution."
exit 0
