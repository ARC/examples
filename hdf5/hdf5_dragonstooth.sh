#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load hdf5/1.8.16
module load szip/2.1
#
echo "HDF5_DRAGONSTOOTH: Normal beginning of execution."
#
h5cc -c hdf5_test.c
if [ $? -ne 0 ]; then
  echo "HDF5_DRAGONSTOOTH: Compile error."
  exit 1
fi
#
h5cc -o hdf5_test hdf5_test.o -L$SZIP_LIB -lsz
if [ $? -ne 0 ]; then
  echo "HDF5_DRAGONSTOOTH: Load error."
  exit 1
fi
rm hdf5_test.o
#
./hdf5_test > hdf5_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "HDF5_DRAGONSTOOTH: Run error."
  exit 1
fi
rm hdf5_test
#
echo "HDF5_DRAGONSTOOTH: Normal end of execution."
exit 0
