#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load hdf5/1.8.17
module load szip/2.1
#
echo "HDF5_CASCADES_SKYLAKE: Normal beginning of execution."
#
h5cc -c hdf5_test.c
if [ $? -ne 0 ]; then
  echo "HDF5_CASCADES_SKYLAKE: Compile error."
  exit 1
fi
#
h5cc -o hdf5_test hdf5_test.o
if [ $? -ne 0 ]; then
  echo "HDF5_CASCADES_SKYLAKE: Load error."
  exit 1
fi
rm hdf5_test.o
#
./hdf5_test > hdf5_cascades_skhylake.txt
if [ $? -ne 0 ]; then
  echo "HDF5_CASCADES_SKYLAKE: Run error."
  exit 1
fi
rm hdf5_test
#
echo "HDF5_CASCADES_SKYLAKE: Normal end of execution."
exit 0
