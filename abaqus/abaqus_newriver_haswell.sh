#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=12
#PBS -W group_list=newriver
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load abaqus/6.14_4
#
echo "ABAQUS_NEWRIVER_HASWELL: Normal beginning of execution."
#
abaqus job=adams cpus=12 interactive > abaqus_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "AQAQUS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "ABAQUS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
