#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=12
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load abaqus/6.14_4
#
echo "ABAQUS_CASCADES_BROADWELL: Normal beginning of execution."
#
abaqus job=adams cpus=12 interactive > abaqus_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "AQAQUS_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "ABAQUS_CASCADES_BROADWELL: Normal end of execution."
exit 0
