#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=12
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load abaqus/6.14_4
#
echo "ABAQUS_DRAGONSTOOTH: Normal beginning of execution."
#
abaqus job=adams cpus=12 interactive > abaqus_newriver.txt
if [ $? -ne 0 ]; then
  echo "AQAQUS_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "ABAQUS_DRAGONSTOOTH: Normal end of execution."
exit 0
