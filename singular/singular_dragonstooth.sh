#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load cddlib/0.94h
module load flint/2.5.2
module load singular/4.0.3
#
echo "SINGULAR_DRAGONSTOOTH: Normal beginning of execution."
#
Singular < singular_input.txt > singular_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "SINGULAR_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "SINGULAR_DRAGONSTOOTH: Normal end of execution."
exit 0
