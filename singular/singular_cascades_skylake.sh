#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load cddlib/0.94h
module load flint/2.5.2
module load singular/4.0.3
#
echo "SINGULAR_CASCADES_SKYLAKE: Normal beginning of execution."
#
Singular < singular_input.txt > singular_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "SINGULAR_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "SINGULAR_CASCADES_SKYLAKE: Normal end of execution."
exit 0
