#! /bin/bash
#
#PBS -l nodes=2:ppn=16
#PBS -l walltime=00:05:00
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.8.4
module load namd/2.10
#
echo "NAMD_BLUERIDGE: Normal beginning of execution."
#
#  We need the following files in this directory:
#
#    par_all27_prot_lipid.inp
#    ubq_wb.pdb
#    ubq_wb.psf
#    ubq_wb_eq.conf
#
ls -la
#
#  Run the program with $PBS_NP MPI processes.
#
charmrun namd2 +p$PBS_NP ubq_wb_eq.conf > namd_blueridge.txt
if [ $? -ne 0 ]; then
  echo "NAMD_BLUERIDGE: Run error."
  exit 1
fi
#
echo "NAMD_BLUERIDGE: Normal end of execution."
exit 0
