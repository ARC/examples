#! /bin/bash
#
#PBS -l nodes=2:ppn=24
#PBS -l walltime=00:30:00
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/3.0.0
module load namd/2.10
#
echo "NAMD_CASCADES_BROADWELL: Normal beginning of execution."
#
#  We need the following files in this directory:
#
#    par_all27_prot_lipid.inp
#    ubq_wb.pdb
#    ubq_wb.psf
#    ubq_wb_eq.conf
#
ls -la
#
#  Run the program with $PBS_NP MPI processes.
#
charmrun namd2 +p$PBS_NP ubq_wb_eq.conf > namd_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "NAMD_CASCADES_BROADWELL: Run error."
  exit 1
fi
#
echo "NAMD_CASCADES_BROADWELL: Normal end of execution."
exit 0
