#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gaussian/09.E_01
#
echo "GAUSSIAN_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  We must set up the Gaussian environment.
#
source $GAUSSIAN_DIR/bsd/g09.profile
#
#  Run.
#
g09 < g09_input.txt > gaussian_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "GAUSSIAN_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "GAUSSIAN_NEWRIVER_HASWELL: Normal end of execution."
exit 0
