#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load openmpi/1.10.2
module load phdf5/1.8.16
module load parallel-netcdf/1.7.0
module load netcdf-c-par/4.4.0
module load netcdf-fortran-par/4.4.3
#
echo "NETCDF-FORTRAN-PAR_NEWRIVER_HASWELL: Normal beginning of execution."
#
mpif90 -c -I$PNETCDF_INC -I$NETCDF_F_INC writer.f90
if [ $? -ne 0 ]; then
  echo "NETCDF-FORTRAN-PAR_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
mpif90 -o writer writer.o -L$NETCDF_F_LIB -L$NETCDF_C_LIB -L$PNETCDF_LIB -L$PHDF5_LIB -lpnetcdf -lnetcdff -lnetcdf -lhdf5_hl -lhdf5 -lz -lpnetcdf -lcurl
if [ $? -ne 0 ]; then
  echo "NETCDF-FORTRAN-PAR_NEWRIVER_HASWELL: Load error."
  exit 1
fi
rm writer.o
#
mpirun -np 4 ./writer > writer_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "NETCDF-FORTRAN-PAR_NEWRIVER_HASWELL: Run error."
  exit 1
fi
rm writer
#
echo "NETCDF-FORTRAN-PAR_NEWRIVER_HASWELL: Normal end of execution."
exit 0
