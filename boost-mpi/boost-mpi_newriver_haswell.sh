#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/1.8.5
module load python/2.7.10
module load boost-mpi/1.58.0
#
echo "BOOST-MPI_NEWRIVER_HASWELL: Normal beginning of execution."
#
mpicxx -c reduce_test.cpp -I$BOOST_INC 
if [ $? -ne 0 ]; then
  echo "BOOST-MPI_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
mpicxx -o reduce_test reduce_test.o -L$BOOST_LIB -lboost_mpi
if [ $? -ne 0 ]; then
  echo "BOOST-MPI_NEWRIVER_HASWELL: Load error."
  exit 1
fi
rm reduce_test.o
#
mpirun -np 4 ./reduce_test > boost-mpi_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "BOOST-MPI_NEWRIVER_HASWELL: Run error."
  exit 1
fi
rm reduce_test
#
echo "BOOST-MPI_NEWRIVER_HASWELL: Normal end of execution."
exit 0
