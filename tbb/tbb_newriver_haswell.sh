#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load tbb/4.3.5
#
echo "TBB_NEWRIVER_HASWELL: Normal beginning of execution."
#
icpc -c -I$TBB_INC hello.cpp
if [ $? -ne 0 ]; then
  echo "TBB_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
icpc -o hello -L$TBB_LIB hello.o -ltbb
if [ $? -ne 0 ]; then
  echo "TBB_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
#
rm hello.o
#
./hello > tbb_newriver_newriver.txt
if [ $? -ne 0 ]; then
  echo "TBB_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
rm hello
#
echo "TBB_NEWRIVER_HASWELL: Normal end of execution."
exit 0
