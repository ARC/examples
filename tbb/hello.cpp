#include "tbb/tbb.h"
#include <cstdio>

using namespace tbb;

class say_hello
{
  const char* id;

  public:

    say_hello ( const char* s ) : id(s) { }

    void operator( ) ( ) const
    {
      printf ( "hello from task %s\n", id );
    }
};

int main( )
{
  task_group tg;
//
//  Spawn task 1.
//
  tg.run(say_hello("1"));
//
//  Spawn task 2.
//
  tg.run(say_hello("2"));
//
//  Wait for tasks to finish.
//
  tg.wait( );
}
