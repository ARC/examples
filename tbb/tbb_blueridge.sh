#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/13.1
module load tbb/4.1
#
echo "TBB_BLUERIDGE: Normal beginning of execution."
#
icpc -c -I$TBB_INC hello.cpp
if [ $? -ne 0 ]; then
  echo "TBB_BLUERIDGE: Compile error!"
  exit 1
fi
#
icpc -o hello -L$TBB_LIB hello.o -ltbb
if [ $? -ne 0 ]; then
  echo "TBB_BLUERIDGE: Load error!"
  exit 1
fi
#
rm hello.o
#
./hello > tbb_blueridge.txt
if [ $? -ne 0 ]; then
  echo "TBB_BLUERIDGE: Run error!"
  exit 1
fi
#
rm hello
#
echo "TBB_BLUERIDGE: Normal end of execution."
exit 0
