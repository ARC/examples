#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q largemem_q
#
cd $PBS_O_WORKDIR
#
module purge
module load luaJIT/2.0.4
#
echo "LUAJIT_NEWRIVER_IVYBRIDGE: Normal beginning of execution."
#
luajit markov_chain.lua < robinson_crusoe.txt > luajit_newriver_ivybridge.txt
if [ $? -ne 0 ]; then
  echo "LUAJIT_NEWRIVER_IVYBRIDGE: Run error!"
  exit 1
fi
#
echo "LUAJIT_NEWRIVER_IVYBRIDGE: Normal end of execution."
exit 0

