#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load luaJIT/2.0.4
#
echo "LUAJIT_NEWRIVER_HASWELL: Normal beginning of execution."
#
luajit markov_chain.lua < robinson_crusoe.txt > luajit_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "LUAJIT_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "LUAJIT_NEWRIVER_HASWELL: Normal end of execution."
exit 0

