#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load luaJIT/2.0.4
#
echo "LUAJIT_CASCADES_SKYLAKE: Normal beginning of execution."
#
luajit markov_chain.lua < robinson_crusoe.txt > luajit_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "LUAJIT_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "LUAJIT_CASCADES_SKYLAKE: Normal end of execution."
exit 0

