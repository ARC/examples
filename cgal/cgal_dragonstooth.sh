#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load CGAL/4.9
#
echo "CGAL_DRAGONSTOOTH: Normal beginning of execution."
#
g++ -c -I$CGAL_INC points_and_segment.cpp
if [ $? -ne 0 ]; then
  echo "CGAL_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
g++ -o points_and_segment points_and_segment.o -L$CGAL_LIB -lCGAL
if [ $? -ne 0 ]; then
  echo "CGAL_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm points_and_segment.o
#
./points_and_segment > cgal_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "CGAL_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm points_and_segment
#
echo "CGAL_DRAGONSTOOTH: Normal end of execution."
exit 0

