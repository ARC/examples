# include <iostream>
# include <CGAL/Simple_cartesian.h>

typedef CGAL::Simple_cartesian<double> Kernel;
typedef Kernel::Point_2 Point_2;
typedef Kernel::Segment_2 Segment_2;

int main ( )
{
  std::cout << "\n";
  std::cout << "POINTS_AND_SEGMENT:\n";
  std::cout << "  A simple CGAL example.\n";
//
//  Define 2D points P=(1,1) and Q=(10,10).
//
  Point_2 p(1,1), q(10,10);

  std::cout << "\n";
  std::cout << "p = " << p << std::endl;
  std::cout << "q = " << q.x() << " " << q.y() << std::endl;
  std::cout << "sqdist(p,q) = " 
            << CGAL::squared_distance(p,q) << std::endl;
//
//  Define 2D line segment S=P:Q.
//
  Segment_2 s(p,q);
//
//  Define 2D point M = (5,9).
//
  Point_2 m(5, 9);
  
  std::cout << "m = " << m << std::endl;
//
//  Compute the squared distance from M to the segment S.
//
  std::cout << "sqdist(Segment_2(p,q), m) = "
            << CGAL::squared_distance(s,m) << std::endl;
//
//  Determine if P, Q and M are collinear, or make an angle.
//
  std::cout << "p, q, and m ";

  switch (CGAL::orientation(p,q,m))
  {
    case CGAL::COLLINEAR: 
      std::cout << "are collinear\n";
      break;
    case CGAL::LEFT_TURN:
      std::cout << "make a left turn\n";
      break;
    case CGAL::RIGHT_TURN: 
      std::cout << "make a right turn\n";
      break;
  }
//
//  Determine the midpoint of P and Q.
//
  std::cout << " midpoint(p,q) = " << CGAL::midpoint(p,q) << std::endl;

  std::cout << "\n";
  std::cout << "POINTS_AND_SEGMENT:\n";
  std::cout << "  Normal end of execution.\n";

  return 0;
}
