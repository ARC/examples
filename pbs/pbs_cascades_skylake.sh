#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
#  Following the "module purge" you can add "module load xxx" commands.
#
module purge
#
#  Here are some PBS environment variables.
#
echo "PBS_JOBID = "$PBS_JOBID
echo "PBS_JOBNAME = "$PBS_JOBNAME
echo "PBS_NODEFILE = "$PBS_NODEFILE
echo "PBS_NODENUM = "$PBS_NODENUM
echo "PBS_NUM_PPN = "$PBS_NUM_PPN
echo "PBS_O_HOME = "$PBS_O_HOME
echo "PBS_O_HOST = "$PBS_O_HOST
echo "PBS_O_QUEUE = "$PBS_O_QUEUE
echo "PBS_O_SHELL = "$PBS_O_SHELL
echo "PBS_O_WORKDIR = "$PBS_O_WORKDIR
#
echo "PBS_CASCADES_SKYLAKE: Normal beginning of execution."
#
#  Here is where you should include the commands you want to execute.
#  We will just print a message that includes the name of the node.
#
echo "Here is where your computational commands should go."
#
echo ""
echo "PBS_CASCADES_SKYLAKE: Normal end of execution."
exit 0

