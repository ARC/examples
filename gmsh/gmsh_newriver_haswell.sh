#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openblas/0.2.14
module load gmsh/2.12.0
#
echo "GMSH_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  "-2" means perform 2D mesh generation, then exit.
#
gmsh stenosis.geo -2 > gmsh_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "GMSH_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
#  Check that the MSH mesh file was created.
#
ls -l stenosis.msh
#
echo "GMSH_NEWRIVER_HASWELL: Normal end of execution."
exit 0
