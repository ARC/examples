#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openblas/0.2.20
module load gmsh/2.12.0
#
echo "GMSH_CASCADES_BROADWELL: Normal beginning of execution."
#
#  "-2" means perform 2D mesh generation, then exit.
#
gmsh stenosis.geo -2 > gmsh_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "GMSH_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
#  Check that the MSH mesh file was created.
#
ls -l stenosis.msh
#
echo "GMSH_CASCADES_BROADWELL: Normal end of execution."
