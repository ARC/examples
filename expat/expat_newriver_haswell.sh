#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load expat/2.2.4
#
echo "EXPAT_NEWRIVER_HASWELL: Normal beginning of execution."
#
gcc -c -I$EXPAT_INC expat_test.c
if [ $? -ne 0 ]; then
  echo "EXPAT_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
gcc -o expat_test expat_test.o -L$EXPAT_LIB -lexpat
if [ $? -ne 0 ]; then
  echo "EXPAT_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm expat_test.o
#
./expat_test < expat_test.xml > expat_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "EXPAT_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm expat_test
#
echo "EXPAT_NEWRIVER_HASWELL: Normal end of execution."
exit 0
