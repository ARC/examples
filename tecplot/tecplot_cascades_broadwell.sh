#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load tecplot/17.1.0
#
echo "TECPLOT_CASCADES_BROADWELL: Normal beginning of execution."
#
tec360 -b -mesa -p cylinder.mcr
if [ $? -ne 0 ]; then
  echo "TECPLOT_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
tec360 -b -mesa -p cavity.mcr
if [ $? -ne 0 ]; then
  echo "TECPLOT_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "TECPLOT_CASCADES_BROADWELL: Normal end of execution."
exit 0
