#!MC 1400
# Created by Tecplot 360 build 14.0.2.35002
#
#  Create a cylindrical IJK-ordered zone.
#  Use IMAX, JMAX and KMAX divisions in I, J and K dimensions.
#  Circle center is (x,y), radius is R, and Z range is (Z1,Z2).
#  X, Y and Z are variables 1, 2 and 3.
# 
$!createcircularzone imax = 10 jmax = 25 kmax = 10 x = 0 y = 3 z1 = 0 z2 = 1 radius = 1 xvar = 1 yvar = 2 zvar = 3
#
#  Specify that the plot should not use translucency, shading, or element edges.
#
$!fieldlayers usetranslucency = no
$!fieldlayers showshade = no
$!fieldlayers showedge = no
#
#  Define a fourth variable Q using a formula.
#
$!alterdata equation = '{q} = sin(x*y+z*x)'
#
#  Specify that the contour plot is only to appear on the exposed cell faces.
#
$!fieldmap [1]  surfaces { surfacestoplot = exposedcellfaces }
#
#  Specify that the plot is to show contours.
#
$!fieldlayers showcontour = yes
#
#  The contour plot should be of the value of variable 4, the value Q.
#
$!globalcontour 1  var = 4
#
#  Set the contour levels to 15 evenly distributed, nice values.
#
$!contourlevels resettonice contourgroup = 1 approxnumvalues = 15
#
#  Set the viewing angle and viewer position.
#
$!threedview psiangle = 122.217 thetaangle = -146.263 alphaangle = 15.9724 viewerposition { x = 8.080027914835405 y = 15.09840323951778 z = -8.667848498664521 }
#
#  Push the current view onto the view stack.
#
$!view push
#
#  Prepare for image export by specifying the image type, size, properties, and name.
#
$!exportsetup exportformat = jpeg imagewidth = 900 usesupersampleantialiasing = yes exportfname = 'cylinder.jpg'
#
#  Export the image.
#
$!export exportregion = currentframe

