#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load tecplot/15.2.0
#
echo "TECPLOT_NEWRIVER_HASWELL: Normal beginning of execution."
#
tec360 -b -mesa -p cylinder.mcr
if [ $? -ne 0 ]; then
  echo "TECPLOT_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
tec360 -b -mesa -p cavity.mcr
if [ $? -ne 0 ]; then
  echo "TECPLOT_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "TECPLOT_NEWRIVER_HASWELL: Normal end of execution."
exit 0
