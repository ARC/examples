#! /bin/bash
#
#  submit_newriver_broadwell.sh
#
#  Purpose:
#
#    Run short tests of programs on the ARC newriver/broadwell cluster.
#
#  Discussion:
#
#    Revised to run with the new location of the examples.
#
#  Example:
#
#    To execute this script, log onto any newriver login node, and type:
#
#      ./submit_newriver_broadwell.sh
#
#    This should submit all the jobs.  Later, run "reports_newriver_broadwell.sh"
#    to examine the results.
#
#  Modified:
#
#    18 June 2018
#
#  Author:
#
#    John Burkardt
#
date
echo ""
echo "SUBMIT_NEWRIVER_BROADWELL:"
echo "  Submit test suite jobs on newriver/broadwell."
#
cd /home/burkardt/examples
#
#  Identify current directory.
#
BASE=$(pwd)
echo ""
echo "  Working from base directory $BASE"
#
#  Clear out all old report files.
#
rm -f */test_suite_newriver_broadwell.txt

#-------------------------------------------------------------------------------
#
#  Program list #1.
#
PROGRAMS1="
anaconda
ansys
automake
cmake
cuda
cuda_fortran
dcw
fdk-aac
ffmpeg
gcc
gfortran
glog
gnuplot
ifort
intel
jdk
lame
lammps
lua
luajit
matlab
matlab_parallel
mkl_ifort
namd-gpu
p100_dev_q
p100_normal_q
parallel
pgf90
pgi
pigz
python
scons
tecplot
uuid
viennacl
x264
yasm
"
#
#  For each program:
#    move to that directory
#    remove any old copy of the results file
#    submit the job
#    go back to base
#
#  Override queue choice and group_list in job files.
#
QSUBFLAGS1='-qp100_large_q -Aarctest -Wgroup_list=arcadm -otest_suite_newriver_broadwell.txt'

for PROG in $PROGRAMS1; do
  cd $PROG
  rm -f test_suite_newriver_broadwell.txt
  echo "Submit "$PROG"_newriver_broadwell.sh"
  qsub $QSUBFLAGS1 $PROG"_newriver_broadwell.sh"
  cd $BASE
done
#-------------------------------------------------------------------------------
#
echo ""
echo "  Once jobs are run, list outcomes by:"
echo ""
echo "    tail -q -n 1 */test_suite_newriver_broadwell.txt"
echo ""
echo "  or simply run:"
echo ""
echo "    ./report_newriver_broadwell.sh"
#
#  Terminate.
#
echo ""
echo "SUBMIT_NEWRIVER_BROADWELL: Normal end of execution."
exit 0

