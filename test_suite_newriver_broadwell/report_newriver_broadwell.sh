#! /bin/bash
#
#  report_newriver_broadwell.sh
#
#  Purpose:
#
#    Collect results from test suite on newriver/broadwell.
#
#  Discussion:
#
#    We assume that all the jobs have run.
#    We assume that, for program "prog":
#    * there is a directory /home/burkardt/public_html/examples/prog;
#    * there is a script file prog_newriver_braodwell.sh;
#    * there is now an output file test_suite_newriver_broadwell.txt;
#    * the last line of the output file probably contains the line
#        "prog_NEWRIVER_BROADWELL: Normal end of execution";
#
#  Modified:
#
#    07 May 2018
#
#  Author:
#
#    John Burkardt
#
date
echo ""
echo "REPORT_NEWRIVER_BROADWELL:"
echo "  Report results of test suite jobs on newriver/broadwell."
#
cd /home/burkardt/examples
#
#  Identify current directory.
#
BASE=$(pwd)
echo ""
echo "  Working from base directory $BASE"
#
echo ""
echo "----------------------------------------"
tail -n 1 -q */test_suite_newriver_broadwell.txt
echo "----------------------------------------"
#
#  Terminate.
#
echo ""
echo "REPORT_NEWRIVER_BROADWELL: Normal end of execution."
exit 0

