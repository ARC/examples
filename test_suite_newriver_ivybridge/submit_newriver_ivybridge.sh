#! /bin/bash
#
#  submit_newriver_ivybridge.sh
#
#  Purpose:
#
#    Run short tests of programs on the ARC newriver/ivybridge cluster.
#
#  Discussion:
#
#    Revised to run with the new location of the examples.
#
#  Example:
#
#    To execute this script, log onto any newriver login node, and type:
#
#      ./submit_newriver_ivybridge.sh
#
#    This should submit all the jobs.  Later, run "reports_newriver_ivybridge.sh"
#    to examine the results.
#
#  Modified:
#
#    19 June 2018
#
#  Author:
#
#    John Burkardt
#
date
echo ""
echo "SUBMIT_NEWRIVER_IVYBRIDGE:"
echo "  Submit test suite jobs on newriver/ivybridge."
#
cd /home/burkardt/examples
#
#  Identify current directory.
#
BASE=$(pwd)
echo ""
echo "  Working from base directory $BASE"
#
#  Clear out all old report files.
#
rm -f */test_suite_newriver_ivybridge.txt

#-------------------------------------------------------------------------------
#
#  Program list #1.
#
PROGRAMS1="
ansys
apache-ant
automake
cmake
eigen
fastqc
gatk
gaussian
gcc
gfortran
gnuplot
ifort
intel
largemem_q
lua
luajit
mathematica
matlab
matlab_parallel
mkl_ifort
parallel
pgf90
pgi
picard
pigz
python
r
samtools
scons
tecplot
trimmomatic
uuid
"
#
#  For each program:
#    move to that directory
#    remove any old copy of the results file
#    submit the job
#    go back to base
#
#  Override group_list in job files.
#
QSUBFLAGS1='-Aarctest -Wgroup_list=arcadm -otest_suite_newriver_ivybridge.txt'

for PROG in $PROGRAMS1; do
  cd $PROG
  rm -f test_suite_newriver_ivybridge.txt
  echo "Submit "$PROG"_newriver_ivybridge.sh"
  qsub $QSUBFLAGS1 $PROG"_newriver_ivybridge.sh"
  cd $BASE
done
#-------------------------------------------------------------------------------
#
echo ""
echo "  Once jobs are run, list outcomes by:"
echo ""
echo "    tail -q -n 1 */test_suite_newriver_ivybridge.txt"
echo ""
echo "  or simply run:"
echo ""
echo "    ./report_newriver_ivybridge.sh"
#
#  Terminate.
#
echo ""
echo "SUBMIT_NEWRIVER_IVYBRIDGE: Normal end of execution."
exit 0

