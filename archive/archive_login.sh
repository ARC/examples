#! /bin/bash
#
#
echo "ARCHIVE_LOGIN: Normal beginning of execution."
echo ""
echo "ARCHIVE_LOGIN"
echo "  Demonstrate simple use of the ARCHIVE storage facility."
#
#  1: Create a small file "hello.c", that we can archive.
#
echo ""
echo "1: Create a file 'hello.c' to work with, and list it."

cat <<EOF > hello.c
# include <stdio.h>

int main ( )
{
  printf ( "Hello from a C program\n" );
  printf ( "which started life as a 'here' document.\n" );
  return 0;
}
EOF

ls -l hello.c
#
#  2: Place a copy of the file into the archive directory,
#  and verify it is there.
#
echo "2: Copy the file to the archive and list it there."

cp hello.c $ARCHIVE
ls -l $ARCHIVE/hello.c
#
#  3: Remove our local copy and try to list it.
#
echo "3: Remove our local copy and try to list it."

rm hello.c
ls -l hello.c
#
#  4: We didn't mean to delete that file!  
#  No problem, we can retrieve the archived file.
#
echo "4: Copy the archived version back, and list it."

cp $ARCHIVE/hello.c .
ls -l hello.c
#
#  5: The example is done.  Let's clean up.
#
echo "5: The example is done. Get rid of hello.c"

rm hello.c
rm $ARCHIVE/hello.c
#
echo "ARCHIVE_LOGIN: Normal end of execution."
exit 0

