#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.1.0
module load python/2.7.10
module load mothur/1.36.0
#
echo "MOTHUR_BLUERIDGE: Normal beginning of execution."
#
mothur HA_commands.py
if [ $? -ne 0 ]; then
  echo "MOTHUR_BLUERIDGE: Run error."
  exit 1
fi
#
echo "MOTHUR_BLUERIDGE: Normal end of execution."
exit 0
