#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q largemem_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
#
echo "LARGEMEM_Q_CASCADES_BROADWELL: Normal beginning of execution."
echo "LARGEMEM_Q_CASCADES_BROADWELL: Job running in queue "$PBS_QUEUE
#
gcc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "LARGEMEM_Q_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
gcc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "LARGEMEM_Q_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > largemem_q_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "LARGEMEM_Q_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm heated_plate
#
echo "LARGEMEM_Q_CASCADES_BROADWELL: Normal end of execution."
exit 0

