#! /bin/bash
#
#  report_cascades_broadwell.sh
#
#  Purpose:
#
#    Collect results from test suite on Broadwell nodes of Cascades.
#
#  Discussion:
#
#    We assume that all the jobs have run.
#    We assume that, for program "prog":
#    * there is a directory /home/burkardt/public_html/examples/prog;
#    * there is a script file prog_cascades_broadwell.sh;
#    * there is now an output file test_suite_cascades_broadwell.txt;
#    * the last line of the output file probably contains the line
#        "prog_CASCADES_BROADWELL: Normal end of execution";
#
#  Modified:
#
#    21 April 2018
#
#  Author:
#
#    John Burkardt
#
date
echo ""
echo "REPORT_CASCADES_BROADWELL:"
echo "  Report results of test suite jobs on Cascades Broadwell nodes."
#
cd /home/burkardt/examples
#
#  Identify current directory.
#
BASE=$(pwd)
echo ""
echo "  Working from base directory $BASE"
#
echo ""
echo "----------------------------------------"
tail -n 1 -q */test_suite_cascades_broadwell.txt
echo "----------------------------------------"
#
#  Terminate.
#
echo ""
echo "REPORT_CASCADES_BROADWELL: Normal end of execution."
exit 0

