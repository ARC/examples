#! /bin/bash
#
#  submit_cascades_broadwell.sh
#
#  Purpose:
#
#    Run short tests of programs on the Broadwell nodes of ARC Cascades cluster.
#
#  Discussion:
#
#    Revised to run with the new location of the examples.
#
#  Example:
#
#    To execute this script, log onto any Cascades login node, and type:
#
#      ./submit_cascades_broadwell.sh
#
#    This should submit all the jobs.  Later, run 
#
#      ./reports_cascades_broadwell.sh
#
#    to examine the results.
#
#  Modified:
#
#    07 June 2018
#
#  Author:
#
#    John Burkardt
#
date
echo ""
echo "SUBMIT_CASCADES_BROADWELL:"
echo "  Submit test suite jobs on Cascades Broadwell nodes."
#
cd /home/burkardt/examples
#
#  Identify current directory.
#
BASE=$(pwd)
echo ""
echo "  Working from base directory $BASE"
#
#  Clear out all old report files.
#
rm -f */test_suite_cascades_broadwell.txt
#
#  Program #1 list.
#
PROGRAMS1="
abinit
anaconda
ansys
atlas
autodocksuite
automake
bamtools
bcftools
beagle-lib
bedtools
blas_atlas
blas_mkl
boost
bowtie
bowtie2
bwa
bzip2
cddlib
cgal
clapack
cmake
cora
cplex
cufflinks
dcw
ea-utils
eigen
espresso
examl
fastqc
fdk-aac
ffmpeg
fftw
flann
flint
gatb
gatk
gaussian
gcc
gdal
geos
gfortran
glm
gmsh
gnuplot
gromacs
gsl
harminv
hdf5
hmmer
hpl
ifort
intel
jags
jdk
julia
lame
lammps
lapack_atlas
lapack_mkl
libjpeg-turbo
lua
luajit
mathematica
matlab
matlab_parallel
metis
minia
mkl
mkl_ifort
mpe2
mrbayes
mvapich2
namd
nastran
ncbi-blast+
ncl
nco
netcdf
netcdf-c
netcdf-fortran
nose
numpy
openblas
openfoam
openmp
openmpi
p4est
p7zip
papi
parallel
parallel-netcdf
parmetis
pbs
perl
pgf90
pgi
phdf5
pigz
proj
python
r
samtools
seqtk
sox
spades
sparsehash
stata
swig
szip
tecplot
tophat
trimmomatic
trinityrnaseq
valgrind
vasp
x264
yasm
zip
zlib"
#
#  For each program:
#    move to that directory
#    remove any old copy of the results file
#    submit the job
#    go back to base
#
#
#  Override queue choice and group_list in job files.
#
QSUBFLAGS1='-qlarge_q -Aarctest -Wgroup_list=arcadm -otest_suite_cascades_broadwell.txt'

for PROG in $PROGRAMS1; do
  cd $PROG
  rm -f test_suite_cascades.txt
  echo "Submit "$PROG"_cascades_broadwell.sh"
  qsub $QSUBFLAGS1 $PROG"_cascades_broadwell.sh"
  cd $BASE
done
#
#  Program #2 list.
#
PROGRAMS2="
dev_q
largemem_q
normal_q
open_q"
#
#  For each program:
#    move to that directory
#    remove any old copy of the results file
#    submit the job
#    go back to base
#
#
#  Do not override queue choice for program list #2.
#
QSUBFLAGS2='-Aarctest -Wgroup_list=arcadm -otest_suite_cascades_broadwell.txt'

for PROG in $PROGRAMS2; do
  cd $PROG
  rm -f test_suite_cascades.txt
  echo "Submit "$PROG"_cascades_broadwell.sh"
  qsub $QSUBFLAGS2 $PROG"_cascades_broadwell.sh"
  cd $BASE
done
#
echo ""
echo "  Once jobs are run, list outcomes by:"
echo ""
echo "    tail -q -n 1 */test_suite_cascades_broadwell.txt"
echo ""
echo "  or simply run:"
echo ""
echo "    ./report_cascades_broadwell.sh"
#
#  Terminate.
#
echo ""
echo "SUBMIT_CASCADES_BROADWELL: Normal end of execution."
exit 0

