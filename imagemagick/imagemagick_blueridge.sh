#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load ImageMagick/6.8.9
#
echo "IMAGEMAGICK_BLUERIDGE: Normal beginning of execution."
#
#  Apply 3x3 filter to remove most noise.
#
convert -noise 3 balloons_noisy.png balloons.png
if [ $? -ne 0 ]; then
  echo "IMAGEMAGICK_BLUERIDGE: Run error!"
  exit 1
fi
#
#  Make 2, 4, 8, 16 color versions.
#
convert -colors 2 balloons.png balloons2.png
if [ $? -ne 0 ]; then
  echo "IMAGEMAGICK_BLUERIDGE: Run error!"
  exit 1
fi
#
convert -colors 4 balloons.png balloons4.png
if [ $? -ne 0 ]; then
  echo "IMAGEMAGICK_BLUERIDGE: Run error!"
  exit 1
fi
#
convert -colors 8 balloons.png balloons8.png
if [ $? -ne 0 ]; then
  echo "IMAGEMAGICK_BLUERIDGE: Run error!"
  exit 1
fi
#
convert -colors 16 balloons.png balloons16.png
if [ $? -ne 0 ]; then
  echo "IMAGEMAGICK_BLUERIDGE: Run error!"
  exit 1
fi
#
#  Left-right reverse.
#
convert -flop balloons.png snoollab.png
if [ $? -ne 0 ]; then
  echo "IMAGEMAGICK_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "IMAGEMAGICK_BLUERIDGE: Normal end of execution."
exit 0
