#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load atlas/3.11.36
module load openmpi/1.10.2
module load wannier90/2.0.1 
#
echo "WANNIER90_DRAGONSTOOTH: Normal beginning of execution."
#
#  Run the wannier90 executable; it is NOT parallel.
#
wannier90.x gaas
if [ $? -ne 0 ]; then
  echo "WANNIER90_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "WANNIER90_DRAGONSTOOTH: Normal end of execution."
exit 0
