#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1:gpus=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load cuda/8.0.61
module load pgi/18.1
#
echo "CUDA_FORTRAN_CASCADES_SKYLAKE: Normal beginning of execution."
#
export NO_STOP_MESSAGE=yes
pgf90 -Mcuda=cc60 -o saxpy_test saxpy_test.cuf
if [ $? -ne 0 ]; then
  echo "CUDA_FORTRAN_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
./saxpy_test > cuda_fortran_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "CUDA_FORTRAN_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm saxpy_test
rm mathops.mod
#
echo "CUDA_FORTRAN_CASCADES_SKYLAKE: Normal end of execution."
exit 0

