#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1:gpus=1
#PBS -W group_list=newriver
#PBS -q p100_dev_q
#PBS -A arctest
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load cuda/8.0.61
module load pgi/17.5
#
echo "CUDA_FORTRAN_NEWRIVER_BROADWELL: Normal beginning of execution."
#
pgf90 -Mcuda=cc60 -o saxpy_test saxpy_test.cuf
if [ $? -ne 0 ]; then
  echo "CUDA_FORTRAN_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
#
./saxpy_test > cuda_fortran_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "CUDA_FORTRAN_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
rm saxpy_test
rm mathops.mod
#
echo "CUDA_FORTRAN_NEWRIVER_BROADWELL: Normal end of execution."
exit 0

