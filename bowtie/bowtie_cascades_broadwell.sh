#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load bowtie/1.1.2
#
echo "BOWTIE_CASCADES_BROADWELL: Normal beginning of execution."
#
bowtie -t e_coli e_coli_1000.fq e_coli.map &> bowtie_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "BOWTIE_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "BOWTIE_CASCADES_BROADWELL: Normal end of execution."
exit 0
