#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load bowtie/1.1.2
#
echo "BOWTIE_NEWRIVER_HASWELL: Normal beginning of execution."
#
bowtie -t e_coli e_coli_1000.fq e_coli.map &> bowtie_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "BOWTIE_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "BOWTIE_NEWRIVER_HASWELL: Normal end of execution."
exit 0
