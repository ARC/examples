#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load bowtie/1.1.2
#
echo "BOWTIE_BLUERIDGE: Normal beginning of execution."
#
bowtie -t e_coli e_coli_1000.fq e_coli.map &> bowtie_blueridge.txt
if [ $? -ne 0 ]; then
  echo "BOWTIE_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "BOWTIE_BLUERIDGE: Normal end of execution."
exit 0
