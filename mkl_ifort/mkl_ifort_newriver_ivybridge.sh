#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q largemem_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load mkl/11.2.3
#
echo "MKL_IFORT_NEWRIVER_IVYBRIDGE: Normal beginning of execution."
#
ifort -c -mkl $MKL_INC/mkl_dfti.f90 fft_test.f90
if [ $? -ne 0 ]; then
  echo "MKL_IFORT_NEWRIVER_IVYBRIDGE: Compile error."
  exit 1
fi
#
ifort -mkl -o fft_test mkl_dfti.o fft_test.o
if [ $? -ne 0 ]; then
  echo "MKL_IFORT_NEWRIVER_IVYBRIDGE: Load error."
  exit 1
fi
rm mkl_dfti.o
rm fft_test.o
#
./fft_test > mkl_ifort_newriver_ivybridge.txt
if [ $? -ne 0 ]; then
  echo "MKL_IFORT_NEWRIVER_IVYBRIDGE: Run error."
  exit 1
fi
rm fft_test
#
echo "MKL_IFORT_NEWRIVER_IVYBRIDGE: Normal end of execution."
exit 0
