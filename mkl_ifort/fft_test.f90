program main

!*****************************************************************************80
!
! Compile line:
! ifort -mkl /opt/intel/Compiler/11.1/069/mkl/include/mkl_dfti.f90  -O3 tfft.f90 -o tfft
! 1D complex to complex, and real to conjugate-even
!
  use mkl_dfti

  implicit none

  integer, parameter :: x_size = 32
  integer, parameter :: y_size = 32
  real cp
  integer i
  integer ic
  real rp
  integer status
  complex x(x_size)
  type ( dfti_descriptor ), pointer :: x_handle
!
!  May need 2 extra entries?
!
  real y(y_size+2)
  type ( dfti_descriptor ), pointer :: y_handle

  write ( *, '(a)' ) ''
  write ( *, '(a)' ) 'FFT_TEST:'
  write ( *, '(a)' ) '  FORTRAN90 version'
  write ( *, '(a)' ) '  Demonstrate MKL 1D FFT functions.'
!
!  Perform a complex to complex transform
!
  do i = 1, x_size
    x(i) = cmplx ( i * i )
  end do

  status = DftiCreateDescriptor ( x_handle, DFTI_SINGLE, DFTI_COMPLEX, 1, x_size )
  status = DftiCommitDescriptor ( x_handle )
  status = DftiComputeForward ( x_handle, x )
  status = DftiFreeDescriptor ( x_handle )
 
  write ( *, '(a)' ) ''
  write ( *, '(a)' ) '  Result of complex-to-complex FFT:'
  write ( *, '(a)' ) ''

  do i = 1, x_size
    write ( *, '(2x,i2,2(2x,g14.6))' ) i, x(i)
  end do
!
!  Perform a real to complex conjugate-even transform
!
  do i = 1, y_size
    y(i) = real ( i * i )
  end do

  status = DftiCreateDescriptor ( y_handle, DFTI_SINGLE, DFTI_REAL, 1, y_size )
  status = DftiCommitDescriptor ( y_handle )
  status = DftiComputeForward ( y_handle, y )
  status = DftiFreeDescriptor  ( y_handle )

  write ( *, '(a)' ) ''
  write ( *, '(a)' ) '  Result of real-to-complex FFT:'
  write ( *, '(a)' ) ''
  write ( *, '(a)' ) '            Real       Imaginary            Norm'
  write ( *, '(a)' ) ''

  ic = 0
  do i = 1, y_size + 2
    if ( ic == 0 ) then
      rp = y(i)
      ic = 1
    else
      cp = y(i)
      write ( *, '(2x,i2,3(2x,g14.6))' ) i - 1, rp, cp, sqrt ( rp ** 2 + cp ** 2 )
      ic = 0
    end if
  end do
!
!  Terminate.
!
  write ( *, '(a)' ) ''
  write ( *, '(a)' ) 'FFT_TEST:'
  write ( *, '(a)' ) '  Normal end of execution.'

  stop
end
