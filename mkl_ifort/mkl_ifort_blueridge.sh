#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load mkl/11.2.3
#
echo "MKL_IFORT_BLUERIDGE: Normal beginning of execution."
#
ifort -c -mkl $MKL_INC/mkl_dfti.f90 fft_test.f90
if [ $? -ne 0 ]; then
  echo "MKL_IFORT_BLUERIDGE: Compile error."
  exit 1
fi
#
ifort -mkl -o fft_test mkl_dfti.o fft_test.o
if [ $? -ne 0 ]; then
  echo "MKL_IFORT_BLUERIDGE: Load error."
  exit 1
fi
rm mkl_dfti.o
rm fft_test.o
#
./fft_test > mkl_ifort_blueridge.txt
if [ $? -ne 0 ]; then
  echo "MKL_IFORT_BLUERIDGE: Run error."
  exit 1
fi
rm fft_test
#
echo "MKL_IFORT_BLUERIDGE: Normal end of execution."
exit 0
