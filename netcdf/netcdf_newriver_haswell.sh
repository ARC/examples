#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load hdf5/1.8.16
module load netcdf/4.3.3.1
#
echo "NETCDF_NEWRIVER_HASWELL: Normal beginning of execution."
#
gcc -c -I$NETCDF_INC netcdf_test.c
if [ $? -ne 0 ]; then
  echo "NETCDF_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
gcc -o netcdf_test netcdf_test.o -L$NETCDF_LIB -lnetcdf
if [ $? -ne 0 ]; then
  echo "NETCDF_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm netcdf_test.o
#
./netcdf_test > netcdf_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "NETCDF_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm netcdf_test
#
echo "NETCDF_NEWRIVER_HASWELL: Normal end of execution."
exit 0
