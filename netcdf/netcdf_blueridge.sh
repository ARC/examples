#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load hdf5/1.8.8
module load netcdf/4.2
#
echo "NETCDF_BLUERIDGE: Normal beginning of execution."
#
gcc -c -I$NETCDF_INC netcdf_test.c
if [ $? -ne 0 ]; then
  echo "NETCDF_BLUERIDGE: Compile error!"
  exit 1
fi
#
gcc -o netcdf_test netcdf_test.o -L$NETCDF_LIB -lnetcdf
if [ $? -ne 0 ]; then
  echo "NETCDF_BLUERIDGE: Load error!"
  exit 1
fi
rm netcdf_test.o
#
./netcdf_test > netcdf_blueridge.txt
if [ $? -ne 0 ]; then
  echo "NETCDF_BLUERIDGE: Run error!"
  exit 1
fi
rm netcdf_test
#
echo "NETCDF_BLUERIDGE: Normal end of execution."
exit 0
