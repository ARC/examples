#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load hdf5/1.8.16
module load netcdf/4.3.3.1
#
echo "NETCDF_DRAGONSTOOTH: Normal beginning of execution."
#
gcc -c -I$NETCDF_INC netcdf_test.c
if [ $? -ne 0 ]; then
  echo "NETCDF_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
gcc -o netcdf_test netcdf_test.o -L$NETCDF_LIB -lnetcdf
if [ $? -ne 0 ]; then
  echo "NETCDF_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm netcdf_test.o
#
./netcdf_test > netcdf_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "NETCDF_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm netcdf_test
#
echo "NETCDF_DRAGONSTOOTH: Normal end of execution."
exit 0
