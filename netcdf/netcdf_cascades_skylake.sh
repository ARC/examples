#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load hdf5/1.8.17
module load netcdf/4.3.3.1
#
echo "NETCDF_CASCADES_SKYLAKE: Normal beginning of execution."
#
gcc -c -I$NETCDF_INC netcdf_test.c
if [ $? -ne 0 ]; then
  echo "NETCDF_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
gcc -o netcdf_test netcdf_test.o -L$NETCDF_LIB -lnetcdf
if [ $? -ne 0 ]; then
  echo "NETCDF_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm netcdf_test.o
#
./netcdf_test > netcdf_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "NETCDF_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm netcdf_test
#
echo "NETCDF_CASCADES_SKYLAKE: Normal end of execution."
exit 0
