#! /usr/bin/env python
#
def diff2_solve ( ):

#*****************************************************************************80
#
## DIFF2_SOLVE solves a linear system involving the second difference matrix.
#
#  Discussion:
#
#    Some discretized partial differential equations end up having the form:
#
#      A*x=b
#
#    where the matrix A is the negative of the second difference matrix,
#    a tridiagonal matrix with entries (-1,+2,-1).
#
#    This function demonstrates how a matrix A of order N can be set up
#    in the SCIPY Compressed Sparse Row (CSR) format, and the solution
#    computed using a direct solver.
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license. 
#
#  Modified:
#
#    23 April 2017
#
#  Author:
#
#    John Burkardt
#
#  Local Parameters:
#
#    Local, integer N, the number of rows and columns of the matrix.
#
#    Local, integer NZ, the number of nonzero entries in the matrix.
#
#    Local, real VAL(NZ), the nonzero entries of A, listed by row.
#
#    Local, integer COL(NZ), the columns of the nonzero entries.
#
#    Local, integer ROWPTR(N+1); ROWPTR(I) indicates the first entry in
#    VAL and COL for row I, I = 0 to N_1.  ROWPTR(N) indicates the location
#    that would have been used for the first entry in the fictitious row N.
#
  import numpy as np
  import scipy.sparse as sps
  import matplotlib
#
#  Force MATPLOTLIB NOT to use X-windows!
#
  matplotlib.use ( 'Agg' )
  import matplotlib.pyplot as plt
  from scipy.sparse.linalg.dsolve import linsolve

  print ( '' )
  print ( 'DIFF2_SOLVE:' )
  print ( '  Use SCIPY to solve a sparse linear system A*x=b.' )

  n = 10;
  nz = 3 * n - 2;
  val = np.zeros ( nz );
  col = np.zeros ( nz, dtype = np.int32 );
  rowptr = np.zeros ( n + 1, dtype = np.int32 );

  k = 0

  for i in range ( 0, n ):

    rowptr[i] = k

    if ( 0 < i ):
      val[k] = -1.0
      col[k] = i - 1
      k = k + 1

    val[k] = 2.0
    col[k] = i
    k = k + 1

    if ( i < n - 1 ):
      val[k] = -1.0
      col[k] = i + 1
      k = k + 1

  rowptr[n] = k
#
#  Construct the SCIPY CSR matrix from the data.
#
  A = sps.csr_matrix( ( val, col, rowptr ), shape = ( n, n ), \
    dtype = np.float64 )
#
#  Use SPY to display the matrix structure.
#
  A.todense ( )

  plt.clf ( )
  plt.spy ( A, marker = '.', markersize = 2 )
  filename = 'diff2_solve.png'
  plt.savefig ( filename )
#
#  SHOW is only for interactive use.
#
# plt.show ( )
#
#  Set up a special right hand side, (0,0,0,...,0,N+1),
#  for which the exact solution is (1,2,3,...,N).
#
  b = np.zeros ( n, dtype = np.float64 )
  b[n-1] = float ( n + 1 )
#
#  Use a direct solver to solve A*x=b.
#
  x = linsolve.spsolve ( A, b )  
#
#  Compute the error.
#
  print ( '' )
  print ( '  Norm of residual: %r' % np.linalg.norm ( A * x - b ) )
#
#  Terminate.
#
  print ( '' )
  print ( 'DIFF2_SOLVE:' )
  print ( '  Normal end of execution.' )

  return

if ( __name__ == '__main__' ):
  diff2_solve ( )
