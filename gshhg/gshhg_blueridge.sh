#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load hdf5/1.8.8
module load netcdf/4.2
module load gmt/5.0.0
module load gshhg/2.3.0
#
echo "GSHHG_BLUERIDGE: Normal beginning of execution."
#
#  By setting the verbosity level (-Vd), we can have pscoast
#  verify that it is accessing the GSHHG database.
#
pscoast -Vd -R-30/30/-40/40 -Jm0.1i -B5 -I1/1p,blue -N1/0.25p,- \
            -I2/0.25p,blue -W0.25p,white -Ggreen -Sblue -P > gsshg_blueridge.ps
if [ $? -ne 0 ]; then
  echo "GSHHG_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "GSHHG_BLUERIDGE: Normal end of execution."
exit 0
