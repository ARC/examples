#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=2:ppn=24:gpus=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/2.1.3
module load mkl/11.2.3
module load gsl/2.4
module load cuda/9.1.85
module load lammps/11Aug17
#
echo "LAMMPS_CASCADES_SKYLAKE: Normal beginning of execution."
#
mpirun -np 48 lmp_mpi < in.lj > lammps_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "LAMMPS_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "LAMMPS_CASCADES_SKYLAKE: Normal end of execution."
exit 0
