#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=12
#PBS -W group_list=newriver
#pBS -A arctest
#PBS -q p100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load mvapich2/2.1
module load mkl/11.2.3
module load gsl/2.4
module load lammps/11Aug17 
#
echo "LAMMPS_NEWRIVER_BROADWELL: Normal beginning of execution."
#
mpirun -np 24 lmp_mpi < in.lj > lammps_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "LAMMPS_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
#
echo "LAMMPS_NEWRIVER_BROADWELL: Normal end of execution."
exit 0
