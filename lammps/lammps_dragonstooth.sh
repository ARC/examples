#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=2:ppn=24
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/16.1
module load openmpi/1.10.2
module load mkl/11.2.3
module load gsl/2.4
module load lammps/11Aug17
#
echo "LAMMPS_DRAGONSTOOTH: Normal beginning of execution."
#
mpirun -np 48 lmp_mpi < in.lj > lammps_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "LAMMPS_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "LAMMPS_DRAGONSTOOTH: Normal end of execution."
exit 0
