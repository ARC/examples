#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=2:ppn=24
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/2.0.0
module load mkl/11.2.3
module load gsl/2.4
module load lammps/11Aug17
#
echo "LAMMPS_CASCADES_BROADWELL: Normal beginning of execution."
#
mpirun -np 48 lmp_mpi < in.lj > lammps_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "LAMMPS_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "LAMMPS_CASCADES_BROADWELL: Normal end of execution."
exit 0
