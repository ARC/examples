#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=2:ppn=24
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load mvapich2/2.1
module load mkl/11.2.3
module load gsl/2.4
module load lammps/11Aug17 
#
echo "LAMMPS_NEWRIVER_HASWELL: Normal beginning of execution."
#
mpirun -np 48 lmp_mpi < in.lj > lammps_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "LAMMPS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "LAMMPS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
