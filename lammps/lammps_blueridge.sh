#! /bin/bash
#
#PBS -l walltime=00:10:00
#PBS -l nodes=2:ppn=16
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/2.0.0
module load mkl/11.2.3
module load lammps/11Aug17 
#
echo "LAMMPS_BLUERIDGE: Normal beginning of execution."
#
mpirun -np 32 lmp_mpi < in.lj > lammps_blueridge.txt
if [ $? -ne 0 ]; then
  echo "LAMMPS_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "LAMMPS_BLUERIDGE: Normal end of execution."
exit 0
