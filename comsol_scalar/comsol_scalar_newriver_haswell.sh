#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load comsol/5.2.1.229
#
echo "COMSOL_SCALAR_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  This simple script runs COMSOL on 1 node and 1 processor, that is,
#  as a seral job.  
#
comsol batch -inputfile BeamModel.mph -outputfile BeamSolution.mph > comsol_scalar_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "COMSOL_SCALAR_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "COMSOL_SCALAR_NEWRIVER_HASWELL: Normal end of execution."
exit 0
