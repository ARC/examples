#! /usr/bin/env python
#
import numpy
import theano
import theano.tensor as T

print ( "" )
print ( "THEANO_TEST:" )
print ( "  Example program for THEANO" )
print ( "  involving logistic regression." )

rng = numpy.random
#
#  Training sample size.
#
N = 400
#
#  Number of input variables.
#
feats = 784
#
#  Generate a dataset: D = (input_values, target_class)
#
D = ( rng.randn ( N, feats ), rng.randint ( size = N, low = 0, high = 2 ) )
training_steps = 10000
#
#  Declare Theano symbolic variables.
#
x = T.dmatrix ( "x" )
y = T.dvector ( "y" )
#
#  Initialize the weight vector w randomly, and set the bias to 0.
#
#  These are shared so they keep their values between training iterations.
#
w = theano.shared ( rng.randn ( feats ), name = "w" )
b = theano.shared ( 0.0, name = "b" )

print ( "Initial model:" )
print ( w.get_value() )
print ( b.get_value() )
#
#  Construct Theano expression graph.
#
#  Probability that target = 1.
#
p_1 = 1 / ( 1 + T.exp(-T.dot(x, w) - b) )
#
#  The prediction threshold.
#
prediction = p_1 > 0.5
#
#  Cross-entropy loss function.
#
xent = -y * T.log(p_1) - (1-y) * T.log(1-p_1)
#
#  The cost to minimize.
#
cost = xent.mean() + 0.01 * ( w ** 2 ).sum()
#
#  Compute the gradient of the cost with respect to
#  gw, the weight vector, and
#  g, the bias term.
#
gw, gb = T.grad ( cost, [w, b] )
#
#  Compile
#
train = theano.function (
  inputs = [x,y],
  outputs = [prediction, xent],
  updates = ((w, w - 0.1 * gw), (b, b - 0.1 * gb)) )

predict = theano.function ( inputs = [x], outputs = prediction )
#
#  Train
#
for i in range ( training_steps ):
  pred, err = train ( D[0], D[1] )
#
#  Report:
#
print ( "Final model:" )
print ( w.get_value() )
print ( b.get_value() )
print ( "Target values for D:" )
print ( D[1] )
print ( "Prediction on D:" )
print ( predict(D[0]) )
#
#  Terminate.
#
print ( "" )
print ( "THEANO_TEST:" )
print ( "  Normal end of execution." )
