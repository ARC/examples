#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/16.1
module load python/2.7.10
module load mkl/11.2.3
module load cuda/8.0.44
module load pycuda/2016.1.2
module load theano/0.8.2
#
echo "THEANO_BLUERIDGE: Normal beginning of execution."
#
chmod u+x theano_test.py
#
./theano_test.py > theano_blueridge.txt
if [ $? -ne 0 ]; then
  echo "THEANO_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "THEANO_BLUERIDGE: Normal end of execution."
exit 0
