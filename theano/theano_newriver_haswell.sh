#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load python/2.7.10
module load mkl/11.2.3
module load numpy/1.10.1
module load scipy/0.16.1
module load cuda/8.0.44
module load pycuda/2016.1.2
module load theano/0.8.2
#
echo "THEANO_NEWRIVER_HASWELL: Normal beginning of execution."
#
chmod u+x theano_test.py
#
./theano_test.py > theano_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "THEANO_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "THEANO_NEWRIVER_HASWELL: Normal end of execution."
exit 0
