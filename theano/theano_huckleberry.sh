#! /bin/bash
#
#SBATCH -J gcc_huckleberry
#SBATCH -p normal_q
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -t 00:05:00
#SBATCH --mem=100M
#
cd $SLURM_SUBMIT_DIR
#
module purge
module load theano/0.8.2-3ibm2
#
echo "THEANO_HUCKLEBERRY: Normal beginning of execution."
#
chmod u+x theano_test.py
#
./theano_test.py > theano_huckleberry.txt
if [ $? -ne 0 ]; then
  echo "THEANO_HUCKLEBERRY: Run error!"
  exit 1
fi
#
echo "THEANO_HUCKLEBERRY: Normal end of execution."
exit 0
