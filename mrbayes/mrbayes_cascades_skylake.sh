#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/7.3.0
module load mvapich2/2.2
module load beagle-lib/2.1.2
module load mrbayes/3.2.5
#
echo "MRBAYES_CASCADES_SKYLAKE: Normal beginning of execution."
#
#  Run using 4 processes.
#  (There must be at least as many chains as MPI processes, and
#   we only have 7 chains in this example.)
#
mpirun -np 4 $MRBAYES_BIN/mb < primates_input.txt > mrbayes_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "MRBAYES_CASCADES_SKYLAKE: Run error."
  exit 1
fi
#
echo "MRBAYES_CASCADES_SKYLAKE: Normal end of execution."
exit 0
