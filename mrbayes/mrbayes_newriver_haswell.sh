#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.8.5
module load beagle-lib/2.1.2
module load mrbayes/3.2.5
#
echo "MRBAYES_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Run using 4 processes.
#  (There must be at least as many chains as MPI processes, and
#   we only have 7 chains in this example.)
#
mpiexec -np 4 mb < primates_input.txt > mrbayes_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "MRBAYES_NEWRIVER_HASWELL: Run error."
  exit 1
fi
#
echo "MRBAYES_NEWRIVER_HASWELL: Normal end of execution."
exit 0
