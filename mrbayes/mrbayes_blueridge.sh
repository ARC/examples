#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/13.1
module load openmpi/1.6.5
module load beagle-lib/r1092
module load mrbayes/3.2.1
#
echo "MRBAYES_BLUERIDGE: Normal beginning of execution."
#
#  Run using 4 processes.
#  (There must be at least as many chains as MPI processes, and
#   we only have 7 chains in this example.)
#
mpirun -np 4 $MRBAYES_BIN/mb < primates_input.txt > mrbayes_blueridge.txt
if [ $? -ne 0 ]; then
  echo "MRBAYES_BLUERIDGE: Run error."
  exit 1
fi
#
echo "MRBAYES_BLUERIDGE: Normal end of execution."
exit 0
