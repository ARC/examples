#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1:gpus=1
#PBS -W group_list=newriver
#PBS -q p100_dev_q
#PBS -A arctest
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
#
echo "P100_DEV_Q_NEWRIVER_BROADWELL: Normal beginning of execution."
echo "P100_DEV_Q_NEWRIVER_BROADWELL: Job running in queue "$PBS_QUEUE
#
gcc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "P100_DEV_Q_NEWRIVER_BROADWELL: Compile error!"
  exit 1
fi
#
gcc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "P100_DEV_Q_NEWRIVER_BROADWELL: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > p100_dev_q_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "P100_DEV_Q_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
rm heated_plate
#
echo "P100_DEV_Q_NEWRIVER_BROADWELL: Normal end of execution."
exit 0

