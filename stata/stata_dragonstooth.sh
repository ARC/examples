#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load stata/14.0.0
#
echo "STATA_DRAGONSTOOTH: Normal beginning of execution."
#
stata < stata_input.txt > stata_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "STATA_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "STATA_DRAGONSTOOTH: Normal end of execution."
exit 0

