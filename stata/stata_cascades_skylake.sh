#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load stata/14.0.0
#
echo "STATA_CASCADES_SKYLAKE: Normal beginning of execution."
#
stata < stata_input.txt > stata_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "STATA_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "STATA_CASCADES_SKYLAKE: Normal end of execution."
exit 0

