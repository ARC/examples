#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load stata/14.0.0
#
echo "STATA_NEWRIVER_HASWELL: Normal beginning of execution."
#
stata < stata_input.txt > stata_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "STATA_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "STATA_NEWRIVER_HASWELL: Normal end of execution."
exit 0

