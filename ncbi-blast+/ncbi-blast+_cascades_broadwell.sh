#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/2.0.0
module load python/2.7.10
module load boost/1.58.0
module load phdf5/1.8.16
module load ncbi-blast+/2.4.0
#
echo "NCBI-BLAST+_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Unzip the database.
#
echo "Unzipping the database."
gunzip drosoph.nt.gz
#
#  Reformat the database.
#
echo "Reformat the database."
makeblastdb -in drosoph.nt -dbtype nucl -parse_seqids
if [ $? -ne 0 ]; then
  echo "NCBI-BLAST+_CASCADES_BROADWELL: Run error."
  exit 1
fi
#
#  Run ncbi-blast+ against the test sequence file blast.in
#
echo "Running BLASTN."
blastn -query blast.in -db drosoph.nt -task blastn -out ncbi-blast+_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "NCBI-BLAST+_CASCADES_BROADWELL: Run error."
  exit 1
fi
#
#  Rezip the database.
#
echo "Zipping the database."
gzip drosoph.nt
#
echo "NCBI-BLAST+_CASCADES_BROADWELL: Normal end of execution."
exit 0
