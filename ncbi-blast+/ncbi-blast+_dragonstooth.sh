#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load ncbi-blast+/2.4.0
#
echo "NCBI-BLAST+_DRAGONSTOOTH: Normal beginning of execution."
#
#  Unzip the database.
#
echo "Unzipping the database."
gunzip drosoph.nt.gz
#
#  Reformat the database.
#
makeblastdb -in drosoph.nt -dbtype nucl -parse_seqids
if [ $? -ne 0 ]; then
  echo "NCBI-BLAST+_DRAGONSTOOTH: Run error."
  exit 1
fi
#
#  Run ncbi-blast+ against the test sequence file blast.in
#
echo "Running BLASTN."
blastn -query blast.in -db drosoph.nt -task blastn -out ncbi-blast+_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "NCBI-BLAST+_DRAGONSTOOTH: Run error."
  exit 1
fi
#
#  Rezip the database.
#
echo "Zipping the database."
gzip drosoph.nt
#
echo "NCBI-BLAST+_DRAGONSTOOTH: Normal end of execution."
exit 0
