#! /bin/bash
#
#SBATCH -J anaconda2_huckleberry
#SBATCH -p normal_q
#SBATCH -N 1
#SBATCH -t 00:05:00
#SBATCH --mem=100M
#
cd $SLURM_SUBMIT_DIR
#
module purge
module load anaconda2/4.4.0.1
#
echo "ANACONDA2_HUCKLEBERRY: Normal beginning of execution."
#
python fd1d_heat_implicit.py > anaconda2_huckleberry.txt
if [ $? -ne 0 ]; then
  echo "ANACONDA2_HUCKLEBERRY: Run error!"
  exit 1
fi
#
echo "ANACONDA2_HUCKLEBERRY: Normal end of execution."
exit 0
