#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.1.0
module load openmpi/1.8.4
module load atlas/3.10.2
module load lua/5.1.4.5
module load glm/0.9.4.3
module load python/2.7.10
module load boost/1.58.0
module load hdf5/1.8.15
module load phdf5/1.8.15
module load p4est/1.1
module load trilinos/11.12.1
module load dealii/8.2.0
module load aspect/1.3
#
echo "ASPECT_BLUERIDGE: Normal beginning of execution."
#
aspect stokes.prm > aspect_blueridge.txt
if [ $? -ne 0 ]; then
  echo "ASPECT_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "ASPECT_BLUERIDGE: Normal end of execution."
exit 0
