#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.8.5
#module load openmpi/1.10.2
module load atlas/3.11.36
module load lua/5.1.4.8
module load glm/0.9.4.3
module load python/2.7.10
module load boost/1.58.0
module load hdf5/1.8.16
module load phdf5/1.8.15
module load netcdf/4.3.3.1
module load p4est/1.1
module load trilinos/12.8.1
module load dealii/8.4.1
module load aspect/1.5.0rc1
#
echo "ASPECT_NEWRIVER_HASWELL: Normal beginning of execution."
#
aspect stokes.prm > aspect_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "ASPECT_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "ASPECT_NEWRIVER_HASWELL: Normal end of execution."
exit 0
