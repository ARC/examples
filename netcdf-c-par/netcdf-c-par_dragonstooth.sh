#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load openmpi/1.10.2
module load phdf5/1.8.16
module load parallel-netcdf/1.7.0
module load netcdf-c-par/4.4.0
#
echo "NETCDF-C-PAR_DRAGONSTOOTH: Normal beginning of execution."
#
mpicc -c -I$PNETCDF_INC -I$NETCDF_C_INC writer.c
if [ $? -ne 0 ]; then
  echo "NETCDF-C-PAR_DRAGONSTOOTH: Compile error."
  exit 1
fi
#
mpicc -o writer writer.o -L$NETCDF_C_LIB -L$PNETCDF_LIB -L$PHDF5_LIB -lnetcdf -lpnetcdf -lhdf5_hl -lhdf5 -lz
if [ $? -ne 0 ]; then
  echo "NETCDF-C-PAR_DRAGONSTOOTH: Load error."
  exit 1
fi
rm writer.o
#
mpirun -np 4 ./writer writer.nc
if [ $? -ne 0 ]; then
  echo "NETCDF-C-PAR_DRAGONSTOOTH: Run error."
  exit 1
fi
rm writer
#
echo "NETCDF-C-PAR_DRAGONSTOOTH: Normal end of execution."
exit 0
