#! /bin/bash
#
#  submit_huckleberry.sh
#
#  Purpose:
#
#    Run short program tests on the ARC Huckleberry cluster.
#
#  Discussion:
#
#    Revised to run with the new location of the examples.
#
#  Example:
#
#    To execute this script, log onto any Huckleberry login node, and type:
#
#      ./submit_huckleberry.sh
#
#    This should submit all the jobs.  Later, run "reports_huckleberry.sh"
#    to examine the results.
#
#  Modified:
#
#    09 February 2018
#
#  Author:
#
#    John Burkardt
#
date
echo ""
echo "SUBMIT_HUCKLEBERRY:"
echo "  Submit test suite jobs on Huckleberry."
#
#  Override queue choice and group_list in job files.
#
SBATCHFLAGS='--output test_suite_huckleberry.txt'
#
cd /home/burkardt/examples
#
#  Identify current directory.
#
BASE=$(pwd)
echo ""
echo "  Working from base directory $BASE"
#
#  Clear out all old report files.
#
rm */test_suite_huckleberry.txt
#
#  Program list.
#
PROGRAMS="
anaconda2
cuda
gcc
mpich
normal_q
openblas
openmp
slurm
theano
torch
"
#
#  For each program:
#    move to that directory
#    remove any old copy of the results file
#    submit the job
#    go back to base
#
for PROG in $PROGRAMS; do
  cd $PROG
  rm -f test_suite_huckleberry.txt
  echo "Submit "$PROG"_huckleberry.sh"
  sbatch $SBATCHFLAGS $PROG"_huckleberry.sh"
  cd $BASE
done
#
echo ""
echo "  Once jobs are run, list outcomes by:"
echo ""
echo "    tail -q -n 1 */test_suite_huckleberry.txt"
echo ""
echo "  or simply run:"
echo ""
echo "    ./report_huckleberry.sh"
#
#  Terminate.
#
echo ""
echo "SUBMIT_HUCKLEBERRY: Normal end of execution."
exit 0

