#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/7.3.0
module load openblas/0.2.20
module load harminv/1.4.1
#
echo "HARMINV_CASCADES_SKYLAKE: Normal beginning of execution."
#
harminv < co2.txt 0.05-0.15 > harminv_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "HARMINV_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "HARMINV_CASCADES_SKYLAKE: Normal end of execution."
exit 0

