#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openblas/0.2.14
module load harminv/1.4.1
#
echo "HARMINV_DRAGONSTOOTH: Normal beginning of execution."
#
harminv < co2.txt 0.05-0.15 > harminv_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "HARMINV_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "HARMINV_DRAGONSTOOTH: Normal end of execution."
exit 0

