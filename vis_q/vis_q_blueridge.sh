#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q vis_q
#PBS -A arctest
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.3.0
#
echo "VIS_Q_BLUERIDGE: Normal beginning of execution."
echo "VIS_Q_BLUERIDGE: Job running in queue "$PBS_QUEUE
#
gcc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "VIS_Q_BLUERIDGE: Compile error!"
  exit 1
fi
#
gcc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "VIS_Q_BLUERIDGE: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > vis_q_blueridge.txt
if [ $? -ne 0 ]; then
  echo "VIS_Q_BLUERIDGE: Run error!"
  exit 1
fi
rm heated_plate
#
echo "VIS_Q_BLUERIDGE: Normal end of execution."
exit 0

