#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR

module purge
module load gcc/5.2.0
module load ncl/6.2.0
#
echo "NCL_NEWRIVER_HASWELL: Normal beginning of execution."
#
ncl ncl_test.ncl
if [ $? -ne 0 ]; then
  echo "NCL_NEWRIVER_HASWELL: Run error."
  exit 1
fi
#
echo "NCL_NEWRIVER_HASWELL: Normal end of execution."
exit 0

