#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe

cd $PBS_O_WORKDIR

module purge
module load gcc/5.2.0
module load ncl/6.2.0
#
echo "NCL_DRAGONSTOOTH: Normal beginning of execution."
#
ncl ncl_test.ncl
if [ $? -ne 0 ]; then
  echo "NCL_DRAGONSTOOTH: Run error."
  exit 1
fi
#
echo "NCL_DRAGONSTOOTH: Normal end of execution."
exit 0

