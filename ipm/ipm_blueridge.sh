#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/13.1
module load mvapich2/1.9a2
module load ipm/0.983
module load papi/4.2.0
#
echo "IPM_BLUERIDGE: Normal beginning of execution."
#
#  Compile the program with the Intel MPI compiler.
#
export IPM_INCLUDE=/opt/apps/intel13_1/mvapich21_9a2/ipm/0.983/include
export IPM_LIB=/opt/apps/intel13_1/mvapich21_9a2/ipm/0.983/lib
#
mpicc -c -I$IPM_INCLUDE laplace.c
if [ $? -ne 0 ]; then
  echo "IPM_BLUERIDGE: Compile error."
  exit 1
fi
#
mpicc -o laplace laplace.o -L$IPM_LIB -L$PAPI_LIB -lipm -lpapi
if [ $? -ne 0 ]; then
  echo "IPM_BLUERIDGE: Load error."
  exit 1
fi
rm laplace.o
#
mpirun -np 4 ./laplace > ipm_blueridge.txt
#
#  Comment out this check, since MPIRUN is generating bogus errors.
#f [ $? -ne 0 ]; then
#  echo "IPM_BLUERIDGE: Run error."
#  exit 1
#fi
rm laplace
#
echo "IPM_BLUERIDGE: Normal end of execution."
exit 0
