#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load pgi/17.5
#
echo "PGF90_NEWRIVER_HASWELL: Normal beginning of execution."
#
pgf90 -c heated_plate.f90
if [ $? -ne 0 ]; then
  echo "PGF90_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
pgf90 -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "PGF90_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > pgf90_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "PGF90_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm heated_plate
#
echo "PGF90_NEWRIVER_HASWELL: Normal end of execution."
exit 0

