#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load pgi/17.5
#
echo "PGF90_DRAGONSTOOTH: Normal beginning of execution."
#
pgf90 -c heated_plate.f90
if [ $? -ne 0 ]; then
  echo "PGF90_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
pgf90 -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "PGF90_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > pgf90_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "PGF90_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm heated_plate
#
echo "PGF90_DRAGONSTOOTH: Normal end of execution."
exit 0

