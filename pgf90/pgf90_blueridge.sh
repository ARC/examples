#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load pgi/13.8
#
echo "PGF90_BLUERIDGE: Normal beginning of execution."
#
pgf90 -c heated_plate.f90
if [ $? -ne 0 ]; then
  echo "PGF90_BLUERIDGE: Compile error!"
  exit 1
fi
#
pgf90 -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "PGF90_BLUERIDGE: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > pgf90_blueridge.txt
if [ $? -ne 0 ]; then
  echo "PGF90_BLUERIDGE: Run error!"
  exit 1
fi
rm heated_plate
#
echo "PGF90_BLUERIDGE: Normal end of execution."
exit 0

