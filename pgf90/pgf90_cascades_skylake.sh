#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load pgi
module spider pgi
#
echo "PGF90_CASCADES_SKYLAKE: Normal beginning of execution."
#
pgf90 -c heated_plate.f90
if [ $? -ne 0 ]; then
  echo "PGF90_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
pgf90 -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "PGF90_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > pgf90_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "PGF90_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm heated_plate
#
echo "PGF90_CASCADES_SKYLAKE: Normal end of execution."
exit 0

