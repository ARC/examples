#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load autodocksuite/4.2.3
#
echo "AUTODOCKSUITE_BLUERIDGE: Normal beginning of execution."
#
#  Define the autodocksuite executable directory.
#
AD_BIN=/opt/apps/gcc4_7/autodocksuite/4.2.3/bin
#
#  Run autogrid4.
#    input is hsg1.gpf and hsg1.pdbqt
#
$AD_BIN/autogrid4 -p hsg1.gpf -l hsg1.glg
if [ $? -ne 0 ]; then
  echo "AUTODOCKSUITE_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "AUTODOCKSUITE_BLUERIDGE: Normal end of execution."
exit 0
