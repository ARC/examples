#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load autodocksuite/4.2.6
#
echo "AUTODOCKSUITE_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Define the autodocksuite executable directory.
#
AD_BIN=/opt/apps/gcc5_2/autodocksuite/4.2.6/bin
#
#  Run autogrid4.
#    input is hsg1.gpf and hsg1.pdbqt
#
$AD_BIN/autogrid4 -p hsg1.gpf -l hsg1.glg
if [ $? -ne 0 ]; then
  echo "AUTODOCKSUITE_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "AUTODOCKSUITE_NEWRIVER_HASWELL: Normal end of execution."
exit 0
