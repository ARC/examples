#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load autodocksuite/4.2.6
#
echo "AUTODOCKSUITE_CASCADES_SKYLAKE: Normal beginning of execution."
#
#  Run autogrid4.
#    input is hsg1.gpf and hsg1.pdbqt
#
autogrid4 -p hsg1.gpf -l hsg1.glg
if [ $? -ne 0 ]; then
  echo "AUTODOCKSUITE_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "AUTODOCKSUITE_CASCADES_SKYLAKE: Normal end of execution."
exit 0
