#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load gnuplot/5.0.0
#
echo "GNUPLOT_NEWRIVER_BROADWELL: Normal beginning of execution."
#
gcc -c damped_sine.c
if [ $? -ne 0 ]; then
  echo "GNUPLOT_NEWRIVER_BROADWELL: Compile error."
  exit 1
fi
#
gcc -o damped_sine damped_sine.o -lm
if [ $? -ne 0 ]; then
  echo "GNUPLOT_NEWRIVER_BROADWELL: Load error."
  exit 1
fi
rm damped_sine.o
#
./damped_sine > gnuplot_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "GNUPLOT_NEWRIVER_BROADWELL: Run error."
  exit 1
fi
rm damped_sine
#
#
#  Have gnuplot read the commands file, 
#  which tells it to process the data file,
#  and create the plot file.
#
ls damped_sine_data.txt
ls damped_sine_commands.txt
gnuplot < damped_sine_commands.txt
if [ $? -ne 0 ]; then
  echo "GNUPLOT_NEWRIVER_BROADWELL: Run error."
  exit 1
fi
#
ls damped_sine.svg
#
echo "GNUPLOT_NEWRIVER_BROADWELL: Normal end of execution."
exit 0
