#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load gnuplot/4.4.4
#
echo "GNUPLOT_BLUERIDGE: Normal beginning of execution."
#
gcc -c damped_sine.c
if [ $? -ne 0 ]; then
  echo "GNUPLOT_BLUERIDGE: Compile error."
  exit 1
fi
#
gcc -o damped_sine damped_sine.o -lm
if [ $? -ne 0 ]; then
  echo "GNUPLOT_BLUERIDGE: Load error."
  exit 1
fi
rm damped_sine.o
#
./damped_sine > gnuplot_blueridge.txt
if [ $? -ne 0 ]; then
  echo "GNUPLOT_BLUERIDGE: Run error."
  exit 1
fi
rm damped_sine
#
#
#  Have gnuplot read the commands file, 
#  which tells it to process the data file,
#  and create the plot file.
#
gnuplot < damped_sine_commands.txt
if [ $? -ne 0 ]; then
  echo "GNUPLOT_BLUERIDGE: Run error."
  exit 1
fi
#
ls damped_sine.svg
#
echo "GNUPLOT_BLUERIDGE: Normal end of execution."
exit 0
