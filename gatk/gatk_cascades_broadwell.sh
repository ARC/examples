#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load jdk/1.8.0
module load gatk/3.4.46
#
echo "GATK_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Should find 33 reads.
#
echo ""
echo "Count reads (should be 33)"
echo ""
#
java -jar $GATK_DIR/GenomeAnalysisTK.jar \
  -T CountReads \
  -R exampleFASTA.fasta \
  -I exampleBAM.bam
if [ $? -ne 0 ]; then
  echo "GATK_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
#  Count the number of loci; should find 2052.
#
echo ""
echo "Count loci (should be 2052)"
echo ""
#
java -jar $GATK_DIR/GenomeAnalysisTK.jar \
  -T CountLoci \
  -R exampleFASTA.fasta \
  -I exampleBAM.bam \
  -o gatk_cascades.txt
if [ $? -ne 0 ]; then
  echo "GATK_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo ""
echo "Print loci count from file:"
echo ""
#
cat gatk_cascades.txt
#
echo "GATK_CASCADES_BROADWELL: Normal end of execution."
exit 0
