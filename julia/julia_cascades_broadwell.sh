#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load julia/0.6.2
#
echo "JULIA_CASCADES_BROADWELL: Normal beginning of execution."
#
julia laplace.jl
if [ $? -ne 0 ]; then
  echo "JULIA_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "JULIA_CASCADES_BROADWELL: Normal end of execution."
exit 0
