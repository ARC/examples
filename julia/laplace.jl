function lshape(N)
    # make sure N is even
    #N = round(Int, N/2) * 2
    # create a zero-array (the `Int` tells Julia to make it integers)
    L = zeros(Int, N+2, N+2)
    # fill in the "domain" with ones
    # note the ÷ : this is integer division 
    #    (Julia does not like indexing with non-integers)
    L[2:N ÷ 2+1,2:N+1] = 1
    L[2:N+1,N ÷ 2+2:N+1] = 1
    # return the completed array
    return L
end
#
# generate a mesh for demonstration
#
L = lshape(10)
println(L)

function fdlaplacian(G)
    # read info about G
    M, N = size(G)
    nonz = find(G)
    # define function to create a 1D laplacian and a sparse identity
    fdl1(m) = spdiagm((ones(m-1),-2*ones(m),ones(m-1)), [-1,0,1])
    # laplace operator on the full grid
    A = kron(speye(M), fdl1(N)) + kron(fdl1(M), speye(N))
    # return the restriction to the coloured grid points
    return A[nonz, nonz]
end
#
# assemble the right-hand side
#
N = 60
L = lshape(N)
A = - fdlaplacian(L)
b = ones(size(A,1)) / N^2
# solve the linear system;
u = A \ b;

println("Normal end of execution." )