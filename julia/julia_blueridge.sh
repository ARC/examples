#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load atlas/3.10.2
module load python/2.7.10
module load julia/0.3.8
#
echo "JULIA_BLUERIDGE: Normal beginning of execution."
#
julia laplace.jl
if [ $? -ne 0 ]; then
  echo "JULIA_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "JULIA_BLUERIDGE: Normal end of execution."
exit 0
