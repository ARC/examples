#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load atlas/3.11.36
module load python/2.7.10
module load julia/0.5.0
#
echo "JULIA_DRAGONSTOOTH: Normal beginning of execution."
#
julia laplace.jl
if [ $? -ne 0 ]; then
  echo "JULIA_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "JULIA_DRAGONSTOOTH: Normal end of execution."
exit 0
