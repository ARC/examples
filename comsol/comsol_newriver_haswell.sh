#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=2:ppn=4
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load comsol/5.2.1.229
#
echo "COMSOL_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Based on the above "#PBS nodes=???:ppn=???" statement, these three 
#  statements determine:
#    PROC_NUM, the number of processors
#    NODE_NUM, the number of nodes (servers)
#    CORE_NUM, the number of cores per processor (1, as we are not doing OpenMP)
#
PROC_NUM=`wc -l < $PBS_NODEFILE`
NODE_NUM=`uniq $PBS_NODEFILE | wc -l`
CORE_NUM=1
#
#  This one statement is broken up by backslashes for readability.
#
comsol batch                      \
  -nn $PROC_NUM                   \
  -nnhost $NODE_NUM               \
  -np $CORE_NUM                   \
  -f $PBS_NODEFILE                \
  -inputfile BeamModel.mph        \
  -outputfile BeamSolution.mph    \
  -batchlog BeamModel.log > comsol_newriver_haswell.txt
#
if [ $? -ne 0 ]; then
  echo "COMSOL_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "COMSOL_NEWRIVER_HASWELL: Normal end of execution."
exit 0
