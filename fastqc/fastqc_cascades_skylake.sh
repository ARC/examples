#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load jdk/1.8.0
module load fastqc/0.11.3
#
echo "FASTQC_CASCADES_SKYLAKE: Normal beginning of execution."
#
fastqc test.fastq
if [ $? -ne 0 ]; then
  echo "FASTQC_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "FASTQC_CASCADES_SKYLAKE: Normal end of execution."
exit 0
