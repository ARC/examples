#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load jdk/1.7.0
module load fastqc/0.11.3
#
echo "FASTQC_DRAGONSTOOTH: Normal beginning of execution."
#
fastqc test.fastq
if [ $? -ne 0 ]; then
  echo "FASTQC_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "FASTQC_DRAGONSTOOTH: Normal end of execution."
exit 0
