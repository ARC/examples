#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load atlas/3.10.2
#
echo "BLAS_ATLAS_CASCADES_SKYLAKE: Normal beginning of execution."
#
gfortran -c blas_test.f90
if [ $? -ne 0 ]; then
  echo "BLAS_ATLAS_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
gfortran -o blas_atlas_test blas_test.o -L$ATLAS_LIB -llapack -lptf77blas -ltatlas -lgfortran -lm
if [ $? -ne 0 ]; then
  echo "BLAS_ATLAS_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm blas_test.o
#
./blas_atlas_test > blas_atlas_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "BLAS_ATLAS_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm blas_atlas_test
#
echo "BLAS_ATLAS_CASCADES_SKYLAKE: Normal end of execution."
exit 0
