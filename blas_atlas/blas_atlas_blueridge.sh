#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load atlas/3.10.2
#
echo "BLAS_ATLAS_BLUERIDGE: Normal beginning of execution."
#
gfortran -c blas_test.f90
if [ $? -ne 0 ]; then
  echo "BLAS_ATLAS_BLUERIDGE: Compile error!"
  exit 1
fi
#
gfortran -o blas_atlas_test blas_test.o -L$ATLAS_LIB -llapack -lptf77blas -ltatlas -lgfortran -lm
if [ $? -ne 0 ]; then
  echo "BLAS_ATLAS_BLUERIDGE: Load error!"
  exit 1
fi
rm blas_test.o
#
./blas_atlas_test > blas_atlas_blueridge.txt
if [ $? -ne 0 ]; then
  echo "BLAS_ATLAS_BLUERIDGE: Run error!"
  exit 1
fi
rm blas_atlas_test
#
echo "BLAS_ATLAS_BLUERIDGE: Normal end of execution."
exit 0
