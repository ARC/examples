#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load eigen
#
echo "EIGEN_DRAGONSTOOTH: Normal beginning of execution."
#
g++ -c -I$EIGEN_INC eigen_test.cpp
if [ $? -ne 0 ]; then
  echo "EIGEN_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
g++ -o eigen_test eigen_test.o
if [ $? -ne 0 ]; then
  echo "EIGEN_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm eigen_test.o
#
./eigen_test > eigen_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "EIGEN_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm eigen_test
#
echo "EIGEN_DRAGONSTOOTH: Normal end of execution."
exit 0
