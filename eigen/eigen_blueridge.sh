#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load eigen
#
echo "EIGEN_BLUERIDGE: Normal beginning of execution."
#
g++ -c -I$EIGEN_INC eigen_test.cpp
if [ $? -ne 0 ]; then
  echo "EIGEN_BLUERIDGE: Compile error!"
  exit 1
fi
#
g++ -o eigen_test eigen_test.o
if [ $? -ne 0 ]; then
  echo "EIGEN_BLUERIDGE: Load error!"
  exit 1
fi
rm eigen_test.o
#
./eigen_test > eigen_blueridge.txt
if [ $? -ne 0 ]; then
  echo "EIGEN_BLUERIDGE: Run error!"
  exit 1
fi
rm eigen_test
#
echo "EIGEN_BLUERIDGE: Normal end of execution."
exit 0
