# include <iostream>
# include <Eigen/Dense>

using namespace Eigen;
using namespace std;

int main ( );
void test01 ( );
void test02 ( );
void test03 ( );
void jacobiSvd_test ( );
void colPivHouseholderQr_test ( );
void normal_equations_test ( );

//****************************************************************************80

int main ( )

//****************************************************************************80
//
//  Purpose:
//
//    MAIN is the main program for EIGEN_TEST.
//
//  Modified:
//
//    08 March 2017
//
{
  cout << "\n";
  cout <<"EIGEN_TEST:\n";
  cout << "  EIGEN is a C++ linear algebra template library.\n";

  test01 ( );
  test02 ( );
  test03 ( );
  jacobiSvd_test ( );
  colPivHouseholderQr_test ( );
  normal_equations_test ( );
//
//  Terminate.
//
  cout << "\n";
  cout <<"EIGEN_TEST:\n";
  cout << "  Normal end of execution.\n";

  return 0;
}
//****************************************************************************80

void test01 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST01 simply declares, assigns, and prints a 2x2 matrix.
//
//  Modified:
//
//    08 March 2017
//
{
  MatrixXd m(2,2);

  cout << "\n";
  cout <<"TEST01:\n";
  cout << "  Declare, assign and print a 2x2 matrix.\n";

  m(0,0) = 3.0;
  m(1,0) = 2.5;
  m(0,1) = -1.0;
  m(1,1) = m(1,0) + m(0,1);

  cout << "\n";
  cout << "  2X2 example of EIGEN MatrixXd matrix:\n";
  cout << "\n";

  cout << m << "\n";

  return;
}
//****************************************************************************80

void test02 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST02 computes a matrix/vector product using a variable spatial dimension.
//
//  Modified:
//
//    08 March 2017
//
{
  srand ( 123456789 );

  MatrixXd m = MatrixXd::Random ( 3, 3 );

  m = ( m + MatrixXd::Constant ( 3, 3, 1.2 ) ) * 50;

  cout << "\n";
  cout << "TEST02:\n";
  cout << "  Compute a matrix/vector product\n";
  cout << "  using a variable spatial dimension.\n";
  cout << "\n";
  cout << "  m: " << "\n";
  cout << m << "\n";

  VectorXd v(3);
  v << 1, 2, 3;
  cout << "\n";
  cout << "  v: " << "\n";
  cout << v << "\n";

  cout << "\n";
  cout << "  m * v:\n";
  VectorXd mv = m * v;
  cout << mv << "\n";

  return;
}
//****************************************************************************80

void test03 ( )

//****************************************************************************80
//
//  Purpose:
//
//    TEST03 computes a matrix/vector product using dimension 3 explicitly.
//
//  Modified:
//
//    08 March 2017
//
{
  srand ( 123456789 );

  Matrix3d m = Matrix3d::Random ( );

  m = ( m + Matrix3d::Constant ( 1.2 ) ) * 50;

  cout << "\n";
  cout << "TEST03:\n";
  cout << "  Compute a matrix/vector product\n";
  cout << "  using dimension 3 explicitly.\n";
  cout << "\n";
  cout << "  m: " << "\n";
  cout << m << "\n";

  Vector3d v(1,2,3);
  cout << "\n";
  cout << "  v: " << "\n";
  cout << v << "\n";

  cout << "\n";
  cout << "  m * v:\n";
  cout << m * v << "\n";

  return;
}
//****************************************************************************80

void jacobiSvd_test ( )

//****************************************************************************80
//
//  Purpose:
//
//    JACOBISVD_TEST uses jacobiSvd.
//
//  Modified:
//
//    08 March 2017
//
{
  MatrixXf A;
  VectorXf b;
  VectorXf r;
  VectorXf x;

  cout << "\n";
  cout <<"JACOBISVD_TEST:\n";
  cout << "  Least squares solution of A*x=b using jacobiSvd.\n";

  srand ( 123456789 );

  A = MatrixXf::Random ( 3, 2 );

  cout << "\n";
  cout << "  A:\n";
  cout << "\n";
  cout << A << "\n";;

  b = VectorXf::Random ( 3 );

  cout << "\n";
  cout << "  b:\n";
  cout << "\n";
  cout << b << "\n";;

  x = A.jacobiSvd ( ComputeThinU | ComputeThinV ).solve ( b );

  cout << "\n";
  cout << "  x:\n";
  cout << "\n";
  cout << x << "\n";

  r = A * x - b;

  cout << "\n";
  cout << "  r = A * x - b:\n";
  cout << "\n";
  cout << r << "\n";;

  return;
}
//****************************************************************************80

void colPivHouseholderQr_test ( )

//****************************************************************************80
//
//  Purpose:
//
//    COLPIVHOUSEHOLDERQR_TEST uses colPivHouseholderQr.
//
//  Modified:
//
//    08 March 2017
//
{
  MatrixXf A;
  VectorXf b;
  VectorXf r;
  VectorXf x;

  cout << "\n";
  cout <<"COLPIVHOUSEHOLDERQR_TEST:\n";
  cout << "  Least squares solution of A*x=b using colPivHouseholderQr.\n";

  srand ( 123456789 );

  A = MatrixXf::Random ( 3, 2 );

  cout << "\n";
  cout << "  A:\n";
  cout << "\n";
  cout << A << "\n";;

  b = VectorXf::Random ( 3 );

  cout << "\n";
  cout << "  b:\n";
  cout << "\n";
  cout << b << "\n";;

  x = A.colPivHouseholderQr ( ).solve ( b );

  cout << "\n";
  cout << "  x:\n";
  cout << "\n";
  cout << x << "\n";

  r = A * x - b;

  cout << "\n";
  cout << "  r = A * x - b:\n";
  cout << "\n";
  cout << r << "\n";;

  return;
}
//****************************************************************************80

void normal_equations_test ( )

//****************************************************************************80
//
//  Purpose:
//
//    NORMAL_EQUATIONS_TEST uses the normal equations.
//
//  Modified:
//
//    08 March 2017
//
{
  MatrixXf A;
  VectorXf b;
  VectorXf r;
  VectorXf x;

  cout << "\n";
  cout <<"NORMAL_EQUATIONS_TEST:\n";
  cout << "  Least squares solution of A*x=b using normal equations.\n";

  srand ( 123456789 );

  A = MatrixXf::Random ( 3, 2 );

  cout << "\n";
  cout << "  A:\n";
  cout << "\n";
  cout << A << "\n";;

  b = VectorXf::Random ( 3 );

  cout << "\n";
  cout << "  b:\n";
  cout << "\n";
  cout << b << "\n";;

  x = ( A.transpose ( ) * A ).ldlt().solve ( A.transpose ( ) * b );

  cout << "\n";
  cout << "  x:\n";
  cout << "\n";
  cout << x << "\n";

  r = A * x - b;

  cout << "\n";
  cout << "  r = A * x - b:\n";
  cout << "\n";
  cout << r << "\n";;

  return;
}
