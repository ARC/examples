#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load eigen/3.2.10
#
echo "EIGEN_CASCADES_SKYLAKE: Normal beginning of execution."
#
g++ -c -I$EIGEN_INC eigen_test.cpp
if [ $? -ne 0 ]; then
  echo "EIGEN_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
g++ -o eigen_test eigen_test.o
if [ $? -ne 0 ]; then
  echo "EIGEN_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm eigen_test.o
#
./eigen_test > eigen_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "EIGEN_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm eigen_test
#
echo "EIGEN_CASCADES_SKYLAKE: Normal end of execution."
exit 0
