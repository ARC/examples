#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load eigen
#
echo "EIGEN_NEWRIVER_HASWELL: Normal beginning of execution."
#
g++ -c -I$EIGEN_INC eigen_test.cpp
if [ $? -ne 0 ]; then
  echo "EIGEN_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
g++ -o eigen_test eigen_test.o
if [ $? -ne 0 ]; then
  echo "EIGEN_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm eigen_test.o
#
./eigen_test > eigen_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "EIGEN_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm eigen_test
#
echo "EIGEN_NEWRIVER_HASWELL: Normal end of execution."
exit 0
