#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q largemem_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load eigen/3.2.5
#
echo "EIGEN_NEWRIVER_IVYBRIDGE: Normal beginning of execution."
#
g++ -c -I$EIGEN_INC eigen_test.cpp
if [ $? -ne 0 ]; then
  echo "EIGEN_NEWRIVER_IVYBRIDGE: Compile error!"
  exit 1
fi
#
g++ -o eigen_test eigen_test.o
if [ $? -ne 0 ]; then
  echo "EIGEN_NEWRIVER_IVYBRIDGE: Load error!"
  exit 1
fi
rm eigen_test.o
#
./eigen_test > eigen_newriver_ivybridge.txt
if [ $? -ne 0 ]; then
  echo "EIGEN_NEWRIVER_IVYBRIDGE: Run error!"
  exit 1
fi
rm eigen_test
#
echo "EIGEN_NEWRIVER_IVYBRIDGE: Normal end of execution."
exit 0
