#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load Anaconda/2.3.0
#
echo "ANACONDA_DRAGONSTOOTH: Normal beginning of execution."
#
python fd1d_heat_implicit.py > anaconda_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "ANACONDA_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "ANACONDA_DRAGONSTOOTH: Normal end of execution."
exit 0
