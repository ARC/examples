#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load Anaconda/5.1.0
#
echo "ANACONDA_CASCADES_SKYLAKE: Normal beginning of execution."
#
python fd1d_heat_implicit.py > anaconda_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "ANACONDA_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "ANACONDA_CASCADES_SKYLAKE: Normal end of execution."
exit 0
