#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load Anaconda/5.1.0
#
echo "ANACONDA_NEWRIVER_BROADWELL: Normal beginning of execution."
#
python fd1d_heat_implicit.py > anaconda_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "ANACONDA_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
#
echo "ANACONDA_NEWRIVER_BROADWELL: Normal end of execution."
exit 0
