#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.3.0
module load Anaconda/2.3.0
#
echo "ANACONDA_BLUERIDGE: Normal beginning of execution."
#
python fd1d_heat_implicit.py > anaconda_blueridge.txt
if [ $? -ne 0 ]; then
  echo "ANACONDA_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "ANACONDA_BLUERIDGE: Normal end of execution."
exit 0
