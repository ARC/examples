#! /bin/bash
#
#PBS -lwalltime=00:05:00
#PBS -lnodes=1:ppn=24
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
##PBS -joe
#
cd $PBS_O_WORKDIR
#
module purge
module load matlab

echo "MATLAB_PARALLEL_CASCADES_SKYLAKE: Normal beginning of execution."

matlab -r prime_batch
if [ $? -ne 0 ]; then
  echo "MATLAB_PARALLEL_CASCADES_SKYLAKE: Run error!"
  exit 1
fi


echo "MATLAB_PARALLEL_CASCADES_SKYLAKE: Normal end of execution."
