#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/13.1
module load mkl/11
module load openmpi/1.6.5
module load hpl/2.0
#
echo "HPL_BLUERIDGE: Normal beginning of execution."
#
#  Run the program with 8 processes.
#
mpirun -np 8 xhpl > hpl_blueridge.txt
if [ $? -ne 0 ]; then
  echo "HPL_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "HPL_BLUERIDGE: Normal end of execution."
exit 0
