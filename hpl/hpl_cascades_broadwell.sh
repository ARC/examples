#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/3.0.0
module load openblas/0.2.20
module load hpl/2.2
#
echo "HPL_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Run the program with 8 processes.
#
mpirun -np 8 xhpl >> hpl_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "HPL_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "HPL_CASCADES_BROADWELL: Normal end of execution."
exit 0
