#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.10.2
module load hpl/2.1
#
echo "HPL_DRAGONSTOOTH: Normal beginning of execution."
#
#  Run the program with 8 processes.
#
mpirun -np 8 xhpl >> hpl_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "HPL_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "HPL_DRAGONSTOOTH: Normal end of execution."
exit 0
