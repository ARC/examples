#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.8.5
module load hpl/2.1
#
echo "HPL_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Run the program with 8 processes.
#
mpirun -np 8 xhpl >> hpl_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "HPL_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "HPL_NEWRIVER_HASWELL: Normal end of execution."
exit 0
