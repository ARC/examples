#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load yasm/1.3
module load fdk-aac/1.0
#
echo "FDK-AAC_CASCADES_SKYLAKE: Normal beginning of execution."
#
gcc -c -I$FDK_AAC_INC/fdk-aac aac-enc.c
if [ $? -ne 0 ]; then
  echo "FDK-AAC_CASCADES_SKYLAKE: Compile error."
  exit 1
fi
#
gcc -c -I$FDK_AAC_INC wavreader.c
if [ $? -ne 0 ]; then
  echo "FDK-AAC_CASCADES_SKYLAKE: Compile error."
  exit 1
fi
#
gcc -o aac-enc aac-enc.o wavreader.o -L$FDK_AAC_LIB -lfdk-aac -lm
if [ $? -ne 0 ]; then
  echo "FDK-AAC_CASCADES_SKYLAKE: Load error."
  exit 1
fi
rm aac-enc.o
rm wavreader.o
#
#  Convert a WAV file to AAC format.
#
./aac-enc bach.wav bach.aac
if [ $? -ne 0 ]; then
  echo "FDK-AAC_CASCADES_SKYLAKE: Run error."
  exit 1
fi
rm aac-enc
#
echo "FDK-AAC_CASCADES_SKYLAKE: Normal end of execution."
exit 0
