#! /bin/bash
#
#  submit_cascades_skylake.sh
#
#  Purpose:
#
#    Run short tests of programs on the Skylake nodes of ARC Cascades cluster.
#
#  Discussion:
#
#    Revised to run with the new location of the examples.
#
#  Example:
#
#    To execute this script, log onto any Cascades login node, and type:
#
#      ./submit_cascades_skylake.sh
#
#    This should submit all the jobs.  Later, run 
#
#      ./reports_cascades_skylake.sh
#
#    to examine the results.
#
#  Modified:
#
#    07 June 2018
#
#  Author:
#
#    John Burkardt
#
date
echo ""
echo "SUBMIT_CASCADES_SKYLAKE:"
echo "  Submit test suite jobs on Cascades Skylake nodes."
#
cd /home/burkardt/examples
#
#  Identify current directory.
#
BASE=$(pwd)
echo ""
echo "  Working from base directory $BASE"
#
#  Clear out all old report files.
#
rm -f */test_suite_cascades_skylake.txt
#
#  Program #1 list.
#
PROGRAMS1="
abaqus
abinit
abyss
anaconda
ansys
apbs-static
atlas
autodocksuite
automake
bamtools
bcftools
beagle-lib
bedtools
blas_atlas
blas_mkl
boost
bowtie
bowtie2
bzip2
cddlib
clapack
cmake
cora
cuda
cuda_fortran
ea-utils
eigen
fastqc
fdk-aac
ffmpeg
fftw
flann
flint
gaussian
gcc
gromacs
gsl
harminv
hdf5
hmmer
hpl
intel
jdk
lame
lammps
lapack_atlas
lapack_mkl
libjpeg-turbo
lua
luajit
matlab
matlab_parallel
metis
minia
mkl
mpe2
mpip
mrbayes
mvapich2
namd
namd-gpu
nastran
netcdf
openblas
openmp
openmpi
opensees
p4est
p7zip
papi
parallel
parallel-netcdf
parmetis
pbs
perl
pgi
phdf5
pigz
proj
python
samtools
scipy
scons
seqtk
singular
sox
spades
sparsehash
stata
szip
tecplot
tophat
trimmomatic
viennacl
x264
yasm
zip
zlib"
#
#  For each program:
#    move to that directory
#    remove any old copy of the results file
#    submit the job
#    go back to base
#
#
#  Override queue choice and group_list in job files.
#
QSUBFLAGS1='-qv100_large_q -Aarctest -Wgroup_list=arcadm -otest_suite_cascades_skylake.txt'

for PROG in $PROGRAMS1; do
  cd $PROG
  rm -f test_suite_cascades.txt
  echo "Submit "$PROG"_cascades_skylake.sh"
  qsub $QSUBFLAGS1 $PROG"_cascades_skylake.sh"
  cd $BASE
done
#
#  Program #2 list.
#
PROGRAMS2="
v100_dev_q
v100_normal_q
v100_large_q"
#
#  For each program:
#    move to that directory
#    remove any old copy of the results file
#    submit the job
#    go back to base
#
#
#  Do not override queue choice for program list #2.
#
QSUBFLAGS2='-Aarctest -Wgroup_list=arcadm -otest_suite_cascades_skylake.txt'

for PROG in $PROGRAMS2; do
  cd $PROG
  rm -f test_suite_cascades.txt
  echo "Submit "$PROG"_cascades_skylake.sh"
  qsub $QSUBFLAGS2 $PROG"_cascades_skylake.sh"
  cd $BASE
done
#
echo ""
echo "  Once jobs are run, list outcomes by:"
echo ""
echo "    tail -q -n 1 */test_suite_cascades_skylake.txt"
echo ""
echo "  or simply run:"
echo ""
echo "    ./report_cascades_skylake.sh"
#
#  Terminate.
#
echo ""
echo "SUBMIT_CASCADES_SKYLAKE: Normal end of execution."
exit 0

