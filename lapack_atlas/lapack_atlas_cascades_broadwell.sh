#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load atlas/3.11.34
#
echo "LAPACK_ATLAS_CASCADES_BROADWELL: Normal beginning of execution."
#
gfortran -c lapack_test.f90
if [ $? -ne 0 ]; then
  echo "LAPACK_ATLAS_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
gfortran -o lapack_atlas_test lapack_test.o -L$ATLAS_LIB -llapack -lptf77blas -ltatlas -lgfortran -lm
if [ $? -ne 0 ]; then
  echo "LAPACK_ATLAS_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm lapack_test.o
#
./lapack_atlas_test > lapack_atlas_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "LAPACK_ATLAS_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm lapack_atlas_test
#
echo "LAPACK_ATLAS_CASCADES_BROADWELL: Normal end of execution."
exit 0
