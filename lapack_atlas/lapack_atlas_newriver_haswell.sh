#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load atlas/3.11.36
#
echo "LAPACK_ATLAS_NEWRIVER_HASWELL: Normal beginning of execution."
#
gfortran -c lapack_test.f90
if [ $? -ne 0 ]; then
  echo "LAPACK_ATLAS_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
gfortran -o lapack_atlas_test lapack_test.o -L$ATLAS_LIB -llapack -lptf77blas -ltatlas -lgfortran -lm
if [ $? -ne 0 ]; then
  echo "LAPACK_ATLAS_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm lapack_test.o
#
./lapack_atlas_test > lapack_atlas_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "LAPACK_ATLAS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm lapack_atlas_test
#
echo "LAPACK_ATLAS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
