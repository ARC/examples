#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load bedtools/2.25.0
#
echo "BEDTOOLS_BLUERIDGE: Normal beginning of execution."
#
#  Find the base pair overlap between the CPG and GWAS files.
#
bedtools intersect -a cpg.bed -b gwas.bed > bedtools_blueridge.txt
if [ $? -ne 0 ]; then
  echo "BEDTOOLS_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "BEDTOOLS_BLUERIDGE: Normal end of execution."
exit 0
