#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load bedtools/2.25.0
#
echo "BEDTOOLS_CASCADES_SKYLAKE: Normal beginning of execution."
#
#  Find the base pair overlap between the CPG and GWAS files.
#
bedtools intersect -a cpg.bed -b gwas.bed > bedtools_cascade_skylake.txt
if [ $? -ne 0 ]; then
  echo "BEDTOOLS_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "BEDTOOLS_CASCADES_SKYLAKE: Normal end of execution."
exit 0
