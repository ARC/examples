#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load bedtools/2.25.0
#
echo "BEDTOOLS_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Find the base pair overlap between the CPG and GWAS files.
#
bedtools intersect -a cpg.bed -b gwas.bed > bedtools_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "BEDTOOLS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "BEDTOOLS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
