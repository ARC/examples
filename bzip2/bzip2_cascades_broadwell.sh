#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
echo "BZIP2_CASCADES_BROADWELL: Normal beginning of execution."
#
rm bzip2_cascades_broadwell.txt
touch bzip2_cascades_broadwell.txt
#
#  TXT
#
ls -l robinson_crusoe.txt >> bzip2_cascades_broadwell.txt
bzip2 robinson_crusoe.txt
if [ $? -ne 0 ]; then
  echo "BZIP2_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
ls -l robinson_crusoe.txt.bz2 >> bzip2_cascades_broadwell.txt
bunzip2 robinson_crusoe.txt.bz2
if [ $? -ne 0 ]; then
  echo "BZIP2_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
ls -l robinson_crusoe.txt >> bzip2_cascades_broadwell.txt
#
#  PDF
#
ls -l grimm.pdf >> bzip2_cascades_broadwell.txt
bzip2 grimm.pdf
if [ $? -ne 0 ]; then
  echo "BZIP2_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
ls -l grimm.pdf.bz2 >> bzip2_cascades_broadwell.txt
bunzip2 grimm.pdf.bz2
if [ $? -ne 0 ]; then
  echo "BZIP2_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
ls -l grimm.pdf >> bzip2_cascades_broadwell.txt
#
#  PPM binary, uncompressed bitmap
#
ls -l underwater_bmx.ppm >> bzip2_cascades_broadwell.txt
bzip2 underwater_bmx.ppm
if [ $? -ne 0 ]; then
  echo "BZIP2_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
ls -l underwater_bmx.ppm.bz2 >> bzip2_cascades_broadwell.txt
bunzip2 underwater_bmx.ppm.bz2
if [ $? -ne 0 ]; then
  echo "BZIP2_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
ls -l underwater_bmx.ppm >> bzip2_cascades_broadwell.txt
#
#  TAR, an archive file
#
ls -l bzip2.tar >> bzip2_cascades_broadwell.txt
bzip2 bzip2.tar
if [ $? -ne 0 ]; then
  echo "BZIP2_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
ls -l bzip2.tar.bz2 >> bzip2_cascades_broadwell.txt
bunzip2 bzip2.tar.bz2
if [ $? -ne 0 ]; then
  echo "BZIP2_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
ls -l bzip2.tar >> bzip2_cascades_broadwell.txt
#
echo "BZIP2_CASCADES_BROADWELL: Normal end of execution."
exit 0
