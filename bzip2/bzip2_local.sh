#! /bin/bash
#
rm bzip2_local.txt
touch bzip2_local.txt
#
#  TXT
#
ls -l robinson_crusoe.txt >> bzip2_local.txt
bzip2 robinson_crusoe.txt
ls -l robinson_crusoe.txt.bz2 >> bzip2_local.txt
bunzip2 robinson_crusoe.txt.bz2
ls -l robinson_crusoe.txt >> bzip2_local.txt
#
#  PDF
#
ls -l grimm.pdf >> bzip2_local.txt
bzip2 grimm.pdf
ls -l grimm.pdf.bz2 >> bzip2_local.txt
bunzip2 grimm.pdf.bz2
ls -l grimm.pdf >> bzip2_local.txt
#
#  PPM binary, uncompressed bitmap
#
ls -l underwater_bmx.ppm >> bzip2_local.txt
bzip2 underwater_bmx.ppm
ls -l underwater_bmx.ppm.bz2 >> bzip2_local.txt
bunzip2 underwater_bmx.ppm.bz2
ls -l underwater_bmx.ppm >> bzip2_local.txt
#
#  TAR, an archive file
#
ls -l bzip2.tar >> bzip2_local.txt
bzip2 bzip2.tar
ls -l bzip2.tar.bz2 >> bzip2_local.txt
bunzip2 bzip2.tar.bz2
ls -l bzip2.tar >> bzip2_local.txt
#
echo "Normal end of execution."
