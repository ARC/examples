#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load ls-dyna/8.0.0
#
echo "LS-DYNA_DRAGONSTOOTH: Normal beginning of execution."
#
# Define environment variables for license server.
#
export LSTC_LICENSE=network
export LSTC_LICENSE_SERVER=lstc.software.vt.edu
#
$LSDYNA_SMP_BIN/ls-dyna_smp_d_r8_0_0_x64_suse11_pgi134 i = plate.typ3.k > ls-dyna_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "LS-DYNA_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "LS-DYNA_DRAGONSTOOTH: Normal end of execution."
exit 0
