#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load ls-dyna/7.0.0
#
echo "LS-DYNA_BLUERIDGE: Normal beginning of execution."
#
# Define environment variables for license server.
#
export LSTC_LICENSE=network
export LSTC_LICENSE_SERVER=lstc.software.vt.edu
#
$LSDYNA_SMP_BIN/ls-dyna_smp_d_r7_0_0_x64_suse11_pgi105 i = plate.typ3.k > ls-dyna_blueridge.txt
if [ $? -ne 0 ]; then
  echo "LS-DYNA_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "LS-DYNA_BLUERIDGE: Normal end of execution."
exit 0
