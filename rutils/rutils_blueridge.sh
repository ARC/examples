#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load Rutils/3.2.0
module load R/3.2.0
#
echo "RUTILS_BLUERIDGE: Normal beginning of execution."
#
R --vanilla < adist_test.r > rutils_blueridge.txt
if [ $? -ne 0 ]; then
  echo "RUTILS_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "RUTILS_BLUERIDGE: Normal end of execution."
exit 0
