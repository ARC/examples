#
#  ADIST performs approximate string matching.
#
#  The "distance" counts the number of deletions, insertions and
#  substitutions necessary to convert one string into another.
#
adist ( 'GTAGCGGCG', 'GTAACGGCG' )
adist ( 'Muscle', 'mussel' )
adist ( 'ant', 'aunt' )
adist ( 'boy', 'girl' )
adist ( 'palindrome', 'emordnilap' )
adist ( '12345', '12three45' )