#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load Rutils/3.2.2
module load openblas/0.2.14
module load R/3.3.1
#
echo "RUTILS_DRAGONSTOOTH: Normal beginning of execution."
#
R --vanilla < adist_test.r > rutils_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "RUTILS_DRAGOONSTOOTH: Run error!"
  exit 1
fi
#
echo "RUTILS_DRAGONSTOOTH: Normal end of execution."
exit 0
