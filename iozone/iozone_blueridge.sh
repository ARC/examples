#! /bin/bash
#
#PBS -l walltime=0:20:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load iozone/3
#
echo "IOZONE_BLUERIDGE: Normal beginning of execution."
#
#  Move to $TMP to avoid hitting file quotas.
#
cd $TMP
#
iozone -a > $PBS_O_WORKDIR/iozone_blueridge.txt
if [ $? -ne 0 ]; then
  echo "IOZONE_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "IOZONE_BLUERIDGE: Normal end of execution."
exit 0
