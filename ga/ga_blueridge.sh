#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=2
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
#  This job will FAIL using gcc or openmpi because of weird missing
#  library functions.
#
module purge
module load gcc/4.7.2
#module load intel/13.1
module load openmpi/1.6.5
#module load mvapich2/1.9a2
module load ga/5.3
#
echo "GA_BLUERIDGE: Normal beginning of execution."
#
mpicc -c -I$GA_INC matrix_multiply.c
if [ $? -ne 0 ]; then
  echo "GA_BLUERIDGE: Compile error!"
  exit 1
fi
#
mpicc -o matrix_multiply matrix_multiply.o -L$GA_LIB -lga -larmci -lgfortran
if [ $? -ne 0 ]; then
  echo "GA_BLUERIDGE: Load error!"
  exit 1
fi
rm matrix_multiply.o
#
mpirun -np 2 ./matrix_multiply > ga_blueridge.txt
if [ $? -ne 0 ]; then
  echo "GA_BLUERIDGE: Run error!"
  exit 1
fi
rm matrix_multiply
#
echo "GA_BLUERIDGE: Normal end of execution."
exit 0
