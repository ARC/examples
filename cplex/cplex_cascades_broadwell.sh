#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load cplex/12.6.2
#
echo "CPLEX_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Feed CPLEX the input.
#
cplex < input.txt > cplex_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "CPLEX_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "CPLEX_CASCADES_BROADWELL: Normal end of execution."
exit 0
