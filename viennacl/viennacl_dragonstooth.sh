#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load cuda/8.0.44
module load ViennaCL/1.7.1
#
echo "VIENNACL_DRAGONSTOOTH: Normal beginning of execution."
#
#nvcc -o matrix-free -I$VIENNACL_DIR -DVIENNACL_WITH_CUDA matrix-free.cu
nvcc -o matrix-free -I$VIENNACL_DIR  matrix-free.cu
if [ $? -ne 0 ]; then
  echo "VIENNACL_DRAGONSTOOTH: Compile/Load error!"
  exit 1
fi
#
./matrix-free > viennacl_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "VIENNACL_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm matrix-free
#
echo "VIENNACL_DRAGONSTOOTH: Normal end of execution."
exit 0
