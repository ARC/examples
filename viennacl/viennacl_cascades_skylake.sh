#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1:gpus=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load cuda/8.0.61
module load ViennaCL/1.7.1
#
echo "VIENNACL_CASCADES_SKYLAKE: Normal beginning of execution."
#
nvcc -o matrix-free -I$VIENNACL_DIR -DVIENNACL_WITH_CUDA matrix-free.cu
if [ $? -ne 0 ]; then
  echo "VIENNACL_CASCADES_SKYLAKE: Compile/Load error!"
  exit 1
fi
#
./matrix-free > viennacl_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "VIENNACL_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm matrix-free
#
echo "VIENNACL_CASCADES_SKYLAKE: Normal end of execution."
exit 0
