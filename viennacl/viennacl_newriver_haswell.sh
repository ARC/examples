#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1:gpus=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load cuda/8.0.61
module load ViennaCL/1.7.1
#
echo "VIENNACL_NEWRIVER_HASWELL: Normal beginning of execution."
#
nvcc -o matrix-free -I$VIENNACL_DIR -DVIENNACL_WITH_CUDA matrix-free.cu
if [ $? -ne 0 ]; then
  echo "VIENNACL_NEWRIVER_HASWELL: Compile/Load error!"
  exit 1
fi
#
./matrix-free > viennacl_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "VIENNACL_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm matrix-free
#
echo "VIENNACL_NEWRIVER_HASWELL: Normal end of execution."
exit 0
