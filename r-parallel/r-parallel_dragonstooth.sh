#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=16
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/1.10.2
module load hdf5/1.8.16
module load netcdf-c/4.4.0
module load mkl/11.2.3
module load R/3.4.1
module load R-parallel/3.4.1
#
echo "R-PARALLEL_DRAGONSTOOTH: Normal beginning of execution."
#
Rscript mh_snow.r > r-parallel_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "R-PARALLEL_DRAGONSTOOTH: Load error!"
  exit 1
fi
#
echo "R-PARALLEL_DRAGONSTOOTH: Normal end of execution."
exit 0

