#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=16
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load openmpi/1.8.5
module load hdf5/1.8.16
module load netcdf/4.3.3.1
module load mkl/11.2.3
module load R/3.3.1
module load R-parallel/3.3.1
#
echo "R-PARALLEL_NEWRIVER_HASWELL: Normal beginning of execution."
#
Rscript mh_snow.r > r-parallel_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "R-PARALLEL_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
#
echo "R-PARALLEL_NEWRIVER_HASWELL: Normal end of execution."
exit 0

