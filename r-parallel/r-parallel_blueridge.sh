#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=16
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge

module load intel/13.1
module load openmpi/1.8.4
module load hdf5/1.8.8
module load netcdf/4.2
module load R/3.2.0
module load R-parallel/3.2.0
#
echo "R-PARALLEL_BLUERIDGE: Normal beginning of execution."
#
Rscript mh_snow.r > r-parallel_blueridge.txt
if [ $? -ne 0 ]; then
  echo "R-PARALLEL_BLUERIDGE: Load error!"
  exit 1
fi
#
echo "R-PARALLEL_BLUERIDGE: Normal end of execution."
exit 0

