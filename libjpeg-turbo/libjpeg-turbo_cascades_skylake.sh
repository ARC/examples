#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load libjpeg-turbo/1.5.1
#
echo "LIBJPEG-TURBO_SKYLAKE: Normal beginning of execution."
#
gcc -c -I$LIBJPEG_TURBO_INC libjpeg-turbo_test.c
if [ $? -ne 0 ]; then
  echo "LIBJPEG-TURBO_SKYLAKE: Compile error."
  exit 1
fi
#
gcc -o libjpeg-turbo_test libjpeg-turbo_test.o -L$LIBJPEG_TURBO_LIB -ljpeg -lturbojpeg -lm
if [ $? -ne 0 ]; then
  echo "LIBJPEG-TURBO_SKYLAKE: Load error."
  exit 1
fi
#
rm libjpeg-turbo_test.o
#
./libjpeg-turbo_test > libjpeg-turbo_test_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "LIBJPEG-TURBO_SKYLAKE: Run error."
  exit 1
fi
rm libjpeg-turbo_test
#
echo "LIBJPEG-TURBO_SKYLAKE: Normal end of execution."
exit 0
