# include <math.h>
# include <setjmp.h>
# include <stdio.h>
# include <stdlib.h>

# include "jerror.h"
# include "jpeglib.h"

int main ( );
int image_data_compare ( int image_width, int image_height, char *image_data1,
  char *image_data2 );
char *image_data_example ( int image_width, int image_height );
int read_jpeg_file ( int *image_width, int *image_height, char **image_data,
  char *filename );
void write_jpeg_file ( int image_width, int image_height, char *image_data, 
  char *filename, int quality );

/******************************************************************************/

int main ( )

/******************************************************************************/
/*
  Purpose:

    MAIN is the main program for LIBJPEG-TURBO_TEST.

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    28 April 2017

  Author:

    John Burkardt
*/
{
  char filename1[] = "image1.jpg";
  char filename2[] = "image2.jpg";
  char *image_data1;
  char *image_data2;
  int image_height;
  int image_height2;
  int image_width;
  int image_width2;
  int quality;
  int total;
  int value;

  printf ( "\n" );
  printf ( "LIBJPEG-TURBO_TEST:\n" );
  printf ( "  Demonstrate the use of the LIBJPEG-TURBO library.\n" );
  printf ( "\n" );
  printf ( "  Note that even 100%% quality JPEG files are LOSSY.\n" );
  printf ( "  Thus, if we write data to a JPEG file at 100%% quality\n" );
  printf ( "  and then read it back, we will NOT get the same data!\n" );
  printf ( "  However, visually, the difference will be indetectable.\n" );
  printf ( "\n" );
/*
  Set up test RGB data.
*/
  image_width = 360;
  image_height = 480;
  image_data1 = image_data_example ( image_width, image_height );

  printf ( "  Set up %d by %d example RGB data array.\n", 
    image_width, image_height );
/*
  Write the computed data to a JPEG file.
*/
  quality = 100;
  write_jpeg_file ( image_width, image_height, image_data1, 
    filename1, quality );
  printf ( "  Wrote the computed RGB data to file '%s'.\n", filename1 );
/*
  Read the data from a JPEG file.
*/
  read_jpeg_file ( &image_width2, &image_height2, &image_data2, filename1 );
  printf ( "  Reading %d by %d image file\n", image_width2, image_height2 );
/*
  Compare the data we wrote to the data we recovered.
*/
  value = image_data_compare ( image_width, image_height, image_data1, 
    image_data2 );

  total = 3 * image_width * image_height;
  printf ( "  Files differ in %d RGB values out of %d.\n", 
    value, total );
/*
  Write the recovered data to a JPEG file.
*/
  quality = 100;
  write_jpeg_file ( image_width, image_height, image_data2, 
    filename2, quality );
  printf ( "  Wrote the recovered RGB data to file '%s'.\n", filename2 );

  printf ( "\n" );
  printf ( "  On visual comparison, the two JPEG files will seem identical.\n" );
/*
  Free memory.
*/
  free ( image_data1 );
  free ( image_data2 );
/*
  Terminate.
*/
  printf ( "\n" );
  printf ( "LIBJPEG-TURBO_TEST:\n" );
  printf ( "  Normal end of execution.\n" );

  return 0;
}
/******************************************************************************/

int image_data_compare ( int image_width, int image_height, char *image_data1,
  char *image_data2 )

/******************************************************************************/
/*
  Purpose:

    IMAGE_DATA_COMPARE compares two equal size arrays of RGB data.

  Discussion:

    Even at 100% quality, JPEG applies compression to the input data.
    Thus, JPEG is always a lossy format, and data recovered from a JPEG
    file will not precisely match its input values.

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    27 April 2017

  Author:

    John Burkardt

  Parameters:

    Input, int IMAGE_WIDTH, the width of the image in pixels.
 
    Input, int IMAGE_HEIGHT, the height of the image in pixels.

    Input, char *IMAGE_DATA1, *IMAGE_DATA_2, two arrays of 
    3 * IMAGE_WIDTH * IMAGE_HEIGHT RGB values to be compared.

    Output, int IMAGE_DATA_EXAMPLE, the number of RGB values that differed.
*/
{
  int i;
  int j;
  int k;
  int l;
  int value = 0;

  l = 0;
  for ( i = 0; i < image_height; i++ )
  {
    for ( j = 0; j < image_width; j++ ) 
    {
      for ( k = 0; k < 3; k++ )
      {
        if ( image_data1[l] != image_data2[l] )
        {
          value = value + 1;
        }
        l = l + 1;
      }
    }
  }

  return value;
}
/******************************************************************************/

char *image_data_example ( int image_width, int image_height )

/******************************************************************************/
/*
  Purpose:

    IMAGE_DATA_EXAMPLE sets up some RGB data.

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    26 April 2017

  Author:

    John Burkardt

  Parameters:

    Input, int IMAGE_WIDTH, the width of the image in pixels.
 
    Input, int IMAGE_HEIGHT, the height of the image in pixels.

    Output, char *IMAGE_DATA_EXAMPLE, 3 * IMAGE_WIDTH * IMAGE_HEIGHT RGB values.
*/
{
  float f1;
  float f2;
  float f3;
  int i;
  char *image_data;
  int j;
  char *k;
  const float r4_pi = 3.14159265;
  float x;
  float y;

  image_data = ( char * ) 
    malloc ( 3 * image_width * image_height * sizeof ( char ) );

  k = image_data;

  for ( i = 0; i < image_height; i++ )
  {
    y = ( float ) ( image_height + 1 - i ) / ( float ) ( image_height - 1 );
    for ( j = 0; j < image_width; j++ )
    {
      x = ( float ) ( j ) / ( float ) ( image_width - 1 );

      f1 = 4.0 * ( x - 0.5 ) * ( x - 0.5 );
      f2 = sin ( r4_pi * x );
      f3 = x;

      if ( y <= f1 )
      {
        *k = ( char ) ( 255.0 * f1 );
      }
      else
      {
        *k = ( char ) 50;
      }
      k = k + 1;

      if ( y <= f2 )
      {
        *k = ( char ) ( 255.0 * f2 );
      }
      else
      {
        *k = ( char ) 150;
      }
      k = k + 1;

      if ( y <= f3 )
      {
        *k = ( char ) ( 255.0 * f3 );
      }
      else
      {
        *k = ( char ) 250;
      }
      k = k + 1;
    }
  }

  return image_data;
}
/******************************************************************************/

int read_jpeg_file ( int *image_width, int *image_height, char **image_data,
  char *filename )

/******************************************************************************/
/*
  Purpose:

    READ_JPEG_FILE is an example routine for JPEG decompression.  

  Modified:

    27 April 2017

  Author:

    Based on "example.c", part of the JPEG distribution.
    Modifications by John Burkardt.

  Parameters:

    Output, int IMAGE_WIDTH, the width of the image in pixels.
 
    Output, int IMAGE_HEIGHT, the height of the image in pixels.

    Input, char *FILENAME, the name of the JPEG file to be read.

    Output, int READ_JPEG_FILE, is 0 for failure, and 1 for success.
*/
{
/* 
  The CINFO struct contains decompression parameters and workspace pointers.
*/
  struct jpeg_decompress_struct cinfo;
  FILE *infile;
  unsigned char *k;
/* 
  JERR is a struct which represents a JPEG error handler.
*/
  struct jpeg_error_mgr jerr;
/* 
  ROW_STRIDE is the "width" of a row of image data. 
*/
  int row_stride;
/*
  Step 0: Open the file.
*/
  infile = fopen ( filename, "rb" );

  if ( infile == NULL )
  {
    fprintf ( stderr, "READ_JPEG_FILE: can't open %s\n", filename );
    return 0;
  }
/* 
  Step 1: set the error routine, initialize the decompression object.
*/
  cinfo.err = jpeg_std_error ( &jerr );

  jpeg_create_decompress ( &cinfo );
/* 
  Step 2: specify the data source.
*/
  jpeg_stdio_src ( &cinfo, infile );
/* 
  Step 3: read the file parameters.
*/
  jpeg_read_header ( &cinfo, TRUE );
/* 
  Step 4: set parameters for decompression.
  For this example, we don't need to change anything.
*/

/* 
  Step 5: Start decompressor.
*/
  jpeg_start_decompress ( &cinfo );
/*
  Set up return variables.
*/
  *image_width = cinfo.output_width;
  *image_height = cinfo.output_height;
  *image_data = ( char * ) 
    malloc ( 3 * ( *image_width ) * ( *image_height ) * sizeof ( char ) );
/* 
  Step 6: read scanlines one at a time.
*/
  k = *image_data;

  while ( cinfo.output_scanline < cinfo.output_height )
  {
    jpeg_read_scanlines ( &cinfo, &k, 1 );
    k = k + 3 * ( *image_width );
  }
/* 
  Step 7: Finish decompression.
*/
  jpeg_finish_decompress ( &cinfo );
/* 
  Step 8: Release JPEG decompression object and close the file.
*/
  jpeg_destroy_decompress ( &cinfo );

  fclose ( infile );
/* 
  Terminate.
*/
  return 1;
}
/******************************************************************************/

void write_jpeg_file ( int image_width, int image_height, char *image_data, 
  char *filename, int quality )

/******************************************************************************/
/*
  Purpose:

    WRITE_JPEG_FILE is an example routine for JPEG compression.

  Modified:

    27 April 2017

  Author:

    Based on "example.c", part of the JPEG distribution.
    Modifications by John Burkardt.

  Parameters:

    Input, int IMAGE_WIDTH, the width of the image in pixels.
 
    Input, int IMAGE_HEIGHT, the height of the image in pixels.

    Input, char *IMAGE_DATA, 3 * IMAGE_WIDTH * IMAGE_HEIGHT RGB values.

    Input, char *FILENAME, the name of the file to be written.

    Input, int QUALITY, the JPEG quality level, between 0 and 100.
*/
{
/* 
  CINFO is a struct containing compression parameters and workspace pointers.
*/
  struct jpeg_compress_struct cinfo;
/* 
  JERR is a struct which represents a JPEG error handler.
*/
  struct jpeg_error_mgr jerr;
  FILE *outfile;
/* 
  JSAMPROW points to a row of the data.
*/
  JSAMPROW row_pointer[1];	
/*
  ROWSTRIDE is the "width" of one row of image data.
*/
  int row_stride;
/* 
  Step 1: allocate and initialize JPEG compression object.

  Set up the error handler.
*/
  cinfo.err = jpeg_std_error ( &jerr );
/*
  Initialize the JPEG compression object.
*/
  jpeg_create_compress ( &cinfo );
/* 
  Step 2: specify data destination.
*/
  outfile = fopen ( filename, "wb" );

  if ( outfile == NULL )
  {
    fprintf ( stderr, "WRITE_JPEG_FILE: Can't open %s\n", filename );
    exit ( 1 );
  }

  jpeg_stdio_dest ( &cinfo, outfile );
/* 
  Step 3: set parameters for compression.
  
  CINFO requires weight and height in pixels, number of color components,
  and the colorspace.
*/
  cinfo.image_width = image_width;
  cinfo.image_height = image_height;
  cinfo.input_components = 3;
  cinfo.in_color_space = JCS_RGB;
/* 
  Set default compression parameters.
*/
  jpeg_set_defaults ( &cinfo );
/* 
  Now you can set any non-default parameters you wish to.
  Here we just illustrate the use of quality (quantization table) scaling:
  The TRUE argument limits to baseline-JPEG values.
*/
  jpeg_set_quality ( &cinfo, quality, TRUE );
/* 
  Step 4: Start the compressor.

  TRUE ensures that we will write a complete interchange-JPEG file.
  Pass TRUE unless you are very sure of what you're doing.
*/
  jpeg_start_compress ( &cinfo, TRUE );
/* 
  Step 5: process image data one row at a time.
*/
  row_stride = image_width * 3;

  while ( cinfo.next_scanline < cinfo.image_height )
  {
    row_pointer[0] = & image_data [ cinfo.next_scanline * row_stride ];

    jpeg_write_scanlines ( &cinfo, row_pointer, 1 );
  }
/*
  Step 6: Finish compression and close the output file.
*/
  jpeg_finish_compress ( &cinfo );

  fclose ( outfile );
/* 
  Step 7: release JPEG compression object.
*/
  jpeg_destroy_compress ( &cinfo );

  return;
}
