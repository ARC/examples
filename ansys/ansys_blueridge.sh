#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load ansys/17.0
#
echo "ANSYS_BLUERIDGE: Normal beginning of execution."
#
ansys170 < truss.txt > ansys_blueridge.txt
if [ $? -ne 0 ]; then
  echo "ANSYS_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "ANSYS_BLUERIDGE: Normal end of execution."
exit 0
