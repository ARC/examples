#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
#
cd $PBS_O_WORKDIR
#
module purge
module load ansys/17.0
#
echo "ANSYS_NEWRIVER_BROADWELL: Normal beginning of execution."
#
ansys170 < truss.txt > ansys_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "ANSYS_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
#
echo "ANSYS_NEWRIVER_BROADWELL: Normal end of execution."
exit 0
