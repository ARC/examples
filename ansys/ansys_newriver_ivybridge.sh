#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q largemem_q
#PBS -A arctest
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load ansys/17.0
#
echo "ANSYS_NEWRIVER_IVYBRIDGE: Normal beginning of execution."
#
ansys170 < truss.txt > ansys_newriver_ivybridge.txt
if [ $? -ne 0 ]; then
  echo "ANSYS_NEWRIVER_IVYBRIDGE: Run error!"
  exit 1
fi
#
echo "ANSYS_NEWRIVER_IVYBRIDGE: Normal end of execution."
exit 0
