#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q normal_q
#PBS -j oe
#PBS -A arctest
#
cd $PBS_O_WORKDIR
#
module purge
module load ansys/17.0
#
echo "ANSYS_NEWRIVER_HASWELL: Normal beginning of execution."
#
ansys170 < truss.txt > ansys_haswell.txt
if [ $? -ne 0 ]; then
  echo "ANSYS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "ANSYS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
