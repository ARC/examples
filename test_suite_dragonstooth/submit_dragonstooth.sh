#! /bin/bash
#
#  submit_dragonstooth.sh
#
#  Purpose:
#
#    Run short tests of programs on the ARC DragonsTooth cluster.
#
#  Discussion:
#
#    Revised to run with the new location of the examples.
#
#  Example:
#
#    To execute this script, log onto any Dragonstooth login node, and type:
#
#      ./submit_dragonstooth.sh
#
#    This should submit all the jobs.  Later, run "reports_dragonstooth.sh"
#    to examine the results.
#
#  Modified:
#
#    23 February 2018
#
#  Author:
#
#    John Burkardt
#
date
echo ""
echo "SUBMIT_DRAGONSTOOTH:"
echo "  Submit test suite jobs on DragonsTooth."
#
cd /home/burkardt/examples
#
#  Identify current directory.
#
BASE=$(pwd)
echo ""
echo "  Working from base directory $BASE"
#
#  Clear out all old report files.
#
rm */test_suite_dragonstooth.txt
#
#  Program list #1.
#
PROGRAMS1="
abaqus
amber
anaconda
apache-ant
apbs-static
aspect
atlas
autodocksuite
automake
bamtools
bcftools
beagle-lib
bedtools
blas_atlas
blas_mkl
boost
boost-ucs2
bowtie
bowtie2
bwa
bzip2
cddlib
cgal
clapack
cmake
cora
cplex
cuda
cufflinks
dcw
ea-utils
eigen
espresso
examl
fastqc
fastx_toolkit
fdk-aac
ffmpeg
fftw
flint
gatb
gatk
gaussian
gcc
gdal
geos
glm
glog
gmsh
gnuplot
gromacs
gsl
guile
harminv
hdf5
hmmer
hpl
imagemagick
impi
intel
jdk
julia
lame
lammps
lapack_atlas
lapack_mkl
libgtextutils
libjpeg-turbo
lordec
ls-dyna
lsopt
lua
luajit
m4
mathematica
matlab
metis
minia
mkl
mpe2
mpich
mpip
nastran
ncbi-blast+
ncl
nco
netcdf
netcdf-c
netcdf-c-par
netcdf-cxx
netcdf-fortran
netcdf-fortran-par
octave
openblas
opencv
openmp
openmpi
opensees
p2fa
p4est
p7zip
papi
parallel
parallel-netcdf
parmetis
pbs
perl
pgi
phdf5
picard
pigz
proj
python-ucs2
python3gdal
r
r-parallel
rutils
samtools
scalapack
scons
seqtk
singular
sox
spades
sparsehash
sprai
stata
szip
tecplot
tophat
trimmomatic
trinityrnaseq
uuid
valgrind
vasp
velvet
viennacl
wannier90
x264
yasm
zip
zlib"
#
#  For each program:
#    move to that directory
#    remove any old copy of the results file
#    submit the job
#    go back to base
#
#
#  Override queue choice and group_list in job files.
#
QSUBFLAGS1='-qlarge_q -Aarctest -Wgroup_list=arcadm -otest_suite_dragonstooth.txt'

for PROG in $PROGRAMS1; do
  cd $PROG
  rm -f test_suite_dragonstooth.txt
  echo "Submit "$PROG"_dragonstooth.sh"
  qsub $QSUBFLAGS1 $PROG"_dragonstooth.sh"
  cd $BASE
done
#
#  Program list #2.
#
PROGRAMS2="
dev_q
normal_q
open_q"
#
#  For each program:
#    move to that directory
#    remove any old copy of the results file
#    submit the job
#    go back to base
#
#
#  Don't override queue choice for program list #2
#
QSUBFLAGS2='-Aarctest -Wgroup_list=arcadm -otest_suite_dragonstooth.txt'

for PROG in $PROGRAMS2; do
  cd $PROG
  rm -f test_suite_dragonstooth.txt
  echo "Submit "$PROG"_dragonstooth.sh"
  qsub $QSUBFLAGS2 $PROG"_dragonstooth.sh"
  cd $BASE
done
#
echo ""
echo "  Once jobs are run, list outcomes by:"
echo ""
echo "    tail -q -n 1 */test_suite_dragonstooth.txt"
echo ""
echo "  or simply run:"
echo ""
echo "    ./report_dragonstooth.sh"
#
#  Terminate.
#
echo ""
echo "SUBMIT_DRAGONSTOOTH: Normal end of execution."
exit 0

