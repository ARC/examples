#! /bin/bash
#
#  report_dragonstooth.sh
#
#  Purpose:
#
#    Collect results from test suite on DragonsTooth.
#
#  Discussion:
#
#    We assume that all the jobs have run.
#    We assume that, for program "prog":
#    * there is a directory /home/burkardt/public_html/examples/prog;
#    * there is a script file prog_dragonstooth.sh;
#    * there is now an output file test_suite_dragonstooth.txt;
#    * the last line of the output file probably contains the line
#        "prog_DRAGONSTOOTH: Normal end of execution";
#
#  Modified:
#
#    09 February 2018
#
#  Author:
#
#    John Burkardt
#
date
echo ""
echo "REPORT_DRAGONSTOOTH:"
echo "  Report results of test suite jobs on DragonsTooth."
#
cd /home/burkardt/examples
#
#  Identify current directory.
#
BASE=$(pwd)
echo ""
echo "  Working from base directory $BASE"
#
echo ""
echo "----------------------------------------"
tail -n 1 -q */test_suite_dragonstooth.txt
echo "----------------------------------------"
#
#  Terminate.
#
echo ""
echo "REPORT_DRAGONSTOOTH: Normal end of execution."
exit 0

