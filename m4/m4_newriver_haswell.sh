#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load m4/1.4.18
#
echo "M4_NEWRIVER_HASWELL: Normal beginning of execution."
#
m4 input.m4 > m4_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "M4_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
echo "M4_NEWRIVER_HASWELL: Normal end of execution."
exit 0

