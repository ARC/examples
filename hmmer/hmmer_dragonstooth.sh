#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load impi/5.0
module load hmmer/3.1b2
#
echo "HMMER_DRAGONSTOOTH: Normal beginning of execution."
#
#  Build a profile.
#
hmmbuild globins4.hmm globins4.sto > hmmer_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "HMMER_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
#  Search the sequence database.
#
hmmsearch globins4.hmm globins45.fa >> hmmer_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "HMMER_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "HMMER_DRAGONSTOOTH: Normal end of execution."
exit 0
