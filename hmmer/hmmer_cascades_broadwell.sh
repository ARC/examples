#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/2.0.0
module load hmmer/3.1b2
#
echo "HMMER_CASCADES_BROADWELL: Normal beginning of execution."
#
#  Build a profile.
#
hmmbuild globins4.hmm globins4.sto > hmmer_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "HMMER_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
#  Search the sequence database.
#
hmmsearch globins4.hmm globins45.fa >> hmmer_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "HMMER_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "HMMER_CASCADES_BROADWELL: Normal end of execution."
exit 0
