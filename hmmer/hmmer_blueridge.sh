#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.6.5
module load hmmer/3.0
#
echo "HMMER_BLUERIDGE: Normal beginning of execution."
#
#  Build a profile.
#
hmmbuild globins4.hmm globins4.sto > hmmer_blueridge.txt
if [ $? -ne 0 ]; then
  echo "HMMER_BLUERIDGE: Run error!"
  exit 1
fi
#
#  Search the sequence database.
#
hmmsearch globins4.hmm globins45.fa >> hmmer_blueridge.txt
if [ $? -ne 0 ]; then
  echo "HMMER_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "HMMER_BLUERIDGE: Normal end of execution."
exit 0
