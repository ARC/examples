#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe

cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load hdf5/1.8.8
module load netcdf/4.2
module load nco/4.4.4
#
echo "NCO_BLUERIDGE: Normal beginning of execution."
#
rm -f pres_temp_4D_avg.nc
#
ncra pres_temp_4D.nc -o pres_temp_4D_avg.nc
if [ $? -ne 0 ]; then
  echo "NCO_BLUERIDGE: Run error."
  exit 1
fi
#
echo "NCO_BLUERIDGE: Normal end of execution."
exit 0
