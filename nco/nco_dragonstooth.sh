#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe

cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load hdf5/1.8.16
module load netcdf/4.3.3.1
module load nco/4.4.9
#
echo "NCO_DRAGONSTOOTH: Normal beginning of execution."
#
rm -f pres_temp_4D_avg.nc
#
ncra pres_temp_4D.nc -o pres_temp_4D_avg.nc
if [ $? -ne 0 ]; then
  echo "NCO_DRAGONSTOOTH: Run error."
  exit 1
fi
#
echo "NCO_DRAGONSTOOTH: Normal end of execution."
exit 0
