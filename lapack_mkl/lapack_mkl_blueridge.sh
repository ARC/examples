#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load mkl/11.2.3
#
echo "LAPACK_MKL_BLUERIDGE: Normal beginning of execution."
#
ifort -c -mkl lapack_test.f90
if [ $? -ne 0 ]; then
  echo "LAPACK_MKL_BLUERIDGE: Compile error."
  exit 1
fi
#
ifort -mkl -o lapack_mkl_test lapack_test.o
if [ $? -ne 0 ]; then
  echo "LAPACK_MKL_BLUERIDGE: Load error."
  exit 1
fi
rm lapack_test.o
#
./lapack_mkl_test > lapack_mkl_blueridge.txt
if [ $? -ne 0 ]; then
  echo "LAPACK_MKL_BLUERIDGE: Run error."
  exit 1
fi
rm lapack_mkl_test
#
echo "LAPACK_MKL_BLUERIDGE: Normal end of execution."
exit 0
