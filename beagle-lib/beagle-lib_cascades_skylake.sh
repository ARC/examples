#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load beagle-lib/2.1.2
module load jdk/1.8.0
#
echo "BEAGLE-LIB_CASCADES_SKYLAKE: Normal beginning of execution."
#
echo ""
echo "  Running test analysis with \"gt=\" argument:"
echo ""
java -jar beagle.21Jan17.6cc.jar gt=test.21Jan17.6cc.vcf.gz out=out.gt
if [ $? -ne 0 ]; then
  echo "BEAGLE-LIB_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo ""
echo "  Running test analysis with \"gl=\" argument:"
echo ""
java -jar beagle.21Jan17.6cc.jar gl=test.21Jan17.6cc.vcf.gz out=out.gl
if [ $? -ne 0 ]; then
  echo "BEAGLE-LIB_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo ""
echo "  Running test analysis with \"ref=\" argument:"
echo ""
java -jar beagle.21Jan17.6cc.jar ref=ref.21Jan17.6cc.vcf.gz gt=target.21Jan17.6cc.vcf.gz out=out.ref
if [ $? -ne 0 ]; then
  echo "BEAGLE-LIB_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo ""
echo "  Making \"bref\" file:"
echo ""
java -jar bref.21Jan17.6cc.jar ref.21Jan17.6cc.vcf.gz
if [ $? -ne 0 ]; then
  echo "BEAGLE-LIB_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo ""
echo "  Running test analysis with \"bref=\" argument:"
echo ""
java -jar beagle.21Jan17.6cc.jar ref=ref.21Jan17.6cc.bref gt=target.21Jan17.6cc.vcf.gz out=out.bref
if [ $? -ne 0 ]; then
  echo "BEAGLE-LIB_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
echo "BEAGLE-LIB_CASCADES_SKYLAKE: Normal end of execution."
exit 0
