#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.4.0
module load openmpi/2.1.3
module load openblas/0.2.20
module load python/2.7.13
module load boost/1.58.0
#
echo "BOOST_CASCADES_SKYLAKE: Normal beginning of execution."
#
g++ -c -I $BOOST_INC histogram.cpp
if [ $? -ne 0 ]; then
  echo "BOOST_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
g++ -o histogram histogram.o -L$BOOST_LIB
if [ $? -ne 0 ]; then
  echo "BOOST_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm histogram.o
#
./histogram > boost_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "BOOST_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm histogram
#
echo "BOOST_CASCADES_SKYLAKE: Normal end of execution."
exit 0
