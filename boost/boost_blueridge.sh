#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.6.5
module load python/2.7.10
module load boost/1_49_0
#
echo "BOOST_BLUERIDGE: Normal beginning of execution."
#
g++ -c -I $BOOST_INC histogram.cpp
if [ $? -ne 0 ]; then
  echo "BOOST_BLUERIDGE: Compile error!"
  exit 1
fi
#
g++ -o histogram histogram.o -L$BOOST_LIB
if [ $? -ne 0 ]; then
  echo "BOOST_BLUERIDGE: Load error!"
  exit 1
fi
rm histogram.o
#
./histogram > boost_blueridge.txt
if [ $? -ne 0 ]; then
  echo "BOOST_BLUERIDGE: Run error!"
  exit 1
fi
rm histogram
#
echo "BOOST_BLUERIDGE: Normal end of execution."
exit 0
