#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load python/2.7.10
module load boost/1.58.0
#
echo "BOOST_CASCADES_BROADWELL: Normal beginning of execution."
#
g++ -c -I $BOOST_INC histogram.cpp
if [ $? -ne 0 ]; then
  echo "BOOST_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
g++ -o histogram histogram.o -L$BOOST_LIB
if [ $? -ne 0 ]; then
  echo "BOOST_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm histogram.o
#
./histogram > boost_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "BOOST_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm histogram
#
echo "BOOST_CASCADES_BROADWELL: Normal end of execution."
exit 0
