#! /bin/bash
#
#  report_blueridge.sh
#
#  Purpose:
#
#    Collect results from test suite on BlueRidge.
#
#  Discussion:
#
#    We assume that all the jobs have run.
#    We assume that, for program "prog":
#    * there is a directory /home/burkardt/public_html/examples/prog;
#    * there is a script file prog_blueridge.sh;
#    * there is now an output file test_suite_blueridge.txt;
#    * the last line of the output file probably contains the line
#        "prog_BLUERIDGE: Normal end of execution";
#
#  Modified:
#
#    08 February 2018
#
#  Author:
#
#    John Burkardt
#
date
echo ""
echo "REPORT_BLUERIDGE:"
echo "  Report results of test suite jobs on BlueRidge."
#
cd /home/burkardt/examples
#
#  Identify current directory.
#
BASE=$(pwd)
echo ""
echo "  Working from base directory $BASE"
#
echo ""
echo "----------------------------------------"
tail -n 1 -q */test_suite_blueridge.txt
echo "----------------------------------------"
#
#  Terminate.
#
echo ""
echo "REPORT_BLUERIDGE: Normal end of execution."
exit 0

