#! /bin/bash
#
#  submit_blueridge.sh
#
#  Purpose:
#
#    Run short tests of programs on the ARC BlueRidge cluster.
#
#  Discussion:
#
#    Revised to run with the new location of the examples.
#
#  Example:
#
#    To execute this script, log onto any BlueRidge login node, and type:
#
#      ./submit_blueridge.sh
#
#    This should submit all the jobs.  Later, run "reports_blueridge.sh"
#    to examine the results.
#
#  Modified:
#
#    20 February 2018
#
#  Author:
#
#    John Burkardt
#
date
echo ""
echo "SUBMIT_BLUERIDGE:"
echo "  Submit test suite jobs on BlueRidge."
#
cd /home/burkardt/examples
#
#  Identify current directory.
#
BASE=$(pwd)
echo ""
echo "  Working from base directory $BASE"
#
#  Clear out all old report files.
#
rm */test_suite_blueridge.txt
#
#  Program #1 list.
#
echo ""
echo "Process program list #1:"

PROGRAMS1="
abaqus
abinit
allinea-forge
anaconda
ansys
apache-ant
aspect
atlas
autodocksuite
bamtools
bcftools
beagle-lib
bedtools
blas_atlas
blas_mkl
boost
bowtie
bowtie2
bwa
bzip2
caelus
citcoms
clapack
cmake
cora
cplex
cufflinks
dcw
eigen
espresso
examl
fastqc
fdk-aac
ffmpeg
fftw
fluent
ga
gatk
gaussian
gcc
gdal
glm
glog
gmsh
gmt
gnuplot
gromacs
gshhg
gsl
guile
hdf5
hmmer
hpl
imagemagick
impi
intel
iozone
ipm
ipp
jags
jdk
julia
lame
lammps
lapack_atlas
lapack_mkl
ls-dyna
lsopt
lua
mathematica
matlab
metis
mkl
mothur
mpe2
mrbayes
mvapich2
namd
ncl
nco
netcdf
netcdf-par
nose
openblas
openfoam
openmp
openmpi
opensees
p4est
p7zip
papi
parallel
parallel-netcdf
parmetis
pbdr
pbs
pgi
phdf5
picard
r
r-parallel
rutils
sac
samtools
scalapack
scons
sparsehash
szip
tbb
tecplot
theano
tophat
torch
trimmomatic
trinityrnaseq
uuid
valgrind
vasp
velvet
x264
yasm
zip"
#
#  For each program:
#    move to that directory
#    remove any old copy of the results file
#    submit the job
#    go back to base
#
#  Override queue choice and group_list in job files.
#
QSUBFLAGS1='-qlarge_q -Aarctest -Wgroup_list=arcadm -otest_suite_blueridge.txt'

for PROG in $PROGRAMS1; do
  cd $PROG
  rm -f test_suite_blueridge.txt
  echo "Submit "$PROG"_blueridge.sh"
  qsub $QSUBFLAGS1 $PROG"_blueridge.sh"
  cd $BASE
done
#
#  Program #2 list.
#
echo ""
echo "Process program list #2:"

PROGRAMS2="
dev_q
normal_q
open_q
vis_q"
#
#  For list #2 programs, do NOT override queue choice.
#
QSUBFLAGS2='-Aarctest -Wgroup_list=arcadm -otest_suite_blueridge.txt'

for PROG in $PROGRAMS2; do
  cd $PROG
  rm -f test_suite_blueridge.txt
  echo "Submit "$PROG"_blueridge.sh"
  qsub $QSUBFLAGS2 $PROG"_blueridge.sh"
  cd $BASE
done
#
echo ""
echo "  Once jobs are run, list outcomes by:"
echo ""
echo "    tail -q -n 1 */test_suite_blueridge.txt"
echo ""
echo "  or simply run:"
echo ""
echo "    ./report_blueridge.sh"
#
#  Terminate.
#
echo ""
echo "SUBMIT_BLUERIDGE: Normal end of execution."
exit 0

