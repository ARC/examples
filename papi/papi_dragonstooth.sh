#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load papi/5.4.1
#
echo "PAPI_DRAGONSTOOTH: Normal beginning of execution."
#
icc -c -O0 -I$PAPI_INC papi_test.c
if [ $? -ne 0 ]; then
  echo "PAPI_DRAGONSTOOTH: Compile error."
  exit 1
fi
#
icc -o papi_test papi_test.o -L$PAPI_LIB -lpapi
if [ $? -ne 0 ]; then
  echo "PAPI_DRAGONSTOOTH: Load error."
  exit 1
fi
rm papi_test.o
#
./papi_test > papi_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "PAPI_DRAGONSTOOTH: Run error."
  exit 1
fi
rm papi_test
#
echo "PAPI_DRAGONSTOOTH: Normal end of execution."
exit 0
