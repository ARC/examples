# include <stdio.h>
# include <stdlib.h>

# include "papi.h"

int main ( );
int user_code ( int n );

int main ( )
{
  long long e;
  long long e1;
  int n;
  int retval;
  long long s;
  long long s1;

  printf ( "\n" );
  printf ( "PAPI_TEST:\n" );
  printf ( "  Demonstrate the PAPI Performance Application Programming Interface.\n" );

  if ( ( retval = PAPI_library_init ( PAPI_VER_CURRENT ) ) != PAPI_VER_CURRENT )
  {
    printf ( "\n" );
    printf ( "PAPI_TEST:\n" );
    printf ( "  Library initialization error! \n" );
    exit ( 1 );
  }
/* 
  Here you get initial cycles and time.
*/
  s = PAPI_get_real_cyc ( );

  n = 100;
  user_code ( n );
/*
  Get final cycles and time.
*/
  e = PAPI_get_real_cyc ( );

  s1 = PAPI_get_real_usec ( );

  user_code ( n );

  e1 = PAPI_get_real_usec ( );

  printf ( "\n" );
  printf ( "  Wallclock cycles             : %lld\n", e - s );
  printf ( "  Wallclock time (microseconds): %lld\n", e1 - s1 );
/* 
  Clean up.
*/
  PAPI_shutdown();
/*
  Terminate.
*/
  printf ( "\n" );
  printf ( "PAPI_TEST:\n" );
  printf ( "  Normal end of execution.\n" );

  return 0;
}

int user_code ( int n )
{
  float *a;
  float *b;
  float *c;
  int i;
  int j;
  int k;

  a = ( float * ) malloc ( n * n * sizeof ( float ) );
  b = ( float * ) malloc ( n * n * sizeof ( float ) );
  c = ( float * ) malloc ( n * n * sizeof ( float ) );

  for ( i = 0; i < n; i++ )
  {
    for ( j = 0; j < n; j++ )
    {
      a[i+j*n] = ( float ) rand ( ) / ( float ) RAND_MAX;
      b[i+j*n] = ( float ) rand ( ) / ( float ) RAND_MAX;
      c[i+j*n] = 0.0;
    }
  }

  for ( i = 0; i < n; i++ )
  {
    for ( j = 0; j < n; j++ )
    {
      for ( k = 0; k < n; k++ )
      {
        c[i+j*n] = c[i+j*n] + a[i+k*n] * b[k+j*n];
      }
    }
  }

  free ( a );
  free ( b );
  free ( c );

  return 0;
}
 
