#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load papi/5.4.1
#
echo "PAPI_CASCADES_BROADWELL: Normal beginning of execution."
#
gcc -c -O0 -I$PAPI_INC papi_test.c
if [ $? -ne 0 ]; then
  echo "PAPI_CASCADES_BROADWELL: Compile error."
  exit 1
fi
#
gcc -o papi_test papi_test.o -L$PAPI_LIB -lpapi
if [ $? -ne 0 ]; then
  echo "PAPI_CASCADES_BROADWELL: Load error."
  exit 1
fi
rm papi_test.o
#
./papi_test > papi_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "PAPI_CASCADES_BROADWELL: Run error."
  exit 1
fi
rm papi_test
#
echo "PAPI_CASCADES_BROADWELL: Normal end of execution."
exit 0
