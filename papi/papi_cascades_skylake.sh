#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load papi/5.4.1
#
echo "PAPI_CASCADES_SKYLAKE: Normal beginning of execution."
#
gcc -c -O0 -I$PAPI_INC papi_test.c
if [ $? -ne 0 ]; then
  echo "PAPI_CASCADES_SKYLAKE: Compile error."
  exit 1
fi
#
gcc -o papi_test papi_test.o -L$PAPI_LIB -lpapi
if [ $? -ne 0 ]; then
  echo "PAPI_CASCADES_SKYLAKE: Load error."
  exit 1
fi
rm papi_test.o
#
./papi_test > papi_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "PAPI_CASCADES_SKYLAKE: Run error."
  exit 1
fi
rm papi_test
#
echo "PAPI_CASCADES_SKYLAKE: Normal end of execution."
exit 0
