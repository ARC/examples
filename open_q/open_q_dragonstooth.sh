#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
#
echo "OPEN_Q_DRAGONSTOOTH: Normal beginning of execution."
echo "OPEN_Q_DRAGONSTOOTH: Job running in queue "$PBS_QUEUE
#
gcc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "OPEN_Q_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
gcc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "OPEN_Q_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > open_q_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "OPEN_Q_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm heated_plate
#
echo "OPEN_Q_DRAGONSTOOTH: Normal end of execution."
exit 0

