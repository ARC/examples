#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load sox/14.4.2
#
echo "SOX_CASCADES_BROADWELL: Normal beginning of execution."
#
#  From a 20 second clip of Bach, 
#  Start at 5 seconds, extract 10 seconds of sound.
#
sox bach20.wav bach10.wav trim 5 10
if [ $? -ne 0 ]; then
  echo "SOX_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "SOX_CASCADES_BROADWELL: Normal end of execution."
exit 0
