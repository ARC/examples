#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load sox/14.4.2
#
echo "SOX_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  From a 20 second clip of Bach, 
#  Start at 5 seconds, extract 10 seconds of sound.
#
sox bach20.wav bach10.wav trim 5 10
if [ $? -ne 0 ]; then
  echo "SOX_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "SOX_NEWRIVER_HASWELL: Normal end of execution."
exit 0
