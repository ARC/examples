#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=24
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.4.0
module load openmpi/2.1.3
module load cuda/9.1.85
module load fftw/3.3.6
module load openblas/0.2.20
module load gromacs.c9/2018.1
#
echo "GROMACS_CASCADES_SKYLAKE: Normal beginning of execution."
#
#  Discard the previous output file, if any.
#
if [ -e gromacs_cascades_skylake.txt ]; then
  rm gromacs_cascades_skylake.txt
fi
#
#  Our primary input data is 1aki.pdb.
#  For certain commands, we must access auxilliary *.mdp files of parameters.
#  Because this is not an interactive run, occasional interactive input
#  is supplied by *input.txt files.
#  All the output goes into a single file "gromacs_cascades_skylake.txt".
#
#  pdb2gmx reads the pdb file, and generates:
#  * the topology of the molecule (topol.top);
#  * a position restraint file (posre.itp);
#  * a post-processed structure file (1aki_processed.gro).
#
gmx_s pdb2gmx -f 1aki.pdb -o 1aki_processed.gro -water spce < pdb2gmx_input.txt &> gromacs_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "GROMACS_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
#  editconf defines the box dimensions.
#  We choose centering, place the protein at least 1 nm from the box edge,
#  and use a cubic box.
#
gmx_s editconf -f 1aki_processed.gro -o 1aki_newbox.gro -c -d 1.0 -bt cubic &>> gromacs_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "GROMACS_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
#  solvate fills the box with water.
#
gmx_s solvate -cp 1aki_newbox.gro -cs spc216.gro -o 1aki_solv.gro -p topol.top &>> gromacs_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "GROMACS_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
#
#  We terminate the run at this point, because this is simply
#  a quick test program.
#
echo ""
echo "GROMACS_CASCADES_SKYLAKE: Terminate this short demo at this point."
#
#  Terminate.
#
echo "GROMACS_CASCADES_SKYLAKE: Normal end of execution."
exit 0
