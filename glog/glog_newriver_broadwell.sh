#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
#
cd $PBS_O_WORKDIR

module purge
module load glog/0.3.5
#
echo "GLOG_NEWRIVER_BROADWELL: Normal beginning of execution."
#
g++ -c -I$GLOG_INC glog_test.cpp
if [ $? -ne 0 ]; then
  echo "GLOG_NEWRIVER_BROADWELL: Compile error!"
  exit 1
fi
#
g++ glog_test.o -o glog_test -L$GLOG_LIB -lglog
if [ $? -ne 0 ]; then
  echo "GLOG_NEWRIVER_BROADWELL: Load error!"
  exit 1
fi
rm glog_test.o
#
#  Without this switch, INFO and WARNING messages only go to files
#  called /tmp/[PROGRAM_NAME].INFO, /tmp/[PROGRAM_NAME].WARNING.
#  This way, all messages appear on stderr.
#
export GLOG_logtostderr=1
#
#  Because we are deliberately triggering errors, glog_test
#  will terminate with an error condition.
#
#  Paradoxically, this is "good", so we will reverse the
#  interpretation of the exit status!
#
./glog_test &> glog_newriver_broadwell.txt
if [ $? -eq 0 ]; then
  echo "GLOG_NEWRIVER_BROADWELL: Failed to trigger error state!"
  echo "GLOG_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
#
rm glog_test
#
echo "GLOG_NEWRIVER_BROADWELL: Normal end of execution."
exit 0
