# include <cmath>
# include <iomanip>
# include <iostream>
# include <glog/logging.h>

using namespace std;
using namespace google;

int main ( int argc, char *argv[] )
{
  double df;
  double dx;
  double fx;
  double fxp;  
  const double epsilon = 2.220446049250313E-016;
  int i;
  double x;
  double xp;

  cout << "\n";
  cout << "GLOG_TEST:\n";
  cout << "  GLOG is the Google Logging facility, which enables the logging\n";
  cout << "  of INFO, WARNING, ERROR and FATAL level incidents during a\n";
  cout << "  program run.\n";
//
//  Initialize GLOG.
//
  google::InitGoogleLogging ( argv[0] );
  LOG ( INFO ) << "GLOG has been initialized.";

  x = 10000.0;
  if ( x <= 0.0 )
  {
    LOG ( FATAL ) << "Argument X must be positive!";
  }
  fx = log ( x );
  dx = 0.1;

  for ( i = 0; i < 10; i++ )
  {
    xp = x + dx;
    if ( xp - x == 0.0 )
    {
      LOG ( FATAL ) << "Division by zero!";
    }
    fxp = log ( xp );
    if ( fx == fxp )
    {
      LOG ( ERROR ) << "Arguments so close that f(x)=f(xp)";
    }
    df = ( fxp - fx ) / ( xp - x );
    cout << "  DX = " << dx << "  DF = " << df << "\n";
    dx = dx * dx;
    if ( dx < sqrt ( epsilon ) * x )
    {
      LOG ( WARNING ) << "DX may be too small.";
    }
  }

  LOG ( INFO ) << "GLOG_TEST terminated normally.";
//
//  Terminate.
//
  cout << "\n";
  cout << "GLOG_TEST:\n";
  cout << "  Normal end of execution.\n";

  return 0;
}
