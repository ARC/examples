#! /bin/bash
#
#PBS -l walltime=5:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe

cd $PBS_O_WORKDIR

module purge
module load glog/0.3.3
#
echo "GLOG_DRAGONSTOOTH: Normal beginning of execution."
#
g++ -c -I$GLOG_INC glog_test.cpp
if [ $? -ne 0 ]; then
  echo "GLOG_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
g++ glog_test.o -o glog_test -L$GLOG_LIB -lglog
if [ $? -ne 0 ]; then
  echo "GLOG_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm glog_test.o
#
#  Without this switch, INFO and WARNING messages only go to files
#  called /tmp/[PROGRAM_NAME].INFO, /tmp/[PROGRAM_NAME].WARNING.
#  This way, all messages appear on stderr.
#
export GLOG_logtostderr=1
#
#  Because we are deliberately triggering errors, glog_test
#  will terminate with an error condition.
#
#  Paradoxically, this is "good", so we will reverse the
#  interpretation of the exit status!
#
./glog_test &> glog_dragonstooth.txt
if [ $? -eq 0 ]; then
  echo "GLOG_DRAGONSTOOTH: Failed to trigger error state!"
  echo "GLOG_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
rm glog_test
#
echo "GLOG_DRAGONSTOOTH: Normal end of execution."
exit 0
