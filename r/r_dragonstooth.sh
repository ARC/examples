#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openblas/0.2.14
module load R/3.4.1
#
echo "R_DRAGONSTOOTH: Normal beginning of execution."
#
R --vanilla < input.r > r_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "R_DRAGONSTOOTH: Run error!"
  exit 1
fi
#
echo "R_DRAGONSTOOTH: Normal end of execution."
exit 0
