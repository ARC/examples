#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
module load mkl/11.2.3
module load R/3.4.1
#
echo "R_CASCADES_BROADWELL: Normal beginning of execution."
#
R --vanilla < input.r > r_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "R_CASCADES_BROADWELL: Run error!"
  exit 1
fi
#
echo "R_CASCADES_BROADWELL: Normal end of execution."
exit 0
