#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.1.0
module load R/3.2.0
#
echo "R_BLUERIDGE: Normal beginning of execution."
#
R --vanilla < input.r > r_newriver.txt
if [ $? -ne 0 ]; then
  echo "R_BLUERIDGE: Run error!"
  exit 1
fi
#
echo "R_BLUERIDGE: Normal end of execution."
exit 0
