#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openblas/0.2.14
module load R/3.2.2
#
echo "R_NEWRIVER_HASWELL: Normal beginning of execution."
#
R --vanilla < input.r > r_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "R_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "R_NEWRIVER_HASWELL: Normal end of execution."
exit 0
