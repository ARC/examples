#! /bin/bash
#
#SBATCH -t 00:05:00
#SBATCH -n 1
#SBATCH -p normal_q
#SBATCH --mem=100
#SBATCH -J normal_q_huckleberry
#
cd $SLURM_SUBMIT_DIR
#
module purge
module load gcc/6.1.0
#
echo "NORMAL_Q_HUCKLEBERRY: Normal beginning of execution."
echo "NORMAL_Q_HUCKLEBERRY: Job running in queue "$PBS_QUEUE
#
gcc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "NORMAL_Q_HUCKLEBERRY: Compile error!"
  exit 1
fi
#
gcc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "NORMAL_Q_HUCKLEBERRY: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > normal_q_huckleberry.txt
if [ $? -ne 0 ]; then
  echo "NORMAL_Q_HUCKLEBERRY: Run error!"
  exit 1
fi
rm heated_plate
#
echo "NORMAL_Q_HUCKLEBERRY: Normal end of execution."
exit 0

