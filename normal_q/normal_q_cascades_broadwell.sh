#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q normal_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
#
echo "NORMAL_Q_CASCADES_BROADWELL: Normal beginning of execution."
echo "NORMAL_Q_CASCADES_BROADWELL: Job running in queue "$PBS_QUEUE
#
gcc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "NORMAL_Q_CASCADES_BROADWELL: Compile error!"
  exit 1
fi
#
gcc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "NORMAL_Q_CASCADES_BROADWELL: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > normal_q_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "NORMAL_Q_CASCADES_BROADWELL: Run error!"
  exit 1
fi
rm heated_plate
#
echo "NORMAL_Q_CASCADES_BROADWELL: Normal end of execution."
exit 0

