total 140
-rw-r--r-- 1 burkardt burkardt 59142 Jul 22 08:55 49
-rw-r--r-- 1 burkardt burkardt   380 Jul 22 08:55 49.tree
-rw-r--r-- 1 burkardt burkardt  2815 Jul 22 08:55 examl_article.html
-rw-r--r-- 1 burkardt burkardt  1160 Jul 29 10:27 examl_blueridge.sh
-rw-r--r-- 1 burkardt burkardt  1168 Aug  2 10:26 examl_cascades.sh
-rw-r--r-- 1 burkardt burkardt  1179 Aug  2 07:46 examl_dragonstooth.sh
-rw-r--r-- 1 burkardt burkardt  1167 Jul 29 10:27 examl_newriver.sh
-rw-r--r-- 1 burkardt burkardt  4323 Jul 22 08:55 examl_newriver.txt
-rw------- 1 burkardt arcadm    4330 Aug  1 07:42 test_suite_blueridge.txt
-rw------- 1 burkardt burkardt  5493 Aug  2 09:41 test_suite_dragonstooth.txt
-rw------- 1 burkardt arcadm    5386 Aug  1 08:31 test_suite_newriver.txt
gappyness: 0.074048
Pattern compression: ON

Alignment has 51 completely undetermined sites that will be automatically removed from the binary alignment file



Your alignment has 628 unique patterns


Under CAT the memory required by ExaML for storing CLVs and tip vectors will be
1015476 bytes
991 kiloBytes
0 MegaBytes
0 GigaBytes


Under GAMMA the memory required by ExaML for storing CLVs and tip vectors will be
3969588 bytes
3876 kiloBytes
3 MegaBytes
0 GigaBytes

Please note that, these are just the memory requirements for doing likelihood calculations!
To be on the safe side, we recommend that you execute ExaML on a system with twice that memory.


Binary and compressed alignment file written to file 49.unpartitioned.binary

Parsing completed, exiting now ... 


This is ExaML FINE-GRAIN MPI Process Number: 3

This is ExaML FINE-GRAIN MPI Process Number: 0

This is ExaML FINE-GRAIN MPI Process Number: 1

This is ExaML FINE-GRAIN MPI Process Number: 2

proc	offset	length	part
0	314	157	0
1	157	157	0
2	471	157	0
3	0	157	0

#proc	#part	#sites
0	1	157
1	1	157
2	1	157
3	1	157



This is ExaML version 3.0.11 released by Alexandros Stamatakis, Andre Aberer, and Alexey Kozlov in April 10 2014.


Alignment has 628 distinct alignment patterns

Proportion of gaps and completely undetermined characters in this alignment: 7.40%

ExaML rapid hill-climbing mode

Using 1 distinct models/data partitions with joint branch length optimization


All free model parameters will be estimated by ExaML
GAMMA model of rate heteorgeneity, ML estimate of alpha-parameter

Partition: 0
Alignment Patterns: 628
Name: No Name Provided
DataType: DNA
Substitution Matrix: GTR




ExaML was called as follows:

examl -t 49.tree -m GAMMA -s 49.unpartitioned.binary -n T1 


Memory Saving Option: DISABLED

Best rearrangement radius: 5

Likelihood of best tree: -16225.531373


Overall Time for 1 Inference 10.624421

Overall accumulated Time (in case of restarts): 10.624421

Likelihood   : -16225.531373


Model parameters written to:           /home/burkardt/public_html/examples/examl/ExaML_modelFile.T1
Final tree written to:                 /home/burkardt/public_html/examples/examl/ExaML_result.T1
Execution Log File written to:         /home/burkardt/public_html/examples/examl/ExaML_log.T1
Execution information file written to: /home/burkardt/public_html/examples/examl/ExaML_info.T1
total 6308
-rw-r--r-- 1 burkardt burkardt  59142 Jul 22 08:55 49
-rw-r--r-- 1 burkardt burkardt    380 Jul 22 08:55 49.tree
-rw-r--r-- 1 burkardt arcadm    33906 Aug  3 07:52 49.unpartitioned.binary
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_0
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_1
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_10
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_11
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_12
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_13
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_2
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_3
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_4
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_5
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_6
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_7
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_8
-rw-r--r-- 1 burkardt arcadm   441308 Aug  3 07:52 ExaML_binaryCheckpoint.T1_9
-rw-r--r-- 1 burkardt arcadm     1295 Aug  3 07:52 ExaML_info.T1
-rw-r--r-- 1 burkardt arcadm      323 Aug  3 07:52 ExaML_log.T1
-rw-r--r-- 1 burkardt arcadm      340 Aug  3 07:52 ExaML_modelFile.T1
-rw-r--r-- 1 burkardt arcadm     2569 Aug  3 07:52 ExaML_result.T1
-rw-r--r-- 1 burkardt arcadm      757 Aug  3 07:52 RAxML_info.49.unpartitioned
-rw-r--r-- 1 burkardt burkardt   2815 Jul 22 08:55 examl_article.html
-rw-r--r-- 1 burkardt burkardt   1160 Jul 29 10:27 examl_blueridge.sh
-rw-r--r-- 1 burkardt burkardt   1168 Aug  2 10:26 examl_cascades.sh
-rw-r--r-- 1 burkardt burkardt   1179 Aug  2 07:46 examl_dragonstooth.sh
-rw-r--r-- 1 burkardt burkardt   1167 Jul 29 10:27 examl_newriver.sh
-rw-r--r-- 1 burkardt burkardt   4323 Jul 22 08:55 examl_newriver.txt
-rw------- 1 burkardt arcadm     4330 Aug  1 07:42 test_suite_blueridge.txt
-rw------- 1 burkardt burkardt   5493 Aug  2 09:41 test_suite_dragonstooth.txt
-rw------- 1 burkardt arcadm     5386 Aug  1 08:31 test_suite_newriver.txt
EXAML_CASCADES: Normal end of execution.
