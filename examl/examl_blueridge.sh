#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load openmpi/1.6.5
module load examl/1.0.0
#
echo "EXAML_BLUERIDGE: Normal beginning of execution."
#
#  Remove files from previous runs.
#
rm -f 49.unpartitioned.binary
rm -f *binaryCheckpoint*
rm -f *.T1
rm -f RAxML_info.49.unpartitioned
#
#  List current files.
#
ls -l
#
#  Call parser to transform the unpartitioned phylip test data file "49" 
#  into a binary file "49.unpartitioned.binary".
#
parser -s 49 -m DNA -n 49.unpartitioned
if [ $? -ne 0 ]; then
  echo "EXAML_BLUERIDGE: Run error!"
  exit 1
fi
#
#  Normally, we would use RAxML to generate a starting tree.
#  Instead, we use a precomputed one, "49.tree"
#
#raxml -y -m GTRCAT -p 12345 -s 49 -n StartingTree
#
#  Run examl, creating 4 files:
#    "ExaML_info.T1"
#    "ExaML_log.T1"
#    "ExaML_modelFile.T1"
#    "ExaML_result.T1"
#
mpirun -np 4 examl -t 49.tree -m GAMMA -s 49.unpartitioned.binary -n T1
if [ $? -ne 0 ]; then
  echo "EXAML_BLUERIDGE: Run error!"
  exit 1
fi
#
#  List current files.
#
ls -l
#
echo "EXAML_BLUERIDGE: Normal end of execution."
exit 0
