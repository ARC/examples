#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/3.0.0
module load p4est/1.1
#
echo "P4EST_CASCADES_BROADWELL: Normal beginning of execution."
#
mpicc -c -I$P4EST_INC simple2.c
if [ $? -ne 0 ]; then
  echo "P4EST_CASCADES_BROADWELL: Compile error."
  exit 1
fi
#
mpicc -o simple2 simple2.o -L$P4EST_LIB -lp4est -lsc
if [ $? -ne 0 ]; then
  echo "P4EST_CASCADES_BROADWELL: Load error."
  exit 1
fi
rm simple2.o
#
mpirun -np 4 simple2 unit 3
if [ $? -ne 0 ]; then
  echo "P4EST_CASCADES_BROADWELL: Run error."
  exit 1
fi
rm simple2
#
echo "P4EST_CASCADES_BROADWELL: Normal end of execution."
exit 0
