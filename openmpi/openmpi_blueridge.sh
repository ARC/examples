#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.3.0
module load openmpi/2.0.0
#
echo "OPENMPI_BLUERIDGE: Normal beginning of execution."
#
mpicc -c prime.c
if [ $? -ne 0 ]; then
  echo "OPENMPI_BLUERIDGE: Compile error!"
  exit 1
fi
#
mpicc -o prime prime.o
if [ $? -ne 0 ]; then
  echo "OPENMPI_BLUERIDGE: Load error!"
  exit 1
fi
rm prime.o
#
mpirun -np 8 ./prime openmpi_blueridge.txt
if [ $? -ne 0 ]; then
  echo "OPENMPI_BLUERIDGE: Run error!"
  exit 1
fi
rm prime
#
echo "OPENMPI_BLUERIDGE: Normal end of execution."
exit 0
