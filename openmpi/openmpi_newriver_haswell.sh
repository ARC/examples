#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/2.0.0
#
echo "OPENMPI_NEWRIVER_HASWELL: Normal beginning of execution."
#
mpicc -c prime.c
if [ $? -ne 0 ]; then
  echo "OPENMPI_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
mpicc -o prime prime.o
if [ $? -ne 0 ]; then
  echo "OPENMPI_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm prime.o
#
mpirun -np 8 ./prime openmpi_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "OPENMPI_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm prime
#
echo "OPENMPI_NEWRIVER_HASWELL: Normal end of execution."
exit 0
