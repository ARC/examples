#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.1.0
module load openmpi/1.10.2
#
echo "OPENMPI_DRAGONSTOOTH: Normal beginning of execution."
#
mpicc -c prime.c
if [ $? -ne 0 ]; then
  echo "OPENMPI_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
mpicc -o prime prime.o
if [ $? -ne 0 ]; then
  echo "OPENMPI_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm prime.o
#
mpirun -np 8 ./prime openmpi_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "OPENMPI_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm prime
#
echo "OPENMPI_DRAGONSTOOTH: Normal end of execution."
exit 0
