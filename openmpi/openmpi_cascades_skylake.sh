#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=8
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.4.0
module load openmpi/2.1.3
#
echo "OPENMPI_CASCADES_SKYLAKE: Normal beginning of execution."
#
mpicc -c prime.c
if [ $? -ne 0 ]; then
  echo "OPENMPI_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
mpicc -o prime prime.o
if [ $? -ne 0 ]; then
  echo "OPENMPI_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm prime.o
#
mpirun -np 8 ./prime openmpi_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "OPENMPI_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm prime
#
echo "OPENMPI_CASCADES_SKYLAKE: Normal end of execution."
exit 0
