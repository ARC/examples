#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load geos/3.4.2
#
echo "GEOS_CASCADES_BROADWELL: Normal beginning of execution."
#
gcc -c -I $GEOS_INC geostest.c
if [ $? -ne 0 ]; then
  echo "GEOS_CASCADES_BROADWELL: Compile error."
  exit 1
fi
#
gcc -o geostest geostest.o -L$GEOS_LIB -lgeos_c
if [ $? -ne 0 ]; then
  echo "GEOS_CASCADES_BROADWELL: Load error."
  exit 1
fi
rm geostest.o
#
./geostest test.wkt > geos_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "GEOS_CASCADES_BROADWELL: Run error."
  exit 1
fi
rm geostest
#
echo "GEOS_CASCADES_BROADWELL: Normal end of execution."
exit 0

