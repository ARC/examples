#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/4.7.2
module load CLAPACK/3.1.1.1
#
echo "CLAPACK_BLUERIDGE: Normal beginning of execution."
#
gcc -c -I$CLAPACK_INC clapack_test.c
if [ $? -ne 0 ]; then
  echo "CLAPACK_BLUERIDGE: Compile error!"
  exit 1
fi
#
gcc -o clapack_test clapack_test.o -L$CLAPACK_LIB -L$CLAPACK_LIB/F2CLIBS -ltmglib -llapack -lblas -lm -lf2c
if [ $? -ne 0 ]; then
  echo "CLAPACK_BLUERIDGE: Load error!"
  exit 1
fi
rm clapack_test.o
#
./clapack_test > clapack_blueridge.txt
if [ $? -ne 0 ]; then
  echo "CLAPACK_BLUERIDGE: Run error!"
  exit 1
fi
rm clapack_test
#
echo "CLAPACK_BLUERIDGE: Normal end of execution."
exit 0
