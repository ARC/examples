#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=dragonstooth
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/16.1
#
echo "INTEL_DRAGONSTOOTH: Normal beginning of execution."
#
icc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "INTEL_DRAGONSTOOTH: Compile error!"
  exit 1
fi
#
icc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "INTEL_DRAGONSTOOTH: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > intel_dragonstooth.txt
if [ $? -ne 0 ]; then
  echo "INTEL_DRAGONSTOOTH: Run error!"
  exit 1
fi
rm heated_plate
#
echo "INTEL_DRAGONSTOOTH: Normal end of execution."
exit 0

