#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/16.1
#
echo "INTEL_NEWRIVER_HASWELL: Normal beginning of execution."
#
icc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "INTEL_NEWRIVER_HASWELL: Compile error!"
  exit 1
fi
#
icc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "INTEL_NEWRIVER_HASWELL: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > intel_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "INTEL_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
rm heated_plate
#
echo "INTEL_NEWRIVER_HASWELL: Normal end of execution."
exit 0

