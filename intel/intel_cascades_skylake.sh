#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/15.3
#
echo "INTEL_CASCADES_SKYLAKE: Normal beginning of execution."
#
ifort -c heated_plate.f90
if [ $? -ne 0 ]; then
  echo "INTEL_CASCADES_SKYLAKE: Compile error!"
  exit 1
fi
#
ifort -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "INTEL_CASCADES_SKYLAKE: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > intel_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "INTEL_CASCADES_SKYLAKE: Run error!"
  exit 1
fi
rm heated_plate
#
echo "INTEL_CASCADES_SKYLAKE: Normal end of execution."
exit 0

