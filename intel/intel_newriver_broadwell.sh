#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
#
cd $PBS_O_WORKDIR
#
module purge
module load intel/18.2
#
echo "INTEL_NEWRIVER_BROADWELL: Normal beginning of execution."
#
icc -c heated_plate.c
if [ $? -ne 0 ]; then
  echo "INTEL_NEWRIVER_BROADWELL: Compile error!"
  exit 1
fi
#
icc -o heated_plate heated_plate.o
if [ $? -ne 0 ]; then
  echo "INTEL_NEWRIVER_BROADWELL: Load error!"
  exit 1
fi
rm heated_plate.o
#
./heated_plate > intel_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "INTEL_NEWRIVER_BROADWELL: Run error!"
  exit 1
fi
rm heated_plate
#
echo "INTEL_NEWRIVER_BROADWELL: Normal end of execution."
exit 0

