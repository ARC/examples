#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load python/2.7.10
#
echo "PYTHON_CASCADES_BROADWELL: Normal beginning of execution."
#
python satisfy.py > python_cascades_broadwell.txt
if [ $? -ne 0 ]; then
  echo "PYTHON_CASCADES_BROADWELL: Compile error."
  exit 1
fi
#
echo "PYTHON_CASCADES_BROADWELL: Normal end of execution."
exit 0
