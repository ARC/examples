#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -A arctest
#PBS -q p100_dev_q
##PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load atlas/3.10.2
module load python/2.7.13
#
echo "PYTHON_NEWRIVER_BROADWELL: Normal beginning of execution."
#
python satisfy.py > python_newriver_broadwell.txt
if [ $? -ne 0 ]; then
  echo "PYTHON_NEWRIVER_BROADWELL: Compile error."
  exit 1
fi
#
echo "PYTHON_NEWRIVER_BROADWELL: Normal end of execution."
exit 0
