#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=cascades
#PBS -A arctest
#PBS -q v100_dev_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/6.4.0
module load openblas/0.2.20
module load python/2.7.13
#
echo "PYTHON_CASCADES_SKYLAKE: Normal beginning of execution."
#
python satisfy.py > python_cascades_skylake.txt
if [ $? -ne 0 ]; then
  echo "PYTHON_CASCADES_SKYLAKE: Compile error."
  exit 1
fi
#
echo "PYTHON_CASCADES_SKYLAKE: Normal end of execution."
exit 0
