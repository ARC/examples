#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load python/2.7.10
#
echo "PYTHON_NEWRIVER_HASWELL: Normal beginning of execution."
#
python satisfy.py > python_newriver_haswell.txt
if [ $? -ne 0 ]; then
  echo "PYTHON_NEWRIVER_HASWELL: Compile error."
  exit 1
fi
#
echo "PYTHON_NEWRIVER_HASWELL: Normal end of execution."
exit 0
