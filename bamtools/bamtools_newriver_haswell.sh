#! /bin/bash
#
#PBS -l walltime=0:05:00
#PBS -l nodes=1:ppn=1
#PBS -W group_list=newriver
#PBS -q open_q
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load bamtools/2.4
#
echo "BAMTOOLS_NEWRIVER_HASWELL: Normal beginning of execution."
#
#  Report the number of alignments.
#
bamtools count -in sim_reads_aligned.bam
if [ $? -ne 0 ]; then
  echo "BAMTOOLS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
#  Convert to FASTQ format.
#
bamtools convert -format fastq -in sim_reads_aligned.bam -out sim_reads_aligned.fq
if [ $? -ne 0 ]; then
  echo "BAMTOOLS_NEWRIVER_HASWELL: Run error!"
  exit 1
fi
#
echo "BAMTOOLS_NEWRIVER_HASWELL: Normal end of execution."
exit 0
