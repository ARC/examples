#! /bin/bash
#
#PBS -l walltime=00:05:00
#PBS -l nodes=1:ppn=4
#PBS -W group_list=blueridge
#PBS -q open_q
#PBS -j oe
#
cd $PBS_O_WORKDIR
#
module purge
module load gcc/5.2.0
module load openmpi/1.8.5
module load allinea-forge/5.0.1
#
echo "ALLINEA-FORGE_BLUERIDGE: Normal beginning of execution."
#
gcc -g -c md.c
if [ $? -ne 0 ]; then
  echo "ALLINEA-FORGE_BLUERIDGE: Compile error."
  exit 1
fi
#
gcc -o md md.o -lm
if [ $? -ne 0 ]; then
  echo "ALLINEA-FORGE_BLUERIDGE: Load error."
  exit
fi
#
map --profile ./md
if [ $? -ne 0 ]; then
  echo "ALLINEA-FORGE_BLUERIDGE: Run error."
  exit 1
fi
#
ls *.map
rm md.o
rm md
#
echo "ALLINEA-FORGE_BLUERIDGE: Normal end of execution."
exit 0
