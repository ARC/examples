#! /usr/bin/env python
#
def fd1d_heat_implicit_test ( ):

#*****************************************************************************80
#
## FD1D_HEAT_IMPLICIT_TEST does a simple test problem.
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license. 
#
#  Modified:
#
#    14 April 2017
#
#  Author:
#
#    John Burkardt
#
  import numpy as np

  print ( '' )
  print ( 'FD1D_HEAT_IMPLICIT_TEST:' )
  print ( '  Compute an approximate solution to the time-dependent' )
  print ( '  one dimensional heat equation:' )
  print ( '    dH/dt - K * d2H/dx2 = f(x,t)' )
  print ( '  Run a simple test case.' )
#
#  Set the heat coefficient.
#
  k = 0.002
#
#  X_NUM is the number of equally spaced nodes to use between 0 and 1.
#
  x_num = 21
  x_min = 0.0
  x_max = 1.0
  dx = ( x_max - x_min ) / ( x_num - 1 )
  x = np.linspace ( x_min, x_max, x_num )
#
#  T_NUM is the number of equally spaced time points between 0 and 10.0.
#
  t_num = 201
  t_min = 0.0
  t_max = 80.0
  dt = ( t_max - t_min ) / ( t_num - 1 )
  t = np.linspace ( t_min, t_max, t_num )
#
#  Compute the CFL coefficient.
#
  cfl = fd1d_heat_implicit_cfl ( k, t_num, t_min, t_max, x_num, x_min, x_max )

  print ( '' )
  print ( '  Number of X nodes = %d' % ( x_num ) )
  print ( '  X interval is [%f,%f]' % ( x_min, x_max ) )
  print ( '  X spacing is %f' % ( dx ) )
  print ( '  Number of T values = %d' % ( t_num ) )
  print ( '  T interval is [%f,%f]' % ( t_min, t_max ) )
  print ( '  T spacing is %f' % ( dt ) )
  print ( '  Constant K = %g' % ( k ) )
  print ( '  CFL coefficient = %g' % ( cfl ) )
#
#  Compute the system matrix.
#
  a = fd1d_heat_implicit_matrix ( x_num, cfl )
#
#  Save every solution vector H in a matrix HMAT, for plotting.
#
  hmat = np.zeros ( ( x_num, t_num ) )
#
#  Compute T_NUM solutions (including the initial condition).
#
  for j in range ( 0, t_num ):

    if ( j == 0 ):
      h = ic_fun ( x_num, x, t[j] )
      h = bc_fun ( x_num, x, t[j], h )
    else:
      h = fd1d_heat_implicit ( a, x_num, x, t[j-1], dt, cfl, rhs_fun, \
        bc_fun, h )

    for i in range ( 0, x_num ):
      hmat[i,j] = h[i]
#
#  Terminate.
#
  print ( '' )
  print ( 'FD1D_HEAT_IMPLICIT_TEST:' )
  print ( '  Normal end of execution.' )
  return

def bc_fun ( x_num, x, t, h ):

#*****************************************************************************80
#
## BC_FUN evaluates the boundary conditions.
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license. 
#
#  Modified:
#
#    14 April 2017
#
#  Author:
#
#    John Burkardt
#
#  Parameters:
#
#    Input, integer X_NUM, the number of nodes.
#
#    Input, real X(X_NUM), the node coordinates.
#
#    Input, real T, the current time.
#
#    Input, real H(X_NUM), the current heat values.
#
#    Output, real H(X_NUM), the current heat values, after boundary
#    conditions have been imposed.
#
  h[0]       = 90.0
  h[x_num-1] = 70.0

  return h

def fd1d_heat_implicit ( a, x_num, x, t, dt, cfl, rhs_fun, bc_fun, u ):

#*****************************************************************************80
#
## FD1D_HEAT_IMPLICIT: Finite difference solution of 1D heat equation.
#
#  Discussion:
#
#    FD1D_HEAT_IMPLICIT solves the 1D heat equation with an implicit method.
#
#    This program solves
#
#      dUdT - k * d2UdX2 = F(X,T)
#
#    over the interval [A,B] with boundary conditions
#
#      U(A,T) = UA(T),
#      U(B,T) = UB(T),
#
#    over the time interval [T0,T1] with initial conditions
#
#      U(X,T0) = U0(X)
#
#    The code uses the finite difference method to approximate the
#    second derivative in space, and an implicit backward Euler approximation
#    to the first derivative in time.
#
#    The finite difference form can be written as
#
#      U(X,T+dt) - U(X,T)                  ( U(X-dx,T+dt) - 2 U(X,T+dt) + U(X+dx,T+dt) )
#      ------------------ = F(X,T+dt) + k *  --------------------------------------
#               dt                                   dx * dx
#
#    so that we have the following linear system for the values of U at time T+dt:
#
#            -     k * dt / dx / dx   * U(X-dt,T+dt)
#      + ( 1 + 2 * k * dt / dx / dx ) * U(X,   T+dt)
#            -     k * dt / dx / dx   * U(X+dt,T+dt)
#      =               dt             * F(X,   T+dt)
#      +                                U(X,   T)
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license.
#
#  Modified:
#
#    14 April 2017
#
#  Author:
#
#    John Burkardt
#
#  Parameters:
#
#    Input, real A(X_NUM,X_NUM), the system matrix.
#
#    Input, integer X_NUM, the number of nodes.
#
#    Input, real X(X_NUM), the node coordinates.
#
#    Input, real T, the current time.
#
#    Input, real DT, the size of the time step.
#
#    Input, real CFL, the Courant-Friedrichs-Loewy coefficient.
#
#    Input, f = RHS_FUN ( x_num, x, t ), returns in F the right hand side
#    forcing function at every non-boundary node.
#
#    Input, hbc = BC_FUN ( x_num, x, t, h ), returns in HBC a copy of the
#    input solution H, after imposing Dirichlet boundary conditions.
#
#    Input, real U(X_NUM), the solution values at the old time.
#
#    Output, real U(X_NUM), the solution values at the new time.
#
  import numpy as np
#
#  Compute b, the right hand side of the system.
#
  fvec = rhs_fun ( x_num, x, t )

  b = u.copy ( )
  for i in range ( 1, x_num - 1 ):
    b[i] = b[i] + dt * fvec[i]
#
#  Solve A*u=b.
#
  u = np.linalg.solve ( a, b )
#
#  Impose boundary conditions on U.
#
  u = bc_fun ( x_num, x, t, u )

  return u

def fd1d_heat_implicit_cfl ( k, t_num, t_min, t_max, x_num, x_min, x_max ):

#*****************************************************************************80
#
## FD1D_HEAT_IMPLICIT_CFL: compute the Courant-Friedrichs-Loewy coefficient.
#
#  Discussion:
#
#    The equation to be solved has the form:
#
#      dUdT - k * d2UdX2 = F(X,T)
#
#    over the interval [X_MIN,X_MAX] with boundary conditions
#
#      U(X_MIN,T) = U_X_MIN(T),
#      U(X_MIN,T) = U_X_MAX(T),
#
#    over the time interval [T_MIN,T_MAX] with initial conditions
#
#      U(X,T_MIN) = U_T_MIN(X)
#
#    The code uses the finite difference method to approximate the
#    second derivative in space, and an explicit forward Euler approximation
#    to the first derivative in time.
#
#    The finite difference form can be written as
#
#      U(X,T+dt) - U(X,T)                  ( U(X-dx,T) - 2 U(X,T) + U(X+dx,T) )
#      ------------------  = F(X,T) + k *  ------------------------------------
#               dt                                   dx * dx
#
#    or, assuming we have solved for all values of U at time T, we have
#
#      U(X,T+dt) = U(X,T) + cfl * ( U(X-dx,T) - 2 U(X,T) + U(X+dx,T) ) + dt * F(X,T) 
#
#    Here "cfl" is the Courant-Friedrichs-Loewy coefficient:
#
#      cfl = k * dt / dx / dx
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license. 
#
#  Modified:
#
#    14 April 2017
#
#  Author:
# 
#    John Burkardt
#
#  Reference:
#
#    George Lindfield, John Penny,
#    Numerical Methods Using MATLAB,
#    Second Edition,
#    Prentice Hall, 1999,
#    ISBN: 0-13-012641-1,
#    LC: QA297.P45.
#
#  Parameters:
#
#    Input, real K, the heat conductivity coefficient.
#
#    Input, integer T_NUM, the number of time values, including the initial
#    value.
#
#    Input, real T_MIN, T_MAX, the minimum and maximum times.
#
#    Input, integer X_NUM, the number of nodes.
#
#    Input, real X_MIN, X_MAX, the minimum and maximum spatial coordinates.
#
#    Output, real CFL, the Courant-Friedrichs-Loewy coefficient.
#
  x_step = ( x_max - x_min ) / float ( x_num - 1 )
  t_step = ( t_max - t_min ) / float ( t_num - 1 )
  cfl = k * t_step / x_step / x_step

  return cfl

def fd1d_heat_implicit_matrix ( x_num, cfl ):

#*****************************************************************************80
#
## FD1D_HEAT_IMPLICIT_MATRIX: set the system matrix.
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license.
#
#  Modified:
#
#    14 April 2017
#
#  Author:
#
#    John Burkardt
#
#  Parameters:
#
#    Input, integer X_NUM, the number of nodes.
#
#    Input, real CFL, the Courant-Friedrichs-Loewy coefficient.
#
#    Output, real A(X_NUM,X_NUM), the system matrix.
#
  import numpy as np

  a = np.zeros ( ( x_num, x_num ) )

  a[0,0] = 1.0

  for i in range ( 1, x_num - 1 ):
    a[i,i-1] =           - cfl
    a[i,i  ] = 1.0 + 2.0 * cfl
    a[i,i+1] =           - cfl

  a[x_num-1,x_num-1] = 1.0

  return a

def ic_fun ( x_num, x, t ):

#*****************************************************************************80
#
## IC_FUN evaluates the initial condition.
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license. 
#
#  Modified:
#
#    14 April 2017
#
#  Author:
#
#    John Burkardt
#
#  Parameters:
#
#    Input, integer X_NUM, the number of nodes.
#
#    Input, real X(X_NUM), the node coordinates.
#
#    Input, real T, the initial time.
#
#    Output, real H(X_NUM), the heat values at the initial time.
#
  import numpy as np

  h = np.zeros ( x_num )

  for i in range ( 0, x_num ):
    h[i] = 50.0

  return h

def rhs_fun ( x_num, x, t ):

#*****************************************************************************80
#
## RHS_FUN evaluates the right hand side.
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license. 
#
#  Modified:
#
#    14 April 2017
#
#  Author:
#
#    John Burkardt
#
#  Parameters:
#
#    Input, integer X_NUM, the number of nodes.
#
#    Input, real X(X_NUM), the node coordinates.
#
#    Input, real T, the current time.
#
#    Output, real VALUE(X_NUM), the source term.
#
  import numpy as np

  value = np.zeros ( x_num )

  return value

def timestamp ( ):

#*****************************************************************************80
#
## TIMESTAMP prints the date as a timestamp.
#
#  Licensing:
#
#    This code is distributed under the GNU LGPL license. 
#
#  Modified:
#
#    06 April 2013
#
#  Author:
#
#    John Burkardt
#
#  Parameters:
#
#    None
#
  import time

  t = time.time ( )
  print ( time.ctime ( t ) )

  return None

if ( __name__ == '__main__' ):
  timestamp ( )
  fd1d_heat_implicit_test( )
  timestamp ( )

